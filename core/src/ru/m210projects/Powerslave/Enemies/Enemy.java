// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Anim.GetAnimSprite;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Main.boardService;
import static ru.m210projects.Powerslave.Main.engine;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.Sprites.GetSpriteHeight;

public class Enemy {

    public static int MoveCreature(int nSprite) {
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return 0;
        }

        return engine.movesprite( nSprite, spr.getXvel() << 8, spr.getYvel() << 8,
                spr.getZvel(), 15360, -5120, 0);
    }

    public static int MoveCreatureWithCaution(final int nSprite) {
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return 0;
        }

        int x = spr.getX();
        int y = spr.getY();
        int z = spr.getZ();

        short nOldSector = spr.getSectnum();
        Sector osec = boardService.getSector(nOldSector);
        int hitMove = MoveCreature(nSprite);
        int nSector = spr.getSectnum();
        Sector sec = boardService.getSector(nSector);
        if (nSector != nOldSector && sec != null && osec != null) {
            if (klabs(osec.getFloorz() - sec.getFloorz()) > 15360 || (SectFlag[nSector] & 0x2000) != 0
                    || SectBelow[nSector] > -1 && SectFlag[SectBelow[nSector]] != 0 || SectDamage[nSector] != 0) {

                spr.setX(x);
                spr.setY(y);
                spr.setZ(z);
                engine.mychangespritesect( nSprite, nOldSector);

                spr.setAng( ((spr.getAng() + 256) & 0x7FF));
                spr.setXvel( (EngineUtils.sin((spr.getAng() + 512) & 0x7FF) >> 2));
                spr.setYvel( (EngineUtils.sin(spr.getAng() & 0x7FF) >> 2));

                return 0;
            }
        }

        return hitMove;
    }

    public static void DropMagic(int nSprite) {
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return;
        }

        if (lFinaleStart == 0 && --nMagicCount <= 0) {
            int v1 = BuildAnim(-1, 64, 0, spr.getX(), spr.getY(), spr.getZ(),
                    spr.getSectnum(), 0x30, 4);
            int animIndex = GetAnimSprite(v1);
            Sprite animSprite =  boardService.getSprite(animIndex);
            if (animSprite == null) {
                return;
            }

            animSprite.setOwner( v1);
            AddFlash(animSprite.getSectnum(), animSprite.getX(), animSprite.getY(), animSprite.getZ(), 128);
            engine.changespritestat( animIndex,  950);
            nMagicCount = RandomSize(2);
        }
    }

    public static int UpdateEnemy(int nTarget) {
        Sprite pTarget = boardService.getSprite(nTarget);
        if (pTarget != null) {
            if ((pTarget.getCstat() & 0x101) == 0) {
                nTarget = -1;
            }
            return nTarget;
        }

        return -1;
    }

    public static int PlotCourseToSprite(int a1, int nTarget) {
        Sprite v3 = boardService.getSprite(a1);
        Sprite pTarget = boardService.getSprite(nTarget);
        if (v3 != null && pTarget != null) {
            int dx = pTarget.getX() - v3.getX();
            int dy = pTarget.getY() - v3.getY();
            v3.setAng(engine.GetMyAngle(dx, dy));
            return EngineUtils.sqrt(dx * dx + dy * dy);
        }

        return -1;
    }

    public static int FindPlayer(int nDude, int dist) {
        boolean bCourse = true;
        if (nDude < 0) {
            nDude = -nDude;
            bCourse = false;
        }

        if (dist < 0) {
            dist = 100;
        }
        dist <<= 8;

        Sprite pDude = boardService.getSprite(nDude);
        if (pDude == null) {
            return -1;
        }

        int dz = pDude.getZ() - GetSpriteHeight(nDude);
        for (int i = 0; i < numplayers; i++) {
            Sprite pPlayer = boardService.getSprite(PlayerList[i].spriteId);
            if (pPlayer != null && (pPlayer.getCstat() & 257) != 0 && (pPlayer.getCstat() & 0x8000) == 0) {
                if (klabs(pPlayer.getX() - pDude.getX()) < dist && klabs(pPlayer.getY() - pDude.getY()) < dist && engine.cansee(pPlayer.getX(),
                        pPlayer.getY(), pPlayer.getZ() - 7680, pPlayer.getSectnum(), pDude.getX(), pDude.getY(), dz, pDude.getSectnum())) {
                    if (bCourse) {
                        PlotCourseToSprite(nDude, PlayerList[i].spriteId);
                    }
                    return PlayerList[i].spriteId;
                }
            }
        }

        return -1;
    }

    public static class EnemyStruct {
        public static final int size = 16;

        public int nHealth;
        public int nSeq;
        public int nState;
        public int nSprite;
        public int nTarget;
        public int field_A;
        public int field_C;
        public int nFunc = -1;

        public void save(OutputStream os) throws IOException {
            StreamUtils.writeShort(os, nHealth);
            StreamUtils.writeShort(os, nSeq);
            StreamUtils.writeShort(os, nState);
            StreamUtils.writeShort(os, nSprite);
            StreamUtils.writeShort(os, nTarget);
            StreamUtils.writeShort(os, field_A);
            StreamUtils.writeShort(os, field_C);
            StreamUtils.writeShort(os, nFunc);
        }

        public void load(InputStream is) throws IOException {
            nHealth =  StreamUtils.readShort(is);
            nSeq =  StreamUtils.readShort(is);
            nState =  StreamUtils.readShort(is);
            nSprite =  StreamUtils.readShort(is);
            nTarget =  StreamUtils.readShort(is);
            field_A =  StreamUtils.readShort(is);
            field_C =  StreamUtils.readShort(is);
            nFunc =  StreamUtils.readShort(is);
        }

        public EnemyStruct copy(EnemyStruct src) {
            nHealth = src.nHealth;
            nSeq = src.nSeq;
            nState = src.nState;
            nSprite = src.nSprite;
            nTarget = src.nTarget;
            field_A = src.field_A;
            field_C = src.field_C;
            nFunc = src.nFunc;

            return this;
        }
    }

}
