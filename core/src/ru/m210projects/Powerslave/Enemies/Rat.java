// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sprites.*;

public class Rat {

    public static final int MAX_RAT = 50;
    private static final short[][] ActionSeq_X_14 = new short[][]{{0, 1}, {1, 0}, {1, 0}, {9, 1}, {0, 1}};
    private static int nRatCount;
    private static int nMinChunk;
    private static int nMaxChunk;
    private static final EnemyStruct[] RatList = new EnemyStruct[MAX_RAT];

    public static void InitRats() {
        nRatCount = 0;
        nMinChunk = 9999;
        nMaxChunk = -1;

        for (int i = 122; i <= 131; i++) {
            int pic = GetSeqPicnum(25, i, 0);
            if (pic < nMinChunk) {
                nMinChunk = pic;
            }
            if (pic > nMaxChunk) {
                nMaxChunk = pic;
            }
        }

        nPlayerPic = GetSeqPicnum(25, 120, 0);
    }

    public static void saveRat(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nRatCount);
        for (int i = 0; i < nRatCount; i++) {
            RatList[i].save(os);
        }

        StreamUtils.writeInt(os, nMinChunk);
        StreamUtils.writeInt(os, nMaxChunk);
        StreamUtils.writeInt(os, nPlayerPic);
    }

    public static void loadRat(SafeLoader loader) {
        nRatCount = loader.nRatCount;
        for (int i = 0; i < loader.nRatCount; i++) {
            if (RatList[i] == null) {
                RatList[i] = new EnemyStruct();
            }
            RatList[i].copy(loader.RatList[i]);
        }

        nMinChunk = loader.nMinChunk;
        nMaxChunk = loader.nMaxChunk;
        nPlayerPic = loader.nPlayerPic;
    }

    public static void loadRat(SafeLoader loader, InputStream is) throws IOException {
        loader.nRatCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nRatCount; i++) {
            if (loader.RatList[i] == null) {
                loader.RatList[i] = new EnemyStruct();
            }
            loader.RatList[i].load(is);
        }

        loader.nMinChunk = StreamUtils.readInt(is);
        loader.nMaxChunk = StreamUtils.readInt(is);
        loader.nPlayerPic = StreamUtils.readInt(is);
    }

    public static void SetRatVel(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            pSprite.setXvel((EngineUtils.cos(pSprite.getAng() & 0x7FF) >> 2));
            pSprite.setYvel((EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 2));
        }
    }

    public static void BuildRat(int spr, int x, int y, int z, int sectnum, int ang) {
        if (nRatCount >= MAX_RAT) {
            return;
        }

        int count = nRatCount++;

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  108);
            pSprite = boardService.getSprite(spr);

            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            z = pSprite.getZ();
            ang = pSprite.getAng();
            engine.changespritestat(spr,  108);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setShade(-12);
        pSprite.setCstat(257);
        pSprite.setClipdist(30);
        pSprite.setXrepeat(50);
        pSprite.setYrepeat(50);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setPicnum(1);
        pSprite.setAng( ang);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (RatList[count] == null) {
            RatList[count] = new EnemyStruct();
        }

        if (ang >= 0) {
            RatList[count].nState = 2;
        } else {
            RatList[count].nState = 4;
        }
        RatList[count].nSeq = 0;
        RatList[count].nSprite =  spr;
        RatList[count].nTarget = -1;
        RatList[count].field_A =  RandomSize(5);
        RatList[count].field_C =  RandomSize(3);

        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent36 | count));
        RatList[count].nFunc =  AddRunRec(NewRun, nEvent36 | count);

    }

    public static int FindFood(int spr) {
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            return -1;
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return -1;
        }

        short sectnum = pSprite.getSectnum();
        int sx = pSprite.getX();
        int sy = pSprite.getY();
        int sz = (sec.getCeilingz() + pSprite.getZ()) / 2;

        int chunkSpriteIndex, bodySpriteIndex;
        if (nChunkTotal != 0) {
            chunkSpriteIndex = RandomSize(7) % nChunkTotal;
            int chunkSpriteId = nChunkSprite[chunkSpriteIndex];
            Sprite chunkSprite = boardService.getSprite(chunkSpriteId);
            if (chunkSprite != null && engine.cansee(sx, sy, sz, sectnum,
                    chunkSprite.getX(),
                    chunkSprite.getY(),
                    chunkSprite.getZ(),
                    chunkSprite.getSectnum())) {
                return chunkSpriteId;
            }
        }

        if (nBodyTotal != 0) {
            bodySpriteIndex = RandomSize(7) % nBodyTotal;
            int bodySpriteId = nBodySprite[bodySpriteIndex];
            Sprite bodySprite = boardService.getSprite(bodySpriteId);
            if (bodySprite != null && nPlayerPic == bodySprite.getPicnum() &&
                    engine.cansee(sx, sy, pSprite.getZ(), pSprite.getSectnum(),
                            bodySprite.getX(),
                            bodySprite.getY(),
                            bodySprite.getZ(),
                            bodySprite.getSectnum())) {
                return bodySpriteId;
            }
        }

        return -1;
    }

    public static void FuncRat(int nStack, int a2, int RunPtr) {
        int nRat = RunData[RunPtr].getObject();
        final int nSprite = RatList[nRat].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = RatList[nRat].nState;
        short plr = (short) (nStack & 0xFFFF);
        int damage = a2;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int v51 = 0;
                int v10 = ActionSeq_X_14[nState][0] + SeqOffsets[77];
                int v13 = RatList[nRat].nSeq;
                pSprite.setPicnum( GetSeqPicnum2(v10, v13));
                MoveSequence(nSprite, v10, v13);
                RatList[nRat].nSeq++;
                if (RatList[nRat].nSeq >= SeqSize[v10]) {
                    RatList[nRat].nSeq = 0;
                    v51 = 1;
                }

                Sprite pTarget;
                Gravity(nSprite);

                switch (nState) {
                    case 2:
                        if ((pSprite.getYvel() | pSprite.getXvel() | pSprite.getZvel()) != 0) {
                            if (isOriginal()) {
                                MoveCreature(nSprite);
                            } else {
                                MoveCreatureWithCaution(nSprite);
                            }
                        }

                        RatList[nRat].field_A--;
                        if (RatList[nRat].field_A <= 0) {
                            RatList[nRat].nTarget = FindFood(nSprite);
                            pTarget = boardService.getSprite(RatList[nRat].nTarget);
                            if (pTarget != null) {
                                PlotCourseToSprite(nSprite, RatList[nRat].nTarget);
                                SetRatVel(nSprite);
                                RatList[nRat].nState = 1;
                                RatList[nRat].field_C = 900;
                                RatList[nRat].nSeq = 0;
                                break;
                            }
                            RatList[nRat].field_A =  RandomSize(6);
                            if ((pSprite.getXvel() | pSprite.getYvel()) != 0) {
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                            } else {
                                pSprite.setAng( RandomSize(11));
                                SetRatVel(nSprite);
                            }
                        }
                        break;
                    case 1:
                        if (--RatList[nRat].field_C <= 0) {
                            RatList[nRat].nState = 2;
                            RatList[nRat].nSeq = 0;

                            RatList[nRat].nTarget = -1;
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                        }

                        if (isOriginal()) {
                            MoveCreature(nSprite);
                        } else {
                            MoveCreatureWithCaution(nSprite);
                        }

                        pTarget = boardService.getSprite(RatList[nRat].nTarget);
                        if (pTarget != null && (klabs(pSprite.getX() - pTarget.getX()) >= 50
                                || klabs(pSprite.getY() - pTarget.getY()) >= 50)) {
                            if (--RatList[nRat].field_A < 0) {
                                PlotCourseToSprite(nSprite, RatList[nRat].nTarget);
                                SetRatVel(nSprite);
                                RatList[nRat].field_A = 32;
                            }
                        } else {
                            RatList[nRat].nState = 0;
                            RatList[nRat].nSeq = 0;
                            RatList[nRat].field_C =  RandomSize(3);
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                        }
                        break;
                    case 0:
                        if (--RatList[nRat].field_A <= 0) {
                            pTarget = boardService.getSprite(RatList[nRat].nTarget);

                            if (pTarget != null && klabs(pSprite.getX() - pTarget.getX()) <= 50
                                    && klabs(pSprite.getY() - pTarget.getY()) <= 50) {
                                RatList[nRat].nSeq ^= 1;
                                RatList[nRat].field_A =  (RandomSize(5) + 4);
                                if (--RatList[nRat].field_C <= 0) {
                                    int v44 = FindFood(nSprite);
                                    if (v44 != -1) {
                                        RatList[nRat].nTarget =  v44;
                                        PlotCourseToSprite(nSprite, v44);
                                        SetRatVel(nSprite);
                                        RatList[nRat].nState = 1;
                                        RatList[nRat].field_C = 900;
                                        RatList[nRat].nSeq = 0;
                                    }
                                }
                            } else {
                                RatList[nRat].nState = 2;
                                RatList[nRat].nSeq = 0;
                                RatList[nRat].nTarget = -1;
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                            }
                        }
                        break;
                    case 3:
                        if (v51 != 0) {
                            DoSubRunRec(pSprite.getOwner());
                            FreeRun(pSprite.getLotag() - 1);
                            SubRunRec(RatList[nRat].nFunc);
                            pSprite.setCstat( 32768);
                            engine.mydeletesprite( nSprite);
                        }
                        break;
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(plr);
                PlotSequence(tsp, ActionSeq_X_14[nState][0] + SeqOffsets[77], RatList[nRat].nSeq,
                        ActionSeq_X_14[nState][1]);
                return;
            case nEventRadialDamage:
                damage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (damage != 0) {
                    pSprite.setCstat(0);
                    pSprite.setXvel(0);
                    pSprite.setYvel(0);
                    RatList[nRat].nSeq = 0;
                    RatList[nRat].nState = 3;
                }
        }
    }
}
