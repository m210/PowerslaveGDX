// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BulletStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Enemies.Spider.BuildSpider;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Scorp {

    public static final int MAXSCORP = 5;
    private static final short[][] ActionSeq_X_7 = new short[][]{{0, 0}, {8, 0}, {29, 0}, {19, 0}, {45, 1},
            {46, 1}, {47, 1}, {48, 1}, {50, 1}, {53, 1}};
    private static int ScorpCount;
    private static final ScorpStruct[] ScorpList = new ScorpStruct[MAXSCORP];
    private static final int[] ScorpChan = new int[MAXSCORP];

    public static void InitScorp() {
        ScorpCount = MAXSCORP;
    }

    public static void saveScorp(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  ScorpCount);
        for (int i = ScorpCount; i < MAXSCORP; i++) {
            ScorpList[i].save(os);
        }
        for (int i = 0; i < MAXSCORP; i++) {
            StreamUtils.writeShort(os, ScorpChan[i]);
        }
    }

    public static void loadScorp(SafeLoader loader) {
        ScorpCount = loader.ScorpCount;
        for (int i = loader.ScorpCount; i < MAXSCORP; i++) {
            if (ScorpList[i] == null) {
                ScorpList[i] = new ScorpStruct();
            }
            ScorpList[i].copy(loader.ScorpList[i]);
        }
        System.arraycopy(loader.ScorpChan, 0, ScorpChan, 0, MAXSCORP);
    }

    public static void loadScorp(SafeLoader loader, InputStream is) throws IOException {
        loader.ScorpCount = StreamUtils.readShort(is);
        for (int i = loader.ScorpCount; i < MAXSCORP; i++) {
            if (loader.ScorpList[i] == null) {
                loader.ScorpList[i] = new ScorpStruct();
            }
            loader.ScorpList[i].load(is);
        }
        for (int i = 0; i < MAXSCORP; i++) {
            loader.ScorpChan[i] =  StreamUtils.readShort(is);
        }
    }

    public static void BuildScorp(int spr, int x, int y, int z, int sectnum, int ang, int channel) {
        int count = --ScorpCount;

        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  122);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            sectnum = pSprite.getSectnum();
            Sector sec = boardService.getSector(sectnum);
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat( spr,  122);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setXoffset(0);
        pSprite.setShade(-12);
        pSprite.setYoffset(0);
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(70);
        pSprite.setAng( ang);
        pSprite.setXrepeat(80);
        pSprite.setYrepeat(80);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (ScorpList[count] == null) {
            ScorpList[count] = new ScorpStruct();
        }

        ScorpList[count].nState = 0;
        ScorpList[count].nHealth = 20000;
        ScorpList[count].nSeq = 0;
        ScorpList[count].nSprite =  spr;
        ScorpList[count].nTarget = -1;
        ScorpList[count].field_A = 1;
        ScorpList[count].field_C = 0;
        ScorpChan[count] =  channel;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent34 | count));
        ScorpList[count].nFunc =  AddRunRec(NewRun, nEvent34 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncScorp(int nStack, int nDamage, int RunPtr) {
        int nScorp = RunData[RunPtr].getObject();
        if (nScorp < 0 || nScorp >= 5) {
            throw new AssertException("scorp>=0 && scorp<MAXSCORP");
        }

        ScorpStruct pScorp = ScorpList[nScorp];
        int nSprite = pScorp.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pScorp.nState;
        final int nTarget = pScorp.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);

        short nObject = (short) (nStack & 0xFFFF);

        int v5 = 0;
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if (pScorp.nHealth != 0) {
                    Gravity(nSprite);
                }

                int nSeq = ActionSeq_X_7[nState][0] + SeqOffsets[24];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pScorp.nSeq));
                MoveSequence(nSprite, nSeq, pScorp.nSeq);
                if (++pScorp.nSeq >= SeqSize[nSeq]) {
                    pScorp.nSeq = 0;
                    v5 = 1;
                }
                int nFlags = FrameFlag[pScorp.nSeq + SeqBase[nSeq]];
                switch (nState) {
                    case 0:
                        if (pScorp.field_C <= 0) {
                            if ((nScorp & 0x1F) == (totalmoves & 0x1F) && nTarget < 0) {
                                int nNewTarget = FindPlayer(nSprite, 500);
                                if (nNewTarget >= 0) {
                                    D3PlayFX(StaticSound[41], nSprite);
                                    pScorp.nSeq = 0;
                                    pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                                    pScorp.nState = 1;
                                    pScorp.nTarget = nNewTarget;
                                    pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                                }
                            }
                        } else {
                            pScorp.field_C--;
                        }
                        return;
                    case 1:
                        if (--pScorp.field_A <= 0) {
                            pScorp.field_A =  RandomSize(5);
                            PlotCourseToSprite(nSprite, nTarget);
                            pSprite.setAng(pSprite.getAng() + RandomSize(7) - 63);
                            pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                        } else {
                            int hitMove = MoveCreatureWithCaution(nSprite);
                            switch (hitMove & PS_HIT_TYPE_MASK) {
                                case PS_HIT_WALL:
                                case PS_HIT_SPRITE:
                                    if ((hitMove & PS_HIT_INDEX_MASK) == nTarget) {
                                        if (pTarget != null && AngleDiff(pSprite.getAng(), EngineUtils.getAngle(pTarget.getX() - pSprite.getX(),
                                                pTarget.getY() - pSprite.getY())) < 64) {
                                            pScorp.nState = 2;
                                            pScorp.nSeq = 0;
                                            break;
                                        }
                                    }
                                    PlotCourseToSprite(nSprite, nTarget);
                                    pSprite.setAng(pSprite.getAng() + RandomSize(7) - 63);
                                    pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                                    pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                                    break;
                            }
                        }
                        break;
                    case 2:
                        if (nTarget == -1) {
                            pScorp.nState = 0;
                            pScorp.field_C = 5;
                        } else if (PlotCourseToSprite(nSprite, nTarget) >= 768) {
                            pScorp.nState = 1;
                        } else if ((nFlags & 0x80) != 0) {
                            DamageEnemy(nTarget, nSprite, 7);
                        }
                        break;
                    case 3:
                        if (v5 != 0 && --pScorp.field_B <= 0) {
                            pScorp.nState = 1;
                            pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                        } else if ((nFlags & 0x80) != 0) {
                            BulletStruct.BulletResult nBullet = BuildBullet(nSprite, 16, -1, pSprite.getAng(), nTarget + 10000, 1);
                            if (nBullet.hasResult()) {
                                PlotCourseToSprite(nBullet.getSpriteIndex(), nTarget);
                            }
                        }
                        return;
                    case 8:
                        if (v5 != 0) {
                            pScorp.nState++;
                            ChangeChannel(ScorpChan[nScorp], 1);
                        } else {
                            int nSpider = BuildSpider(-1, pSprite.getX(), pSprite.getY(), pSprite.getZ(),
                                    pSprite.getSectnum(), pSprite.getAng());
                            Sprite pSpider = boardService.getSprite(nSpider);
                            if (pSpider != null) {
                                pSpider.setAng( (RandomSize(11) & 0x7FF));
                                int vel = RandomSize(5) + 1;
                                pSpider.setXvel( (vel * (EngineUtils.sin((pSpider.getAng() + 512) & 0x7FF) >> 8)));
                                pSpider.setYvel( (vel * (EngineUtils.sin(pSpider.getAng()) >> 8)));
                                pSpider.setZvel( (-256 * (RandomSize(5) + 3)));
                            }
                        }
                        return;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        if (v5 != 0) {
                            if (pScorp.nHealth > 0) {
                                pScorp.nState = 1;
                                pScorp.field_C = 0;
                            } else {
                                if (--pScorp.field_C > 0) {
                                    pScorp.nState =  (RandomBit() + 6);
                                } else {
                                    pScorp.nState = 8;
                                }
                            }
                        }
                        return;
                    case 9:
                        pSprite.setCstat(pSprite.getCstat() & ~0x101);
                        if (v5 != 0) {
                            SubRunRec(pScorp.nFunc);
                            DoSubRunRec(pSprite.getOwner());
                            FreeRun(pSprite.getLotag() - 1);
                            engine.mydeletesprite( nSprite);
                        }
                        return;
                    default:
                        return;
                }
                break;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_7[nState][0] + SeqOffsets[24], pScorp.nSeq, ActionSeq_X_7[nState][1]);
                return;
            case nEventRadialDamage:
                nDamage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (nDamage != 0) {
                    if (pScorp.nHealth > 0) {
                        pScorp.nHealth -= nDamage;
                        if (pScorp.nHealth > 0) {
                            Sprite pObject = boardService.getSprite(nObject);
                            if (pObject != null) {
                                if (pObject.getStatnum() == 100 || pObject.getStatnum() < 199 && RandomSize(5) == 0) {
                                    pScorp.nTarget = nObject;
                                }
                            }
                            if (RandomSize(5) == 0) {
                                pScorp.nState =  (RandomSize(2) + 4);
                                pScorp.nSeq = 0;
                                return;
                            }

                            if (RandomSize(2) == 0) {
                                D3PlayFX(StaticSound[41], nSprite);
                                PlotCourseToSprite(nSprite, nTarget);
                                pSprite.setAng(pSprite.getAng() + RandomSize(7) - 63);
                                pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                                pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                                break;
                            }
                        } else {
                            pSprite.setZvel(0);
                            pScorp.nHealth = 0;
                            pSprite.setYvel(0);
                            pScorp.nState = 4;
                            pSprite.setXvel(0);
                            pScorp.nSeq = 0;
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            pScorp.field_C = 10;
                            nCreaturesLeft--;
                        }
                    }
                }
                return;
        }

        if (nState != 2) {
            if (pScorp.field_C != 0) {
                pScorp.field_C--;
            } else {
                pScorp.field_C = 45;
                if (pTarget != null && engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ() - GetSpriteHeight(nSprite), pSprite.getSectnum(),
                        pTarget.getX(), pTarget.getY(), pTarget.getZ() - GetSpriteHeight(nTarget),
                        pTarget.getSectnum())) {
                    pSprite.setYvel(0);
                    pSprite.setXvel(0);
                    pSprite.setAng(engine.GetMyAngle(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY()));
                    int v41 = RandomSize(3);
                    pScorp.field_B =  (v41 + RandomSize(2));
                    if (pScorp.field_B != 0) {
                        pScorp.nState = 3;
                        pScorp.nSeq = 0;
                    } else {
                        pScorp.field_C =  RandomSize(5);
                    }
                }
            }
        }

        if (nState != 0 && pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
            pScorp.nState = 0;
            pScorp.nSeq = 0;
            pScorp.field_C = 30;
            pScorp.nTarget = -1;
            pSprite.setXvel(0);
            pSprite.setYvel(0);
        }
    }

    public static class ScorpStruct extends EnemyStruct {
        public static final int size = 18;
        public int field_B;

        @Override
        public void save(OutputStream os) throws IOException {
            super.save(os);
            StreamUtils.writeShort(os, field_B);
        }

        @Override
        public void load(InputStream is) throws IOException {
            super.load(is);
            field_B =  StreamUtils.readShort(is);
        }

        public void copy(ScorpStruct src) {
            super.copy(src);
            field_B = src.field_B;
        }
    }

}
