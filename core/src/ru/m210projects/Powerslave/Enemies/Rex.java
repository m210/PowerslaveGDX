// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.SetQuake;
import static ru.m210projects.Powerslave.Player.GetPlayerFromSprite;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Rex {

    public static final int MAX_REX = 50;
    private static final int[][] ActionSeq_X_8 = new int[][]{{29, 0}, {0, 0}, {0, 0}, {37, 0}, {9, 0},
            {18, 0}, {27, 1}, {28, 1}};
    private static int RexCount;
    private static final EnemyStruct[] RexList = new EnemyStruct[MAX_REX];
    private static final int[] RexChan = new int[MAX_REX];

    public static void InitRexs() {
        RexCount = MAX_REX;
    }

    public static void saveRex(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  RexCount);
        for (int i = RexCount; i < MAX_REX; i++) {
            RexList[i].save(os);
        }
        for (int i = 0; i < MAX_REX; i++) {
            StreamUtils.writeShort(os, RexChan[i]);
        }
    }

    public static void loadRex(SafeLoader loader) {
        RexCount = loader.RexCount;
        for (int i = loader.RexCount; i < MAX_REX; i++) {
            if (RexList[i] == null) {
                RexList[i] = new EnemyStruct();
            }
            RexList[i].copy(loader.RexList[i]);
        }
        System.arraycopy(loader.RexChan, 0, RexChan, 0, MAX_REX);
    }


    public static void loadRex(SafeLoader loader, InputStream is) throws IOException {
            loader.RexCount = StreamUtils.readShort(is);
            for (int i = loader.RexCount; i < MAX_REX; i++) {
                if (loader.RexList[i] == null) {
                    loader.RexList[i] = new EnemyStruct();
                }
                loader.RexList[i].load(is);
            }
            for (int i = 0; i < MAX_REX; i++) {
                loader.RexChan[i] =  StreamUtils.readShort(is);
            }
    }

    public static void BuildRex(int spr, int x, int y, int z, int sectnum, int ang, int channel) {
        int count = --RexCount;
        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite(sectnum,  119);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            Sector sec = boardService.getSector(pSprite.getSectnum());
            if (sec != null) {
                x = pSprite.getX();
                y = pSprite.getY();
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat(spr,  119);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setXoffset(0);
        pSprite.setShade(-12);
        pSprite.setYoffset(0);
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(80);
        pSprite.setAng( ang);
        pSprite.setXrepeat(64);
        pSprite.setYrepeat(64);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (RexList[count] == null) {
            RexList[count] = new EnemyStruct();
        }

        RexList[count].nState = 0;
        RexList[count].nHealth = 4000;
        RexList[count].nSeq = 0;
        RexList[count].nSprite =  spr;
        RexList[count].nTarget = -1;
        RexList[count].field_A = 0;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent24 | count));
        AddRunRec(NewRun, nEvent24 | count);
        RexChan[count] =  channel;

        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncRex(int nStack, int nDamage, int RunPtr) {
        int nRex = RunData[RunPtr].getObject();
        if (nRex < 0 || nRex >= MAX_REX) {
            throw new AssertException("rex>=0 && rex<MAX_REX");
        }

        EnemyStruct pRex = RexList[nRex];
        final int nSprite = pRex.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pRex.nState;
        int v67 = 0;

        short nObject = (short) (nStack & 0xFFFF);
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                Gravity(nSprite);

                int nSeq = ActionSeq_X_8[nState][0] + SeqOffsets[47];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pRex.nSeq));

                int loops = 1;
                if (nState == 2) {
                    loops = 2;
                }
                for (int i = 0; i < loops; i++) {
                    MoveSequence(nSprite, nSeq, pRex.nSeq);
                    if (++pRex.nSeq >= SeqSize[nSeq]) {
                        pRex.nSeq = 0;
                        v67 = 1;
                    }
                }
                int nFlags = FrameFlag[pRex.nSeq + SeqBase[nSeq]];

                switch (nState) {
                    case 0:
                        if (pRex.field_A != 0) {
                            if (--pRex.field_A <= 0) {
                                pRex.nState = 1;
                                pRex.nSeq = 0;
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 2));
                                D3PlayFX(StaticSound[48], nSprite);
                                pRex.field_A = 30;
                            }
                        } else if ((nRex & 0x1F) == (totalmoves & 0x1F)) {
                            Sprite pTarget = boardService.getSprite(pRex.nTarget);
                            if (pTarget != null) {
                                pRex.field_A = 60;
                            } else {
                                short v17 = pSprite.getAng();
                                pRex.nTarget = FindPlayer(nSprite, 60);
                                pSprite.setAng(v17);
                            }
                        }
                        return;
                    case 2:
                        if (--pRex.field_A > 0) {
                            PlotCourseToSprite(nSprite, pRex.nTarget);
                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1));
                            int hitMove = MoveCreatureWithCaution(nSprite);

                            switch (hitMove & PS_HIT_TYPE_MASK) {
                                case PS_HIT_WALL:
                                    SetQuake(nSprite, 25);
                                    pRex.field_A = 60;
                                    break;
                                case PS_HIT_SPRITE:
                                    pRex.nState = 3;
                                    pRex.nSeq = 0;
                                    int spr = hitMove & PS_HIT_INDEX_MASK;
                                    Sprite hitSpr = boardService.getSprite(spr);
                                    if (hitSpr != null && hitSpr.getStatnum() != 0 && hitSpr.getStatnum() < 107) {
                                        DamageEnemy(spr, nSprite, 15);
                                        int xvel = 15 * EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF);
                                        int yvel = 15 * EngineUtils.sin(pSprite.getAng() & 0x7FF);
                                        if (hitSpr.getStatnum() == 100) {
                                            int v32 = GetPlayerFromSprite(spr);
                                            nXDamage[v32] += 16 * xvel;
                                            nYDamage[v32] += 16 * yvel;
                                            hitSpr.setZvel(-3584);
                                        } else {
                                            hitSpr.setXvel(hitSpr.getXvel() + (xvel >> 3));
                                            hitSpr.setYvel(hitSpr.getYvel() + (yvel >> 3));
                                            hitSpr.setZvel(-2880);
                                        }
                                    }
                                    pRex.field_A >>= 2;
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            pRex.nState = 1;
                            pRex.nSeq = 0;
                            pRex.field_A = 90;
                            return;
                        }
                        break;
                    case 3:
                        if (v67 != 0) {
                            pRex.nState = 2;
                        }
                        return;
                    case 1:
                        if (pRex.field_A > 0) {
                            pRex.field_A--;
                        }

                        if ((totalmoves & 0xF) == (nRex & 0xF)) {
                            if (RandomSize(1) == 0) {
                                pRex.nState = 5;
                                pRex.nSeq = 0;
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                                return;
                            }

                            if (PlotCourseToSprite(nSprite, pRex.nTarget) >> 8 < 60) {
                                if (pRex.field_A <= 0) {
                                    pRex.nState = 2;
                                    pRex.field_A = 240;
                                    D3PlayFX(StaticSound[48], nSprite);
                                    pRex.nSeq = 0;
                                    return;
                                }
                            }

                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 2));
                        }

                        int hitMove = MoveCreatureWithCaution(nSprite);
                        switch (hitMove & PS_HIT_TYPE_MASK) {
                            case PS_HIT_WALL:
                                break;
                            case PS_HIT_SPRITE:
                                if ((hitMove & PS_HIT_INDEX_MASK) == pRex.nTarget) {
                                    PlotCourseToSprite(nSprite, pRex.nTarget);
                                    pRex.nState = 4;
                                    pRex.nSeq = 0;
                                    checkTarget(pRex, nState);
                                    return;
                                }
                                break;
                            default:
                                checkTarget(pRex, nState);
                                return;
                        }
                        break;
                    case 5:
                        if (v67 != 0) {
                            pRex.nState = 1;
                            pRex.field_A = 15;
                        }
                        return;
                    case 4:
                        if (pRex.nTarget == -1 || PlotCourseToSprite(nSprite, pRex.nTarget) >= 768) {
                            pRex.nState = 1;
                        } else if ((nFlags & 0x80) != 0) {
                            DamageEnemy(pRex.nTarget, nSprite, 15);
                        }
                        checkTarget(pRex, nState);
                        return;
                    case 6:
                        if (v67 != 0) {
                            pRex.nState = 7;
                            ChangeChannel(RexChan[nRex], 1);
                        }
                        return;
                    case 7:
                        pSprite.setCstat(pSprite.getCstat() & 0xFEFE);
                        return;
                    default:
                        return;
                }

                pSprite.setAng( ((pSprite.getAng() + 256) & 0x7FF));
                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 2));
                nState = pRex.nState = 1;
                pRex.nSeq = 0;
                break;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_8[nState][0] + SeqOffsets[47], pRex.nSeq, ActionSeq_X_8[nState][1]);
                return;
            case nEventRadialDamage:
                if (nState == 5) {
                    nDamage = CheckRadialDamage(nSprite);
                }
            case nEventDamage:
                if (nDamage != 0) {
//                    Sprite pTarget = boardService.getSprite(pRex.nTarget);
//                    if (pTarget != null && pTarget.getStatnum() == 100) {
//                        pRex.nTarget =  nTarget;
//                    }

                    if (pRex.nState == 5) {
                        if (pRex.nHealth > 0) {
                            pRex.nHealth -= nDamage;
                            if (pRex.nHealth <= 0) {
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                                pSprite.setZvel(0);
                                pSprite.setCstat(pSprite.getCstat() & ~0x101);
                                pRex.nHealth = 0;
                                nCreaturesLeft--;
                                if (pRex.nState < 6) {
                                    pRex.nState = 6;
                                    pRex.nSeq = 0;
                                }
                            }
                        }
                    }
                }
                return;
        }

        checkTarget(pRex, nState);
    }

    private static void checkTarget(EnemyStruct pRex, int nState) {
        Sprite pTarget = boardService.getSprite(pRex.nTarget);
        final int nSprite = pRex.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (nState > 0 && pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
            pRex.nState = 0;
            pRex.nSeq = 0;
            pRex.field_A = 0;
            pRex.nTarget = -1;
            pSprite.setXvel(0);
            pSprite.setYvel(0);
        }
    }
}
