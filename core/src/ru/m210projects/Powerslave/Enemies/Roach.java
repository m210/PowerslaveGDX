// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sprites.CheckRadialDamage;
import static ru.m210projects.Powerslave.Sprites.Gravity;

public class Roach {

    public static final int MAXROACH = 100;
    private static final short[][] ActionSeq_X_11 = new short[][]{{24, 0}, {0, 0}, {0, 0}, {16, 0}, {8, 0}, {32, 1}, {42, 1}};
    private static int RoachCount;
    //	private static int RoachSprite;
    private static final EnemyStruct[] RoachList = new EnemyStruct[MAXROACH];

    public static void InitRoach() {
        RoachCount = MAXROACH;
//		RoachSprite = 1;
    }

    public static void saveRoach(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  RoachCount);
        for (int i = RoachCount; i < MAXROACH; i++) {
            RoachList[i].save(os);
        }
    }

    public static void loadRoach(SafeLoader loader) {
        RoachCount = loader.RoachCount;
        for (int i = loader.RoachCount; i < MAXROACH; i++) {
            if (RoachList[i] == null) {
                RoachList[i] = new EnemyStruct();
            }
            RoachList[i].copy(loader.RoachList[i]);
        }
    }

    public static void loadRoach(SafeLoader loader, InputStream is) throws IOException {
        loader.RoachCount = StreamUtils.readShort(is);
        for (int i = loader.RoachCount; i < MAXROACH; i++) {
            if (loader.RoachList[i] == null) {
                loader.RoachList[i] = new EnemyStruct();
            }
            loader.RoachList[i].load(is);
        }
    }

    public static void BuildRoach(int a1, int spr, int x, int y, int z, int sectnum, int ang) {
        int count = --RoachCount;
        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  105);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            sectnum = pSprite.getSectnum();
            Sector sec = boardService.getSector(sectnum);
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat( spr,  105);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setAng(ang);
        pSprite.setXrepeat(40);
        pSprite.setYrepeat(40);
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(60);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (RoachList[count] == null) {
            RoachList[count] = new EnemyStruct();
        }

        if (a1 != 0) {
            RoachList[count].nState = 0;
        } else {
            RoachList[count].nState = 1;
        }

        RoachList[count].nSeq = 0;
        RoachList[count].nHealth = 600;
        RoachList[count].nTarget = -1;
        RoachList[count].nSprite = spr;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent28 | count));
        RoachList[count].field_C = 0;
        RoachList[count].field_A =  AddRunRec(NewRun, nEvent28 | count);

        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void GoRoach(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            pSprite.setXvel(((EngineUtils.cos(pSprite.getAng() & 0x7FF) >> 1) - (EngineUtils.cos(pSprite.getAng() & 0x7FF) >> 3)));
            pSprite.setYvel(((EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1) - (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 3)));
        }
    }

    public static void FuncRoach(int nStack, int nDamage, int RunPtr) {
        int nRoach = RunData[RunPtr].getObject();
        if (nRoach < 0 || nRoach >= MAXROACH) {
            throw new AssertException("roach>=0 && roach<MAXROACH");
        }

        EnemyStruct pRoach = RoachList[nRoach];
        int nSprite = pRoach.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pRoach.nState;
        int v42 = 0;

        short nObject = (short) (nStack & 0xFFFF);
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                Gravity(nSprite);

                int nSeq = ActionSeq_X_11[nState][0] + SeqOffsets[50];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pRoach.nSeq));
                MoveSequence(nSprite, nSeq, pRoach.nSeq);
                if (++pRoach.nSeq >= SeqSize[nSeq]) {
                    pRoach.nSeq = 0;
                    v42 = 1;
                }

                if (nState > 5) {
                    return;
                }

                int plDist;
                switch (nState) {
                    case 0:
                        if (pRoach.nSeq == 1) {
                            if (--pRoach.field_C > 0) {
                                pRoach.nSeq = 0;
                            } else {
                                pRoach.field_C =  RandomSize(6);
                            }
                        }
//                        plDist = 50;
                    case 1:
                        plDist = 100;
                        if ((nRoach & 0xF) == (totalmoves & 0xF) && pRoach.nTarget < 0) {
                            int v18 = FindPlayer(nSprite, plDist);
                            if (v18 >= 0) {
                                pRoach.nState = 2;
                                pRoach.nSeq = 0;
                                pRoach.nTarget = v18;
                                GoRoach(nSprite);
                            }
                        }
                        return;
                    case 2:
                        if ((totalmoves & 0xF) == (nRoach & 0xF)) {
                            PlotCourseToSprite(nSprite, pRoach.nTarget);
                            GoRoach(nSprite);
                        }
                        int hitMove = MoveCreatureWithCaution(nSprite);
                        switch (hitMove & PS_HIT_TYPE_MASK) {
                            default:
                                if (pRoach.field_C != 0) {
                                    pRoach.field_C--;
                                    break;
                                }
                            case PS_HIT_SPRITE:
                                if (((hitMove & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE && (hitMove & PS_HIT_INDEX_MASK) == pRoach.nTarget)
                                        || (hitMove & PS_HIT_TYPE_MASK) < PS_HIT_WALL) { // hitMove == 0
                                    pRoach.nFunc = (RandomSize(2) + 1);
                                    pRoach.nState = 3;
                                    pSprite.setXvel(0);
                                    pSprite.setYvel(0);
                                    Sprite pTarget = boardService.getSprite(pRoach.nTarget);
                                    if (pTarget != null) {
                                        pSprite.setAng(engine.GetMyAngle(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY()));
                                    }
                                    pRoach.nSeq = 0;
                                    break;
                                }
                            case PS_HIT_WALL:
                                pSprite.setAng(((pSprite.getAng() + 256) & 0x7FF));
                                GoRoach(nSprite);
                                break;
                        }
                        break;
                    case 3:
                        if (v42 != 0) {
                            if (pRoach.nFunc-- <= 0) {
                                pRoach.nState = 2;
                                GoRoach(nSprite);
                                pRoach.nSeq = 0;
                                pRoach.field_C =  RandomSize(7);
                            }
                        } else if ((FrameFlag[pRoach.nSeq + SeqBase[nSeq]] & 0x80) != 0) {
                            BuildBullet(nSprite, 13, -1, pSprite.getAng(), pRoach.nTarget + 10000, 1);
                        }
                        return;
                    case 4:
                        if (v42 == 0) {
                            return;
                        }
                        pRoach.nState = 2;
                        return;
                    case 5:
                        if (v42 != 0) {
                            pSprite.setCstat(0);
                            pRoach.nState = 6;
                            pRoach.nSeq = 0;
                        }
                        return;
                }

                Sprite pTarget = boardService.getSprite(pRoach.nTarget);
                if (pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
                    pRoach.nState = 1;
                    pRoach.nSeq = 0;
                    pRoach.field_C = 100;
                    pRoach.nTarget = -1;
                    pSprite.setXvel(0);
                    pSprite.setYvel(0);
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_11[nState][0] + SeqOffsets[50], pRoach.nSeq, ActionSeq_X_11[nState][1]);
                return;
            case nEventRadialDamage:
                nDamage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (nDamage != 0) {
                    if (pRoach.nHealth > 0) {
                        pRoach.nHealth -= nDamage;
                        if (pRoach.nHealth > 0) {
                            Sprite pObject = boardService.getSprite(nObject);
                            if (pObject != null) {
                                if (pObject.getStatnum() < 199) {
                                    pRoach.nTarget = nObject;
                                }

                                if (nState != 0 && nState != 1) {
                                    if (RandomSize(4) == 0) {
                                        pRoach.nState = 4;
                                        pRoach.nSeq = 0;
                                    }
                                } else {
                                    pRoach.nState = 2;
                                    GoRoach(nRoach);
                                    pRoach.nSeq = 0;
                                }
                            }
                        } else {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(0);
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            pRoach.nHealth = 0;
                            if (pRoach.nState < 5) {
                                DropMagic(nSprite);
                                pRoach.nState = 5;
                                pRoach.nSeq = 0;
                            }
                            nCreaturesLeft--;
                        }
                    }
                }
        }
    }
}
