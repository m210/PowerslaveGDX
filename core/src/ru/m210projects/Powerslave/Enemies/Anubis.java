// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.BuildObject;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Anubis {

    public static final int MAXANUBIS = 80;

    private static final short[][] ActionSeq_X_1 = new short[][]{{0, 0}, {8, 0}, {16, 0}, {24, 0}, {32, 0},
            {-1, 0}, {46, 1}, {46, 1}, {47, 1}, {49, 1}, {49, 1}, {40, 1}, {42, 1}, {41, 1},
            {43, 1},};

    private static int AnubisCount;
    //	private static int AnubisSprite;
    private static int nAnubisDrum;

    private static final EnemyStruct[] AnubisList = new EnemyStruct[MAXANUBIS];

    public static void InitAnubis() {
        AnubisCount = MAXANUBIS;
//		AnubisSprite = 1;
        nAnubisDrum = 1;
    }

    public static void saveAnubis(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  AnubisCount);
        StreamUtils.writeByte(os, (byte) nAnubisDrum);

        for (int i = AnubisCount; i < MAXANUBIS; i++) {
            AnubisList[i].save(os);
        }
    }

    public static void loadAnubis(SafeLoader loader) {
        AnubisCount = loader.AnubisCount;
        nAnubisDrum = loader.nAnubisDrum;
        for (int i = loader.AnubisCount; i < MAXANUBIS; i++) {
            if (AnubisList[i] == null) {
                AnubisList[i] = new EnemyStruct();
            }
            AnubisList[i].copy(loader.AnubisList[i]);
        }
    }

    public static void loadAnubis(SafeLoader loader, InputStream is) throws IOException {
        loader.AnubisCount = StreamUtils.readShort(is);
        loader.nAnubisDrum = StreamUtils.readByte(is);
        for (int i = loader.AnubisCount; i < MAXANUBIS; i++) {
            if (loader.AnubisList[i] == null) {
                loader.AnubisList[i] = new EnemyStruct();
            }
            loader.AnubisList[i].load(is);
        }
    }

    public static void BuildAnubis(int sn, int x, int y, int z, int sectnum, int ang, int a7) {
        int count = --AnubisCount;

        if (count < 0) {
            return;
        }

        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            sn = engine.insertsprite( sectnum,  101);
            spr = boardService.getSprite(sn);
            if (spr == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = spr.getX();
            y = spr.getY();
            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = spr.getAng();
            engine.changespritestat( sn,  101);
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        spr.setX(x);
        spr.setY(y);
        spr.setZ(z);
        spr.setCstat(257);
        spr.setXoffset(0);
        spr.setShade(-12);
        spr.setYoffset(0);
        spr.setPicnum(1);
        spr.setPal(sec.getCeilingpal());
        spr.setClipdist(60);
        spr.setAng( ang);
        spr.setXrepeat(40);
        spr.setYrepeat(40);
        spr.setXvel(0);
        spr.setYvel(0);
        spr.setZvel(0);
        spr.setHitag(0);
        spr.setLotag( (HeadRun() + 1));
        spr.setExtra(-1);

        if (AnubisList[count] == null) {
            AnubisList[count] = new EnemyStruct();
        }

        if (a7 != 0) {
            AnubisList[count].nState =  (nAnubisDrum++ + 6);
            if (nAnubisDrum >= 5) {
                nAnubisDrum = 0;
            }
        } else {
            AnubisList[count].nState = 0;
        }

        AnubisList[count].nHealth = 540;
        AnubisList[count].nSeq = 0;
        AnubisList[count].nSprite =  sn;
        AnubisList[count].nTarget = -1;
        AnubisList[count].field_C = 0;
        spr.setOwner(AddRunRec(spr.getLotag() - 1, nEventView | count));
        AddRunRec(NewRun, nEventView | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncAnubis(int nStack, int a2, int RunPtr) {
        int nAnubis = RunData[RunPtr].getObject();
        if (nAnubis < 0 || nAnubis >= MAXANUBIS) {
            throw new AssertException("anubis>=0 && anubis<MAXANUBIS");
        }

        int nSprite = AnubisList[nAnubis].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int damage = a2;

        int nState = AnubisList[nAnubis].nState;
        int v66 = 0;
        short plr = (short) (nStack & 0xFFFF);
        Sprite plrSpr = boardService.getSprite(plr);

        switch (nStack & EVENT_MASK) {
            case nEventProcess:

                if (nState < 11) {
                    Gravity(nSprite);
                }

                int nSeq = ActionSeq_X_1[nState][0] + SeqOffsets[17];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, AnubisList[nAnubis].nSeq));

                MoveSequence(nSprite, nSeq, AnubisList[nAnubis].nSeq);
                int nTarget = AnubisList[nAnubis].nTarget;
                Sprite pTarget = boardService.getSprite(nTarget);
                if (++AnubisList[nAnubis].nSeq >= SeqSize[nSeq]) {
                    AnubisList[nAnubis].nSeq = 0;
                    v66 = 1;
                }

                int flags = FrameFlag[AnubisList[nAnubis].nSeq + SeqBase[nSeq]];

                int v60 = 0;
                if (AnubisList[nAnubis].nState > 0 && AnubisList[nAnubis].nState < 11) {
                    v60 = MoveCreatureWithCaution(nSprite);
                }

                switch (nState) {
                    default:
                        return;
                    case 0:
                        if ((nAnubis & 0x1F) == (totalmoves & 0x1F)) {
                            if (nTarget < 0) {
                                nTarget = FindPlayer(nSprite, 100);
                                pTarget = boardService.getSprite(nTarget);
                            }

                            if (pTarget != null) {
                                D3PlayFX(StaticSound[8], nSprite);
                                AnubisList[nAnubis].nState = 1;
                                AnubisList[nAnubis].nSeq = 0;
                                AnubisList[nAnubis].nTarget =  nTarget;
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 2));
                            }
                        }
                        return;
                    case 3:
                        if (v66 == 0) {
                            if ((flags & 0x80) != 0) {
                                BuildBullet(nSprite, 8, -1, pSprite.getAng(), nTarget + 10000, 1);
                            }
                            return;
                        }
                        AnubisList[nAnubis].nState = 1;
                        pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                        pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 2));
                        AnubisList[nAnubis].nSeq = 0;
                        return;
                    case 4:
                    case 5:
                        pSprite.setXvel(0);
                        pSprite.setYvel(0);
                        if (v66 != 0) {
                            AnubisList[nAnubis].nState = 1;
                            AnubisList[nAnubis].nSeq = 0;
                        }
                        return;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        if (v66 != 0) {
                            AnubisList[nAnubis].nState =  (RandomSize(3) % 5 + 6);
                            AnubisList[nAnubis].nSeq = 0;
                        }
                        return;
                    case 11:
                    case 12:
                        if (v66 != 0) {
                            AnubisList[nAnubis].nState =  (nState + 2);
                            AnubisList[nAnubis].nSeq = 0;
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                        }
                        return;
                    case 13:
                    case 14:
                        pSprite.setCstat(pSprite.getCstat() & 0xFEFE);
                        return;
                    case 2:
                        if (nTarget == -1) {
                            AnubisList[nAnubis].nState = 0;
                            AnubisList[nAnubis].field_C = 50;
                        } else if (PlotCourseToSprite(nSprite, nTarget) >= 768) {
                            AnubisList[nAnubis].nState = 1;
                        } else if ((flags & 0x80) != 0) {
                            DamageEnemy(nTarget, nSprite, 7);
                        }
                        break;
                    case 1:
                        if ((nAnubis & 0x1F) == (totalmoves & 0x1F)) {
                            PlotCourseToSprite(nSprite, nTarget);
                            int ang = pSprite.getAng() & 0xFFF8;
                            pSprite.setXvel( (EngineUtils.sin((ang + 512) & 0x7FF) >> 2));
                            pSprite.setYvel( (EngineUtils.sin(ang & 0x7FF) >> 2));
                        }
                        switch (v60 & PS_HIT_TYPE_MASK) {
                            case PS_HIT_SPRITE:
                                if ((v60 & PS_HIT_INDEX_MASK) == nTarget) {
                                    if (pTarget != null && AngleDiff(pSprite.getAng(),
                                            EngineUtils.getAngle(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY())) < 64) {
                                        AnubisList[nAnubis].nState = 2;
                                        AnubisList[nAnubis].nSeq = 0;
                                    }
                                    break;
                                }
                            case PS_HIT_WALL:
                                pSprite.setAng( ((pSprite.getAng() + 256) & 0x7FF));
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 2));
                                break;
                            default:
                                if (AnubisList[nAnubis].field_C != 0) {
                                    AnubisList[nAnubis].field_C--;
                                } else {
                                    AnubisList[nAnubis].field_C = 60;
                                    if (pTarget != null && engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ() - GetSpriteHeight(nSprite),
                                            pSprite.getSectnum(), pTarget.getX(), pTarget.getY(),
                                            pTarget.getZ() - GetSpriteHeight(nTarget), pTarget.getSectnum())) {
                                        AnubisList[nAnubis].nState = 3;
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                        pSprite.setAng(engine.GetMyAngle(pTarget.getX() - pSprite.getX(),
                                                pTarget.getY() - pSprite.getY()));
                                        AnubisList[nAnubis].nSeq = 0;
                                    }
                                }
                                break;
                        }
                        break;
                }

                if (pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
                    AnubisList[nAnubis].nState = 0;
                    AnubisList[nAnubis].nSeq = 0;
                    AnubisList[nAnubis].field_C = 100;
                    AnubisList[nAnubis].nTarget = -1;
                    pSprite.setXvel(0);
                    pSprite.setYvel(0);
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(plr);
                PlotSequence(tsp, ActionSeq_X_1[AnubisList[nAnubis].nState][0] + SeqOffsets[17], AnubisList[nAnubis].nSeq,
                        ActionSeq_X_1[AnubisList[nAnubis].nState][1]);
                return;
            case nEventRadialDamage:
                if (AnubisList[nAnubis].nState >= 11) {
                    return;
                }

                damage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (damage != 0) {
                    if (AnubisList[nAnubis].nHealth > 0) {
                        AnubisList[nAnubis].nHealth -= damage;
                        if (AnubisList[nAnubis].nHealth > 0) {
                            if (plrSpr != null) {
                                int statnum = plrSpr.getStatnum();
                                if ((statnum < 199) && RandomSize(5) == 0) {
                                    AnubisList[nAnubis].nTarget = plr;
                                }
                                if (RandomSize(1) != 0) {
                                    if (AnubisList[nAnubis].nState >= 6 && AnubisList[nAnubis].nState <= 10) {
                                        int j = engine.insertsprite(pSprite.getSectnum(),  98);
                                        Sprite spr2 = boardService.getSprite(j);
                                        if (spr2 != null) {
                                            spr2.setX(pSprite.getX());
                                            spr2.setY(pSprite.getY());
                                            Sector sec2 = boardService.getSector(spr2.getSectnum());
                                            if (sec2 != null) {
                                                spr2.setZ(sec2.getFloorz());
                                            }
                                            spr2.setYrepeat(40);
                                            spr2.setXrepeat(40);
                                            spr2.setShade(-64);
                                            BuildObject(j, 2, 0);
                                        }
                                    }

                                    AnubisList[nAnubis].nState = 4;
                                    AnubisList[nAnubis].nSeq = 0;
                                } else {
                                    D3PlayFX(StaticSound[39], nSprite);
                                }
                            }
                        } else {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(0);

                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null) {
                                pSprite.setZ(sec.getFloorz());
                            }

                            AnubisList[nAnubis].nHealth = 0;
                            pSprite.setCstat(pSprite.getCstat() & 0xFEFE);

                            if (AnubisList[nAnubis].nState < 11) {
                                DropMagic(nSprite);
                                AnubisList[nAnubis].nState =  ((((nStack & EVENT_MASK) == nEventRadialDamage) ? 1 : 0) + 11);
                                AnubisList[nAnubis].nSeq = 0;
                            }
                            nCreaturesLeft--;
                        }
                    }
                }
        }

    }
}
