// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Powerslave.Type.RaStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Globals.Ra;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.SetQuake;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sprites.DamageEnemy;
import static ru.m210projects.Powerslave.Sprites.GetSpriteHeight;
import static ru.m210projects.Powerslave.Weapons.*;

public class Ra {
    //	private static int RaCount;
    private static final short[] ActionSeq_X_3 = {2, 0, 1, 2};

    public static void InitRa() {
//		RaCount = 0;
    }

    public static void saveRa(OutputStream os) throws IOException {
        Ra[nLocalPlayer].save(os);
    }

    public static void loadRa(SafeLoader loader) {
        if (Ra[nLocalPlayer] == null) {
            Ra[nLocalPlayer] = new RaStruct();
        }
        Ra[nLocalPlayer].copy(loader.Ra[nLocalPlayer]);
    }

    public static void loadRa(SafeLoader loader, InputStream is) throws IOException {
        if (loader.Ra[nLocalPlayer] == null) {
            loader.Ra[nLocalPlayer] = new RaStruct();
        }
        loader.Ra[nLocalPlayer].load(is);
    }

    public static void BuildRa(int nPlayer) {
        Sprite plrSpr = boardService.getSprite(PlayerList[nPlayer].spriteId);
        if (plrSpr == null) {
            return;
        }

        int j = engine.insertsprite(plrSpr.getSectnum(),  203);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setCstat( 32768);
        spr2.setXvel(0);
        spr2.setYvel(0);
        spr2.setZvel(0);
        spr2.setExtra(-1);
        spr2.setLotag( (HeadRun() + 1));
        spr2.setHitag(0);
        spr2.setOwner( AddRunRec(spr2.getLotag() - 1, nEvent33 | nPlayer));
        spr2.setPal(1);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setX(plrSpr.getX());
        spr2.setY(plrSpr.getY());
        spr2.setZ(plrSpr.getZ());

        if (Ra[nPlayer] == null) {
            Ra[nPlayer] = new RaStruct();
        }

        Ra[nPlayer].nSprite = j;
        Ra[nPlayer].nFunc =  AddRunRec(NewRun, nEvent33 | nPlayer);
        Ra[nPlayer].nTarget = -1;
        Ra[nPlayer].nSeq = 0;
        Ra[nPlayer].nState = 0;
        Ra[nPlayer].field_C = 0;
        Ra[nPlayer].nPlayer =  nPlayer;
    }

    public static void FreeRa(int a1) {
        SubRunRec(Ra[a1].nFunc);
        Sprite spr = boardService.getSprite(Ra[a1].nSprite);
        if (spr != null) {
            DoSubRunRec(spr.getOwner());
            FreeRun(spr.getLotag() - 1);
        }
        engine.mydeletesprite(Ra[a1].nSprite);
    }

    public static void FuncRa(int nStack, int ignored, int a3) {
        int nRa = RunData[a3].getObject();
        RaStruct pRa = Ra[nRa];
        int v18 = 0;
        int nCurrentWeapon = PlayerList[nRa].currentWeapon;
        int nSeq = ActionSeq_X_3[pRa.nState] + SeqOffsets[68];

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                Ra[nRa].nTarget = sPlayerInput[nRa].nTarget;
                final int nSprite = pRa.nSprite;
                Sprite spr = boardService.getSprite(nSprite);
                if (spr == null) {
                    break;
                }

                spr.setPicnum(GetSeqPicnum2(nSeq, pRa.nSeq));
                if (pRa.nState != 0) {
                    MoveSequence(nSprite, nSeq, pRa.nSeq);
                    if (++pRa.nSeq >= SeqSize[nSeq]) {
                        pRa.nSeq = 0;
                        v18 = 1;
                    }
                }

                switch (pRa.nState) {
                    case 0:
                        MoveRaToEnemy(nRa);
                        if (pRa.field_C != 0 && pRa.nTarget > -1) {
                            spr.setCstat(spr.getCstat() & ~0x8000);
                            pRa.nState = 1;
                            pRa.nSeq = 0;
                            break;
                        }
                        spr.setCstat( 32768);
                        return;
                    case 1:
                        if (pRa.field_C != 0) {
                            if (v18 != 0) {
                                pRa.nState = 2;
                            }
                            MoveRaToEnemy(nRa);
                        } else {
                            pRa.nState = 3;
                            pRa.nSeq = 0;
                        }
                        return;
                    case 2:
                        MoveRaToEnemy(nRa);
                        if (nCurrentWeapon != 6) {
                            pRa.nState = 3;
                            pRa.nSeq = 0;
                            break;
                        }

                        if (pRa.nSeq != 0 || pRa.nTarget <= -1) {
                            if (v18 == 0) {
                                return;
                            }
                            pRa.nState = 3;
                            pRa.nSeq = 0;
                        } else if (PlayerList[nRa].AmmosAmount[6] <= 0) {
                            pRa.nState = 3;
                            SelectNewWeapon(nRa);
                        } else {
                            DamageEnemy(pRa.nTarget, PlayerList[pRa.nPlayer].spriteId, 200);
                            AddAmmo(nRa, 6, -weaponInfo[6].field_1E);
                            SetQuake(nSprite, 100);
                        }
                        break;
                    case 3:
                        if (v18 != 0) {
                            spr.setCstat(spr.getCstat() | 0x8000);
                            pRa.nState = 0;
                            pRa.nSeq = 0;
                            pRa.field_C = 0;
                        }
                        break;
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, nSeq, pRa.nSeq, 1);
        }
    }

    public static void MoveRaToEnemy(int nRa) {
        int nTarget = Ra[nRa].nTarget;
        int nSprite = Ra[nRa].nSprite;
        int nState = Ra[nRa].nState;
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return;
        }

        Sprite pTarget = boardService.getSprite(nTarget);
        if (pTarget == null) {
            if (nState == 1 || nState == 2) {
                Ra[nRa].nState = 3;
                Ra[nRa].nSeq = 0;
                return;
            }

            if (nState != 0) {
                return;
            }
            nTarget = PlayerList[nRa].spriteId;
            pTarget = boardService.getSprite(nTarget);
            spr.setCstat(32768);
        } else {
            if ((pTarget.getCstat() & 0x101) == 0 || !boardService.isValidSector(pTarget.getSectnum())) {
                Ra[nRa].nTarget = -1;
                if (nState == 0 || nState == 3) {
                    return;
                }

                Ra[nRa].nState = 3;
                Ra[nRa].nSeq = 0;
                return;
            }

            if (spr.getSectnum() != pTarget.getSectnum()) {
                engine.mychangespritesect( nSprite, pTarget.getSectnum());
            }
        }

        if (pTarget == null) {
            return;
        }

        spr.setX(pTarget.getX());
        spr.setY(pTarget.getY());
        spr.setZ(pTarget.getZ() - GetSpriteHeight(nTarget));
        if (spr.getSectnum() == pTarget.getSectnum()) {
            return;
        }

        engine.mychangespritesect( nSprite, pTarget.getSectnum());
    }

}
