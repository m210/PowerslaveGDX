// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Enemies.LavaDude.BuildLavaLimb;
import static ru.m210projects.Powerslave.Enemies.Wasp.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.Sprites.*;
import static ru.m210projects.Powerslave.Weapons.bobangle;

public class Queen {

    public static final int MAX_QUEEN = 1;
    public static final int MAX_EGGS = 10;
    private static final int[][] HeadSeq = new int[][]{{56, 1}, {65, 0}, {65, 0}, {65, 0}, {65, 0},
            {65, 0}, {74, 0}, {82, 0}, {90, 0}};
    private static final int[][] EggSeq = new int[][]{{19, 1}, {18, 1}, {0, 0}, {9, 0}, {23, 1},};
    private static final int[][] ActionSeq_X_10 = new int[][]{{0, 0}, {0, 0}, {9, 0}, {36, 0}, {18, 0},
            {27, 0}, {45, 0}, {45, 0}, {54, 1}, {53, 1}, {55, 1},};
    private static int QueenCount;
    private static final int[] nEggFree = new int[MAX_EGGS];
    private static int nEggsFree;
    private static final EnemyStruct[] QueenEgg = new EnemyStruct[MAX_EGGS];
    private static int nVelShift;
    private static final EnemyStruct QueenHead = new EnemyStruct();
    private static int nHeadVel;
    private static final QueenStruct QueenList = new QueenStruct();
    private static int QueenChan;
    private static final int[] tailspr = new int[7];
    private static int nQHead;
    private static final int[] MoveQX = new int[25];
    private static final int[] MoveQY = new int[25];
    private static final int[] MoveQZ = new int[25];
    private static final int[] MoveQA = new int[25];
    private static final int[] MoveQS = new int[25];

    public static void InitQueens() {
        QueenCount = 1;
        for (int i = 0; i < MAX_EGGS; i++) {
            if (QueenEgg[i] == null) {
                QueenEgg[i] = new EnemyStruct();
            }
            nEggFree[i] =  i;
            QueenEgg[i].nFunc = -1;
        }
        nEggsFree = MAX_EGGS;
    }

    public static void saveQueen(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  QueenCount);
        QueenList.save(os);
        QueenHead.save(os);

        StreamUtils.writeShort(os,  nEggsFree);
        for (int i = 0; i < MAX_EGGS; i++) {
            StreamUtils.writeShort(os, nEggFree[i]);
            QueenEgg[i].save(os);
        }

        StreamUtils.writeInt(os, nVelShift);
        StreamUtils.writeInt(os, nHeadVel);
        StreamUtils.writeShort(os,  QueenChan);
        for (int i = 0; i < 7; i++) {
            StreamUtils.writeShort(os,  tailspr[i]);
        }
        StreamUtils.writeInt(os, nQHead);
        for (int i = 0; i < 25; i++) {
            StreamUtils.writeInt(os, MoveQX[i]);
            StreamUtils.writeInt(os, MoveQY[i]);
            StreamUtils.writeInt(os, MoveQZ[i]);
            StreamUtils.writeShort(os, MoveQA[i]);
            StreamUtils.writeShort(os, MoveQS[i]);
        }
    }

    public static void loadQueen(SafeLoader loader) {
        QueenCount = loader.QueenCount;
        QueenList.copy(loader.QueenList);
        QueenHead.copy(loader.QueenHead);

        nEggsFree = loader.nEggsFree;
        System.arraycopy(loader.nEggFree, 0, nEggFree, 0, MAX_EGGS);
        for (int i = 0; i < MAX_EGGS; i++) {
            if (QueenEgg[i] == null) {
                QueenEgg[i] = new EnemyStruct();
            }
            QueenEgg[i].copy(loader.QueenEgg[i]);
        }

        nVelShift = loader.nVelShift;
        nHeadVel = loader.nHeadVel;
        QueenChan = loader.QueenChan;

        nQHead = loader.nQHead;
        System.arraycopy(loader.tailspr, 0, tailspr, 0, 7);
        System.arraycopy(loader.MoveQX, 0, MoveQX, 0, 25);
        System.arraycopy(loader.MoveQY, 0, MoveQY, 0, 25);
        System.arraycopy(loader.MoveQZ, 0, MoveQZ, 0, 25);
        System.arraycopy(loader.MoveQA, 0, MoveQA, 0, 25);
        System.arraycopy(loader.MoveQS, 0, MoveQS, 0, 25);
    }

    public static void loadQueen(SafeLoader loader, InputStream is) throws IOException {
            loader.QueenCount = StreamUtils.readShort(is);
            loader.QueenList.load(is);
            loader.QueenHead.load(is);

            loader.nEggsFree = StreamUtils.readShort(is);
            for (int i = 0; i < MAX_EGGS; i++) {
                loader.nEggFree[i] =  StreamUtils.readShort(is);
                if (loader.QueenEgg[i] == null) {
                    loader.QueenEgg[i] = new EnemyStruct();
                }
                loader.QueenEgg[i].load(is);
            }

            loader.nVelShift = StreamUtils.readInt(is);
            loader.nHeadVel = StreamUtils.readInt(is);
            loader.QueenChan = StreamUtils.readShort(is);
            for (int i = 0; i < 7; i++) {
                loader.tailspr[i] =  StreamUtils.readShort(is);
            }
            loader.nQHead = StreamUtils.readInt(is);
            for (int i = 0; i < 25; i++) {
                loader.MoveQX[i] = StreamUtils.readInt(is);
                loader.MoveQY[i] = StreamUtils.readInt(is);
                loader.MoveQZ[i] = StreamUtils.readInt(is);
                loader.MoveQA[i] =  StreamUtils.readShort(is);
                loader.MoveQS[i] =  StreamUtils.readShort(is);
            }
    }

    public static int GrabEgg() {
        if (nEggsFree != 0) {
            return nEggFree[--nEggsFree];
        }
        return -1;
    }

    public static void BlowChunks(int a1) {
        int v2 = 41;
        for (int i = 0; i < 4; i++) {
            BuildCreatureChunk(a1, GetSeqPicnum(16, v2++, 0));
        }
    }

    public static void DestroyEgg(int nEgg) {
        int nSprite = QueenEgg[nEgg].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (QueenEgg[nEgg].nState == 4) {
            for (int i = 0; i < 4; i++) {
                BuildCreatureChunk(nSprite, GetSeqPicnum(56, i % 2 + 24, 0));
            }
        } else {
            BuildAnim(-1, 34, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getXrepeat(), 4);
        }
        DoSubRunRec(pSprite.getOwner());
        DoSubRunRec(pSprite.getLotag() - 1);
        SubRunRec(QueenEgg[nEgg].nFunc);
        QueenEgg[nEgg].nFunc = -1;
        engine.mydeletesprite( nSprite);
        nEggFree[nEggsFree++] =  nEgg;
    }

    public static void DestroyAllEggs() {
        for (int i = 0; i < MAX_EGGS; i++) {
            if (QueenEgg[i].nFunc > -1) {
                DestroyEgg(i);
            }
        }
    }

    public static void SetHeadVel(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (nVelShift < 0) {
            pSprite.setXvel( (EngineUtils.sin(pSprite.getAng() + 512 & 0x7FF) << (-nVelShift)));
            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) << (-nVelShift)));
        } else {
            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> nVelShift));
            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> nVelShift));
        }
    }

    public static void BuildExplosion(int a1) {
        Sprite pSprite = boardService.getSprite(a1);
        if (pSprite == null) {
            return;
        }

        short nSector = pSprite.getSectnum();
        Sector sec = boardService.getSector(nSector);
        if ((SectFlag[nSector] & 0x2000) != 0) {
            BuildAnim(-1, 75, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getXrepeat(), 4);
        } else if (sec != null && pSprite.getZ() == sec.getFloorz()) {
            BuildAnim(-1, 34, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getXrepeat(), 4);
        } else {
            BuildAnim(-1, 36, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getXrepeat(), 4);
        }
    }

    public static boolean DestroyTailPart() {
        if (QueenHead.field_C == 0) {
            return false;
        }

        int nPart = --QueenHead.field_C;
        int spr = tailspr[nPart];
        BlowChunks(spr);
        BuildExplosion(spr);
        for (int i = 0; i < 5; i++) {
            BuildLavaLimb(spr, i, GetSpriteHeight(nPart));
        }
        engine.mydeletesprite(spr);
        return true;
    }

    public static void SetQueenSpeed(int nSprite, int shift) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> (2 - shift)));
        pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> (2 - shift)));
    }

    public static int QueenAngleChase(int nSprite, int nTarget, int nVel, int a4) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return 0;
        }

        int nNewAngle = pSprite.getAng();
        Sprite pTarget = boardService.getSprite(nTarget);
        if (pTarget != null) {
            int zTop = 2 * pTarget.getYrepeat() * engine.getTile(pTarget.getPicnum()).getHeight();

            int dx = pTarget.getX() - pSprite.getX();
            int dy = pTarget.getY() - pSprite.getY();
            int dz = pTarget.getZ() - pSprite.getZ();

            int nGoalAngle = AngleDelta(pSprite.getAng(), engine.GetMyAngle(dx, dy), 1024);
            if (klabs(nGoalAngle) > 127) {
                nVel /= klabs(nGoalAngle >> 7);
                if (nVel < 256) {
                    nVel = 256;
                }
            }
            if (klabs(nGoalAngle) > a4) {
                if (nGoalAngle >= 0) {
                    nGoalAngle = a4;
                } else {
                    nGoalAngle = -a4;
                }
            }

            nNewAngle =  ((pSprite.getAng() + nGoalAngle) & 0x7FF);
            pSprite.setZvel( (pSprite.getZvel() + (AngleDelta(pSprite.getZvel(),
                    engine.GetMyAngle(EngineUtils.sqrt(dx * dx + dy * dy), (dz - zTop) >> 8), 24)) & 0x7FF));
        } else {
            pSprite.setZvel(0);
        }
        pSprite.setAng(nNewAngle);

        int v28 = klabs(EngineUtils.sin((pSprite.getZvel() + 512) & 0x7FF));
        int xvel = v28 * (nVel * EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 14);
        int yvel = v28 * (nVel * EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 14);
        int v31 = EngineUtils.sqrt((xvel >> 8) * (xvel >> 8) + (yvel >> 8) * (yvel >> 8));

        return engine.movesprite( nSprite, xvel >> 2, yvel >> 2,
                (EngineUtils.sin(bobangle & 0x7FF) >> 5) + (v31 * EngineUtils.sin(pSprite.getZvel() & 0x7FF) >> 13), 0, 0, 1);
    }

    public static void BuildTail() {
        int nSprite = QueenHead.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int x = pSprite.getX();
        int y = pSprite.getY();
        int z = pSprite.getZ();
        int nSector = pSprite.getSectnum();
        Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return;
        }

        for (int i = 0; i < 7; i++) {
            int j = engine.insertsprite(nSector,  121);
            Sprite spr = boardService.getSprite(j);
            if (spr == null) {
                throw new AssertException("Can't create queen's tail!");
            }

            tailspr[i] = j;
            spr.setX(x);
            spr.setY(y);
            spr.setZ(z);
            spr.setCstat(0);
            spr.setXoffset(0);
            spr.setYoffset(0);
            spr.setShade(-12);
            spr.setPicnum(1);
            spr.setPal(sec.getCeilingpal());
            spr.setClipdist(100);
            spr.setXrepeat(80);
            spr.setYrepeat(80);
            spr.setXvel(0);
            spr.setYvel(0);
            spr.setZvel(0);
            spr.setHitag(0);
            spr.setLotag( (HeadRun() + 1));
            spr.setExtra(-1);
            spr.setOwner( AddRunRec(spr.getLotag() - 1, nEvent27 | i));
        }

        for (int i = 0; i < 25; i++) {
            MoveQX[i] = x;
            MoveQY[i] = y;
            MoveQZ[i] = z;
            MoveQS[i] = nSector;
        }

        nQHead = 0;
        QueenHead.field_C = 7;
    }

    public static void BuildQueenEgg(int ignoredQueenIndex, int nState) {
        int nEgg = GrabEgg();
        if (nEgg >= 0) {
            Sprite pQueen = boardService.getSprite(QueenList.nSprite);
            if (pQueen == null) {
                return;
            }
            Sector sec = boardService.getSector(pQueen.getSectnum());
            if (sec == null) {
                return;
            }

            int x = pQueen.getX();
            int y = pQueen.getY();
            int z = sec.getFloorz();
            int ang = pQueen.getAng();
            int j = engine.insertsprite(pQueen.getSectnum(),  121);
            Sprite spr2 = boardService.getSprite(j);
            if (spr2 == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
            spr2.setX(x);
            spr2.setY(y);
            spr2.setPal(0);
            spr2.setZ(z);
            spr2.setClipdist(50);
            spr2.setXoffset(0);
            spr2.setYoffset(0);
            spr2.setShade(-12);
            spr2.setPicnum(1);
            spr2.setAng( ((ang - 256 + RandomSize(9)) & 0x7FF));
            if (nState != 0) {
                spr2.setYrepeat(60);
                spr2.setXrepeat(60);
                spr2.setXvel(0);
                spr2.setYvel(0);
                spr2.setZvel(-2000);
                spr2.setCstat(257);
            } else {
                spr2.setXrepeat(30);
                spr2.setYrepeat(30);
                spr2.setCstat(0);
                spr2.setXvel(EngineUtils.sin((spr2.getAng() + 512) & 0x7FF));
                spr2.setYvel(EngineUtils.sin(spr2.getAng() & 0x7FF));
                spr2.setZvel(-6000);
            }

            spr2.setLotag( (HeadRun() + 1));
            spr2.setExtra(-1);
            spr2.setHitag(0);

            QueenEgg[nEgg].nHealth = 200;
            QueenEgg[nEgg].nSeq = 0;
            QueenEgg[nEgg].nSprite =  j;
            QueenEgg[nEgg].field_C =  nState;
            QueenEgg[nEgg].nTarget = QueenList.nTarget;
            if (nState != 0) {
                nState = 4;
                QueenEgg[nEgg].field_A = 200;
            }
            QueenEgg[nEgg].nState =  nState;
            spr2.setOwner( AddRunRec(spr2.getLotag() - 1, nEvent29 | nEgg));
            QueenEgg[nEgg].nFunc =  AddRunRec(NewRun, nEvent29 | nEgg);
        }
    }

    public static void BuildQueenHead(int ignoredQueenIndex) {
        Sprite pQueen = boardService.getSprite(QueenList.nSprite);
        if (pQueen == null) {
            return;
        }
        Sector sec = boardService.getSector(pQueen.getSectnum());
        if (sec == null) {
            return;
        }

        int x = pQueen.getX();
        int y = pQueen.getY();
        short ang = pQueen.getAng();
        int z = sec.getFloorz();
        int j = engine.insertsprite(pQueen.getSectnum(),  121);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
        }

        spr2.setX(x);
        spr2.setY(y);
        spr2.setZ(z);
        spr2.setPal(0);
        spr2.setClipdist(70);
        spr2.setYrepeat(80);
        spr2.setXrepeat(80);
        spr2.setCstat(0);
        spr2.setPicnum(1);
        spr2.setShade(-12);
        spr2.setXoffset(0);
        spr2.setYoffset(0);
        spr2.setAng(ang);
        nVelShift = 2;
        SetHeadVel(j);
        spr2.setZvel(-8192);
        spr2.setLotag( (HeadRun() + 1));
        spr2.setHitag(0);
        spr2.setExtra(-1);

        QueenHead.nHealth = 800;
        QueenHead.nState = 0;
        QueenHead.nTarget = QueenList.nTarget;
        QueenHead.nSeq = 0;
        QueenHead.nSprite =  j;
        QueenHead.field_A = 0;
        spr2.setOwner( AddRunRec(spr2.getLotag() - 1, nEvent27));
        QueenHead.nFunc =  AddRunRec(NewRun, nEvent27);
        QueenHead.field_C = 0;

        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void BuildQueen(int spr, int x, int y, int z, int sectnum, int ang, int channel) {
        int count = --QueenCount;
        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite(sectnum,  121);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            Sector sec = boardService.getSector(pSprite.getSectnum());
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat( spr,  121);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setPal(0);
        pSprite.setShade(-12);
        pSprite.setClipdist(100);
        pSprite.setXrepeat(80);
        pSprite.setYrepeat(80);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setPicnum(1);
        pSprite.setAng( ang);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);
        pSprite.setHitag(0);

        QueenList.nState = 0;
        QueenList.nHealth = 4000;
        QueenList.nSeq = 0;
        QueenList.nSprite =  spr;
        QueenList.nTarget = -1;
        QueenList.field_A = 0;
        QueenList.field_10 = 5;
        QueenList.field_C = 0;
        QueenChan = channel;
        nHeadVel = 800;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent26 | count));
        AddRunRec(NewRun, nEvent26 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncQueen(int nStack, int nDamage, int RunPtr) {
        int nQueen = RunData[RunPtr].getObject();
        if (nQueen < 0 || nQueen >= MAX_QUEEN) {
            throw new AssertException("queen>=0 && queen<MAX_QUEEN");
        }
        QueenStruct pQueen = QueenList;
        int nSprite = pQueen.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pQueen.nState;
        int field_A = pQueen.field_A;
        int v59 = 0;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if (field_A < 3) {
                    Gravity(nSprite);
                }

                int nSeq = ActionSeq_X_10[nState][0] + SeqOffsets[49];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pQueen.nSeq));

                MoveSequence(nSprite, nSeq, pQueen.nSeq);
                if (++pQueen.nSeq >= SeqSize[nSeq]) {
                    pQueen.nSeq = 0;
                    v59 = 1;
                }

                int nFlags = FrameFlag[pQueen.nSeq + SeqBase[nSeq]];
                int nTarget = pQueen.nTarget;
                Sprite pTarget = boardService.getSprite(nTarget);
                if (pTarget != null && nState < 7 && (pTarget.getCstat() & 0x101) == 0) {
                    nTarget = pQueen.nTarget = -1;
                    pQueen.nState = 0;
                }

                switch (nState) {
                    case 0:
                        if (nTarget < 0) {
                            nTarget = FindPlayer(nSprite, 60);
                            pTarget = boardService.getSprite(nTarget);
                        }

                        if (pTarget != null) {
                            pQueen.nState =  (pQueen.field_A + 1);
                            pQueen.nSeq = 0;
                            pQueen.nTarget =  nTarget;
                            pQueen.field_C =  RandomSize(7);
                            SetQueenSpeed(nSprite, field_A);
                        }
                        return;
                    case 6:
                        if (v59 != 0) {
                            BuildQueenEgg(nQueen, 1);
                            pQueen.nState = 3;
                            pQueen.field_C =  (RandomSize(6) + 60);
                        }
                        return;
                    case 1:
                    case 2:
                    case 3:
                        pQueen.field_C--;
                        if ((totalmoves & 0x1F) == 0) {
                            if (field_A >= 2) {
                                if (pQueen.field_C <= 0) {
                                    if (nWaspCount < MAX_WASPS) {
                                        pQueen.nState = 6;
                                        pQueen.nSeq = 0;
                                        return;
                                    }
                                    pQueen.field_C = 30000;
                                }
                            } else {
                                if (pQueen.field_C <= 0) {
                                    pQueen.nSeq = 0;
                                    pSprite.setYvel(0);
                                    pSprite.setXvel(0);
                                    pQueen.nState =  (field_A + 4);
                                    pQueen.field_C =  (RandomSize(6) + 30);
                                    return;
                                }
                                if (pQueen.field_10 < 5) {
                                    ++pQueen.field_10;
                                }
                            }
                            PlotCourseToSprite(nSprite, nTarget);
                            SetQueenSpeed(nSprite, field_A);
                        }

                        int nHit = MoveCreatureWithCaution(nSprite);
                        switch (nHit & PS_HIT_TYPE_MASK) {
                            case PS_HIT_SPRITE:
                                if (field_A == 2 && (nHit & PS_HIT_INDEX_MASK) == nTarget) {
                                    DamageEnemy(nTarget, nSprite, 5);
                                    break;
                                }
                            case PS_HIT_WALL:
                                pSprite.setAng( ((pSprite.getAng() + 0x100) & 0x7FF));
                                SetQueenSpeed(nSprite, field_A);
                                break;

                        }
                        if (pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
                            pQueen.nState = 0;
                            pQueen.nSeq = 0;
                            pQueen.field_C = 100;
                            pQueen.nTarget = -1;
                            pSprite.setYvel(0);
                            pSprite.setXvel(0);
                        }
                        return;
                    case 4:
                    case 5:
                        if (v59 != 0 && pQueen.field_10 <= 0) {
                            pQueen.nState = 0;
                            pQueen.field_C = 15;
                        } else if ((nFlags & 0x80) != 0) {
                            pQueen.field_10--;
                            PlotCourseToSprite(nSprite, nTarget);
                            if (field_A != 0) {
                                BuildQueenEgg(nQueen, 0);
                            } else {
                                BuildBullet(nSprite, 12, -1, pSprite.getAng(), nTarget + 10000, 1);
                            }
                        }
                        return;
                    case 7:
                        if (v59 != 0) {
                            pQueen.nState = 0;
                            pQueen.nSeq = 0;
                        }
                        return;
                    case 8:
                    case 9:
                        if (v59 == 0) {
                            return;
                        }

                        if (nState == 9) {
                            if (--pQueen.field_C > 0) {
                                return;
                            }

                            pSprite.setCstat(0);
                            for (int i = 0; i < 20; i++) {
                                int j = BuildCreatureChunk(nSprite, GetSeqPicnum(49, 57, 0));
                                Sprite spr2 = boardService.getSprite(j);
                                if (spr2 != null) {
                                    spr2.setPicnum((i % 3 + 3117));
                                    spr2.setYrepeat(100);
                                    spr2.setXrepeat(100);
                                }
                            }

                            int j = BuildCreatureChunk(nSprite, GetSeqPicnum(49, 57, 0));
                            Sprite spr2 = boardService.getSprite(j);
                            if (spr2 != null) {
                                spr2.setPicnum(3126);
                                spr2.setYrepeat(100);
                                spr2.setXrepeat(100);
                            }

                            PlayFXAtXYZ(StaticSound[40], pSprite.getX(), pSprite.getY(), pSprite.getZ(),
                                    pSprite.getSectnum());
                            BuildQueenHead(nQueen);
                        }
                        pQueen.nState++;
                        return;
                    case 10:
                        pSprite.setCstat(pSprite.getCstat() & ~0x101);
                        return;
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, ActionSeq_X_10[nState][0] + SeqOffsets[49], pQueen.nSeq,
                        ActionSeq_X_10[nState][1]);
                return;
            case nEventRadialDamage:
                Sprite pRadialSpr = boardService.getSprite(nRadialSpr);
                if (pRadialSpr != null && pRadialSpr.getStatnum() != 121) {
                    if ((pSprite.getCstat() & 0x101) != 0) {
                        nDamage = CheckRadialDamage(nSprite);
                    }
                }
            case nEventDamage:
                if (nDamage != 0) {
                    if (pQueen.nHealth > 0) {
                        pQueen.nHealth -= nDamage;
                        if (pQueen.nHealth <= 0) {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(0);
                            switch (++pQueen.field_A) {
                                case 1:
                                    pQueen.nHealth = 4000;
                                    pQueen.nState = 7;
                                    BuildAnim(-1, 36, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ() - 7680, pSprite.getSectnum(),
                                            pSprite.getXrepeat(), 4);
                                    break;
                                case 2:
                                    pQueen.nHealth = 4000;
                                    pQueen.nState = 7;
                                    DestroyAllEggs();
                                    break;
                                case 3:
                                    pQueen.nHealth = 0;
                                    pQueen.nState = 8;
                                    pQueen.field_C = 5;
                                    nCreaturesLeft--;
                                    break;
                            }
                            pQueen.nSeq = 0;
                        } else {
                            if (field_A > 0 && RandomSize(4) == 0) {
                                pQueen.nState = 7;
                                pQueen.nSeq = 0;
                            }
                        }
                    }
                }
        }
    }

    public static void FuncQueenHead(int nStack, int nDamage, int ignored) {
        EnemyStruct pHead = QueenHead;
        final int nSprite = pHead.nSprite;
        final Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pHead.nState;
        int nTarget = pHead.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);
        int v73 = 0;
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if (pHead.nState == 0) {
                    Gravity(QueenHead.nSprite);
                }

                int nSeq = HeadSeq[nState][0] + SeqOffsets[49];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pHead.nSeq));
                MoveSequence(nSprite, nSeq, pHead.nSeq);
                if (++pHead.nSeq >= SeqSize[nSeq]) {
                    pHead.nSeq = 0;
                    v73 = 1;
                }

                if (pTarget == null) {
                    nTarget =  FindPlayer(nSprite, 1000);
                    pTarget = boardService.getSprite(pHead.nTarget);
                } else if ((pTarget.getCstat() & 0x101) == 0) {
                    nTarget = -1;
                    pTarget = null;
                }
                pHead.nTarget = nTarget;

                switch (nState) {
                    case 0:
                        int v13 = pHead.field_A;
                        if (v13 > 0) {
                            pHead.field_A--;
                            if (v13 == 1) {
                                BuildTail();
                                pHead.nState = 6;
                                nHeadVel = 800;
                                pSprite.setCstat(257);
                            } else if ((v13 - 1) < 60) {
                                pSprite.setShade(pSprite.getShade() - 1);
                            }
                            return;
                        }
                        int hit = MoveCreature(nSprite);
                        int nNewAngle = 0;
                        switch (hit & 0xFC000) {
                            case PS_HIT_WALL:
                                nNewAngle = engine.GetWallNormal(hit & PS_HIT_INDEX_MASK);
                                break;
                            case PS_HIT_SPRITE:
                                Sprite hitSpr = boardService.getSprite(hit & PS_HIT_INDEX_MASK);
                                if (hitSpr != null) {
                                    nNewAngle = hitSpr.getAng();
                                }
                                break;
                            case nEventProcess:
                                pSprite.setZvel( -(pSprite.getZvel() >> 1));
                                if (pSprite.getZvel() > -256) {
                                    nVelShift = 100;
                                    pSprite.setZvel(0);
                                }
                                break;
                            default:
                                return;
                        }

                        pSprite.setAng(nNewAngle);
                        if (++nVelShift >= 5) {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            if (pSprite.getZvel() == 0) {
                                pHead.field_A = 120;
                            }
                        } else {
                            SetHeadVel(nSprite);
                        }

                        return;
                    case 6:
                        if (v73 != 0) {
                            pHead.nState = 1;
                            pHead.nSeq = 0;
                            return;
                        }
                    case 1:
                        if (pTarget != null && pTarget.getZ() - 51200 <= pSprite.getZ()) {
                            pSprite.setZ(pSprite.getZ() - 2048);
                            break;
                        } else {
                            pHead.nState = 4;
                            pHead.nSeq = 0;
                        }
                        return;
                    case 4:
                    case 7:
                    case 8:
                        if (v73 != 0) {
                            switch (RandomSize(2)) {
                                case 0:
                                    pHead.nState = 4;
                                    break;
                                case 1:
                                    pHead.nState = 7;
                                    break;
                                default:
                                    pHead.nState = 8;
                                    break;
                            }

                        }
                        if (pTarget != null) {
                            int hitMove = QueenAngleChase(nSprite, nTarget, nHeadVel, 64);
                            if ((hitMove & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                                if ((hitMove & PS_HIT_INDEX_MASK) == nTarget) {
                                    DamageEnemy(nTarget, nSprite, 10);
                                    D3PlayFX(StaticSound[50] | 0x2000, nSprite);
                                    pSprite.setAng(pSprite.getAng() + (RandomSize(9) + 0x300));
                                    pSprite.setAng(pSprite.getAng() & 0x7FF);
                                    pSprite.setZvel( (-20 - RandomSize(6)));
                                    SetHeadVel(nSprite);
                                }
                            }
                        }
                        break;
                    case 5:
                        if (--pHead.field_A <= 0) {
                            pHead.field_A = 3;
                            if (pHead.field_C-- != 0) {
                                if (pHead.field_C >= 15 || pHead.field_C < 10) {
                                    int ox = pSprite.getX();
                                    int oy = pSprite.getY();
                                    int oz = pSprite.getZ();
                                    short sect = pSprite.getSectnum();
                                    int nAngle = RandomSize(11) & 0x7FF;
                                    int size = (127 - pHead.field_C);
                                    pSprite.setXrepeat(size);
                                    pSprite.setYrepeat(size);
                                    pSprite.setCstat( 32768);
                                    int v54 = RandomSize(5);
                                    int v53 = RandomSize(5);
                                    engine.movesprite( nSprite, EngineUtils.sin(nAngle & 0x7FF) << 10,
                                            EngineUtils.sin((nAngle + 512) & 0x7FF) << 10, (v54 - v53) << 7, 0, 0, 1);
                                    BlowChunks(nSprite);
                                    BuildExplosion(nSprite);
                                    engine.mychangespritesect( nSprite, sect);
                                    pSprite.setX(ox);
                                    pSprite.setY(oy);
                                    pSprite.setZ(oz);
                                    if (pHead.field_C < 10) {
                                        for (int i = 2 * (10 - pHead.field_C); i > 0; i--) {
                                            BuildLavaLimb(nSprite, i, GetSpriteHeight(nSprite));
                                        }
                                    }
                                }
                            } else {
                                BuildExplosion(nSprite);
                                for (int i = 0; i < 10; i++) {
                                    BlowChunks(nSprite);
                                }
                                for (int i = 0; i < 20; i++) {
                                    BuildLavaLimb(nSprite, i, GetSpriteHeight(nSprite));
                                }
                                SubRunRec(pSprite.getOwner());
                                SubRunRec(pHead.nFunc);
                                engine.mydeletesprite( nSprite);
                                ChangeChannel(QueenChan, 1);
                            }
                        }
                        return;
                }

                MoveQX[nQHead] = pSprite.getX();
                MoveQY[nQHead] = pSprite.getY();
                MoveQZ[nQHead] = pSprite.getZ();
                MoveQS[nQHead] = pSprite.getSectnum();
                MoveQA[nQHead] = pSprite.getAng();

                int nQ = nQHead;
                for (int i = 0; i < pHead.field_C; i++) {
                    nQ -= 3;
                    if (nQ < 0) {
                        nQ += 25;
                    }

                    int j = tailspr[i];
                    Sprite spr2 = boardService.getSprite(j);
                    if (spr2 != null) {
                        if (MoveQS[nQ] != spr2.getSectnum()) {
                            engine.mychangespritesect(j, MoveQS[nQ]);
                        }

                        spr2.setX(MoveQX[nQ]);
                        spr2.setY(MoveQY[nQ]);
                        spr2.setZ(MoveQZ[nQ]);
                        spr2.setAng(MoveQA[nQ]);
                    }
                }

                if (++nQHead >= 25) {
                    nQHead = 0;
                }

                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, HeadSeq[nState][0] + SeqOffsets[49], pHead.nSeq, HeadSeq[nState][1]);
                return;
            case nEventRadialDamage:
                Sprite pRadialSpr = boardService.getSprite(nRadialSpr);
                if (pRadialSpr != null && pRadialSpr.getStatnum() != 121) {
                    if ((pSprite.getCstat() & 0x101) != 0) {
                        nDamage = CheckRadialDamage(nSprite);
                    }
                }
            case nEventDamage:
                if (nDamage != 0) {
                    if (pHead.nHealth > 0) {
                        pHead.nHealth -= nDamage;
                        if (RandomSize(4) == 0) {
                            pHead.nTarget = (short) (nStack & 0xFFFF);
                            pHead.nState = 7;
                            pHead.nSeq = 0;
                        }
                        if (pHead.nHealth <= 0) {
                            if (DestroyTailPart()) {
                                pHead.nHealth = 200;
                                nHeadVel += 100;
                            } else {
                                nCreaturesLeft--;
                                pHead.nState = 5;
                                pHead.nSeq = 0;
                                pHead.field_A = 0;
                                pHead.field_C = 80;
                                pSprite.setCstat(0);
                            }
                        }
                    }
                }
        }
    }

    public static void FuncQueenEgg(int nStack, int nDamage, int RunPtr) {
        int nEgg = RunData[RunPtr].getObject();
        EnemyStruct pEgg = QueenEgg[nEgg];
        int nSprite = pEgg.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pEgg.nState;
        int nTarget = -1;
        Sprite pTarget;
        int v44 = 0;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if (pEgg.nHealth > 0) {
                    if (nState == 0 || nState == 4) {
                        Gravity(nSprite);
                    }

                    int nSeq = EggSeq[nState][0] + SeqOffsets[56];
                    pSprite.setPicnum( GetSeqPicnum2(nSeq, pEgg.nSeq));
                    if (nState != 4) {
                        MoveSequence(nSprite, nSeq, pEgg.nSeq);
                        if (++pEgg.nSeq >= SeqSize[nSeq]) {
                            pEgg.nSeq = 0;
                            v44 = 1;
                        }

                        nTarget = UpdateEnemy(pEgg.nTarget);
                        pTarget = boardService.getSprite(nTarget);
                        pEgg.nTarget =  nTarget;
                        if (pTarget == null || (pTarget.getCstat() & 0x101) != 0) {
                            pEgg.nTarget = FindPlayer(-nSprite, 1000);
                            nTarget = pEgg.nTarget;
                        } else {
                            pEgg.nTarget = -1;
                            pEgg.nState = 0;
                        }
                    }

                    switch (nState) {
                        case 0:
                            int hitMove = MoveCreature(nSprite);
                            int nAngle = 0;
                            switch (hitMove & 0xFC000) {
                                case nEventProcess:
                                    if (RandomSize(1) != 0) {
                                        DestroyEgg(nEgg);
                                        return;
                                    }
                                    pEgg.nState = 1;
                                    pEgg.nSeq = 0;
                                    return;
                                case PS_HIT_WALL:
                                    nAngle = engine.GetWallNormal(hitMove & PS_HIT_INDEX_MASK);
                                    break;
                                case PS_HIT_SPRITE:
                                    Sprite hitSpr = boardService.getSprite(hitMove & PS_HIT_INDEX_MASK);
                                    if (hitSpr != null) {
                                        nAngle = hitSpr.getAng();
                                    }
                                    break;
                                default:
                                    return;
                            }
                            pSprite.setAng(nAngle);
                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1));
                            return;
                        case 1:
                            if (v44 != 0) {
                                pEgg.nState = 3;
                                pSprite.setCstat(257);
                            }
                            return;
                        case 2:
                        case 3:
                            int nHit = QueenAngleChase(nSprite, nTarget, nHeadVel, 64);
                            switch (nHit & PS_HIT_TYPE_MASK) {
                                case PS_HIT_SPRITE:
                                    Sprite hitSpr = boardService.getSprite(nHit & PS_HIT_INDEX_MASK);
                                    if (hitSpr != null && hitSpr.getStatnum() != 121) {
                                        DamageEnemy(nHit & PS_HIT_INDEX_MASK, nSprite, 5);
                                    }
                                case PS_HIT_WALL:
                                    pSprite.setAng(pSprite.getAng() + RandomSize(9) + 0x300);
                                    pSprite.setAng(pSprite.getAng() & 0x7FF);
                                    pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 3));
                                    pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 3));
                                    pSprite.setZvel( -RandomSize(5));
                                    return;
                            }
                            return;
                        case 4:
                            if ((MoveCreature(nSprite) & nEventProcess) != 0) {
                                pSprite.setZvel( -(pSprite.getZvel() - 256));
                                if (pSprite.getZvel() < -512) {
                                    pSprite.setZvel(0);
                                }
                            }

                            if (--pEgg.field_A <= 0) {
                                int spr = BuildWasp(-2, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getAng());
                                Sprite s = boardService.getSprite(spr);
                                if (s != null) {
                                    pSprite.setZ(s.getZ());
                                }
                                DestroyEgg(nEgg);
                                return;
                            }
                    }
                    return;
                }
                DestroyEgg(nEgg);
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, EggSeq[nState][0] + SeqOffsets[56], pEgg.nSeq, EggSeq[nState][1]);
                return;
            case nEventRadialDamage:
                Sprite pRadialSpr = boardService.getSprite(nRadialSpr);
                if (pRadialSpr != null && pRadialSpr.getStatnum() != 121) {
                    if ((pSprite.getCstat() & 0x101) != 0) {
                        nDamage = CheckRadialDamage(nSprite);
                    }
                }
            case nEventDamage:
                if (nDamage != 0) {
                    if (pEgg.nHealth > 0) {
                        pEgg.nHealth -= nDamage;
                        if (pEgg.nHealth <= 0) {
                            DestroyEgg(nEgg);
                        }
                    }
                }
        }
    }

    public static class QueenStruct extends EnemyStruct {
        public static final int size = EnemyStruct.size + 2;

        public short field_10;

        @Override
        public void save(OutputStream os) throws IOException {
            super.save(os);
            StreamUtils.writeShort(os, field_10);
        }

        @Override
        public void load(InputStream is) throws IOException {
            super.load(is);
            field_10 =  StreamUtils.readShort(is);
        }


        public void copy(QueenStruct src) {
            super.copy(src);
            field_10 = src.field_10;
        }
    }
}
