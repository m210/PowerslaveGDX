// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BulletStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Powerslave.Bullet.*;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.SetQuake;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sprites.*;

public class Set {

    public static final int MAX_SET = 10;
    private static final short[][] ActionSeq_X_9 = new short[][]{{0, 0}, {77, 1}, {78, 1}, {0, 0}, {9, 0},
            {63, 0}, {45, 0}, {18, 0}, {27, 0}, {36, 0}, {72, 1}, {74, 1}};
    private static int SetCount;
    private static final SetStruct[] SetList = new SetStruct[MAX_SET];
    private static final int[] SetChan = new int[MAX_SET];

    public static void InitSets() {
        SetCount = MAX_SET;
    }

    public static void saveSet(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  SetCount);
        for (int i = SetCount; i < MAX_SET; i++) {
            SetList[i].save(os);
        }
        for (int i = 0; i < MAX_SET; i++) {
            StreamUtils.writeShort(os, SetChan[i]);
        }
    }

    public static void loadSet(SafeLoader loader) {
        SetCount = loader.SetCount;
        for (int i = loader.SetCount; i < MAX_SET; i++) {
            if (SetList[i] == null) {
                SetList[i] = new SetStruct();
            }
            SetList[i].copy(loader.SetList[i]);
        }
        System.arraycopy(loader.SetChan, 0, SetChan, 0, MAX_SET);
    }

    public static void loadSet(SafeLoader loader, InputStream is) throws IOException {
        loader.SetCount = StreamUtils.readShort(is);
        for (int i = loader.SetCount; i < MAX_SET; i++) {
            if (loader.SetList[i] == null) {
                loader.SetList[i] = new SetStruct();
            }
            loader.SetList[i].load(is);
        }
        for (int i = 0; i < MAX_SET; i++) {
            loader.SetChan[i] = StreamUtils.readShort(is);
        }
    }

    public static void BuildSet(int spr, int x, int y, int z, int sectnum, int ang, int channel) {
        int count = --SetCount;
        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  120);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            sectnum = pSprite.getSectnum();
            Sector sec = boardService.getSector(sectnum);
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat( spr,  120);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setXoffset(0);
        pSprite.setShade(-12);
        pSprite.setYoffset(0);
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(110);
        pSprite.setAng( ang);
        pSprite.setXrepeat(87);
        pSprite.setYrepeat(96);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (SetList[count] == null) {
            SetList[count] = new SetStruct();
        }

        SetList[count].nState = 1;
        SetList[count].nHealth = 8000;
        SetList[count].nSprite =  spr;
        SetList[count].nSeq = 0;
        SetList[count].nTarget = -1;
        SetList[count].field_A = 90;
        SetList[count].field_C = 0;
        SetList[count].field_D = 0;
        SetChan[count] =  channel;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent25 | count));
        AddRunRec(NewRun, nEvent25 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void BuildSoul(int nSet) {
        Sprite pSet = boardService.getSprite(SetList[nSet].nSprite);
        if (pSet == null) {
            return;
        }

        int spr = engine.insertsprite(pSet.getSectnum(),  0);
        if (spr < 0 || spr >= MAXSPRITES) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            return;
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setCstat( 32768);
        pSprite.setShade(-127);
        pSprite.setXrepeat(1);
        pSprite.setYrepeat(1);
        pSprite.setPal(0);
        pSprite.setClipdist(5);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);

        pSprite.setPicnum(GetSeqPicnum(48, 75, 0));
        pSprite.setAng( RandomSize(11));
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel( (-256 - RandomSize(10)));
        pSprite.setX(pSet.getX());
        pSprite.setY(pSet.getY());
        pSprite.setZ((RandomSize(8) << 8) + 0x2000 + (sec.getCeilingz() - GetSpriteHeight(spr)));
        pSprite.setHitag( nSet);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(0);
        pSprite.setOwner( AddRunRec(NewRun, nEvent35 | spr));

    }

    public static void FuncSet(int nStack, int nDamage, int RunPtr) {
        int nSet = RunData[RunPtr].getObject();
        if (nSet < 0 || nSet >= MAX_SET) {
            throw new AssertException("set>=0 && set<MAX_SET");
        }

        SetStruct pSet = SetList[nSet];
        int nSprite = pSet.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pSet.nState;
        int nTarget = pSet.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);
        int v68 = 0;

        short nObject = (short) (nStack & 0xFFFF);

        lCheckTarget:
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                Gravity(nSprite);

                int nSeq = ActionSeq_X_9[nState][0] + SeqOffsets[48];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pSet.nSeq));

                MoveSequence(nSprite, nSeq, pSet.nSeq);
                if (nState == 3 && (pSet.field_D) != 0) {
                    ++pSet.nSeq;
                }

                if (++pSet.nSeq >= SeqSize[nSeq]) {
                    pSet.nSeq = 0;
                    v68 = 1;
                }

                int nFlags = FrameFlag[pSet.nSeq + SeqBase[nSeq]];
                if (pTarget != null && nState < 10 && (pTarget.getCstat() & 0x101) == 0) {
                    pSet.nTarget = -1;
                    pTarget = null;
                    pSet.nState = 0;
                    pSet.nSeq = 0;
                    nTarget = -1;
                }

                int hitMove = MoveCreature(nSprite);
                engine.pushmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 4 * pSprite.getClipdist(), 5120, -5120, 0);
                if (pSprite.getZvel() > 4000 && (hitMove & nEventProcess) != 0) {
                    SetQuake(nSprite, 100);
                }
                switch (nState) {
                    case 0:
                        if ((nSet & 0x1F) == (totalmoves & 0x1F)) {
                            if (nTarget < 0) {
                                nTarget = FindPlayer(nSprite, 1000);
                            }

                            pTarget = boardService.getSprite(nTarget);
                            if (pTarget != null) {
                                pSet.nState = 3;
                                pSet.nSeq = 0;
                                pSet.nTarget =  nTarget;
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1));
                            }
                        }
                        return;
                    case 1:
                        if (FindPlayer(nSprite, 1000) >= 0) {
                            if (--pSet.field_A <= 0) {
                                pSet.nState = 2;
                                pSet.nSeq = 0;
                            }
                        }
                        return;
                    case 2:
                        if (v68 != 0) {
                            pSet.nState = 7;
                            pSet.field_C = 0;
                            pSet.nSeq = 0;
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSet.nTarget =  FindPlayer(nSprite, 1000);
                        }
                        return;
                    case 6:
                        if ((nFlags & 0x80) != 0) {
                            BulletStruct.BulletResult nBullet = BuildBullet(nSprite, 11, -1, pSprite.getAng(), nTarget + 10000, 1);
                            SetBulletEnemy(nBullet.getBullet(), nTarget);
                            if (--pSet.field_E <= 0 || RandomBit() == 0) {
                                pSet.nState = 0;
                                pSet.nSeq = 0;
                            }
                        }
                        return;
                    case 3:
                        if (nTarget == -1) {
                            pSet.nState = 0;
                            pSet.nSeq = 0;
                            return;
                        }

                        if ((nFlags & 0x10) != 0 && (hitMove & nEventProcess) != 0) {
                            SetQuake(nSprite, 100);
                        }
                        int nGoalAngle = PlotCourseToSprite(nSprite, nTarget);

                        boolean v34 = (totalmoves & 0x1F) == (nSet & 0x1F);
                        if (v34) {
                            int v33 = RandomSize(3);
                            v34 = v33 == 0 || v33 == 2;

                            if (v34) {
                                pSet.field_C = 0;
                            } else if (v33 == 1) {
                                break;
                            } else {
                                pSet.field_D =  (nGoalAngle > 100 ? 1 : 0);
                            }
                        }

                        if (!v34) {
                            int nAngle = pSprite.getAng() & 0xFFF8;
                            pSprite.setXvel( (EngineUtils.sin((nAngle + 512) & 0x7FF) >> 1));
                            pSprite.setYvel( (EngineUtils.sin(nAngle) >> 1));
                            if (pSet.field_D != 0) {
                                pSprite.setXvel(pSprite.getXvel() * 2);
                                pSprite.setYvel(pSprite.getYvel() * 2);
                            }

                            switch (hitMove & PS_HIT_TYPE_MASK) {
                                default:
                                    break lCheckTarget;
                                case PS_HIT_WALL:
                                    Wall hitWall = boardService.getWall(hitMove & PS_HIT_INDEX_MASK);
                                    Sector nsec = hitWall != null ? boardService.getSector(hitWall.getNextsector()) : null;
                                    if (nsec == null || (pSprite.getZ() - nsec.getFloorz() >= 55000)
                                            || pSprite.getZ() <= nsec.getCeilingz()) {
                                        pSprite.setAng( ((pSprite.getAng() + 256) & 0x7FF));
                                        pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                                        pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1));
                                        break lCheckTarget;
                                    }
                                    break;
                                case PS_HIT_SPRITE:
                                    if ((hitMove & PS_HIT_INDEX_MASK) == nTarget) {
                                        if (pTarget != null && AngleDiff(pSprite.getAng(),
                                                EngineUtils.getAngle(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY())) < 64) {
                                            pSet.nState = 4;
                                            pSet.nSeq = 0;
                                        }
                                        break lCheckTarget;
                                    }
                                    break;
                            }
                            pSet.field_C = 1;
                        }

                        pSprite.setXvel(0);
                        pSprite.setYvel(0);
                        pSet.nState = 7;
                        pSet.nSeq = 0;
                        return;
                    case 5:
                        if (v68 != 0) {
                            pSet.nState = 0;
                            pSet.field_A = 15;
                        }
                        return;
                    case 4:
                        if (nTarget == -1) {
                            pSet.nState = 0;
                            pSet.field_A = 50;
                        } else if (PlotCourseToSprite(nSprite, nTarget) >= 768) {
                            pSet.nState = 3;
                        } else if ((nFlags & 0x80) != 0) {
                            DamageEnemy(nTarget, nSprite, 5);
                        }
                        break lCheckTarget;
                    case 7:
                        if (v68 != 0) {
                            if (pSet.field_C != 0) {
                                pSprite.setZvel(-10000);
                            } else {
                                pSprite.setZvel( -PlotCourseToSprite(nSprite, nTarget));
                            }

                            pSet.nState = 8;
                            pSet.nSeq = 0;
                            pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                        }
                        return;
                    case 8:
                        if (v68 != 0) {
                            pSet.nSeq =  (SeqSize[nSeq] - 1);
                        }
                        if ((hitMove & nEventProcess) != 0) {
                            SetQuake(nSprite, 200);
                            pSet.nState = 9;
                            pSet.nSeq = 0;
                        }
                        return;
                    case 9:
                        pSprite.setXvel(pSprite.getXvel() >> 1);
                        pSprite.setYvel(pSprite.getYvel() >> 1);
                        if (v68 != 0) {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            break;
                        }
                        return;
                    case 10:
                        if ((nFlags & 0x80) != 0) {
                            pSprite.setZ(pSprite.getZ() - GetSpriteHeight(nSprite));

                            BuildCreatureChunk(nSprite, GetSeqPicnum(48, 76, 0));
                            pSprite.setZ(pSprite.getZ() + GetSpriteHeight(nSprite));
                        }
                        if (v68 != 0) {
                            pSet.nState = 11;
                            pSet.nSeq = 0;
                            ChangeChannel(SetChan[nSet], 1);
                            for (int i = 0; i < 20; i++) {
                                BuildSoul(nSet);
                            }
                        }
                        return;
                    case 11:
                        pSprite.setCstat(pSprite.getCstat() & ~0x101);
                        return;
                }

                PlotCourseToSprite(nSprite, nTarget);
                pSet.nState = 6;
                pSet.nSeq = 0;
                pSet.field_E = 5;
                pSprite.setXvel(0);
                pSprite.setYvel(0);

                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_9[nState][0] + SeqOffsets[48], pSet.nSeq, ActionSeq_X_9[nState][1]);
                return;
            case nEventRadialDamage:
                if (nState == 5) {
                    nDamage = CheckRadialDamage(nSprite);
                }
            case nEventDamage:
                if (nDamage != 0) {
                    if (pSet.nHealth > 0) {
                        if (nState != 1) {
                            pSet.nHealth -= nDamage;
                        }
                        if (pSet.nHealth <= 0) {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(0);
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            pSet.nHealth = 0;
                            nCreaturesLeft--;
                            if (pSet.nState < 10) {
                                pSet.nState = 10;
                                pSet.nSeq = 0;
                            }
                        } else {
                            if (nState == 1) {
                                pSet.nState = 2;
                                pSet.nSeq = 0;
                            }
                        }
                    }
                }
                return;
        }

        if (nState > 0 && pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
            pSet.nState = 0;
            pSet.nSeq = 0;
            pSet.field_A = 100;
            pSet.nTarget = -1;
            pSprite.setXvel(0);
            pSprite.setYvel(0);
        }
    }

    public static void FuncSoul(int nStack, int ignored, int RunPtr) {
        int nSprite = RunData[RunPtr].getObject();
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if ((nStack & EVENT_MASK) == nEventProcess) {
            MoveSequence(nSprite, SeqOffsets[48] + 75, 0);
            if (pSprite.getXrepeat() < 32) {
                pSprite.setXrepeat(pSprite.getXrepeat() + 1);
                pSprite.setYrepeat(pSprite.getYrepeat() + 1);
            }
            pSprite.setExtra(pSprite.getExtra() + (nSprite & 0xF) + 5);
            pSprite.setExtra(pSprite.getExtra() & 0x7FF);

            int vel = EngineUtils.sin((pSprite.getExtra() + 512) & 0x7FF) >> 7;
            if ((engine.movesprite(nSprite, EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) * vel,
                    vel * EngineUtils.sin(pSprite.getAng()), pSprite.getZvel(), 5120, 0, 0) & 0x10000) != 0) {
                int nSet = SetList[pSprite.getHitag()].nSprite;
                Sprite pSet = boardService.getSprite(nSet);
                if (pSet != null) {
                    pSprite.setCstat(0);
                    pSprite.setYrepeat(1);
                    pSprite.setXrepeat(1);
                    pSprite.setX(pSet.getX());
                    pSprite.setY(pSet.getY());
                    pSprite.setZ(pSet.getZ() - (GetSpriteHeight(nSet) >> 1));
                    engine.mychangespritesect(nSprite, pSet.getSectnum());
                }
            }
        }
    }

    public static class SetStruct extends EnemyStruct {

        public int field_D;
        public int field_E;

        @Override
        public void save(OutputStream os) throws IOException {
            super.save(os);
            StreamUtils.writeShort(os, field_D);
            StreamUtils.writeShort(os, field_E);
        }

        @Override
        public void load(InputStream is) throws IOException {
            super.load(is);
            field_D = StreamUtils.readShort(is);
            field_E = StreamUtils.readShort(is);
        }

        public void copy(SetStruct src) {
            super.copy(src);
            field_D = src.field_D;
            field_E = src.field_E;
        }
    }
}
