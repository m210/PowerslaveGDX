// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sprites.*;

public class Wasp {

    public static final int MAX_WASPS = 100;
    private static final short[][] ActionSeq_X_12 = new short[][]{{0, 0}, {0, 0}, {9, 0}, {18, 0}, {27, 1},
            {28, 1}, {29, 1}};
    public static int nWaspCount;
    private static int nVelShift_X_1;
    private static final WaspStruct[] WaspList = new WaspStruct[MAX_WASPS];

    public static void InitWasps() {
        nWaspCount = 0;
    }

    public static void saveWasp(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nWaspCount);
        for (int i = 0; i < nWaspCount; i++) {
            WaspList[i].save(os);
        }
        StreamUtils.writeInt(os, nVelShift_X_1);
    }

    public static void loadWasp(SafeLoader loader) {
        nWaspCount = loader.nWaspCount;
        for (int i = 0; i < loader.nWaspCount; i++) {
            if (WaspList[i] == null) {
                WaspList[i] = new WaspStruct();
            }
            WaspList[i].copy(loader.WaspList[i]);
        }
        nVelShift_X_1 = loader.nVelShift_X_1;
    }

    public static void loadWasp(SafeLoader loader, InputStream is) throws IOException {
        loader.nWaspCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nWaspCount; i++) {
            if (loader.WaspList[i] == null) {
                loader.WaspList[i] = new WaspStruct();
            }
            loader.WaspList[i].load(is);
        }
        loader.nVelShift_X_1 = StreamUtils.readInt(is);
    }

    public static void SetWaspVel(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (nVelShift_X_1 < 0) {
            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) << (-nVelShift_X_1)));
            pSprite.setXvel( (EngineUtils.sin(pSprite.getAng() + 512 & 0x7FF) << (-nVelShift_X_1)));
        } else {
            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> nVelShift_X_1));
            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> nVelShift_X_1));
        }
    }

    public static int BuildWasp(int spr, int x, int y, int z, int sectnum, int ang) {
        int count = nWaspCount++;

        if (nWaspCount >= MAX_WASPS) {
            return -1;
        }

        boolean opt = spr == -2;
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  107);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            z = pSprite.getX();
            ang = pSprite.getAng();
            engine.changespritestat( spr,  107);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return -1;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setAng( ang);
        if (opt) {
            pSprite.setYrepeat(20);
            pSprite.setXrepeat(20);
        } else {
            pSprite.setXrepeat(50);
            pSprite.setYrepeat(50);
        }
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(70);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (WaspList[count] == null) {
            WaspList[count] = new WaspStruct();
        }

        WaspList[count].nState = 0;
        WaspList[count].nSeq = 0;
        WaspList[count].nSprite =  spr;
        WaspList[count].nTarget = -1;
        WaspList[count].nHealth = 800;
        WaspList[count].nDamage = 10;
        if (opt) {
            WaspList[count].field_C = 60;
            WaspList[count].nDamage /= 2;
        } else {
            WaspList[count].field_C =  RandomSize(5);
        }
        WaspList[count].nAttackTime = 0;
        WaspList[count].nVelocity = 0;
        WaspList[count].dTime =  (RandomSize(7) + 127);
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent30 | count));
        WaspList[count].nFunc =  AddRunRec(NewRun, nEvent30 | count);
        nCreaturesLeft++;
        nCreaturesMax++;

        return spr;
    }

    public static void FuncWasp(int nStack, int nDamage, int RunPtr) {
        int nWasp = RunData[RunPtr].getObject();
        WaspStruct pWasp = WaspList[nWasp];
        final int nSprite = pWasp.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pWasp.nState;

        int v48 = 0;
        short nObject = (short) (nStack & 0xFFFF);
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int nSeq = ActionSeq_X_12[nState][0] + SeqOffsets[22];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pWasp.nSeq));
                MoveSequence(nSprite, nSeq, pWasp.nSeq);
                if (++pWasp.nSeq >= SeqSize[nSeq]) {
                    pWasp.nSeq = 0;
                    v48 = 1;
                }

                int nTarget = pWasp.nTarget;
                Sprite pTarget = boardService.getSprite(nTarget);
                if (pWasp.nHealth <= 0 || pTarget == null
                        || ((pTarget.getCstat() & 0x101) != 0 && (SectFlag[pTarget.getSectnum()] & 0x2000) == 0)) {
                    switch (nState) {
                        case 0:
                            pSprite.setZvel( (EngineUtils.sin(pWasp.nAttackTime & 0x7FF) >> 4));
                            pWasp.nAttackTime =  ((pWasp.nAttackTime + pWasp.dTime) & 0x7FF);
                            MoveCreature(nSprite);
                            if (nTarget < 0) {
                                if ((nWasp & 0x1F) == (totalmoves & 0x1F)) {
                                    pWasp.nTarget =  FindPlayer(nSprite, 60);
                                }
                            } else {
                                if (--pWasp.field_C > 0) {
                                    PlotCourseToSprite(nSprite, nTarget);
                                } else {
                                    pWasp.nState = 1;
                                    pSprite.setZvel(0);
                                    pWasp.nSeq = 0;
                                    pWasp.nVelocity = 1500;
                                    pWasp.field_C =  (RandomSize(5) + 60);
                                }
                            }
                            return;
                        case 2:
                        case 3:
                            if (v48 != 0) {
                                break;
                            }
                            return;
                        case 1:
                            if (--pWasp.field_C > 0) {
                                int moveHit = AngleChase(nSprite, nTarget, pWasp.nVelocity, 0, 16);
                                if ((moveHit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                                    if ((moveHit & PS_HIT_INDEX_MASK) == nTarget) {
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                        DamageEnemy(nTarget, nSprite, pWasp.nDamage);
                                        pWasp.nState = 2;
                                        pWasp.nSeq = 0;
                                    }
                                }
                            } else {
                                pWasp.nState = 0;
                                pWasp.field_C =  RandomSize(6);
                            }
                            return;
                        case 4:
                            MoveCreature(nSprite);
                            pWasp.nState = 5;
                            pWasp.nSeq = 0;
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(1024);
                            return;
                        case 5:
                            pSprite.setZ(pSprite.getZ() + pSprite.getZvel());
                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null && pSprite.getZ() >= sec.getFloorz()) {
                                if (SectBelow[pSprite.getSectnum()] > -1) {
                                    BuildSplash(pSprite, pSprite.getSectnum());
                                    pSprite.setCstat(pSprite.getCstat() | 0x8000);
                                }
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                                pSprite.setZvel(0);
                                pWasp.nState = 6;
                                pWasp.nSeq = 0;
                                SubRunRec(pWasp.nFunc);
                            }
                            return;
                        default:
                            return;
                    }
                } else {
                    pWasp.nTarget = -1;
                    pWasp.nState = 0;
                    pWasp.field_C =  RandomSize(6);
                    return;
                }

                break;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_12[nState][0] + SeqOffsets[22], pWasp.nSeq, ActionSeq_X_12[nState][1]);
                return;
            case nEventRadialDamage:
                if ((pSprite.getCstat() & 0x101) == 0) {
                    return;
                }
                nDamage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (nDamage != 0) {
                    if (pWasp.nHealth > 0) {
                        pWasp.nHealth -= nDamage;
                        if (pWasp.nHealth > 0) {
                            if (RandomSize(4) == 0) {
                                pWasp.nState = 3;
                                pWasp.nSeq = 0;
                            }
//                            break; 23.04.2024
                        } else {
                            pWasp.nState = 4;
                            nVelShift_X_1 = 0;
                            pWasp.nSeq = 0;
                            pSprite.setCstat(0);
                            pSprite.setAng( ((pSprite.getAng() + 1024) & 0x7FF));
                            SetWaspVel(nSprite);
                            pSprite.setZvel(512);
                            nCreaturesLeft--;
                        }
                    }
                }
                return;
        }

        pWasp.nState = 1;
        pSprite.setAng(pSprite.getAng() + ((RandomSize(9) + 0x300) & 0x7FF));
        pWasp.nVelocity = 3000;
        pSprite.setZvel( (-20 - RandomSize(6)));
    }

    public static class WaspStruct extends EnemyStruct {
        public int nAttackTime;
        public int dTime;
        public int nVelocity;
        public int nDamage;

        @Override
        public void save(OutputStream os) throws IOException {
            super.save(os);
            StreamUtils.writeShort(os, nAttackTime);
            StreamUtils.writeShort(os, dTime);
            StreamUtils.writeShort(os, nVelocity);
            StreamUtils.writeShort(os, nDamage);
        }

        @Override
        public void load(InputStream is) throws IOException {
            super.load(is);
            nAttackTime =  StreamUtils.readShort(is);
            dTime =  StreamUtils.readShort(is);
            nVelocity =  StreamUtils.readShort(is);
            nDamage =  StreamUtils.readShort(is);
        }

        public void copy(WaspStruct src) {
            super.copy(src);
            nAttackTime = src.nAttackTime;
            dTime = src.dTime;
            nVelocity = src.nVelocity;
            nDamage = src.nDamage;
        }
    }
}
