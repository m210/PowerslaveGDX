// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Lion {

    public static final int MAXLION = 40;
    private static final short[][] ActionSeq_X_13 = new short[][]{{54, 1}, {18, 0}, {0, 0}, {10, 0}, {44, 0},
            {18, 0}, {26, 0}, {34, 0}, {8, 1}, {9, 1}, {52, 1}, {53, 1}};
    private static int LionCount;
    private static final LionStruct[] LionList = new LionStruct[MAXLION];

    public static void InitLion() {
        LionCount = MAXLION;
    }

    public static void saveLion(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  LionCount);
        for (int i = LionCount; i < MAXLION; i++) {
            LionList[i].save(os);
        }
    }

    public static void loadLion(SafeLoader loader, InputStream is) throws IOException {
        loader.LionCount = StreamUtils.readShort(is);
        for (int i = loader.LionCount; i < MAXLION; i++) {
            if (loader.LionList[i] == null) {
                loader.LionList[i] = new LionStruct();
            }
            loader.LionList[i].load(is);
        }
    }

    public static void loadLion(SafeLoader loader) {
        LionCount = loader.LionCount;
        for (int i = loader.LionCount; i < MAXLION; i++) {
            if (LionList[i] == null) {
                LionList[i] = new LionStruct();
            }
            LionList[i].copy(loader.LionList[i]);
        }
    }

    public static void BuildLion(int spr, int x, int y, int z, int sectnum, int ang) {
        int count = --LionCount;

        if (count < 0) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  104);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            sectnum = pSprite.getSectnum();
            Sector sec = boardService.getSector(sectnum);
            if (sec != null) {
                z = sec.getFloorz();
            }
            ang = pSprite.getAng();
            engine.changespritestat( spr,  104);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setAng( ang);
        pSprite.setXrepeat(40);
        pSprite.setYrepeat(40);
        pSprite.setPicnum(1);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(60);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (LionList[count] == null) {
            LionList[count] = new LionStruct();
        }

        LionList[count].nState = 0;
        LionList[count].nHealth = 500;
        LionList[count].nSeq = 0;
        LionList[count].nSprite =  spr;
        LionList[count].nTarget = -1;
        LionList[count].field_C = 0;
        LionList[count].field_A =  count;
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent19 | count));
        LionList[count].MoveHook =  AddRunRec(NewRun, nEvent19 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncLion(int nStack, int nDamage, int RunPtr) {
        int nLion = RunData[RunPtr].getObject();
        if (nLion < 0 || nLion >= MAXLION) {
            throw new AssertException("lion>=0 && lion<MAXLION");
        }

        LionStruct pLion = LionList[nLion];
        int nSprite = pLion.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v114 = 0;
        int nState = pLion.nState;
        final int nTarget = pLion.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);

        short nObject = (short) (nStack & 0xFFFF);
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if (nState != 7) {
                    Gravity(nSprite);
                }

                int nSeq = ActionSeq_X_13[nState][0] + SeqOffsets[40];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pLion.nSeq));
                MoveSequence(nSprite, nSeq, pLion.nSeq);
                if (++pLion.nSeq >= SeqSize[nSeq]) {
                    pLion.nSeq = 0;
                    v114 = 1;
                }

                int nFlags = FrameFlag[pLion.nSeq + SeqBase[nSeq]];
                int moveHit = MoveCreatureWithCaution(nSprite);

                switch (nState) {
                    case 0:
                    case 1:
                        int nTarg;
                        if ((pLion.field_A & 0x1F) != (totalmoves & 0x1F) || nTarget >= 0
                                || (nTarg = FindPlayer(nSprite, 40)) < 0) {
                            if (nState != 0) {
                                if (--pLion.field_C <= 0) {
                                    if (RandomBit() != 0) {
                                        pSprite.setAng( (RandomWord() & 0x7FF));
                                        pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                                        pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 1));
                                    } else {
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                    }
                                    pLion.field_C = 100;
                                }
                            }
                        } else {
                            D3PlayFX(StaticSound[24], nSprite);
                            pLion.nState = 2;
                            pLion.nSeq = 0;
                            pLion.nTarget =  nTarg;
                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 1));
                        }
                        return;
                    case 2:
                        if ((totalmoves & 0x1F) == (pLion.field_A & 0x1F)) {
                            PlotCourseToSprite(nSprite, nTarget);
                            int ang = pSprite.getAng() & 0xFFF8;
                            if ((pSprite.getCstat() & 0x8000) != 0) {
                                pSprite.setXvel( (2 * EngineUtils.sin((ang + 512) & 0x7FF)));
                                pSprite.setYvel( (2 * EngineUtils.sin(ang & 0x7FF)));
                            } else {
                                pSprite.setXvel( (EngineUtils.sin((ang + 512) & 0x7FF) >> 1));
                                pSprite.setYvel( (EngineUtils.sin(ang & 0x7FF) >> 1));
                            }
                        }

                        switch (moveHit & PS_HIT_TYPE_MASK) {
                            case PS_HIT_SPRITE:
                                if ((moveHit & PS_HIT_INDEX_MASK) == nTarget) {
                                    if ((pSprite.getCstat() & 0x8000) != 0) {
                                        pLion.nState = 9;
                                        pSprite.setCstat(pSprite.getCstat() & ~0x8000);
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                    } else {
                                        if (pTarget != null && AngleDiff(pSprite.getAng(), EngineUtils.getAngle(pTarget.getX() - pSprite.getX(),
                                                pTarget.getY() - pSprite.getY())) < 64) {
                                            pLion.nState = 3;
                                        }
                                    }
                                    pLion.nSeq = 0;
                                }
                            case PS_HIT_WALL:
                                pSprite.setAng( ((pSprite.getAng() + 256) & 0x7FF));
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 1));
                                break;
                        }
                        break;
                    case 3:
                        if (nTarget == -1) {
                            pLion.nState = 1;
                            pLion.field_C = 50;
                        } else if (PlotCourseToSprite(nSprite, nTarget) >= 768) {
                            pLion.nState = 2;
                        } else if ((nFlags & 0x80) != 0) {
                            DamageEnemy(nTarget, nSprite, 10);
                        }
                        break;
                    case 10:
                    case 11:
                        if (v114 != 0) {
                            SubRunRec(pSprite.getOwner());
                            SubRunRec(pLion.MoveHook);
                            pSprite.setCstat( 32768);
                        }
                        return;
                    case 4:
                        if (v114 != 0) {
                            pLion.nState = 2;
                            pLion.nSeq = 0;
                        }
                        if ((moveHit & 0x200) != 0) {
                            pSprite.setXvel(pSprite.getXvel() >> 1);
                            pSprite.setYvel(pSprite.getYvel() >> 1);
                        }
                        return;
                    case 8:
                        if (v114 != 0) {
                            pLion.nSeq = 0;
                            pLion.nState = 2;
                            pSprite.setCstat(pSprite.getCstat() | 0x8000);
                        }
                        return;
                    case 9:
                        if (v114 != 0) {
                            pLion.nSeq = 0;
                            pLion.nState = 2;
                            pSprite.setCstat(pSprite.getCstat() | 0x101);
                        }
                        return;
                    case 6:
                        if ((moveHit & nEvent3) != 0) {
                            pLion.nState = 2;
                            pLion.nSeq = 0;
                        } else {
                            if ((moveHit & PS_HIT_TYPE_MASK) == PS_HIT_WALL) {
                                pLion.nState = 7;
                                pSprite.setAng( ((engine.GetWallNormal(moveHit & PS_HIT_INDEX_MASK) + 1024) & 0x7FF));
                                pLion.field_C =  RandomSize(4);
                            } else if ((moveHit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                                if ((moveHit & PS_HIT_INDEX_MASK) == nTarget) {
                                    if (pTarget != null && AngleDiff(pSprite.getAng(), EngineUtils.getAngle(pTarget.getX() - pSprite.getX(),
                                            pTarget.getY() - pSprite.getY())) < 64) {
                                        pLion.nState = 3;
                                        pLion.nSeq = 0;
                                    }
                                } else {
                                    pSprite.setAng( ((pSprite.getAng() + 256) & 0x7FF));
                                    pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                                    pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 1));
                                    break;
                                }
                            }
                        }
                        return;
                    case 5:
                    case 7:
                        if (--pLion.field_C <= 0) {
                            pLion.field_C = 0;
                            if (nState == 7) {
                                if (nTarget <= -1) {
                                    pSprite.setAng( ((pSprite.getAng() + RandomSize(9)) & 0x7FF));
                                } else {
                                    PlotCourseToSprite(nSprite, nTarget);
                                }
                                pSprite.setZvel(-1000);
                            } else {
                                pSprite.setZvel(-4000);
                                int ang = pSprite.getAng();

                                int hita = (ang - 512) & 0x7FF;
                                int xs = pSprite.getX();
                                int ys = pSprite.getY();
                                int zs = pSprite.getZ() - GetSpriteHeight(nSprite) >> 1;
                                short sectnum = pSprite.getSectnum();
                                int v104 = 0x7FFFFFFF, v70;
                                for (int i = 0; i < 5; i++) {
                                    engine.hitscan(xs, ys, zs, sectnum, EngineUtils.sin((hita + 512) & 0x7FF), EngineUtils.sin(hita & 0x7FF),
                                            0, pHitInfo, CLIPMASK1);
                                    if (pHitInfo.hitwall != -1) {
                                        if ((v70 = klabs(pHitInfo.hitx - xs) + klabs(pHitInfo.hity - ys)) < v104) {
                                            v104 = v70;
                                            ang =  hita;
                                        }
                                    }
                                    hita = (hita + 256) & 0x7FF;
                                }
                                pSprite.setAng(ang);
                            }

                            pLion.nState = 6;
                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF)
                                    - (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 3)));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) - (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 3)));
                            D3PlayFX(StaticSound[24], nSprite);
                        }
                        return;
                }

                if (pTarget != null && (pTarget.getCstat() & 0x101) == 0) {
                    pLion.nState = 1;
                    pLion.nSeq = 0;
                    pLion.field_C = 100;
                    pLion.nTarget = -1;
                    pSprite.setXvel(0);
                    pSprite.setYvel(0);
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_13[nState][0] + SeqOffsets[40], pLion.nSeq,
                        ActionSeq_X_13[nState][1]);
                return;
            case nEventRadialDamage:
                nDamage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (nDamage != 0) {
                    if (pLion.nHealth > 0) {
                        pLion.nHealth -= nDamage;
                        if (pLion.nHealth > 0) {
                            Sprite pObject = boardService.getSprite(nObject);
                            if (pObject != null) {
                                if (pObject.getStatnum() < 199) {
                                    pLion.nTarget = nObject;
                                }

                                if (nState != 6) {
                                    if (RandomSize(8) <= pLion.nHealth) {
                                        pLion.nState = 4;
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                    } else if (RandomSize(1) != 0) {
                                        PlotCourseToSprite(nSprite, nObject);
                                        pSprite.setAng(pSprite.getAng() - (RandomSize(1) << 8));
                                        pSprite.setAng(pSprite.getAng() + (RandomSize(1) << 8));
                                        pSprite.setAng(pSprite.getAng() & 0x7FF);
                                        pLion.nState = 5;
                                        pLion.field_C =  RandomSize(3);
                                    } else {
                                        pSprite.setXvel(0);
                                        pSprite.setYvel(0);
                                        pSprite.setCstat(pSprite.getCstat() & ~0x101);
                                        pLion.nState = 8;
                                    }
                                    pLion.nSeq = 0;
                                }
                            }
                        } else {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            pSprite.setZvel(0);
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            pLion.nHealth = 0;
                            if (pLion.nState < 10) {
                                DropMagic(nSprite);
                                pLion.nState =  ((((nStack & EVENT_MASK) == nEventRadialDamage) ? 1 : 0) + 10);
                                pLion.nSeq = 0;
                            }
                            nCreaturesLeft--;
                        }
                    }
                }
        }
    }

    public static class LionStruct extends EnemyStruct {

        public int MoveHook;

        @Override
        public void save(OutputStream os) throws IOException {
            super.save(os);
            StreamUtils.writeShort(os, MoveHook);
        }

        @Override
        public void load(InputStream is) throws IOException {
            super.load(is);
            MoveHook =  StreamUtils.readShort(is);
        }

        public LionStruct copy(LionStruct src) {
            super.copy(src);

            MoveHook = src.MoveHook;
            return this;
        }
    }
}
