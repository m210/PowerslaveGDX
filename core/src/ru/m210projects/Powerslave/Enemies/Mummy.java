// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BulletStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Bullet.*;
import static ru.m210projects.Powerslave.Enemies.Enemy.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.SetQuake;
import static ru.m210projects.Powerslave.Random.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Mummy {

    public static final int MAX_MUMMIES = 150;

    private static final short[][] ActionSeq_X_4 = new short[][]{{8, 0}, {0, 0}, {16, 0}, {24, 0}, {32, 1},
            {40, 1}, {48, 1}, {50, 0}};

    private static int MummyCount;
    private static final EnemyStruct[] MummyList = new EnemyStruct[MAX_MUMMIES];

    public static void InitMummy() {
        MummyCount = 0;
    }

    public static void saveMummy(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  MummyCount);
        for (int i = 0; i < MummyCount; i++) {
            MummyList[i].save(os);
        }
    }

    public static void loadMummy(SafeLoader loader, InputStream is) throws IOException {
        loader.MummyCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.MummyCount; i++) {
            if (loader.MummyList[i] == null) {
                loader.MummyList[i] = new EnemyStruct();
            }
            loader.MummyList[i].load(is);
        }
    }

    public static void loadMummy(SafeLoader loader) {
        MummyCount = loader.MummyCount;
        for (int i = 0; i < loader.MummyCount; i++) {
            if (MummyList[i] == null) {
                MummyList[i] = new EnemyStruct();
            }
            MummyList[i].copy(loader.MummyList[i]);
        }
    }

    public static void BuildMummy(int spr, int x, int y, int z, int sectnum, int ang) {
        int count = MummyCount++;

        if (count >= MAX_MUMMIES) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  102);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            y = pSprite.getY();
            x = pSprite.getX();
            z = pSprite.getZ();
            engine.changespritestat( spr,  102);
            ang = pSprite.getAng();
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setClipdist(32);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setXrepeat(42);
        pSprite.setYrepeat(42);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setAng( ang);
        pSprite.setPicnum(1);
        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent14 | count));

        if (MummyList[count] == null) {
            MummyList[count] = new EnemyStruct();
        }

        MummyList[count].nHealth = 640;
        MummyList[count].nSeq = 0;
        MummyList[count].nState = 0;
        MummyList[count].nSprite =  spr;
        MummyList[count].nTarget = -1;
        MummyList[count].field_A =  count;
        MummyList[count].field_C = 0;
        MummyList[count].nFunc =  AddRunRec(NewRun, nEvent14 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncMummy(int nStack, int a2, int RunPtr) {
        int nMummy = RunData[RunPtr].getObject();
        if (nMummy < 0 || nMummy >= MAX_MUMMIES) {
            throw new AssertException("Mummy>=0 && Mummy<MAX_MUMMIES");
        }

        int nSprite = MummyList[nMummy].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int damage = a2;
        short plr = (short) (nStack & 0xFFFF);

        int nTarget = UpdateEnemy(MummyList[nMummy].nTarget);
        Sprite pTarget = boardService.getSprite(nTarget);
        int nState = MummyList[nMummy].nState;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                Gravity(nSprite);

                int v10 = ActionSeq_X_4[nState][0] + SeqOffsets[10];
                int v13 = MummyList[nMummy].nSeq;
                pSprite.setPicnum( GetSeqPicnum2(v10, v13));

                int flags = FrameFlag[v13 + SeqBase[v10]];
                MoveSequence(nSprite, v10, v13);
                MummyList[nMummy].nSeq++;
                int v18 = 0;
                if (MummyList[nMummy].nSeq >= SeqSize[v10]) {
                    MummyList[nMummy].nSeq = 0;
                    v18 = 1;
                }

                if (pTarget != null && nState < 4 && pTarget.getCstat() == 0 && nState != 0) {
                    MummyList[nMummy].nState = 0;
                    MummyList[nMummy].nSeq = 0;
                    pSprite.setYvel(0);
                    pSprite.setXvel(0);
                }
                int v17 = MoveCreatureWithCaution(nSprite);
                switch (MummyList[nMummy].nState) {
                    case 0:
                        if ((MummyList[nMummy].field_A & 0x1F) == (totalmoves & 0x1F)) {
                            pSprite.setCstat(257);
                            if (pTarget == null) {
                                nTarget = FindPlayer(nSprite, 100);
                                pTarget = boardService.getSprite(nTarget);
                            }

                            if (pTarget != null) {
                                D3PlayFX(StaticSound[7], nSprite);
                                MummyList[nMummy].nSeq = 0;
                                MummyList[nMummy].nTarget = nTarget;
                                MummyList[nMummy].nState = 1;
                                pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                                pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 2));
                                MummyList[nMummy].field_C = 90;
                            }
                        }
                        return;
                    case 1:
                        int v20 = MummyList[nMummy].field_C;
                        if (v20 > 0) {
                            MummyList[nMummy].field_C--;
                        }
                        if ((totalmoves & 0x1F) == (MummyList[nMummy].field_A & 0x1F)) {
                            pSprite.setCstat(257);
                            PlotCourseToSprite(nSprite, nTarget);
                            if (MummyList[nMummy].nState == 1) {
                                if (RandomBit() != 0) {
                                    if (pTarget != null && engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ() - GetSpriteHeight(nSprite),
                                            pSprite.getSectnum(), pTarget.getX(), pTarget.getY(), pTarget.getZ() - GetSpriteHeight(nTarget), pTarget.getSectnum())) {
                                        MummyList[nMummy].nState = 3;
                                        MummyList[nMummy].nSeq = 0;
                                        pSprite.setYvel(0);
                                        pSprite.setXvel(0);
                                        return;
                                    }
                                }
                            }
                        }
                        if (MummyList[nMummy].nSeq == 0) {
                            pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 1));
                            pSprite.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 1));
                        }

                        if (pSprite.getXvel() != 0 && pSprite.getYvel() != 0) {
                            if (pSprite.getXvel() > 0) {
                                pSprite.setXvel( BClipLow(pSprite.getXvel() - 1024, 0));
                            } else {
                                pSprite.setXvel( BClipHigh(pSprite.getXvel() + 1024, 0));
                            }

                            if (pSprite.getYvel() > 0) {
                                pSprite.setYvel( BClipLow(pSprite.getYvel() - 1024, 0));
                            } else {
                                pSprite.setYvel( BClipHigh(pSprite.getYvel() + 1024, 0));
                            }
                        }
                        if (v17 != 0) {
                            switch (v17 & PS_HIT_TYPE_MASK) {
                                case PS_HIT_WALL:
                                    pSprite.setAng( ((pSprite.getAng() + (RandomWord() & 0x300) + 0x400) & 0x7FF));
                                    pSprite.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                                    pSprite.setYvel( (EngineUtils.sin(pSprite.getAng()) >> 2));
                                    break;
                                case PS_HIT_SPRITE:
                                    if ((v17 & PS_HIT_INDEX_MASK) == nTarget) {
                                        if (pTarget != null && AngleDiff(pSprite.getAng(), EngineUtils.getAngle(pTarget.getX() - pSprite.getX(),
                                                pTarget.getY() - pSprite.getY())) < 64) {
                                            MummyList[nMummy].nState = 2;
                                            MummyList[nMummy].nSeq = 0;
                                            pSprite.setYvel(0);
                                            pSprite.setXvel(0);
                                        }
                                    }

                                    break;
                            }
                        }
                        return;
                    case 2:
                        if (nTarget == -1) {
                            MummyList[nMummy].nState = 0;
                        } else {
                            if (PlotCourseToSprite(nSprite, nTarget) < 1024) {
                                if ((flags & 0x80) != 0) {
                                    DamageEnemy(nTarget, nSprite, 5);
                                }
                                return;
                            }
                            MummyList[nMummy].nState = 1;
                        }
                        MummyList[nMummy].nSeq = 0;
                        return;
                    case 3:
                        if (v18 != 0) {
                            MummyList[nMummy].nSeq = 0;
                            MummyList[nMummy].nState = 0;
                            MummyList[nMummy].field_C = 100;
                            MummyList[nMummy].nTarget = -1;
                        } else if ((flags & 0x80) != 0) {
                            SetQuake(nSprite, 100);
                            BulletStruct.BulletResult nBullet = BuildBullet(nSprite, 9, -15360, pSprite.getAng(), nTarget + 10000, 1);
                            CheckMummyRevive(nMummy);
                            if (nBullet.hasResult() && RandomSize(3) == 0) {
                                SetBulletEnemy(nBullet.getBullet(), nTarget);
                                Sprite spr = boardService.getSprite(nBullet.getSpriteIndex());
                                if (spr != null) {
                                    spr.setPal(5);
                                }
                            }
                        }
                        return;
                    case 4:
                        if (v18 != 0) {
                            MummyList[nMummy].nSeq = 0;
                            MummyList[nMummy].nState = 5;
                        }
                        return;
                    case 5:
                        MummyList[nMummy].nSeq = 0;
                        return;
                    case 6:
                        if (v18 != 0) {
                            MummyList[nMummy].nState = 0;
                            pSprite.setCstat(257);
                            MummyList[nMummy].nHealth = 300;
                            MummyList[nMummy].nTarget = -1;
                            nCreaturesLeft++;
                        }
                        return;
                    case 7:
                        if ((v17 & nEventProcess) != 0) {
                            pSprite.setXvel(pSprite.getXvel() >> 1);
                            pSprite.setYvel(pSprite.getYvel() >> 1);
                        }

                        if (v18 != 0) {
                            pSprite.setXvel(0);
                            pSprite.setYvel(0);
                            MummyList[nMummy].nState = 0;
                            MummyList[nMummy].nSeq = 0;
                            pSprite.setCstat(257);
                            MummyList[nMummy].nTarget = -1;
                        }
                        return;
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(plr);
                PlotSequence(tsp, ActionSeq_X_4[nState][0] + SeqOffsets[10], MummyList[nMummy].nSeq,
                        ActionSeq_X_4[nState][1]);
                return;
            case nEventRadialDamage:
                if (MummyList[nMummy].nHealth <= 0) {
                    return;
                }

                damage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (damage != 0) {
                    if (MummyList[nMummy].nHealth > 0) {
                        MummyList[nMummy].nHealth -= damage;
                        if (MummyList[nMummy].nHealth > 0) {
                            if (RandomSize(2) == 0) {
                                MummyList[nMummy].nState = 7;
                                MummyList[nMummy].nSeq = 0;
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                            }
                        } else {
                            MummyList[nMummy].nHealth = 0;
                            pSprite.setCstat(pSprite.getCstat() & 0xFEFE);
                            DropMagic(nSprite);
                            MummyList[nMummy].nSeq = 0;
                            MummyList[nMummy].nState = 4;
                            pSprite.setZvel(0);
                            pSprite.setYvel(0);
                            pSprite.setXvel(0);
                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null) {
                                pSprite.setZ(sec.getFloorz());
                            }
                            nCreaturesLeft--;
                        }
                    }
                }
        }
    }

    private static void CheckMummyRevive(int nMummy) {
        Sprite pSprite = boardService.getSprite(MummyList[nMummy].nSprite);
        if (pSprite == null) {
            return;
        }

        for (int i = 0; i < MummyCount; i++) {
            EnemyStruct pOther = MummyList[i];
            if (i != nMummy) {
                Sprite v3 = boardService.getSprite(pOther.nSprite);
                if (v3 != null && v3.getStatnum() == 102 && pOther.nState == 5) {
                    if (klabs(v3.getX() - pSprite.getX()) >> 8 <= 20 && klabs(v3.getY() - pSprite.getY()) >> 8 <= 20) {
                        if (engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ() - 0x2000, pSprite.getSectnum(), v3.getX(), v3.getY(),
                                v3.getZ() - 0x2000, v3.getSectnum())) {
                            v3.setCstat(0);
                            MummyList[i].nState = 6;
                            MummyList[i].nSeq = 0;
                        }
                    }
                }
            }
        }
    }
}
