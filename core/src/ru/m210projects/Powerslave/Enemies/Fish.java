// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Enemies.Enemy.EnemyStruct;
import ru.m210projects.Powerslave.Type.FishChunk;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Enemies.Enemy.FindPlayer;
import static ru.m210projects.Powerslave.Enemies.Enemy.PlotCourseToSprite;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.PlayFXAtXYZ;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Fish {

    public static final int MAX_FISHS = 128;
    private static final int[][] ActionSeq_X_5 = new int[][]{{8, 0}, {8, 0}, {0, 0}, {24, 0}, {8, 0}, {32, 1}, {33, 1}, {34, 1}, {35, 1}, {39, 1},};
    private static int FishCount;
    //	private static int FishSprite;
    private static int nChunksFree;
    private static final int[] nFreeChunk = new int[MAX_FISHS];
    private static final FishChunk[] FishChunk = new FishChunk[MAX_FISHS];
    private static final EnemyStruct[] FishList = new EnemyStruct[MAX_FISHS];

    public static void InitFishes() {
        FishCount = 0;
//		FishSprite = 1;
        nChunksFree = MAX_FISHS;
        for (int i = 0; i < MAX_FISHS; i++) {
            nFreeChunk[i] = i;
        }

        for (int i = 0; i < MAX_FISHS; i++) {
            if (FishChunk[i] == null) {
                FishChunk[i] = new FishChunk();
            }
        }
    }

    public static void saveFish(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  FishCount);
        StreamUtils.writeByte(os, (byte) nChunksFree);
        for (int i = 0; i < MAX_FISHS; i++) {
            StreamUtils.writeShort(os,  nFreeChunk[i]);
            FishChunk[i].save(os);
        }

        for (int i = 0; i < FishCount; i++) {
            FishList[i].save(os);
        }
    }

    public static void loadFish(SafeLoader loader) {
        FishCount = loader.FishCount;
        nChunksFree = loader.nChunksFree;
        for (int i = 0; i < MAX_FISHS; i++) {
            nFreeChunk[i] = loader.nFreeChunk[i];
            if (FishChunk[i] == null) {
                FishChunk[i] = new FishChunk();
            }
            FishChunk[i].copy(loader.FishChunk[i]);
        }

        for (int i = 0; i < loader.FishCount; i++) {
            if (FishList[i] == null) {
                FishList[i] = new EnemyStruct();
            }
            FishList[i].copy(loader.FishList[i]);
        }

    }

    public static void loadFish(SafeLoader loader, InputStream is) throws IOException {

        loader.FishCount = StreamUtils.readShort(is);
        loader.nChunksFree = StreamUtils.readUnsignedByte(is);
        for (int i = 0; i < MAX_FISHS; i++) {
            loader.nFreeChunk[i] = StreamUtils.readShort(is);
            if (loader.FishChunk[i] == null) {
                loader.FishChunk[i] = new FishChunk();
            }
            loader.FishChunk[i].load(is);
        }
        for (int i = 0; i < loader.FishCount; i++) {
            if (loader.FishList[i] == null) {
                loader.FishList[i] = new EnemyStruct();
            }
            loader.FishList[i].load(is);
        }
    }

    public static void BuildBlood(int a1, int a2, int a3, int a4) {
        BuildAnim(-1, 19, 36, a1, a2, a3, a4, 0x4B, -128);
    }

    public static void BuildFishLimb(int a1, int a2) {
        if (nChunksFree > 0) {
            final int nSprite = FishList[a1].nSprite;
            Sprite spr = boardService.getSprite(nSprite);
            if (spr == null) {
                return;
            }

            int nChunk = nFreeChunk[nChunksFree-- - 1];
            FishChunk pChunk = FishChunk[nChunk];

            int j = engine.insertsprite(spr.getSectnum(),  99);
            Sprite pSprite = boardService.getSprite(j);
            if (pSprite == null ) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }

            pChunk.nSprite = j;
            pChunk.ActionSeq =  (a2 + 40);
            pChunk.nSeq =  (RandomSize(3) % SeqSize[SeqOffsets[19] + pChunk.ActionSeq]);

            pSprite.setX(spr.getX());
            pSprite.setY(spr.getY());
            pSprite.setZ(spr.getZ());
            pSprite.setCstat(0);
            pSprite.setShade(-12);
            pSprite.setPal(0);
            pSprite.setXvel( (((RandomSize(5) & 0xFFFF) - 16) << 8));
            pSprite.setYvel( (((RandomSize(5) & 0xFFFF) - 16) << 8));
            pSprite.setXrepeat(64);
            pSprite.setYrepeat(64);
            pSprite.setXoffset(0);
            pSprite.setYoffset(0);
            pSprite.setZvel( (-2 * (RandomByte() + 512)));
            a2 = GetSeqPicnum(19, pChunk.ActionSeq, 0); // TODO 22.04.2024 how a2 sets
            pSprite.setPicnum(a2);
            pSprite.setLotag( (HeadRun() + 1));
            pSprite.setClipdist(0);
            pSprite.setExtra(-1);
            pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent32 | nChunk));
            pSprite.setHitag( AddRunRec(NewRun, nEvent32 | nChunk));
        }
    }

    public static void BuildFish(int i, int x, int y, int z, int sectnum, int ang) {
        int count = FishCount++;

        if (count >= MAX_FISHS) {
            return;
        }

        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            i = engine.insertsprite( sectnum,  103);
            spr = boardService.getSprite(i);
            if (spr == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = spr.getX();
            y = spr.getY();
            z = spr.getZ();
            ang = spr.getAng();
            engine.changespritestat( i,  103);
        }

        spr.setX(x);
        spr.setY(y);
        spr.setZ(z);
        spr.setShade(-12);
        spr.setCstat(257);
        spr.setClipdist(80);
        spr.setXrepeat(40);
        spr.setYrepeat(40);
        Sector sec = boardService.getSector(spr.getSectnum());
        spr.setPal(sec != null ? sec.getCeilingpal() : 0);
        spr.setXoffset(0);
        spr.setYoffset(0);
        spr.setPicnum(GetSeqPicnum(19, ActionSeq_X_5[0][0], 0));
        spr.setAng( ang);
        spr.setXvel(0);
        spr.setYvel(0);
        spr.setZvel(0);
        spr.setHitag(0);
        spr.setLotag( (HeadRun() + 1));
        spr.setExtra(-1);

        if (FishList[count] == null) {
            FishList[count] = new EnemyStruct();
        }

        FishList[count].nState = 0;
        FishList[count].nHealth = 200;
        FishList[count].nSprite =  i;
        FishList[count].nTarget = -1;
        FishList[count].field_C = 60;
        FishList[count].nSeq = 0;

        spr.setOwner( AddRunRec(spr.getLotag() - 1, nEvent18 | count));
        FishList[count].nFunc =  AddRunRec(NewRun, nEvent18 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void IdleFish(int nFish, int a2) {
        int nSprite = FishList[nFish].nSprite;
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return;
        }

        spr.setAng(spr.getAng() + (256 - RandomSize(9)) + 1024);
        spr.setAng(spr.getAng() & 0x7FF);

        spr.setXvel( (EngineUtils.sin((spr.getAng() + 512) & 0x7FF) >> 8));
        spr.setYvel( (EngineUtils.sin(spr.getAng() & 0x7FF) >> 8));

        FishList[nFish].nState = 0;
        FishList[nFish].nSeq = 0;
        spr.setZvel( RandomSize(9));

        if (a2 > 0) {
            return;
        }
        if (a2 == 0 && RandomBit() == 0) {
            return;
        }

        spr.setZvel( -spr.getZvel());
    }

    private static void DestroyFish(int nFish) {
        Sprite spr = boardService.getSprite(FishList[nFish].nSprite);
        if (spr == null) {
            return;
        }

        DoSubRunRec(spr.getOwner());
        FreeRun(spr.getLotag() - 1);
        SubRunRec(FishList[nFish].nFunc);
        engine.mydeletesprite(FishList[nFish].nSprite);
    }

    public static void FuncFishLimb(int nStack, int ignored, int RunPtr) {
        int nChunk = RunData[RunPtr].getObject();
        int nSprite = FishChunk[nChunk].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v6 = SeqOffsets[19] + FishChunk[nChunk].ActionSeq;
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                pSprite.setPicnum( GetSeqPicnum2(v6, FishChunk[nChunk].nSeq));
                Gravity(nSprite);
                FishChunk[nChunk].nSeq++;
                if (FishChunk[nChunk].nSeq >= SeqSize[v6]) {
                    FishChunk[nChunk].nSeq = 0;
                    if (RandomBit() != 0) {
                        BuildBlood(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum());
                    }
                }

                Sector sec = boardService.getSector(pSprite.getSectnum());
                if (sec == null) {
                    return;
                }

                if (sec.getFloorz() > pSprite.getZ()) {
                    if (engine.movesprite(nSprite, pSprite.getXvel() << 8, pSprite.getYvel() << 8, pSprite.getZvel(), 2560, -2560, 1) != 0) {
                        pSprite.setXvel(0);
                        pSprite.setYvel(0);
                    }
                } else {
                    pSprite.setZ(pSprite.getZ() + 256);
                    int dz = pSprite.getZ() - sec.getFloorz();
                    if (dz <= 25600) {
                        if (dz > 0) {
                            pSprite.setZvel(1024);
                        }
                    } else {
                        pSprite.setZvel(0);
                        DoSubRunRec(pSprite.getOwner());
                        FreeRun(pSprite.getLotag() - 1);
                        SubRunRec(pSprite.getHitag());
                        engine.mydeletesprite(nSprite);
                    }
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, v6, FishChunk[nChunk].nSeq, 1);
        }
    }

    public static void FuncFish(int nStack, int a2, int RunPtr) {
        int nFish = RunData[RunPtr].getObject();
        if (nFish < 0 || nFish >= MAX_FISHS) {
            throw new AssertException("Fish>=0 && Fish<MAX_FISHS");
        }

        final int nSprite = FishList[nFish].nSprite;
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return;
        }

        int nState = FishList[nFish].nState;
        short plr = (short) (nStack & 0xFFFF);
        int damage = a2;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if ((SectFlag[spr.getSectnum()] & 0x2000) == 0) {
                    Gravity(nSprite);
                }

                int v10 = ActionSeq_X_5[nState][0] + SeqOffsets[19];
                spr.setPicnum( GetSeqPicnum2(v10, FishList[nFish].nSeq));
                MoveSequence(nSprite, v10, FishList[nFish].nSeq);
                FishList[nFish].nSeq++;
                if (FishList[nFish].nSeq >= SeqSize[v10]) {
                    FishList[nFish].nSeq = 0;
                }

                int nTarget = FishList[nFish].nTarget;
                Sprite pTarget = boardService.getSprite(nTarget);
                switch (nState) {
                    case 1:
                    default:
                        return;
                    case 4:
                        if (FishList[nFish].nSeq == 0) {
                            IdleFish(nFish, 0);
                        }
                        return;
                    case 9:
                        if (FishList[nFish].nSeq == 0) {
                            DestroyFish(nFish);
                        }
                        return;
                    case 2:
                    case 3:
                        FishList[nFish].field_C--;
                        if (FishList[nFish].field_C <= 0) {
                            IdleFish(nFish, 0);
                            return;
                        }

                        if (pTarget == null) {
                            break;
                        }

                        PlotCourseToSprite(nSprite, nTarget);
                        int dz = pTarget.getZ() - spr.getZ();
                        if (klabs(dz) <= GetSpriteHeight(nSprite) >> 1) {
                            int cos = EngineUtils.sin((spr.getAng() + 512) & 0x7FF);
                            int sin = EngineUtils.sin(spr.getAng() & 0x7FF);
                            spr.setXvel( ((cos >> 5) - (cos >> 7)));
                            spr.setYvel( ((sin >> 5) - (sin >> 7)));
                        } else {
                            spr.setXvel(0);
                            spr.setYvel(0);
                        }
                        spr.setZvel( (dz >> 3));
                        break;
                    case 0:
                        FishList[nFish].field_C--;
                        if (FishList[nFish].field_C <= 0) {
                            nTarget = FindPlayer(nSprite, 60);
                            pTarget = boardService.getSprite(nTarget);
                            if (pTarget == null) {
                                IdleFish(nFish, 0);
                            } else {
                                FishList[nFish].nTarget =  nTarget;
                                FishList[nFish].nState = 2;
                                FishList[nFish].nSeq = 0;
                                spr.setZvel( (EngineUtils.sin(engine.GetMyAngle(pTarget.getX() - spr.getX(), pTarget.getZ() - spr.getZ()) & 0x7FF) >> 5));
                                FishList[nFish].field_C =  (RandomSize(6) + 90);
                            }
                        }
                        break;
                }
                int ox = spr.getX();
                int oy = spr.getY();
                int oz = spr.getZ();
                short osect = spr.getSectnum();
                int hitMove = engine.movesprite( nSprite, spr.getXvel() << 13, spr.getYvel() << 13, 4 * spr.getZvel(), 0, 0, 0);

                int v18;
                if ((SectFlag[spr.getSectnum()] & 0x2000) != 0) {

                    if (hitMove == 0) {
                        if (nState == 3) {
                            FishList[nFish].nState = 2;
                            FishList[nFish].nSeq = 0;
                        }
                        return;
                    }

                    if ((hitMove & nEvent3) == 0) {
                        switch (hitMove & PS_HIT_TYPE_MASK) {
                            default:
                                return;
                            case PS_HIT_SPRITE:
                                int nHitObject = hitMove & PS_HIT_INDEX_MASK;
                                pTarget = boardService.getSprite(nHitObject);
                                if (pTarget != null && pTarget.getStatnum() == 100) {
                                    nTarget = FishList[nFish].nTarget = nHitObject;
                                    spr.setAng(engine.GetMyAngle(pTarget.getX() - spr.getX(), pTarget.getY() - spr.getY()));

                                    if (nState != 3) {
                                        FishList[nFish].nState = 3;
                                        FishList[nFish].nSeq = 0;
                                    }
                                    if (FishList[nFish].nSeq == 0) {
                                        DamageEnemy(nTarget, nSprite, 2);
                                    }
                                }
                                return;
                            case PS_HIT_WALL:
                                IdleFish(nFish, 0);
                                return;
                        }
                    }
                    if ((hitMove & nEventProcess) != 0) {
                        v18 = -1;
                    } else {
                        v18 = 1;
                    }
                } else {
                    engine.mychangespritesect( nSprite, osect);
                    spr.setX(ox);
                    spr.setY(oy);
                    spr.setZ(oz);
                    v18 = 0;
                }
                IdleFish(nFish, v18);
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(plr);
                PlotSequence(tsp, ActionSeq_X_5[FishList[nFish].nState][0] + SeqOffsets[19], FishList[nFish].nSeq, ActionSeq_X_5[FishList[nFish].nState][1]);
                return;
            case nEventRadialDamage:
                if (FishList[nFish].nHealth <= 0) {
                    return;
                }
                damage = CheckRadialDamage(nSprite);
                if (damage != 0) {
                    FishList[nFish].field_C = 10;
                }
            case nEventDamage:
                if (damage != 0) {
                    if (FishList[nFish].nHealth > 0) {
                        FishList[nFish].nHealth -= damage;
                        if (FishList[nFish].nHealth > 0) {
                            Sprite plrSpr = boardService.getSprite(plr);
                            if (plrSpr != null && plrSpr.getStatnum() < 199) {
                                FishList[nFish].nTarget = plr;
                            }
                            FishList[nFish].nSeq = 0;
                            FishList[nFish].nState = 4;
                            FishList[nFish].field_C += 10;
                        } else {
                            FishList[nFish].nHealth = 0;
                            spr.setCstat(spr.getCstat() & 0xFEFE);
                            if ((nStack & EVENT_MASK) != nEventDamage) {
                                FishList[nFish].nState = 9;
                                FishList[nFish].nSeq = 0;
                                return;
                            }
                            for (int i = 0; i < 3; i++) {
                                BuildFishLimb(nFish, i);
                            }
                            PlayFXAtXYZ(StaticSound[40], spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());
                            DestroyFish(nFish);

                            nCreaturesLeft--;
                        }
                    }
                }
        }
    }
}
