// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Enemies.Enemy.EnemyStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Enemies.Enemy.FindPlayer;
import static ru.m210projects.Powerslave.Enemies.Enemy.PlotCourseToSprite;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.AngleDiff;

public class LavaDude {

    public static final int MAX_LAVAS = 20;
    private static final short[][] ActionSeq_X_6 = new short[][]{{0, 1}, {0, 1}, {1, 0}, {10, 0}, {19, 0},
            {28, 1}, {29, 1}, {33, 0}, {42, 1},};
    private static final EnemyStruct[] LavaList = new EnemyStruct[MAX_LAVAS];
    //	private static int LavaSprite;
    private static int LavaCount;

    public static void InitLava() {
        LavaCount = 0;
//		LavaSprite = 1;	
    }

    public static void saveLava(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, LavaCount);
        for (int i = 0; i < LavaCount; i++) {
            LavaList[i].save(os);
        }
    }

    public static void loadLava(SafeLoader loader, InputStream is) throws IOException {
        loader.LavaCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.LavaCount; i++) {
            if (loader.LavaList[i] == null) {
                loader.LavaList[i] = new EnemyStruct();
            }
            loader.LavaList[i].load(is);
        }
    }

    public static void loadLava(SafeLoader loader) {
        LavaCount = loader.LavaCount;
        for (int i = 0; i < loader.LavaCount; i++) {
            if (LavaList[i] == null) {
                LavaList[i] = new EnemyStruct();
            }
            LavaList[i].copy(loader.LavaList[i]);
        }
    }

    public static void BuildLava(int spr, int x, int y, int sectnum, int ang, int channel) {
        int count = LavaCount++;
        if (count >= 20) {
            return;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite(sectnum, 118);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
            sectnum = pSprite.getSectnum();
            ang = pSprite.getAng();
            engine.changespritestat(spr, 118);
        }

        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(sec.getFloorz());
        pSprite.setCstat(32768);
        pSprite.setXoffset(0);
        pSprite.setShade(-12);
        pSprite.setYoffset(0);
        pSprite.setPicnum(GetSeqPicnum(42, ActionSeq_X_6[3][0], 0));
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setClipdist(127);
        pSprite.setAng(ang);
        pSprite.setXrepeat(200);
        pSprite.setYrepeat(200);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setHitag(0);
        pSprite.setLotag((HeadRun() + 1));
        pSprite.setExtra(-1);

        if (LavaList[count] == null) {
            LavaList[count] = new EnemyStruct();
        }

        LavaList[count].nState = 0;
        LavaList[count].nHealth = 4000;
        LavaList[count].nSprite = spr;
        LavaList[count].nTarget = -1;
        LavaList[count].field_C = channel;
        LavaList[count].nSeq = 0;
        pSprite.setOwner(AddRunRec(pSprite.getLotag() - 1, nEvent21 | count));
        LavaList[count].nFunc = AddRunRec(NewRun, nEvent21 | count);
        nCreaturesLeft++;
        nCreaturesMax++;
    }

    public static void FuncLava(int nStack, int nDamage, int RunPtr) {
        int nLava = RunData[RunPtr].getObject();
        if (nLava < 0 || nLava >= MAX_LAVAS) {
            throw new AssertException("Lava>=0 && Lava<MAX_LAVAS");
        }

        EnemyStruct pLava = LavaList[nLava];
        int nSprite = pLava.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nState = pLava.nState;
        int nTarget = pLava.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);
        short nObject = (short) (nStack & 0xFFFF);
        int v57 = 0;
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int nSeq = ActionSeq_X_6[nState][0] + SeqOffsets[42];
                pSprite.setPicnum(GetSeqPicnum2(nSeq, pLava.nSeq));
                if (nState != 0) {
                    MoveSequence(nSprite, nSeq, pLava.nSeq);
                    if (++pLava.nSeq >= SeqSize[nSeq]) {
                        pLava.nSeq = 0;
                        v57 = 1;
                    }
                }
                int nFlags = FrameFlag[pLava.nSeq + SeqBase[nSeq]];
                if (pTarget != null && nState < 4) {
                    if ((pTarget.getCstat() & 0x101) == 0 || pTarget.getSectnum() >= 1024) {
                        nTarget = -1;
                        pTarget = null;
                        pLava.nTarget = -1;
                    }
                }

                switch (nState) {
                    case 0:
                        if ((nLava & 0x1F) == (totalmoves & 0x1F)) {
                            if (nTarget < 0) {
                                nTarget = FindPlayer(nSprite, 76800);
                                pTarget = boardService.getSprite(nTarget);
                            }
                            PlotCourseToSprite(nSprite, nTarget);
                            pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));

                            if (pTarget != null && RandomSize(1) == 0) {
                                pLava.nTarget = nTarget;
                                pLava.nState = 2;
                                pSprite.setCstat(257);
                                pLava.nSeq = 0;
                                break;
                            }
                        }

                        int sx = pSprite.getX();
                        int sy = pSprite.getY();
                        int sz = pSprite.getZ();
                        short ssec = pSprite.getSectnum();
                        int hitMove = engine.movesprite(nSprite, pSprite.getXvel() << 8, pSprite.getYvel() << 8, 0, 0, 0, 0);
                        if (ssec == pSprite.getSectnum()) {
                            if (hitMove == 0) {
                                break;
                            }

                            switch (hitMove & PS_HIT_TYPE_MASK) {
                                case PS_HIT_WALL:
                                    pSprite.setAng(((pSprite.getAng() + ((RandomWord() & 0x300) + 1024)) & 0x7FF));
                                    pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                                    pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                                    pSprite.setPal(1);
                                    return;
                                case PS_HIT_SPRITE:
                                    if ((hitMove & PS_HIT_INDEX_MASK) == nTarget) {
                                        if (pTarget != null && AngleDiff(pSprite.getAng(), EngineUtils.getAngle(pTarget.getX() - pSprite.getX(),
                                                pTarget.getY() - pSprite.getY())) < 64) {
                                            pLava.nState = 2;
                                            pSprite.setCstat(257);
                                            pLava.nSeq = 0;
                                        }
                                    }
                                    pSprite.setPal(1);
                                    return;
                            }
                        } else {
                            engine.changespritesect(nSprite, ssec);
                            pSprite.setX(sx);
                            pSprite.setY(sy);
                            pSprite.setZ(sz);
                        }

                        pSprite.setAng(((pSprite.getAng() + ((RandomWord() & 0x300) + 1024)) & 0x7FF));
                        pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                        pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                        break;
                    case 2:
                        if (v57 != 0) {
                            pLava.nState = 3;
                            pLava.nSeq = 0;
                            PlotCourseToSprite(nSprite, nTarget);
                            pSprite.setCstat(pSprite.getCstat() | 0x101);
                        }
                        break;
                    case 3:
                        if ((nFlags & 0x80) != 0 && pTarget != null) {
                            // GetUpAngle(nSprite, -64000, nTarget, -(GetSpriteHeight(nSprite) >> 1));
                            BuildBullet(nSprite, 10, -1, pSprite.getAng(), nTarget + 10000, 1);
                        } else if (v57 != 0) {
                            PlotCourseToSprite(nSprite, nTarget);
                            pLava.nState = 7;
                            pLava.nSeq = 0;
                        }
                        break;
                    case 5:
                        if ((nFlags & 0x40) != 0) {
                            D3PlayFX(StaticSound[26], BuildLavaLimb(nSprite, pLava.nSeq, 64000));
                        }
                        if (pLava.nSeq != 0) {
                            if ((nFlags & 0x80) != 0) {
                                for (int i = 0; i < 20; i++) {
                                    BuildLavaLimb(nSprite, i, 64000);
                                }
                                ChangeChannel(pLava.field_C, 1);
                            }
                        } else {
                            for (int i = 0; i < 20; i++) {
                                BuildLavaLimb(nSprite, i, 256);
                            }
                            DoSubRunRec(pSprite.getOwner());
                            FreeRun(pSprite.getLotag() - 1);
                            SubRunRec(pLava.nFunc);
                            engine.mydeletesprite(nSprite);
                        }
                        break;
                    case 4:
                        if (v57 != 0) {
                            pLava.nState = 7;
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                        }
                        break;
                    case 7:
                        if (v57 != 0) {
                            pLava.nState = 8;
                        }
                        break;
                    case 8:
                        if (v57 != 0) {
                            pLava.nState = 0;
                            pSprite.setCstat(32768);
                        }
                        break;
                }

                pSprite.setPal(1);
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                PlotSequence(tsp, ActionSeq_X_6[nState][0] + SeqOffsets[42], pLava.nSeq, ActionSeq_X_6[nState][1]);
                tsp.setOwner(-1);
                return;
            case nEventDamage:
                if (nDamage != 0) {
                    if (pLava.nHealth > 0) {
                        pLava.nHealth -= nDamage;
                        if (pLava.nHealth <= 0) {
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            pLava.nHealth = 0;
                            pLava.nState = 5;
                            pLava.nSeq = 0;
                            nCreaturesLeft--;
                        } else {
                            if (pTarget != null && pTarget.getStatnum() < 199) {
                                pLava.nTarget = nTarget;
                            }
                            if (nState == 3 && RandomSize(2) == 0) {
                                pLava.nState = 4;
                                pLava.nSeq = 0;
                                pSprite.setCstat(0);
                            }
                            BuildLavaLimb(nSprite, totalmoves, 64000);
                        }
                    }
                }
        }
    }

    public static int BuildLavaLimb(int nSprite, int a2, int a3) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return -1;
        }

        int spr = engine.insertsprite(pSprite.getSectnum(), 118);
        Sprite lavaSprite = boardService.getSprite(spr);
        if (lavaSprite == null) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
        }


        lavaSprite.setX(pSprite.getX());
        lavaSprite.setY(pSprite.getY());
        lavaSprite.setZ(pSprite.getZ() - RandomLong() % a3);
        lavaSprite.setCstat(0);
        lavaSprite.setShade(-127);
        lavaSprite.setPal(1);
        lavaSprite.setXvel(((RandomSize(5) - 16) << 8));
        lavaSprite.setYvel(((RandomSize(5) - 16) << 8));
        lavaSprite.setZvel((2560 - (RandomSize(5) << 8)));
        lavaSprite.setYoffset(0);
        lavaSprite.setXoffset(0);
        lavaSprite.setXrepeat(90);
        lavaSprite.setYrepeat(90);
        lavaSprite.setPicnum(((a2 & 3) % 3));
        lavaSprite.setClipdist(0);
        lavaSprite.setExtra(-1);
        lavaSprite.setLotag((HeadRun() + 1));
        lavaSprite.setOwner(AddRunRec(lavaSprite.getLotag() - 1, nEvent22 | spr));
        lavaSprite.setHitag(AddRunRec(NewRun, nEvent22 | spr));

        return spr;
    }

    public static void FuncLavaLimb(int nStack, int ignored, int RunPtr) {
        int spr = RunData[RunPtr].getObject();
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
        }

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                pSprite.setShade(pSprite.getShade() + 3);
                if (engine.movesprite(spr, pSprite.getXvel() << 12, pSprite.getYvel() << 12, pSprite.getZvel(), 2560, -2560,
                        1) != 0 || (pSprite.getShade() > 100)) {
                    pSprite.setXvel(0);
                    pSprite.setYvel(0);
                    pSprite.setZvel(0);

                    if (pSprite.getOwner() != -1) {
                        System.out.println("Bad sprite " + pSprite);
                        DoSubRunRec(pSprite.getOwner());
                    }
                    FreeRun(pSprite.getLotag() - 1);
                    SubRunRec(pSprite.getHitag());
                    engine.mydeletesprite(spr);
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, pSprite.getPicnum() + SeqOffsets[42] + 30, 0, 1);
                break;
        }
    }
}
