// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Enemies.Enemy.EnemyStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Powerslave.Enemies.Enemy.FindPlayer;
import static ru.m210projects.Powerslave.Enemies.Enemy.PlotCourseToSprite;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class Spider {

    public static final int MAX_SPIDERS = 100;

    private static final short[][] ActionSeq_X_2 = new short[][]{{16, 0}, {8, 0}, {32, 0}, {24, 0}, {0, 0},
            {40, 1}, {41, 1} };

    public static final EnemyStruct[] SpiderList = new EnemyStruct[MAX_SPIDERS];

    private static int SpiderCount;
//	private static int SpiderSprite;

    public static void InitSpider() {
        SpiderCount = 0;
//		SpiderSprite = 1;
    }

    public static void saveSpider(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  SpiderCount);
        for (int i = 0; i < SpiderCount; i++) {
            SpiderList[i].save(os);
        }
    }

    public static void loadSpider(SafeLoader loader) {
        SpiderCount = loader.SpiderCount;
        for (int i = 0; i < loader.SpiderCount; i++) {
            if (SpiderList[i] == null) {
                SpiderList[i] = new EnemyStruct();
            }
            SpiderList[i].copy(loader.SpiderList[i]);
        }
    }

    public static void loadSpider(SafeLoader loader, InputStream is) throws IOException {
        loader.SpiderCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.SpiderCount; i++) {
            if (loader.SpiderList[i] == null) {
                loader.SpiderList[i] = new EnemyStruct();
            }
            loader.SpiderList[i].load(is);
        }
    }

    public static int BuildSpider(int spr, int x, int y, int z, int sectnum, int ang) {
        int count = SpiderCount++;

        if (count >= MAX_SPIDERS) {
            return -1;
        }

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            spr = engine.insertsprite( sectnum,  99);
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }
        } else {
            y = pSprite.getY();
            z = pSprite.getZ();
            x = pSprite.getX();
            ang = pSprite.getAng();
            engine.changespritestat( spr,  99);
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return -1;
        }

        pSprite.setX(x);
        pSprite.setY(y);
        pSprite.setZ(z);
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setClipdist(15);
        pSprite.setXvel(0);
        pSprite.setYvel(0);

        pSprite.setZvel(0);
        pSprite.setXrepeat(40);
        pSprite.setYrepeat(40);
        pSprite.setPal(sec.getCeilingpal());
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setAng( ang);
        pSprite.setPicnum(1);

        pSprite.setHitag(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setExtra(-1);

        if (SpiderList[count] == null) {
            SpiderList[count] = new EnemyStruct();
        }

        SpiderList[count].nHealth = 160;
        SpiderList[count].nSeq = 0;
        SpiderList[count].nState = 0;
        SpiderList[count].nSprite =  spr;
        SpiderList[count].nTarget = -1;

        pSprite.setOwner(AddRunRec(pSprite.getLotag() - 1, nEvent12 | count));
        SpiderList[count].nFunc =  AddRunRec(NewRun, nEvent12 | count);
        nCreaturesLeft++;
        nCreaturesMax++;

        return spr;
    }

    public static void FuncSpider(int nStack, int nDamage, int RunPtr) {
        int nSpider = RunData[RunPtr].getObject();
        if (nSpider < 0 || nSpider >= MAX_SPIDERS) {
            throw new AssertException("nSpider>=0 && nSpider<MAX_SPIDERS");
        }

        EnemyStruct pSpider = SpiderList[nSpider];
        int nSprite = pSpider.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return;
        }

        int nState = pSpider.nState;
        int nTarget = pSpider.nTarget;
        Sprite pTarget = boardService.getSprite(nTarget);

        short plr = (short) (nStack & 0xFFFF);
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int vel = 6;
                if (pSpider.nHealth != 0) {
                    if ((pSprite.getCstat() & 8) != 0) {
                        game.pInt.setsprinterpolate(nSprite, pSprite);
                        pSprite.setZ(GetSpriteHeight(nSprite) + sec.getCeilingz());
                    } else {
                        Gravity(nSprite);
                    }
                }

                int nSeq = ActionSeq_X_2[nState][0] + SeqOffsets[16];
                pSprite.setPicnum( GetSeqPicnum2(nSeq, pSpider.nSeq));
                MoveSequence(nSprite, nSeq, pSpider.nSeq);
                if (++pSpider.nSeq >= SeqSize[nSeq]) {
                    pSpider.nSeq = 0;
                }
                int nFlags = FrameFlag[pSpider.nSeq + SeqBase[nSeq]];

                if (pTarget == null || (pTarget.getCstat() & 0x101) != 0) {
                    switch (nState) {
                        case 0:
                            if ((nSpider & 0x1F) != (totalmoves & 0x1F)) {
                                break;
                            }

                            if (nTarget < 0) {
                                nTarget = FindPlayer(nSprite, 100);
                                pTarget = boardService.getSprite(nTarget);
                            }

                            if (pTarget == null) {
                                break;
                            }

                            pSpider.nState = 1;
                            pSpider.nSeq = 0;
                            pSpider.nTarget = nTarget;
                            pSprite.setXvel(EngineUtils.cos(pSprite.getAng() & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                            return;
                        case 1:
                            if (nTarget >= 0) {
                                vel = 7;
                            }
                        case 3:
                        case 4:
                            if (nState == 4 && pSpider.nSeq == 0) {
                                pSpider.nState = 1;
                            }
                            if ((pSprite.getCstat() & 8) != 0) {
                                pSprite.setZvel(0);
                                pSprite.setZ(32 * engine.getTile(pSprite.getPicnum()).getHeight() + sec.getCeilingz());
                                if ((sec.getCeilingstat() & 1) != 0) {
                                    pSprite.setCstat(pSprite.getCstat() ^ 8);
                                    pSprite.setZvel(1);
                                    pSpider.nState = 3;
                                    pSpider.nSeq = 0;
                                }
                            }
                            if ((totalmoves & 0x1F) == (nSpider & 0x1F)) {
                                PlotCourseToSprite(nSprite, nTarget);
                                if (RandomSize(3) != 0) {
                                    pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                                    pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                                } else {
                                    pSprite.setXvel(0);
                                    pSprite.setYvel(0);
                                }

                                if (nState == 1 && RandomBit() != 0) {
                                    if ((pSprite.getCstat() & 8) != 0) {
                                        pSprite.setCstat(pSprite.getCstat() ^ 8);
                                        pSprite.setZvel(1);
                                        pSprite.setZ(GetSpriteHeight(nSprite) + sec.getCeilingz());
                                    } else {
                                        pSprite.setZvel(-5120);
                                    }

                                    pSpider.nState = 3;
                                    pSpider.nSeq = 0;
                                    if (RandomSize(3) == 0) {
                                        D3PlayFX(StaticSound[29], nSprite);
                                    }
                                }
                            }
                            break;
                        case 2:
                            if (nTarget == -1) {
                                pSpider.nSeq = 0;
                                pSpider.nState = 0;
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                                break;
                            }
                            if ((nFlags & 0x80) != 0) {
                                DamageEnemy(nTarget, nSprite, 3);
                                D3PlayFX(StaticSound[38], nSprite);
                            }
                            if (PlotCourseToSprite(nSprite, nTarget) < 1024) {
                                return;
                            }

                            pSpider.nSeq = 0;
                            pSpider.nState = 1;
                            break;
                        case 5:
                            if (pSpider.nSeq == 0) {
                                DoSubRunRec(pSprite.getOwner());
                                FreeRun(pSprite.getLotag() - 1);
                                SubRunRec(pSpider.nFunc);
                                pSprite.setCstat( 32768);
                                engine.mydeletesprite( nSprite);
                            }
                            return;
                        default:
                            return;
                    }
                } else {
                    pSpider.nTarget = -1;
                    pSpider.nState = 0;
                    pSprite.setYvel(0);
                    pSprite.setXvel(0);
                    pSpider.nSeq = 0;
                }

                int osect = pSprite.getSectnum();
                int oz = pSprite.getZ();
                int hitMove = engine.movesprite( nSprite, pSprite.getXvel() << vel, pSprite.getYvel() << vel, pSprite.getZvel(),
                        1280, -1280, 0);

                int sect = pSprite.getSectnum();
                sec = boardService.getSector(pSprite.getSectnum());
                if (sec == null) {
                    return;
                }

                if (!isOriginal() && osect != sect && (SectFlag[osect] & 0x2000) == 0 && (SectFlag[sect] & 0x2000) != 0) {
                    engine.changespritesect( nSprite, osect);

                    pSprite.setZ(oz - 2048);
                    pSpider.nState = 1;
                    pSprite.setZvel(-512);
                    return;
                }

                if (hitMove != 0) {
                    if ((hitMove & nEvent1) != 0 && pSprite.getZvel() < 0 && (zr_ceilhit & PS_HIT_TYPE_MASK) != PS_HIT_SPRITE
                            && (sec.getCeilingstat() & 1) == 0) {
                        pSprite.setCstat(pSprite.getCstat() | 8);

                        pSprite.setZ(GetSpriteHeight(nSprite) + sec.getCeilingz());
                        pSprite.setZvel(0);
                        pSpider.nState = 1;
                        pSpider.nSeq = 0;
                        return;
                    }

                    switch (hitMove & PS_HIT_TYPE_MASK) {
                        case PS_HIT_WALL:
                            pSprite.setAng( ((pSprite.getAng() + 256) & 0x7EF));
                            pSprite.setXvel(EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF));
                            pSprite.setYvel(EngineUtils.sin(pSprite.getAng() & 0x7FF));
                            return;
                        case PS_HIT_SPRITE:
                            if ((hitMove & PS_HIT_INDEX_MASK) == nTarget && pTarget != null) {
                                int v34 = EngineUtils.getAngle(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY());
                                if (AngleDiff(pSprite.getAng(), v34) < 64) {
                                    pSpider.nState = 2;
                                    pSpider.nSeq = 0;
                                }
                            }
                            return;
                    }
                    if (nState == 3) {
                        pSpider.nState = 1;
                        pSpider.nSeq = 0;
                    }
                }

                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(plr);
                PlotSequence(tsp, ActionSeq_X_2[nState][0] + SeqOffsets[16], pSpider.nSeq,
                        ActionSeq_X_2[nState][1]);
                return;
            case nEventRadialDamage:
                if (pSpider.nHealth <= 0) {
                    return;
                }

                nDamage = CheckRadialDamage(nSprite);
            case nEventDamage:
                if (nDamage != 0) {
                    if (pSpider.nHealth > 0) {
                        pSpider.nHealth -= nDamage;
                        if (pSpider.nHealth > 0) {
                            Sprite plrSpr = boardService.getSprite(plr);
                            if (plrSpr != null && plrSpr.getStatnum() == 100) {
                                pSpider.nTarget = plr;
                            }

                            pSpider.nSeq = 0;
                            pSpider.nState = 4;
                        } else {
                            pSpider.nSeq = 0;
                            pSpider.nHealth = 0;
                            pSpider.nState = 5;
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            for (int i = 0, seq = 41; i < 7; i++, seq++) {
                                BuildCreatureChunk(nSprite, GetSeqPicnum(16, seq, 0));
                            }

                            nCreaturesLeft--;
                        }
                    }
                }
        }
    }
}
