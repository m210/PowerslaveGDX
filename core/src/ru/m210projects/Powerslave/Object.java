// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Bullet.BuildBullet;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.*;
import static ru.m210projects.Powerslave.Player.GetPlayerFromSprite;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.Sprites.*;

public class Object {

    public static int ElevCount;
    public static final ElevStruct[] Elevator = new ElevStruct[1024];

    public static int WallFaceCount;
    public static final WallFaceStruct[] WallFace = new WallFaceStruct[4096];
    public static int longSeek_out;
    private static int FinaleMoves;
    private static int FinaleClock;
    private static boolean bLightTrig;

    public static void InitElev() {
        ElevCount = 1024;
        for (int i = 0; i < 1024; i++) {
            Elevator[i] = new ElevStruct();
        }
    }

    public static void saveElevs(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  ElevCount);
        for (int i = ElevCount; i < 1024; i++) {
            Elevator[i].save(os);
        }
    }

    public static void loadElevs(SafeLoader loader, InputStream is) throws IOException {
        loader.ElevCount = StreamUtils.readShort(is);
        for (int i = loader.ElevCount; i < 1024; i++) {
            if (loader.Elevator[i] == null) {
                loader.Elevator[i] = new ElevStruct();
            }
            loader.Elevator[i].load(is);
        }
    }

    public static void loadElevs(SafeLoader loader) {
        ElevCount = loader.ElevCount;
        for (int i = loader.ElevCount; i < 1024; i++) {
            if (Elevator[i] == null) {
                Elevator[i] = new ElevStruct();
            }
            Elevator[i].copy(loader.Elevator[i]);
        }
    }

    public static int BuildElevC(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int... a8) {
        if (ElevCount <= 0) {
            throw new AssertException("ElevCount>0");
        }

        int i = --ElevCount;
        Elevator[i].field_0 =  a1;
        if ((a1 & 4) != 0) {
            a5 /= 2;
        }

        Elevator[i].field_6 = a5;
        Elevator[i].field_E = 0;
        Elevator[i].field_36 = 0;
        Elevator[i].field_10 = 0;
        Elevator[i].field_A = a6;
        Elevator[i].field_32 = -1;
        Elevator[i].channel =  a2;
        Elevator[i].sectnum =  a3;

        if (a4 < 0) {
            a4 = BuildWallSprite(a3);
        }

        Elevator[i].field_34 =  a4;
        for (int j = 0; j < a7; j++) {
            Elevator[i].field_12[Elevator[i].field_E] = a8[j];
            Elevator[i].field_E++;
        }

        return i;
    }

    public static int BuildElevF(int a1, int a2, int a3, int a4, int a5, int a6, int... a7) {
        if (ElevCount <= 0) {
            throw new AssertException("ElevCount>0");
        }

        int i = --ElevCount;
        if ((a1 & 4) != 0) {
            a5 /= 2;
        }

        Elevator[i].field_0 = 2;
        Elevator[i].field_6 = a4;
        Elevator[i].field_32 = -1;
        Elevator[i].field_A = a5;
        Elevator[i].channel =  a1;
        Elevator[i].sectnum =  a2;
        Elevator[i].field_E = 0;
        Elevator[i].field_10 = 0;
        Elevator[i].field_36 = 0;

        if (a3 < 0) {
            a3 = BuildWallSprite(a2);
        }

        Elevator[i].field_34 =  a3;
        for (int j = 0; j < a6; j++) {
            Elevator[i].field_12[Elevator[i].field_E] = a7[j];
            Elevator[i].field_E++;
        }

        return i;
    }

    public static void FuncElev(int nStack, int ignored, int RunPtr) {
        int elev = RunData[RunPtr].getObject();
        if (elev < 0 || elev >= 1024) {
            throw new AssertException("elev>=0 && elev<MAXELEV");
        }

        int nRun = nStack & EVENT_MASK;
        if (nRun != nEventProcess && nRun != nEvent1) {
            return;
        }

        int chan = Elevator[elev].channel;
        if (chan < 0 || chan >= 4096) {
            throw new AssertException("chan>=0 && chan<MAXCHAN");
        }

        Channel pChannel = channel[chan];
        int nFlags = Elevator[elev].field_0;
        Sector elevSec = boardService.getSector(Elevator[elev].sectnum);
        if (elevSec == null) {
            return;
        }

        switch (nRun) {
            case nEventProcess:
                if ((nFlags & 2) != 0) {
                    game.pInt.setfloorinterpolate(Elevator[elev].sectnum, elevSec);
                    int v18 = LongSeek(elevSec.getFloorz(), Elevator[elev].field_12[Elevator[elev].field_10], Elevator[elev].field_6, Elevator[elev].field_A);
                    elevSec.setFloorz(longSeek_out);
                    if (v18 == 0) {
                        if ((nFlags & 0x10) != 0) {
                            Elevator[elev].field_10 ^= 1;
                            StartElevSound(Elevator[elev].field_34, nFlags);
                        } else {
                            StopSpriteSound(Elevator[elev].field_34);
                            SubRunRec(RunPtr);
                            Elevator[elev].field_32 = -1;
                            ReadyChannel(chan);
                            D3PlayFX(StaticSound[nStopSound], Elevator[elev].field_34);
                        }

                        int v16 = Elevator[elev].field_34;
                        while (v16 != -1) {
                            int v29 = v16;
                            Sprite spr = boardService.getSprite(v29);
                            if (spr == null) {
                                break;
                            }

                            v16 = spr.getOwner();
                            game.pInt.setsprinterpolate(v29, spr);
                            spr.setZ(spr.getZ() + v18);
                        }
                        return;
                    }
                    MoveSectorSprites(Elevator[elev].sectnum, v18);
                    if (v18 >= 0 || !CheckSectorSprites(Elevator[elev].sectnum, 2)) {
                        int v16 = Elevator[elev].field_34;
                        while (v16 != -1) {
                            int v29 = v16;
                            Sprite spr = boardService.getSprite(v29);
                            if (spr == null) {
                                break;
                            }

                            v16 = spr.getOwner();
                            game.pInt.setsprinterpolate(v29, spr);
                            spr.setZ(spr.getZ() + v18);
                        }
                        return;
                    }

                    ChangeChannel(chan, channel[chan].field_4 == 0 ? 1 : 0);
                    return;
                }

                game.pInt.setceilinterpolate(Elevator[elev].sectnum, elevSec);
                int v30 = elevSec.getCeilingz();
                int v27 = LongSeek(v30, Elevator[elev].field_12[Elevator[elev].field_10], Elevator[elev].field_6, Elevator[elev].field_A);
                v30 = longSeek_out;

                if (v27 != 0) {
                    if (v27 > 0) {
                        if (v30 == Elevator[elev].field_12[Elevator[elev].field_10]) {
                            if ((nFlags & 4) != 0) {
                                SetQuake(Elevator[elev].field_34, 30);
                            }
                            Sprite spr = boardService.getSprite(Elevator[elev].field_34);
                            if (spr != null) {
                                PlayFXAtXYZ(StaticSound[26], spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());
                            }
                        }
                        if ((nFlags & 4) != 0) {
                            if (CheckSectorSprites((short) Elevator[elev].sectnum, 1)) {
                                return;
                            }
                        } else if (CheckSectorSprites((short) Elevator[elev].sectnum, 0)) {
                            ChangeChannel(chan, channel[chan].field_4 == 0 ? 1 : 0);
                            return;
                        }
                    }
                    elevSec.setCeilingz(v30);
                    int v16 = Elevator[elev].field_34;
                    while (v16 != -1) {
                        int v29 = v16;
                        Sprite spr = boardService.getSprite(v29);
                        if (spr == null) {
                            break;
                        }
                        v16 = spr.getOwner();
                        game.pInt.setsprinterpolate(v29, spr);
                        spr.setZ(spr.getZ() + v27);
                    }
                    return;
                }

                if ((nFlags & 0x10) == 0) {
                    SubRunRec(RunPtr);
                    Elevator[elev].field_32 = -1;
                    StopSpriteSound(Elevator[elev].field_34);
                    D3PlayFX(StaticSound[nStopSound], Elevator[elev].field_34);
                    ReadyChannel(chan);
                    return;
                }
                Elevator[elev].field_10 ^= 1;
                StartElevSound((short) Elevator[elev].field_34, nFlags);
                return;
            case nEvent1:
                int a4 = 0;
                if ((nFlags & 8) == 0) {
                    if ((nFlags & 0x10) != 0) {
                        if (Elevator[elev].field_32 < 0) {
                            Elevator[elev].field_32 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent1
                            a4 = 1;
                            StartElevSound((short) Elevator[elev].field_34, nFlags);
                        }
                        if (a4 != 0) {
                            if (Elevator[elev].field_32 >= 0) {
                                return;
                            }
                            Elevator[elev].field_32 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent1
                            StartElevSound((short) Elevator[elev].field_34, nFlags);
                            return;
                        }

                        if (Elevator[elev].field_32 >= 0) {
                            SubRunRec(Elevator[elev].field_32);
                            Elevator[elev].field_32 = -1;
                        }
                        return;
                    }

                    if (pChannel.field_4 >= 0) {
                        if (pChannel.field_4 == Elevator[elev].field_10 || pChannel.field_4 >= Elevator[elev].field_E) {
                            Elevator[elev].field_36 = pChannel.field_4;
                        } else {
                            Elevator[elev].field_10 = channel[chan].field_4;
                        }
                        if (Elevator[elev].field_32 >= 0) {
                            return;
                        }
                        Elevator[elev].field_32 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent1
                        StartElevSound((short) Elevator[elev].field_34, nFlags);
                        return;
                    }

                    if (Elevator[elev].field_32 >= 0) {
                        SubRunRec(Elevator[elev].field_32);
                        Elevator[elev].field_32 = -1;
                    }
                    return;
                }

                if (pChannel.field_4 == 0) {
                    if (Elevator[elev].field_32 >= 0) {
                        SubRunRec(Elevator[elev].field_32);
                        Elevator[elev].field_32 = -1;
                    }
                    return;
                }

                if (Elevator[elev].field_32 >= 0) {
                    return;
                }

                Elevator[elev].field_32 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent1
                StartElevSound((short) Elevator[elev].field_34, nFlags);
        }
    }

    public static void BuildObject(int nSprite, int a2, int a3) {
        int v20 = ObjectCount;
        if (ObjectCount >= 128) {
            throw new AssertException("Too many objects!");
        }

        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (ObjectList[v20] == null) {
            ObjectList[v20] = new ObjectStruct();
        }

        int v5 = ObjectStatnum[a2];
        ObjectCount++;
        engine.changespritestat(nSprite, v5);

        pSprite.setCstat( ((pSprite.getCstat() | 0x101) & (~(0x8000 | 2))));
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setExtra(-1);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setHitag(0);
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent23 | v20));

        if (pSprite.getStatnum() == 97) {
            ObjectList[v20].obj_2 = 4;
        } else {
            ObjectList[v20].obj_2 = 120;
        }
        int v14 = NewRun;
        ObjectList[v20].field_6 = nSprite;
        ObjectList[v20].field_4 =  AddRunRec(v14, nEvent23 | v20);
        int v15 = ObjectSeq[a2];
        if (v15 <= -1) {
            ObjectList[v20].field_0 = 0;
            int v23 = pSprite.getStatnum();
            ObjectList[v20].field_8 = -1;
            if (v23 == 97) {
                ObjectList[v20].obj_A = -1;
            } else {
                ObjectList[v20].obj_A =  -a3;
            }
        } else {
            int v16 = SeqOffsets[v15];
            ObjectList[v20].field_8 =  v16;
            if (a2 == 0) {
                int v17 = SeqSize[v16] - 1;
                int v18 = RandomSize(4);
                ObjectList[v20].field_0 =  (v18 % v17);
            }
            int v21 = engine.insertsprite(pSprite.getSectnum(),  0);
            Sprite spr = boardService.getSprite(v21);
            if (spr != null) {
                ObjectList[v20].obj_A = v21;
                spr.setCstat(32768);
                spr.setX(pSprite.getX());
                spr.setY(pSprite.getY());
                spr.setZ(pSprite.getZ());
            } else {
                ObjectList[v20].obj_A = -1;
            }
        }
    }

    public static void saveObjects(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  ObjectCount);
        for (short i = 0; i < ObjectCount; i++) {
            ObjectList[i].save(os);
        }
    }

    public static void loadObjects(SafeLoader loader) {
        ObjectCount = loader.ObjectCount;
        for (short i = 0; i < loader.ObjectCount; i++) {
            if (ObjectList[i] == null) {
                ObjectList[i] = new ObjectStruct();
            }
            ObjectList[i].copy(loader.ObjectList[i]);
        }
    }

    public static void loadObjects(SafeLoader loader, InputStream is) throws IOException {
        loader.ObjectCount = StreamUtils.readShort(is);
        for (short i = 0; i < loader.ObjectCount; i++) {
            if (loader.ObjectList[i] == null) {
                loader.ObjectList[i] = new ObjectStruct();
            }
            loader.ObjectList[i].load(is);
        }
    }

    public static void FuncObject(int nStack, int a2, int a3) {
        ObjectStruct v5 = ObjectList[RunData[a3].getObject()];
        int nSprite = v5.field_6;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v43 = pSprite.getStatnum();
        int v8 = (nStack & EVENT_MASK);
        int v9 = v5.field_8;

        switch (v8) {
            case nEventProcess:
                if (v43 != 97 && (pSprite.getCstat() & 0x101) != 0) {
                    if (v43 != 152) {
                        Gravity(nSprite);
                    }
                    if (v9 != -1) {
                        int v10 = v5.field_0 + 1;
                        v5.field_0 =  v10;
                        if (v10 >= SeqSize[v9]) {
                            v5.field_0 = 0;
                        }
                        pSprite.setPicnum( GetSeqPicnum2(v9, v5.field_0));
                    }

                    if (v5.obj_2 >= 0 || (v5.obj_2++ != -1)) {
                        if (v43 != 152) {
                            int v24 = engine.movesprite(nSprite, pSprite.getXvel() << 6, pSprite.getYvel() << 6, pSprite.getZvel(), 0, 0, 0);
                            if (pSprite.getStatnum() == 141) {
                                pSprite.setPal(1);
                            }
                            if ((v24 & nEventProcess) != 0) {
                                pSprite.setXvel(pSprite.getXvel() - (pSprite.getXvel() >> 3));
                                pSprite.setYvel(pSprite.getYvel() - (pSprite.getYvel() >> 3));
                            }
                            if ((v24 & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                                pSprite.setYvel(0);
                                pSprite.setXvel(0);
                            }
                        }
                    } else {
                        Sector sec = boardService.getSector(pSprite.getSectnum());
                        if (sec == null) {
                            return;
                        }

                        int v44;
                        if (v43 != 152 && pSprite.getZ() >= sec.getFloorz()) {
                            v44 = 34;
                        } else {
                            v44 = 36;
                        }
                        AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 128);
                        BuildAnim(-1, v44, 0, pSprite.getX(), pSprite.getY(), sec.getFloorz(), pSprite.getSectnum(), 0xF0, 4);
                        int hitMove = nSprite | PS_HIT_SECTOR;
                        if (v43 == 141) {
                            for (int i = 4; i < 8; i++) {
                                BuildCreatureChunk(hitMove, GetSeqPicnum(46, (i >> 2) + 1, 0));
                            }
                            RadialDamageEnemy(nSprite, 200, 20);
                        } else if (v43 == 152) {
                            for (int i = 0; i < 8; i++) {
                                BuildCreatureChunk(hitMove, GetSeqPicnum(46, (i >> 1) + 3, 0));
                            }
                        }
                        if (levelnum <= 20 || v43 != 141) {
                            SubRunRec(pSprite.getOwner());
                            SubRunRec(v5.field_4);
                            engine.mydeletesprite(nSprite);
                        } else {
                            StartRegenerate(nSprite);
                            int v19 = v5.obj_A;
                            Sprite pObj = boardService.getSprite(v19);
                            if (pObj == null) {
                                return;
                            }

                            v5.obj_2 = 120;
                            pSprite.setX(pObj.getX());
                            pSprite.setY(pObj.getY());
                            pSprite.setZ(pObj.getZ());
                            engine.mychangespritesect(nSprite, pObj.getSectnum());
                        }
                    }
                }
                return;
            case nEventDamage:
                if (v43 >= 150) {
                    return;
                }
                if (v5.obj_2 <= 0) {
                    return;
                }
                if (v43 == 98) {
                    D3PlayFX((RandomSize(2) << 9) | StaticSound[47] | 0x2000, v5.field_6);
                    return;
                }
                v5.obj_2 -= a2;
                if (v5.obj_2 > 0) {
                    return;
                }

                if (v43 == 97) {
                    ExplodeScreen(nSprite);
                    return;
                }

                v5.obj_2 =  -(RandomSize(3) + 1);
                return;
            case nEventView:
                if (v9 > -1) {
                    Renderer renderer = game.getRenderer();
                    TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                    PlotSequence(tsp, v9, v5.field_0, 1);
                }
                return;
            case nEventRadialDamage:
                Sprite pRadialSpr = boardService.getSprite(nRadialSpr);
                if (v5.obj_2 > 0 && (pSprite.getCstat() & 0x101) != 0 && (v43 != 152 || (pRadialSpr != null && pRadialSpr.getStatnum() == 201) || nRadialBullet != 3 && nRadialBullet > -1 || (pRadialSpr != null && pRadialSpr.getStatnum() == 141))) {
                    int v28 = CheckRadialDamage(nSprite);
                    if (v28 > 0) {
                        if (pSprite.getStatnum() != 98) {
                            v5.obj_2 -= v28;
                        }
                        int v31 = pSprite.getStatnum();
                        if (v31 == 152) {
                            pSprite.setZvel(0);
                            pSprite.setYvel(0);
                            pSprite.setXvel(0);
                        } else if (v31 != 98) {
                            pSprite.setXvel(pSprite.getXvel() >> 1);
                            pSprite.setYvel(pSprite.getYvel() >> 1);
                            pSprite.setZvel(pSprite.getZvel() >> 1);
                        }
                        if (v5.obj_2 <= 0) {
                            int v35 = pSprite.getStatnum();
                            if (v35 == 152) {
                                int v36 = v5.obj_A;
                                v5.obj_2 = -1;
                                if (v36 >= 0) {
                                    if (ObjectList[v36].obj_2 > 0) {
                                        ObjectList[v36].obj_2 = -1;
                                    }
                                }
                                return;
                            }
                            if (v35 == 97) {
                                v5.obj_2 = 0;
                                ExplodeScreen(nSprite);
                                return;
                            }
                            v5.obj_2 =  -(RandomSize(4) + 1);
                        }
                    }
                }
        }
    }

    public static void StartRegenerate(final int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v2 = -1;
        int nRegenerate = nFirstRegenerate;
        int v3 = 0;
        while (true) {
            if (v3 >= nRegenerates) {
                pSprite.setXvel(pSprite.getXrepeat());
                pSprite.setZvel(pSprite.getShade());
                pSprite.setYvel(pSprite.getPal());
                break;
            }

            if (nRegenerate == nSprite) {
                if (v2 == -1) {
                    nFirstRegenerate = pSprite.getAng();
                } else {
                    Sprite spr = boardService.getSprite(v2);
                    if (spr != null) {
                        spr.setAng(pSprite.getAng());
                    }
                }
                --nRegenerates;
                break;
            }

            v2 = nRegenerate;
            Sprite spr = boardService.getSprite(nRegenerate);
            if (spr != null) {
                nRegenerate = spr.getAng();
            }
            v3++;
        }

        pSprite.setExtra(1350);
        pSprite.setAng( nFirstRegenerate);
        if (levelnum <= 20) {
            pSprite.setExtra(pSprite.getExtra() / 5);
        }
        pSprite.setCstat(-32768);
        pSprite.setXrepeat(1);
        pSprite.setYrepeat(1);
        pSprite.setPal(1);

        nFirstRegenerate = nSprite;
        nRegenerates++;
    }

    public static void DoRegenerates() {
        int spr = nFirstRegenerate;
        Sprite pSprite;
        for (int i = nRegenerates; i > 0; i--, spr = pSprite.getAng()) {
            pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                break;
            }

            if (pSprite.getExtra() <= 0) {
                if (pSprite.getXrepeat() < pSprite.getXvel()) {
                    pSprite.setXrepeat(pSprite.getXrepeat() + 2);
                    pSprite.setYrepeat(pSprite.getYrepeat() + 2);
                    continue;
                }
            } else {
                pSprite.setExtra(pSprite.getExtra() - 1);
                if (pSprite.getExtra() > 0) {
                    continue;
                }
                BuildAnim(-1, 38, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 0x40, 4);
                D3PlayFX(StaticSound[12], spr);
            }

            pSprite.setXrepeat(pSprite.getXvel());
            pSprite.setYrepeat(pSprite.getXvel());

            pSprite.setPal(pSprite.getYvel());
            pSprite.setXvel(0);
            pSprite.setYvel(0);
            pSprite.setZvel(0);

            if (pSprite.getStatnum() == 141) {
                pSprite.setCstat(257);
            } else {
                pSprite.setCstat(0);
            }

            nRegenerates--;
            if (nRegenerates == 0) {
                nFirstRegenerate = -1;
            }
        }
    }

    private static void MoveSectorSprites(int a1, int a2) {
        for (ListNode<Sprite> node = boardService.getSectNode(a1); node != null; node = node.getNext()) {
            Sprite spr = node.get();
            if (spr.getStatnum() != 200) {
                game.pInt.setsprinterpolate(node.getIndex(), spr);
                spr.setZ(spr.getZ() + a2);
            }
        }
    }

    private static boolean CheckSectorSprites(int a1, int a2) {
        boolean v10 = false;
        if (a2 != 0) {
            Sector sec = boardService.getSector(a1);
            if (sec == null) {
                return false;
            }

            int v5 = sec.getFloorz() - sec.getCeilingz();
            ListNode<Sprite> v4 = boardService.getSectNode(a1);
            while (v4 != null) {
                Sprite spr = v4.get();
                int index = v4.getIndex();
                if ((spr.getCstat() & 0x101) != 0 && v5 < GetSpriteHeight(index)) {
                    if (a2 != 1) {
                        return true;
                    }
                    v10 = true;
                    DamageEnemy(index, -1, 5);
                    if (spr.getStatnum() == 100 && PlayerList[GetPlayerFromSprite(index)].HealthAmount <= 0) {
                        int v8 = spr.getSectnum() | PS_HIT_SECTOR;

                        PlayFXAtXYZ(StaticSound[60], spr.getX(), spr.getY(), spr.getZ(), v8);
                    }
                }
                v4 = v4.getNext();
            }
            return v10;
        } else {
            for (ListNode<Sprite> node = boardService.getSectNode(a1); node != null; node = node.getNext()) {
                if ((node.get().getCstat() & 0x101) != 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void SetQuake(int nSprite, int a2) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v3 = 0;
        int v4 = 0;
        int v5 = a2 << 8;
        int v11 = pSprite.getX();
        int v10 = pSprite.getY();
        int v6 = 0;
        while (v3 < numplayers) {
            int v7 = PlayerList[v4].spriteId;
            Sprite spr = boardService.getSprite(v7);
            if (spr != null) {
                int v8 = EngineUtils.sqrt(((spr.getX() - v11) >> 8) * ((spr.getX() - v11) >> 8) + ((spr.getY() - v10) >> 8) * ((spr.getY() - v10) >> 8));
                int v9 = v5;
                if (v8 != 0) {
                    v9 = v5 / v8;
                    if (v5 / v8 >= 256) {
                        if (v9 > 3840) {
                            v9 = 3840;
                        }
                    } else {
                        v9 = 0;
                    }
                }
                if (v9 > nQuake[v6]) {
                    nQuake[v6] = v9;
                }
            }
            ++v4;
            ++v6;
            ++v3;
        }
    }

    public static int LongSeek(int a1, int a2, int a3, int a4) {
        longSeek_out = a1;
        int v4 = a2 - longSeek_out;
        if (v4 < 0) {
            int v5 = -a3;
            if (v5 > v4) {
                v4 = v5;
            }
            longSeek_out += v4;
        }
        if (v4 > 0) {
            if (a4 < v4) {
                v4 = a4;
            }
            longSeek_out += v4;
        }
        return v4;
    }

    public static int BuildWallSprite(int a1) {
        Sector sec = boardService.getSector(a1);
        if (sec == null) {
            return -1;
        }

        int nWall = sec.getWallptr();
        Wall wall = boardService.getWall(nWall);
        if (wall == null) {
            return -1;
        }

        Wall wall2 = wall.getWall2();
        int nSprite = engine.insertsprite( a1,  401);
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return -1;
        }

        pSprite.setX((wall.getX() + wall2.getX()) / 2);
        pSprite.setY((wall.getY() + wall2.getY()) / 2);
        pSprite.setZ((sec.getCeilingz() + sec.getFloorz()) / 2);
        pSprite.setCstat(32768);

        return nSprite;
    }

    public static void InitWallFace() {
        WallFaceCount = 4096;

        for (int i = 0; i < 4096; i++) {
            WallFace[i] = new WallFaceStruct();
        }
    }

    public static void saveWallFaces(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  WallFaceCount);
        for (int i = WallFaceCount; i < 4096; i++) {
            WallFace[i].save(os);
        }
    }

    public static void loadWallFaces(SafeLoader loader, InputStream is) throws IOException {
        loader.WallFaceCount = StreamUtils.readShort(is);
        for (int i = loader.WallFaceCount; i < 4096; i++) {
            if (loader.WallFace[i] == null) {
                loader.WallFace[i] = new WallFaceStruct();
            }
            loader.WallFace[i].load(is);
        }
    }

    public static void loadWallFaces(SafeLoader loader) {
        WallFaceCount = loader.WallFaceCount;
        for (int i = loader.WallFaceCount; i < 4096; i++) {
            if (WallFace[i] == null) {
                WallFace[i] = new WallFaceStruct();
            }
            WallFace[i].copy(loader.WallFace[i]);
        }
    }

    public static int BuildWallFace(int a1, short a2, int a3, int... a4) {
        if (WallFaceCount <= 0) {
            throw new AssertException("Too many wall faces!");
        }
        int i = --WallFaceCount;

        WallFace[i].field_4 = 0;
        WallFace[i].field_2 = a2;
        WallFace[i].field_0 = a1;

        if (a3 > 8) {
            a3 = 8;
        }
        for (int j = 0; j < a3; j++) {
            WallFace[i].field_6[WallFace[i].field_4] =  a4[j];
            WallFace[i].field_4++;
        }

        return i | nEvent7;
    }

    public static void FuncWallFace(int nStack, int ignored, int a3) {
        int ws = RunData[a3].getObject();
        if (ws < 0 || ws >= 4096) {
            throw new AssertException("ws>=0 && ws<MAXWALLFACE");
        }

        if ((nStack & EVENT_MASK) == nEvent1) {
            int v6 = channel[WallFace[ws].field_0].field_4;
            if (v6 <= WallFace[ws].field_4 && v6 >= 0) {
                Wall wall = boardService.getWall(WallFace[ws].field_2);
                if (wall != null) {
                    wall.setPicnum(WallFace[ws].field_6[v6]);
                }
            }
        }
    }

    public static int FindTrail(int a1) {
        int v2 = 0;
        int v3 = 0;
        while (v2 < nTrails) {
            if (a1 == sTrail[v3].field_2) {
                return v2;
            }
            ++v3;
            ++v2;
        }
        sTrail[nTrails].field_2 =  a1;
        sTrail[nTrails].field_0 = -1;

        return nTrails++;
    }

    public static void saveTrails(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nTrails);
        StreamUtils.writeShort(os,  nTrailPoints);

        for (int i = 0; i < nTrails; i++) {
            sTrail[i].save(os);
        }

        for (int i = 0; i < nTrailPoints; i++) {
            sTrailPoint[i].save(os);
        }

        for (int i = 0; i < 100; i++) {
            StreamUtils.writeShort(os, nTrailPointVal[i]);
            StreamUtils.writeShort(os, nTrailPointNext[i]);
            StreamUtils.writeShort(os, nTrailPointPrev[i]);
        }
    }

    public static void loadTrails(SafeLoader loader, InputStream is) throws IOException {
        loader.nTrails = StreamUtils.readShort(is);
        loader.nTrailPoints = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nTrails; i++) {
            if (loader.sTrail[i] == null) {
                loader.sTrail[i] = new TrailStruct();
            }
            loader.sTrail[i].load(is);
        }

        for (int i = 0; i < loader.nTrailPoints; i++) {
            if (loader.sTrailPoint[i] == null) {
                loader.sTrailPoint[i] = new TrailPointStruct();
            }
            loader.sTrailPoint[i].load(is);
        }

        for (int i = 0; i < 100; i++) {
            loader.nTrailPointVal[i] =  StreamUtils.readShort(is);
            loader.nTrailPointNext[i] =  StreamUtils.readShort(is);
            loader.nTrailPointPrev[i] =  StreamUtils.readShort(is);
        }
    }

    public static void loadTrails(SafeLoader loader) {
        nTrails = loader.nTrails;
        nTrailPoints = loader.nTrailPoints;
        for (int i = 0; i < loader.nTrails; i++) {
            if (sTrail[i] == null) {
                sTrail[i] = new TrailStruct();
            }
            sTrail[i].copy(loader.sTrail[i]);
        }

        for (int i = 0; i < loader.nTrailPoints; i++) {
            if (sTrailPoint[i] == null) {
                sTrailPoint[i] = new TrailPointStruct();
            }
            sTrailPoint[i].copy(loader.sTrailPoint[i]);
        }

        System.arraycopy(loader.nTrailPointVal, 0, nTrailPointVal, 0, nTrailPoints);
        System.arraycopy(loader.nTrailPointNext, 0, nTrailPointNext, 0, 100);
        System.arraycopy(loader.nTrailPointPrev, 0, nTrailPointPrev, 0, 100);
    }

    public static void BuildSpark(int nSprite, int a2) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int i = engine.insertsprite(pSprite.getSectnum(),  0);
        if (i != -1) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                return;
            }

            spr.setX(pSprite.getX());
            spr.setY(pSprite.getY());
            spr.setCstat(0);
            spr.setShade(-127);
            spr.setPal(1);
            spr.setXrepeat(50);
            spr.setXoffset(0);
            spr.setYoffset(0);
            spr.setYrepeat(50);

            if (a2 >= 2) {
                spr.setPicnum(3605);
                nSmokeSparks++;
                if (a2 == 3) {
                    spr.setXrepeat(120);
                    spr.setYrepeat(120);
                } else {
                    int size = (pSprite.getXrepeat() + 15);
                    spr.setXrepeat(size);
                    spr.setYrepeat(size);
                }
            } else {
                int v11 = (pSprite.getAng() + 256) - RandomSize(9);
                if (a2 != 0) {
                    spr.setXvel( (EngineUtils.sin((v11 + 512) & 0x7FF) >> 5));
                    spr.setYvel( (EngineUtils.sin(v11 & 0x7FF) >> 5));
                } else {
                    spr.setXvel( (EngineUtils.sin((v11 + 512) & 0x7FF) >> 6));
                    spr.setYvel( (EngineUtils.sin(v11 & 0x7FF) >> 6));
                }
                spr.setZvel( (-128 * RandomSize(4)));
                spr.setPicnum( (a2 + 985));
            }
            spr.setZ(pSprite.getZ());
            spr.setLotag( (HeadRun() + 1));
            spr.setClipdist(1);
            spr.setHitag(0);
            spr.setExtra(-1);
            spr.setOwner( AddRunRec(spr.getLotag() - 1, nEvent38 | i));
            spr.setHitag( AddRunRec(NewRun, nEvent38 | i));
        }

    }

    public static void FuncSpark(int nStack, int ignored, int RunPtr) {
        int nSprite = RunData[RunPtr].getObject();
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if ((nStack & EVENT_MASK) == nEventProcess) {
            pSprite.setShade(pSprite.getShade() + 3);
            pSprite.setXrepeat(pSprite.getXrepeat() - 2);
            if (pSprite.getXrepeat() >= 4 && pSprite.getShade() < 100) {
                pSprite.setYrepeat(pSprite.getYrepeat() - 2);

                if (pSprite.getPicnum() == 986 && (pSprite.getXrepeat() & 2) != 0) {
                    BuildSpark(nSprite, 2);
                }

                if (pSprite.getPicnum() >= 3000) {
                    return;
                }

                pSprite.setZvel(pSprite.getZvel() + 128);
                int result = engine.movesprite(nSprite, pSprite.getXvel() << 12, pSprite.getYvel() << 12, pSprite.getZvel(), 2560, -2560, 1);
                if (result == 0 || pSprite.getZvel() <= 0) {
                    return;
                }
            }

            pSprite.setZvel(0);
            pSprite.setYvel(0);
            pSprite.setXvel(0);
            if (pSprite.getPicnum() > 3000) {
                --nSmokeSparks;
            }

            DoSubRunRec(pSprite.getOwner());
            FreeRun(pSprite.getLotag() - 1);
            SubRunRec(pSprite.getHitag());
            engine.mydeletesprite(nSprite);
        }
    }

    public static int BuildFireBall(int a1, int a2, int a3) {
        return BuildTrap(a1, 1, a2, a3);
    }

    public static int BuildArrow(int a1, int a2) {
        return BuildTrap(a1, 0, -1, a2);
    }

    public static void saveTraps(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nTraps);
        for (short i = 0; i < 40; i++) {
            StreamUtils.writeShort(os, nTrapInterval[i]);
        }

        for (short i = 0; i < nTraps; i++) {
            sTrap[i].save(os);
        }
    }

    public static void loadTraps(SafeLoader loader) {
        nTraps = loader.nTraps;
        System.arraycopy(loader.nTrapInterval, 0, nTrapInterval, 0, nTraps);
        for (short i = 0; i < loader.nTraps; i++) {
            if (sTrap[i] == null) {
                sTrap[i] = new TrapStruct();
            }
            sTrap[i].copy(loader.sTrap[i]);
        }
    }

    public static void loadTraps(SafeLoader loader, InputStream is) throws IOException {
        loader.nTraps = StreamUtils.readShort(is);
        for (short i = 0; i < 40; i++) {
            loader.nTrapInterval[i] =  StreamUtils.readShort(is);
        }

        for (short i = 0; i < loader.nTraps; i++) {
            if (loader.sTrap[i] == null) {
                loader.sTrap[i] = new TrapStruct();
            }
            loader.sTrap[i].load(is);
        }
    }

    public static int BuildTrap(int nSprite, int a2, int a3, int a4) {
        if (nTraps >= 40) {
            throw new AssertException("Too many traps!");
        }
        int v5 = nTraps++;

        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return -1;
        }

        engine.changespritestat( nSprite,  0);
        pSprite.setCstat( 32768);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setExtra(-1);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setHitag( AddRunRec(NewRun, nEvent31 | v5));
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent31 | v5));

        if (sTrap[v5] == null) {
            sTrap[v5] = new TrapStruct();
        }

        sTrap[v5].field_2 =  nSprite;

        sTrap[v5].field_4 =  (((a2 == 0) ? 1 : 0) + 14);
        sTrap[v5].field_0 = -1;

        nTrapInterval[v5] =  (64 - 2 * a4);
        if (nTrapInterval[v5] < 5) {
            nTrapInterval[v5] = 5;
        }

        sTrap[v5].field_C = 0;
        sTrap[v5].field_A = 0;
        if (a3 != -1) {
            sTrap[v5].nWall = -1;
            sTrap[v5].nWall2 = -1;

            Sector sec = boardService.getSector(pSprite.getSectnum());
            if (sec != null) {
                int v16 = 0;
                int v17 = sec.getWallptr();
                while (v16 < sec.getWallnum()) {
                    Wall wall = boardService.getWall(v17);
                    if (wall != null) {
                        if (a3 == wall.getHitag()) {
                            if (sTrap[v5].nWall != -1) {
                                sTrap[v5].nWall2 = v17;
                                sTrap[v5].field_C = wall.getPicnum();
                                return nEvent31 | v5;
                            }
                            sTrap[v5].nWall = v17;
                            sTrap[v5].field_A = wall.getPicnum();
                        }
                    }
                    ++v16;
                    ++v17;
                }
            }
        }

        return nEvent31 | v5;
    }

    public static void FuncTrap(int nStack, int ignored, int RunPtr) {
        int nTrap = RunData[RunPtr].getObject();
        TrapStruct pTrap = sTrap[nTrap];
        int nSprite = pTrap.field_2;

        switch (nStack & EVENT_MASK) {
            case nEvent1:
                if (channel[nStack & PS_HIT_INDEX_MASK].field_4 <= 0) {
                    pTrap.field_0 = -1;
                } else {
                    pTrap.field_0 = 12;
                }
                return;
            case nEventProcess:
                int v8 = pTrap.field_0;
                if (pTrap.field_0 >= 0) {
                    if (--pTrap.field_0 <= 10) {
                        int v10 = pTrap.field_4;
                        if (v8 == 1) {
                            pTrap.field_0 = nTrapInterval[nTrap];
                            if (v10 == 14) {
                                Wall wall = boardService.getWall(pTrap.nWall);
                                if (wall != null) {
                                    wall.setPicnum(pTrap.field_A);
                                }
                                Wall wall2 = boardService.getWall(pTrap.nWall2);
                                if (wall2 != null) {
                                    wall2.setPicnum(pTrap.field_C);
                                }
                            }
                        } else if (pTrap.field_0 == 5) {
                            Sprite pSprite = boardService.getSprite(nSprite);
                            if (pSprite == null) {
                                return;
                            }

                            BulletStruct.BulletResult bulletResult = BuildBullet(nSprite, v10, 0, pSprite.getAng(), 0, 1);
                            Sprite pBulletOwner = bulletResult.getSprite();
                            if (pBulletOwner == null) {
                                return;
                            }

                            if (v10 == 15) {
                                pBulletOwner.setAng( ((pBulletOwner.getAng() - 512) & 0x7FF));
                                D3PlayFX(StaticSound[32], nSprite);
                            } else {
                                pBulletOwner.setClipdist(50);
                                Wall wall = boardService.getWall(pTrap.nWall);
                                if (wall != null) {
                                    wall.setPicnum( (pTrap.field_A + 1));
                                }

                                Wall wall2 = boardService.getWall(pTrap.nWall2);
                                if (wall2 != null) {
                                    wall2.setPicnum( (pTrap.field_C + 1));
                                }
                                D3PlayFX(StaticSound[36], nSprite);
                            }
                        }
                    }
                }
        }
    }

    public static void saveDrips(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nDrips);
        for (short i = 0; i < nDrips; i++) {
            sDrip[i].save(os);
        }
    }

    public static void loadDrips(SafeLoader loader, InputStream is) throws IOException {
        loader.nDrips = StreamUtils.readShort(is);
        for (short i = 0; i < loader.nDrips; i++) {
            if (loader.sDrip[i] == null) {
                loader.sDrip[i] = new DripStruct();
            }
            loader.sDrip[i].load(is);
        }
    }

    public static void loadDrips(SafeLoader loader) {
        nDrips = loader.nDrips;
        for (short i = 0; i < loader.nDrips; i++) {
            if (sDrip[i] == null) {
                sDrip[i] = new DripStruct();
            }
            sDrip[i].copy(loader.sDrip[i]);
        }
    }

    public static void BuildDrip(int nSprite) {
        if (nDrips >= 50) {
            throw new AssertException("Too many drips!");
        }

        if (sDrip[nDrips] == null) {
            sDrip[nDrips] = new DripStruct();
        }
        DripStruct v4 = sDrip[nDrips];
        ++nDrips;
        v4.field_0 =  nSprite;
        v4.field_2 =  (RandomSize(8) + 90);
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            pSprite.setCstat(32768);
        }
    }

    public static void saveBobs(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nBobs);
        for (short i = 0; i < 200; i++) {
            StreamUtils.writeShort(os, sBobID[i]);
        }

        for (short i = 0; i < nBobs; i++) {
            sBob[i].save(os);
        }
    }

    public static void loadBobs(SafeLoader loader, InputStream is) throws IOException {
        loader.nBobs = StreamUtils.readShort(is);
        for (short i = 0; i < 200; i++) {
            loader.sBobID[i] =  StreamUtils.readShort(is);
        }
        for (short i = 0; i < loader.nBobs; i++) {
            if (loader.sBob[i] == null) {
                loader.sBob[i] = new BobStruct();
            }
            loader.sBob[i].load(is);
        }
    }

    public static void loadBobs(SafeLoader loader) {
        nBobs = loader.nBobs;
        System.arraycopy(loader.sBobID, 0, sBobID, 0, nBobs);
        for (short i = 0; i < loader.nBobs; i++) {
            if (sBob[i] == null) {
                sBob[i] = new BobStruct();
            }
            sBob[i].copy(loader.sBob[i]);
        }
    }

    public static void DoDrips() {
        for (int i = 0; i < nDrips; i++) {
            if (--sDrip[i].field_2 <= 0) {
                Sprite spr = boardService.getSprite(sDrip[i].field_0);
                if (spr != null) {
                    int v3 = SeqOffsets[62];
                    if (SectFlag[spr.getSectnum() & PS_HIT_FLAG] == 0) {
                        v3 = SeqOffsets[62] + 1;
                    }
                    MoveSequence(sDrip[i].field_0, v3, RandomSize(2) % SeqSize[v3]);
                }
                sDrip[i].field_2 =  (RandomSize(8) + 90);
            }
        }

        for (int i = 0; i < nBobs; i++) {
            Sector sec = boardService.getSector(sBob[i].field_0);
            if (sec == null) {
                continue;
            }

            sBob[i].field_2 += 4;
            int v10 = EngineUtils.sin(8 * sBob[i].field_2 & 0x7FF) >> 4;
            if (sBob[i].field_3 != 0) {
                game.pInt.setceilinterpolate(sBob[i].field_0, sec);
                sec.setCeilingz(sBob[i].field_4 + v10);
            } else {
                int v13 = v10 + sBob[i].field_4;
                int v14 = v13 - sec.getFloorz();
                game.pInt.setfloorinterpolate(sBob[i].field_0, sec);
                sec.setFloorz(v13);
                MoveSectorSprites(sBob[i].field_0, v14);
            }
        }
    }

    public static void DoFinale() {
        if (lFinaleStart == 0) {
            return;
        }

        if (++FinaleMoves >= 90) {
            DimLights();
            if (nDronePitch <= -2400) {
                if (nFinaleStage < 2) {
                    if (nFinaleStage == 1) {
                        StopLocalSound();
                        PlayLocalSound(StaticSound[76], 0);
                        FinaleClock = engine.getTotalClock() + 120;
                        ++nFinaleStage;
                    }
                } else if (nFinaleStage == 2) {
                    if (engine.getTotalClock() >= FinaleClock) {
                        PlayLocalSound(StaticSound[77], 0);
                        ++nFinaleStage;
                        FinaleClock = engine.getTotalClock() + 360;
                    }
                } else if (nFinaleStage == 3 && engine.getTotalClock() >= FinaleClock) {
                    FinishLevel();
                }
            } else {
                nDronePitch -= 128;
                BendAmbientSound();
                nFinaleStage = 1;
            }
        } else {
            if ((FinaleMoves & 2) == 0) {
                Sprite pSprite = boardService.getSprite(nFinaleSpr);
                if (pSprite != null) {
                    pSprite.setAng( RandomSize(11));
                    BuildSpark(nFinaleSpr, 1);
                }
            }
            if (RandomSize(2) == 0) {
                PlayFX2(StaticSound[78] | 0x2000, nFinaleSpr);
                for (int i = 0; i < numplayers; i++) {
                    nQuake[i] = 1280;
                }
            }
        }
    }

    private static void DimLights() {
        bLightTrig = !bLightTrig;
        if (!bLightTrig) {
            return;
        }

        Sector[] sectors = boardService.getBoard().getSectors();
        for (Sector sec : sectors) {
            if (sec.getCeilingshade() < 100) {
                sec.setCeilingshade(sec.getCeilingshade() + 1);
            }
            if (sec.getFloorshade() < 100) {
                sec.setFloorshade(sec.getFloorshade() + 1);
            }
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall w = wn.get();
                if (w.getShade() < 100) {
                    w.setShade(w.getShade() + 1);
                }
            }
        }
    }
}
