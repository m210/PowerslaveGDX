// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Light.AddGlow;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Palette.*;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.Sound.*;

public class SpiritHead {

    public static int nSpiritSprite;
    public static short nHeadStage;
    public static boolean nTalkTime;
    private static short nSpiritRepeatX, nSpiritRepeatY;
    private static short nMouthTile;
    private static short nPixels;
    private static int nHeadTimeStart, lHeadStartClock, lNextStateChange;
    private static int nPixelsToShow;
    private static int nPupData;
    private static ByteBuffer pPupData;

    private static int word_964E6;
    private static int word_964E8;
    private static int word_964EA;
    private static int word_964EC;

    private static final byte[] pixelval = new byte[10282];
    private static final byte[] origx = new byte[10282];
    private static final byte[] origy = new byte[10282];
    private static final byte[] velx = new byte[10282];
    private static final byte[] vely = new byte[10282];
    private static final int[] curx = new int[10282];
    private static final int[] cury = new int[10282];
    private static final byte[] destvelx = new byte[10282];
    private static final byte[] destvely = new byte[10282];

    public static void InitSpiritHead() {
        nPixels = 0;
        Sprite pSpiritSprite = boardService.getSprite(nSpiritSprite);
        if (pSpiritSprite == null) {
            return;
        }

        nSpiritRepeatX = pSpiritSprite.getXrepeat();
        nSpiritRepeatY = pSpiritSprite.getYrepeat();

        ArtEntry art590 = engine.getTile(590);
        ArtEntry art592 = engine.getTile(592);
        if (!art590.hasSize() || !art592.hasSize()) {
            return;
        }

        byte[] data590 = art590.getBytes();
        byte[] data592 = art592.getBytes();

        for (int i = 0; i < MAXSPRITES; i++) {
            Sprite sprite = boardService.getSprite(i);
            if (sprite != null && sprite.getStatnum() != 0) {
                sprite.setCstat(sprite.getCstat() | 0x8000);
            }
        }

        int ptr = 0;
        int ox = -48;
        for (int x = 0; x < 97; x++) {
            int oy = -53;
            for (int y = 0; y < 106; y++) {
                if (data592[ptr] != -1) {
                    pixelval[nPixels] = data590[(106 * x) + y];
                    origx[nPixels] = (byte) ox;
                    origy[nPixels] = (byte) oy;
                    curx[nPixels] = 0;
                    cury[nPixels] = 0;
                    vely[nPixels] = 0;
                    velx[nPixels] = 0;
                    destvelx[nPixels] = (byte) (RandomSize(2) + 1);
                    if (velx[nPixels] > 0) {
                        destvelx[nPixels] = (byte) -destvelx[nPixels];
                    }
                    destvely[nPixels] = (byte) (RandomSize(2) + 1);
                    if (vely[nPixels] > 0) {
                        destvely[nPixels] = (byte) -destvely[nPixels];
                    }
                    nPixels++;
                }
                ptr++;
                oy++;
            }
            ox++;
        }

        DynamicArtEntry pic = engine.allocatepermanenttile(591, 194, 212);
        Arrays.fill(pic.getBytes(), (byte) 255);

        pSpiritSprite.setYrepeat(140);
        pSpiritSprite.setXrepeat(140);
        pSpiritSprite.setPicnum(591);
        pSpiritSprite.setCstat(pSpiritSprite.getCstat() & ~0x8000);
        nHeadStage = 0;
        nHeadTimeStart = engine.getTotalClock();
        nPixelsToShow = 0;

        if (levelnum == 1) {
            playCDtrack(3, false);
        } else {
            playCDtrack(7, false);
        }
        StartSwirlies();

        lNextStateChange = engine.getTotalClock();
        lHeadStartClock = engine.getTotalClock();

        if (levelnum > 0) {
            pPupData = ByteBuffer.wrap(game.cache.getEntry("LEV" + levelnum + ".PUP", true).getBytes());
            pPupData.order(ByteOrder.LITTLE_ENDIAN);
            nPupData = pPupData.capacity();
        }

        nMouthTile = 0;
        nTalkTime = true;
        UpdateSounds();
        pic.invalidate();
    }

    public static void DoSpiritHead() {
        PlayerList[0].horiz += (nDestVertPan[0] - PlayerList[0].horiz) / 4;
        ArtEntry picHead = engine.getTile(591);
        if (!(picHead instanceof DynamicArtEntry) || !picHead.exists()) {
            return;
        }

        DynamicArtEntry pic591 = (DynamicArtEntry) engine.getTile(591);
        switch (nHeadStage) {
            case 0:
            case 1:
                Arrays.fill(pic591.getBytes(), (byte) 255);
                break;
            case 5:
                if (lNextStateChange <= engine.getTotalClock()) {
                    if (nPupData != 0) {
                        short clock = pPupData.getShort();
                        nPupData -= 2;
                        if (nPupData > 0) {
                            lNextStateChange = lHeadStartClock + clock - 10;
                            nTalkTime = !nTalkTime;
                        } else {
                            nTalkTime = false;
                            nPupData = 0;
                        }
                    } else if (!cfg.bSubtitles) {
                        levelnew = levelnum + 1;
                    }
                }

                if (--word_964E8 <= 0) {
                    word_964EA = 2 * RandomBit();
                    word_964E8 = RandomSize(5) + 4;
                }

                int tilenum = 592;
                if (--word_964EC < 3) {
                    tilenum = 593;
                    if (word_964EC <= 0) {
                        word_964EC = RandomSize(6) + 4;
                    }
                }

                CopyHeadToWorkTile(word_964EA + tilenum);

                if (nTalkTime) {
                    if (nMouthTile < 2) {
                        nMouthTile++;
                    }
                } else if (nMouthTile != 0) {
                    nMouthTile--;
                }

                if (nMouthTile != 0) {
                    int srctile = nMouthTile + 598;
                    ArtEntry pic = engine.getTile(srctile);
                    if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                        pic = engine.allocatepermanenttile(pic);
                        if (!pic.exists()) {
                            return;
                        }
                    }

                    int sizx = pic.getWidth();
                    int sizy = pic.getHeight();
                    int workptr = 212 * (97 - sizx / 2) + 159 - sizy;
                    int srcptr = 0;
                    while (sizx > 0) {
                        System.arraycopy(pic.getBytes(), srcptr, pic591.getBytes(), workptr, sizy);
                        workptr += 212;
                        srcptr += sizy;
                        sizx--;
                    }
                }

                pic591.invalidate();
                return;
        }

        nPixelsToShow = 15 * (engine.getTotalClock() - nHeadTimeStart);
        if (nPixelsToShow > nPixels) {
            nPixelsToShow = nPixels;
        }

        Sprite pSpiritSprite = boardService.getSprite(nSpiritSprite);
        if (pSpiritSprite == null) {
            return;
        }

        switch (nHeadStage) {
            case 3:
                FixPalette();
                if (nPalDiff == 0) {
                    nFreeze = 2;
                    nHeadStage++;
                }
                return;
            case 0:
            case 1:
            case 2:
                UpdateSwirlies();
                if (pSpiritSprite.getShade() > -127) {
                    pSpiritSprite.setShade(pSpiritSprite.getShade() - 1);
                }
                if (--word_964E6 < 0) {
                    DimSector(pSpiritSprite.getSectnum());
                    word_964E6 = 5;
                }

                if (nHeadStage == 0) {
                    if (engine.getTotalClock() - nHeadTimeStart > 480) {
                        nHeadStage = 1;
                        nHeadTimeStart = engine.getTotalClock() + 480;
                    }

                    byte[] data591 = pic591.getBytes();
                    for (int i = 0; i < nPixelsToShow; i++) {
                        if (destvely[i] >= 0) {
                            if (++vely[i] >= destvely[i]) {
                                destvely[i] = (byte) -(RandomSize(2) + 1);
                            }
                        } else {
                            if (--vely[i] <= destvely[i]) {
                                destvely[i] = (byte) (RandomSize(2) + 1);
                            }
                        }

                        if (destvelx[i] >= 0) {
                            if (++velx[i] >= destvelx[i]) {
                                destvelx[i] = (byte) -(RandomSize(2) + 1);
                            }
                        } else {
                            if (--velx[i] <= destvelx[i]) {
                                destvelx[i] = (byte) (RandomSize(2) + 1);
                            }
                        }

                        int x = (curx[i] >> 8) + velx[i];
                        if (x < 97) {
                            if (x < -96) {
                                x = 0;
                                velx[i] = 0;
                            }
                        } else {
                            x = 0;
                            velx[i] = 0;
                        }

                        int y = (cury[i] >> 8) + vely[i];
                        if (y < 106) {
                            if (y < -105) {
                                y = 0;
                                vely[i] = 0;
                            }
                        } else {
                            y = 0;
                            vely[i] = 0;
                        }

                        curx[i] =  (x << 8);
                        cury[i] =  (y << 8);

                        data591[212 * (x + 97) + 106 + y] = pixelval[i++];
                    }
                    pic591.invalidate();
                }

                if (nHeadStage == 1) {
                    if (pSpiritSprite.getXrepeat() > nSpiritRepeatX) {
                        pSpiritSprite.setXrepeat(pSpiritSprite.getXrepeat() - 2);
                        if (pSpiritSprite.getXrepeat() < nSpiritRepeatX) {
                            pSpiritSprite.setXrepeat(nSpiritRepeatX);
                        }
                    }
                    if (pSpiritSprite.getYrepeat() > nSpiritRepeatY) {
                        pSpiritSprite.setYrepeat(pSpiritSprite.getYrepeat() - 2);
                        if (pSpiritSprite.getYrepeat() < nSpiritRepeatY) {
                            pSpiritSprite.setYrepeat(nSpiritRepeatY);
                        }
                    }

                    int nCount = 0;
                    byte[] data591 = pic591.getBytes();
                    for (int i = 0; i < nPixels; i++) {
                        int dx, dy;
                        if (origx[i] << 8 == curx[i] || klabs((origx[i] << 8) - curx[i]) >= 8) {
                            dx = ((origx[i] << 8) - curx[i]) >> 3;
                        } else {
                            dx = 0;
                            curx[i] =  (origx[i] << 8);
                        }

                        if (origy[i] << 8 == cury[i] || klabs((origy[i] << 8) - cury[i]) >= 8) {
                            dy = ((origy[i] << 8) - cury[i]) >> 3;
                        } else {
                            dy = 0;
                            cury[i] =  (origy[i] << 8);
                        }

                        if ((dx | dy) != 0) {
                            curx[i] +=  dx;
                            cury[i] +=  dy;
                            nCount++;
                        }

                        data591[((cury[i] >> 8) + (212 * ((curx[i] >> 8) + 97))) + 106] = pixelval[i];
                    }

                    if (engine.getTotalClock() - lHeadStartClock > 600) {
                        CopyHeadToWorkTile(590);
                    }

                    if (nCount < (15 * nPixels) / 16) {
                        SoundBigEntrance();
                        AddGlow(pSpiritSprite.getSectnum(), 20);
                        AddFlash(pSpiritSprite.getSectnum(), pSpiritSprite.getX(), pSpiritSprite.getY(), pSpiritSprite.getZ(), 128);
                        nHeadStage = 3;
                        TintPalette(63, 63, 63);
                        CopyHeadToWorkTile(592);
                    }

                    pic591.invalidate();
                }
                break;
        }
    }

    private static void CopyHeadToWorkTile(int nTile) {
        ArtEntry src = engine.getTile(nTile);
        if (!(src instanceof DynamicArtEntry) || !src.exists()) {
            src = engine.allocatepermanenttile(src);
            if (!src.exists()) {
                return;
            }
        }

        int workptr = 10441;
        int srcptr = 0;
        ArtEntry pic = engine.getTile(591);
        if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
            return;
        }

        for (int i = 0; i < 97; i++) {
            System.arraycopy(src.getBytes(), srcptr, pic.getBytes(), workptr, 106);
            workptr += 212;
            srcptr += 106;
        }
    }

    private static void DimSector(int sectnum) {
        Sector pSector = boardService.getSector(sectnum);
        if (pSector == null) {
            return;
        }

        int w = pSector.getWallptr();
        for (int i = 0; i < pSector.getWallnum(); i++, w++) {
            Wall wall = boardService.getWall(w);
            if (wall != null && wall.getShade() < 40) {
                wall.setShade(wall.getShade() + 1);
            }
        }

        if (pSector.getFloorshade() < 40) {
            pSector.setFloorshade(pSector.getFloorshade() + 1);
        }

        if (pSector.getCeilingshade() < 40) {
            pSector.setCeilingshade(pSector.getCeilingshade() + 1);
        }
    }

}
