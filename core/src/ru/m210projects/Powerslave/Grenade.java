// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import java.io.InputStream;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Powerslave.Type.GrenadeStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Bullet.BulletInfo;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Random.RandomByte;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;
import static ru.m210projects.Powerslave.Weapons.SelectNewWeapon;

public class Grenade {

    public static final int MAX_GRENADES = 50;
    public static final int[] GrenadeFree = new int[MAX_GRENADES];
    public static final int[] nGrenadePlayer = new int[MAX_GRENADES];
    public static final GrenadeStruct[] GrenadeList = new GrenadeStruct[MAX_GRENADES];
    private static int nGrenadeCount;
    private static int nGrenadesFree;

    public static void InitGrenades() {
        nGrenadeCount = 0;
        for (short i = 0; i < MAX_GRENADES; i++) {
            GrenadeFree[i] = i;
            if (GrenadeList[i] == null) {
                GrenadeList[i] = new GrenadeStruct();
            }
        }
        nGrenadesFree = MAX_GRENADES;
    }

    public static void saveGrenades(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nGrenadeCount);
        StreamUtils.writeShort(os,  nGrenadesFree);

        for (int i = 0; i < MAX_GRENADES; i++) {
            StreamUtils.writeShort(os, GrenadeFree[i]);
            StreamUtils.writeShort(os, nGrenadePlayer[i]);
            GrenadeList[i].save(os);
        }
    }

    public static void loadGrenades(SafeLoader loader) {
        nGrenadeCount = loader.nGrenadeCount;
        nGrenadesFree = loader.nGrenadesFree;
        System.arraycopy(loader.GrenadeFree, 0, GrenadeFree, 0, MAX_GRENADES);
        System.arraycopy(loader.nGrenadePlayer, 0, nGrenadePlayer, 0, MAX_GRENADES);

        for (int i = 0; i < MAX_GRENADES; i++) {
            if (GrenadeList[i] == null) {
                GrenadeList[i] = new GrenadeStruct();
            }
            GrenadeList[i].copy(loader.GrenadeList[i]);
        }
    }

    public static void loadGrenades(SafeLoader loader, InputStream is) throws IOException {
        loader.nGrenadeCount = StreamUtils.readShort(is);
        loader.nGrenadesFree = StreamUtils.readShort(is);

        for (int i = 0; i < MAX_GRENADES; i++) {
            loader.GrenadeFree[i] =  StreamUtils.readShort(is);
            loader.nGrenadePlayer[i] =  StreamUtils.readShort(is);
            if (loader.GrenadeList[i] == null) {
                loader.GrenadeList[i] = new GrenadeStruct();
            }
            loader.GrenadeList[i].load(is);
        }
    }

    public static int GrabGrenade() {
        return GrenadeFree[--nGrenadesFree];
    }

    public static void DestroyGrenade(int a1) {
        Sprite pSprite = boardService.getSprite(GrenadeList[a1].nSprite);
        if (pSprite == null) {
            return;
        }

        DoSubRunRec(GrenadeList[a1].field_6);
        SubRunRec(GrenadeList[a1].field_8);
        DoSubRunRec(pSprite.getLotag() - 1);
        engine.mydeletesprite(GrenadeList[a1].nSprite);
        GrenadeFree[nGrenadesFree] =  a1;
        nGrenadesFree++;
    }

    public static void BounceGrenade(int a1, int a2) {
        GrenadeList[a1].field_10 >>= 1;
        GrenadeList[a1].xvel = GrenadeList[a1].field_10 * (EngineUtils.sin((a2 + 512) & 0x7FF) >> 5);
        GrenadeList[a1].yvel = GrenadeList[a1].field_10 * (EngineUtils.sin(a2 & 0x7FF) >> 5);
        D3PlayFX(StaticSound[3], GrenadeList[a1].nSprite);
    }

    public static void ThrowGrenade(int a1, int ignored1, int ignored2, int a4, int a5) {
        int v7 = nPlayerGrenade[a1];
        if (v7 >= 0) {
            int nPlayer = PlayerList[a1].spriteId;
            int nGrenade = GrenadeList[v7].nSprite;
            Sprite pGrenade = boardService.getSprite(nGrenade);
            Sprite pPlayerSpr = boardService.getSprite(nPlayer);
            if (pGrenade == null || pPlayerSpr == null) {
                return;
            }

            engine.mychangespritesect(nGrenade, nPlayerViewSect[a1]);

            pGrenade.setX(pPlayerSpr.getX());
            pGrenade.setY(pPlayerSpr.getY());
            pGrenade.setZ(pPlayerSpr.getZ());
            pGrenade.setAng(pPlayerSpr.getAng());

            pGrenade.setCstat(pGrenade.getCstat() & 0x7FF);

            if (a5 >= -3000) {
                GrenadeList[v7].field_10 = 32 * ((cfg.bGrenadeFix && !isOriginal()) ? (90 - GrenadeList[v7].field_E) : totalvel[a1])
                        + (90 - GrenadeList[v7].field_E) * (90 - GrenadeList[v7].field_E);

                pGrenade.setZvel( (-64 * a5 - 4352));
                int dist = 8 * pPlayerSpr.getClipdist();
                int hitMove = engine.movesprite(nGrenade, dist * EngineUtils.sin((pPlayerSpr.getAng() + 512) & 0x7FF),
                        EngineUtils.sin(pPlayerSpr.getAng() & 0x7FF) * dist, a4, 0, 0, 1);
                if ((hitMove & PS_HIT_TYPE_MASK) == PS_HIT_WALL) {
                    BounceGrenade(v7, engine.GetWallNormal(hitMove & PS_HIT_INDEX_MASK));
                }
            } else {
                GrenadeList[v7].field_10 = 0;
                pGrenade.setZvel(pPlayerSpr.getZvel());
            }

            GrenadeList[v7].xvel = GrenadeList[v7].field_10 * (EngineUtils.sin((pPlayerSpr.getAng() + 512) & 0x7FF) >> 4);
            GrenadeList[v7].yvel = GrenadeList[v7].field_10 * (EngineUtils.sin(pPlayerSpr.getAng() & 0x7FF) >> 4);
            nPlayerGrenade[a1] = -1;
        }
    }

    public static void BuildGrenade(int a1) {
        if (nGrenadesFree != 0) {
            int nGrenade = GrabGrenade();
            int nPlayer = PlayerList[a1].spriteId;
            int spr = engine.insertsprite(nPlayerViewSect[a1],  201);
            Sprite pPlayerSpr = boardService.getSprite(nPlayer);
            Sprite pSprite = boardService.getSprite(spr);
            if (pPlayerSpr == null || pSprite == null) {
                return;
            }

            pSprite.setX(pPlayerSpr.getX());
            pSprite.setY(pPlayerSpr.getY());
            pSprite.setZ(pPlayerSpr.getZ() - 3840);
            pSprite.setShade(-64);
            pSprite.setXrepeat(20);
            pSprite.setYrepeat(20);
            pSprite.setCstat(32768);
            pSprite.setPicnum(1);
            pSprite.setPal(0);
            pSprite.setClipdist(30);
            pSprite.setXoffset(0);
            pSprite.setYoffset(0);
            pSprite.setAng(pPlayerSpr.getAng());
            pSprite.setYvel(0);
            pSprite.setOwner(nPlayer);
            pSprite.setXvel(0);
            pSprite.setZvel(0);
            pSprite.setHitag(0);
            pSprite.setLotag( (HeadRun() + 1));
            pSprite.setExtra(-1);

            GrenadeList[nGrenade].field_E = 90;
            GrenadeList[nGrenade].field_2 = 0;
            GrenadeList[nGrenade].field_0 = 16;
            GrenadeList[nGrenade].field_10 = -1;
            GrenadeList[nGrenade].nSprite =  spr;
            GrenadeList[nGrenade].ActionSeq = 0;
            GrenadeList[nGrenade].field_C = 0;
            GrenadeList[nGrenade].field_6 =  AddRunRec(pSprite.getLotag() - 1, nEvent15 | nGrenade);
            GrenadeList[nGrenade].field_8 =  AddRunRec(NewRun, nEvent15 | nGrenade);
            nGrenadePlayer[nGrenade] =  a1;
            nPlayerGrenade[a1] =  nGrenade;
        }

    }

    public static void ExplodeGrenade(int a1) {
        int nPlayer = nGrenadePlayer[a1];
        int spr = GrenadeList[a1].nSprite;
        GrenadeList[a1].field_C = 1;
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            return;
        }

        int v16, v18;
        short sectnum = pSprite.getSectnum();
        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        if ((SectFlag[sectnum] & 0x2000) != 0) {
            v16 = 75;
            v18 = 60;
        } else if (pSprite.getZ() >= sec.getFloorz()) {
//			MonoOut(v14, v15); "GRENBOOM\n"
            v16 = 34;
            v18 = 150;
        } else {
//			MonoOut(v14, 36); "GRENPOW\n"
            v16 = 36;
            v18 = 200;
        }

        if (GrenadeList[a1].field_10 < 0) {
            int nPlayerSpr = PlayerList[nPlayer].spriteId;
            Sprite pPlayerSpr = boardService.getSprite(nPlayerSpr);
            if (pPlayerSpr != null) {
                pSprite.setZ(pPlayerSpr.getZ());
                pSprite.setX(pPlayerSpr.getX() + (EngineUtils.sin((pPlayerSpr.getAng() + 512) & 0x7FF) >> 5));
                pSprite.setY(pPlayerSpr.getY() + (EngineUtils.sin(pPlayerSpr.getAng() & 0x7FF) >> 5));
                engine.changespritesect(spr, pPlayerSpr.getSectnum());
                if (PlayerList[nPlayer].invisibility == 0) {
                    PlayerList[nPlayer].HealthAmount = 1;
                }
            }
        }

        int force = BulletInfo[4].force;
        if (nPlayerDouble[nPlayer] > 0) {
            force = 2 * BulletInfo[4].force;
        }
        RadialDamageEnemy(spr, force, BulletInfo[4].inf_10);
        BuildAnim(-1, v16, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), v18, 4);
        AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 128);
        nGrenadePlayer[a1] = -1;
        DestroyGrenade(a1);
    }


    public static void FuncGrenade(int nStack, int ignored, int RunPtr) {
        int nGrenade = RunData[RunPtr].getObject();
        if (nGrenade < 0 || nGrenade >= MAX_GRENADES) {
            throw new AssertException("nGrenade>=0 && nGrenade<MAX_GRENADES");
        }

        int nSeq;
        final int nSprite = GrenadeList[nGrenade].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (GrenadeList[nGrenade].field_C != 0) {
            nSeq = SeqOffsets[34];
        } else {
            nSeq = GrenadeList[nGrenade].ActionSeq + SeqOffsets[33];
        }

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int v10 = GrenadeList[nGrenade].field_2 >> 8;
                MoveSequence(nSprite, nSeq, v10);
                pSprite.setPicnum( GetSeqPicnum2(nSeq, v10));
                GrenadeList[nGrenade].field_E--;
                if (GrenadeList[nGrenade].field_E == 0) {
                    int nPlayer = nGrenadePlayer[nGrenade];
                    if (GrenadeList[nGrenade].field_10 < 0) {
                        PlayerList[nPlayer].weaponState = 0;
                        PlayerList[nPlayer].seqOffset = 0;
                        if (PlayerList[nPlayer].AmmosAmount[4] != 0) {
                            PlayerList[nPlayer].weaponFire = 0;
                        } else {
                            SelectNewWeapon(nPlayer);
                            PlayerList[nPlayer].currentWeapon = PlayerList[nPlayer].newWeapon;
                            PlayerList[nPlayer].newWeapon = -1;
                        }
                    }
                    ExplodeGrenade(nGrenade);
                    return;
                }
                if (GrenadeList[nGrenade].field_10 < 0) {
                    return;
                }

                GrenadeList[nGrenade].field_2 += GrenadeList[nGrenade].field_0;
                int v19 = GrenadeList[nGrenade].field_2 >> 8;
                if (v19 < 0) {
                    GrenadeList[nGrenade].field_2 += (SeqSize[nSeq] << 8);
                } else if (v19 >= SeqSize[nSeq]) {
                    if (GrenadeList[nGrenade].field_C != 0) {
                        DestroyGrenade(nGrenade);
                        return;
                    }
                    GrenadeList[nGrenade].field_2 = 0;
                }

                if (GrenadeList[nGrenade].field_C != 0) {
                    return;
                }

                int zvel = pSprite.getZvel();

                Gravity(nSprite);

                int hitMove = engine.movesprite(nSprite, GrenadeList[nGrenade].xvel, GrenadeList[nGrenade].yvel, pSprite.getZvel(),
                        pSprite.getClipdist() >> 1, pSprite.getClipdist() >> 1, 1);

                if (hitMove == 0) {
                    return;
                }

                if ((hitMove & nEventProcess) != 0) {
                    if (zvel != 0) {
                        if (SectDamage[pSprite.getSectnum()] > 0) {
                            ExplodeGrenade(nGrenade);
                            return;
                        }
                        GrenadeList[nGrenade].field_0 =  totalmoves;
                        D3PlayFX(StaticSound[3], nSprite);
                        pSprite.setZvel( -(zvel >> 1));
                        if (pSprite.getZvel() > -1280) {
                            D3PlayFX(StaticSound[5], nSprite);
                            GrenadeList[nGrenade].field_0 = 0;
                            GrenadeList[nGrenade].field_2 = 0;
                            pSprite.setZvel(0);
                            GrenadeList[nGrenade].ActionSeq = 1;
                        }
                    }
                    GrenadeList[nGrenade].field_0 =  (255 - 2 * RandomByte());
                    GrenadeList[nGrenade].xvel = GrenadeList[nGrenade].xvel - (GrenadeList[nGrenade].xvel >> 4);
                    GrenadeList[nGrenade].yvel = GrenadeList[nGrenade].yvel - (GrenadeList[nGrenade].yvel >> 4);
                }

                switch (hitMove & PS_HIT_TYPE_MASK) {
                    case PS_HIT_WALL:
                        BounceGrenade(nGrenade, engine.GetWallNormal(hitMove & PS_HIT_INDEX_MASK));
                        GrenadeList[nGrenade].field_2 = 0;
                        return;
                    case PS_HIT_SPRITE:
                        Sprite hitSprite = boardService.getSprite(hitMove & PS_HIT_INDEX_MASK);
                        if (hitSprite != null) {
                            BounceGrenade(nGrenade, hitSprite.getAng());
                            GrenadeList[nGrenade].field_2 = 0;
                        }
                        return;
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, nSeq, GrenadeList[nGrenade].field_2 >> 8, 1);
                return;
            case nEventRadialDamage:
                if (nSprite != nRadialSpr && GrenadeList[nGrenade].field_C == 0) {
                    if (CheckRadialDamage(nSprite) > 280) {
                        GrenadeList[nGrenade].field_E =  (RandomSize(4) + 1);
                    }
                }
        }
    }
}
