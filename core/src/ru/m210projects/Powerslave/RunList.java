// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Enemies.*;
import ru.m210projects.Powerslave.Type.Channel;
import ru.m210projects.Powerslave.Type.FuncProc;
import ru.m210projects.Powerslave.Type.RunData;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Globals.*;

public class RunList {

    public static final int MAXRUN = 25600;
    public static int ChannelList;
    public static int ChannelLast;
    public static final Channel[] channel = new Channel[4096];
    public static int NewRun;
    public static int RunCount = -1;
    public static int RunChain;
    public static boolean word_966BE;
    public static int nStackCount;
    public static final int[] RunFree = new int[MAXRUN];
    public static final RunData[] RunData = new RunData[MAXRUN];
    public static final int[] sRunStack = new int[200];

    public static int GrabRun() {
        if (RunCount <= 0 || RunCount > MAXRUN) {
            throw new AssertException("RunCount >0 && RunCount<=MAXRUN");
        }
        return RunFree[--RunCount];
    }    
    
    private static final FuncProc[] funclist = new FuncProc[] {
            Object::FuncElev,
            Switch::FuncSwReady,
            Switch::FuncSwPause,
            Switch::FuncSwStepOn,
            Switch::FuncSwNotOnPause,
            Switch::FuncSwPressSector,
            Switch::FuncSwPressWall,
            Object::FuncWallFace,
            Slide::FuncSlide,
            Anubis::FuncAnubis,
            Player::FuncPlayer,
            Bullet::FuncBullet,
            Spider::FuncSpider,
            Sprites::FuncCreatureChunk,
            Mummy::FuncMummy,
            Grenade::FuncGrenade,
            Anim::FuncAnim,
            Snake::FuncSnake,
            Fish::FuncFish,
            Lion::FuncLion,
            Sprites::FuncBubble,
            LavaDude::FuncLava,
            LavaDude::FuncLavaLimb,
            Object::FuncObject,
            Rex::FuncRex,
            Set::FuncSet,
            Queen::FuncQueen,
            Queen::FuncQueenHead,
            Roach::FuncRoach,
            Queen::FuncQueenEgg,
            Wasp::FuncWasp,
            Object::FuncTrap,
            Fish::FuncFishLimb,
            ru.m210projects.Powerslave.Enemies.Ra::FuncRa,
            Scorp::FuncScorp,
            Set::FuncSoul,
            Rat::FuncRat,
            PSSector::FuncEnergyBlock,
            Object::FuncSpark
    };

    public static void saveRunList(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, RunCount);
        StreamUtils.writeInt(os, NewRun);
        StreamUtils.writeInt(os, RunChain);

        for (int i = 0; i < MAXRUN; i++) {
            StreamUtils.writeShort(os, RunFree[i]);
        }
        for (int i = 0; i < MAXRUN; i++) {
            RunData[i].save(os);
        }

        StreamUtils.writeInt(os, ChannelList);
        StreamUtils.writeInt(os, ChannelLast);
        for (Channel value : channel) {
            value.save(os);
        }
    }

    public static void loadRunList(SafeLoader loader, InputStream is) throws IOException {
        loader.RunCount = StreamUtils.readInt(is);
        loader.NewRun = StreamUtils.readInt(is);
        loader.RunChain = StreamUtils.readInt(is);

        for (int i = 0; i < MAXRUN; i++) {
            loader.RunFree[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAXRUN; i++) {
            if (loader.RunData[i] == null) {
                loader.RunData[i] = new RunData();
            }
            loader.RunData[i].load(is);
        }

        loader.ChannelList = StreamUtils.readInt(is);
        loader.ChannelLast = StreamUtils.readInt(is);

        for (int i = 0; i < channel.length; i++) {
            if (loader.channel[i] == null) {
                loader.channel[i] = new Channel();
            }
            loader.channel[i].load(is);
        }
    }

    public static void loadRunList(SafeLoader loader) {
        RunCount = loader.RunCount;
        NewRun = loader.NewRun;
        RunChain = loader.RunChain;

        System.arraycopy(loader.RunFree, 0, RunFree, 0, MAXRUN);
        for (int i = 0; i < MAXRUN; i++) {
            if (RunData[i] == null) {
                RunData[i] = new RunData();
            }
            RunData[i].copy(loader.RunData[i]);
        }

        ChannelList = loader.ChannelList;
        ChannelLast = loader.ChannelLast;

        for (int i = 0; i < channel.length; i++) {
            if (channel[i] == null) {
                channel[i] = new Channel();
            }
            channel[i].copy(loader.channel[i]);
        }
    }

    public static void FreeRun(int run) {
        if (RunCount < 0 || RunCount >= MAXRUN) {
            throw new AssertException("RunCount >= 0 && RunCount<MAXRUN: " + RunCount);
        }
        if (run < 0 || run >= MAXRUN) {
            throw new AssertException("run>=0 && run<MAXRUN: " + run);
        }

        RunFree[RunCount] =  run;
        RunData[run].clear();
        RunCount++;
    }

    public static int HeadRun() {
        int run = GrabRun();
        RunData[run].RunPtr = -1;
        RunData[run].RunNum = -1;
        return run;
    }

    public static void InitRun() {
        RunCount = MAXRUN;
        nStackCount = 0;
        for (int i = 0; i < MAXRUN; ++i) {
            if (RunData[i] == null) {
                RunData[i] = new RunData();
            }
            RunFree[i] = i;
            RunData[i].clear();
        }
        int ptr = HeadRun();
        RunChain = ptr;
        NewRun = ptr;

        for (int i = 0; i < 8; i++) {
            PlayerList[i].RunFunc = -1;
        }

        nRadialSpr = -1;
    }

    public static void UnlinkRun(int RunNum) {
        if (RunCount < 0 || RunCount >= MAXRUN) {
            throw new AssertException("RunNum >= 0 && RunNum<MAXRUN: " + RunCount);
        }

        RunData data = RunData[RunNum];
        if (RunNum == RunChain) {
            RunChain = data.RunPtr;
        } else {
            if (data.RunNum >= 0) {
                RunData[data.RunNum].RunPtr = data.RunPtr;
            }
            if (data.RunPtr >= 0) {
                RunData[data.RunPtr].RunNum = data.RunNum;
            }
        }
    }

    public static void InsertRun(int RunLst, int RunNum) {
        if (RunLst < 0 || RunLst >= MAXRUN) {
            throw new AssertException("RunLst >= 0 && RunLst<MAXRUN: " + RunLst);
        }
        if (RunNum < 0 || RunNum >= MAXRUN) {
            throw new AssertException(" RunNum >= 0 && RunNum<MAXRUN: " + RunNum);
        }

        RunData data = RunData[RunNum];
        data.RunNum =  RunLst;
        data.RunPtr = RunData[RunLst].RunPtr;
        if (data.RunPtr >= 0) {
            RunData[data.RunPtr].RunNum = RunNum;
        }
        RunData[RunLst].RunPtr = RunNum;
    }

    public static int AddRunRec(int RunLst, int RunEvent) {
        int RunNum = GrabRun();
        RunData[RunNum].setEvent(RunEvent);
        InsertRun(RunLst, RunNum);
        return RunNum;
    }

    public static void DoSubRunRec(int RunPtr) {
        if (RunPtr < 0 || RunPtr >= MAXRUN) {
            throw new AssertException("RunPtr>=0 && RunPtr<MAXRUN: " + RunPtr);
        }
        UnlinkRun(RunPtr);
        FreeRun(RunPtr);
    }

    public static void CleanRunRecs() {
        int NxtPtr = RunChain;
        if (NxtPtr >= 0) {
            if (NxtPtr >= MAXRUN) {
                throw new AssertException("NxtPtr<MAXRUN: " + NxtPtr);
            }
            NxtPtr = RunData[RunChain].RunPtr;
        }
        while (true) {
            int RunPtr = NxtPtr;
            if (RunPtr < 0) {
                return;
            }
            if (RunPtr >= MAXRUN) {
                break;
            }
            NxtPtr = RunData[NxtPtr].RunPtr;
            if ((RunData[RunPtr].getFunc()) < 0) {
                DoSubRunRec(RunPtr);
            }
        }
        throw new AssertException("RunPtr<MAXRUN");
    }

    public static void SubRunRec(int RunPtr) {
        if (RunPtr < 0 || RunPtr >= MAXRUN) {
            throw new AssertException("RunPtr>=0 && RunPtr<MAXRUN: " + RunPtr);
        }
        RunData[RunPtr].setEvent(-totalmoves);
    }

    public static void SendMessageToRunRec(int RunPtr, int nStack, int nDamage) {
        int func = RunData[RunPtr].getFunc();
        if (func >= 0 && func <= funclist.length) {
            funclist[func].run(nStack, nDamage, RunPtr);
        }
    }

    public static void ExplodeSignalRun() {
        int NxtPtr = RunChain;
        if (NxtPtr >= 0) {
            if (NxtPtr >= MAXRUN) {
                throw new AssertException("NxtPtr<MAXRUN: " + NxtPtr);
            }
            NxtPtr = RunData[NxtPtr].RunPtr;
        }
        while (true) {
            int RunPtr = NxtPtr;
            if (RunPtr < 0) {
                return;
            }
            if (RunPtr >= MAXRUN) {
                break;
            }

            NxtPtr = RunData[NxtPtr].RunPtr;
            if (RunData[RunPtr].getObject() >= 0) {
                SendMessageToRunRec(RunPtr, nEventRadialDamage, 0);
            }
        }
        throw new AssertException("RunPtr<MAXRUN");
    }

    public static void PushMoveRun(int nStack) {
        if (nStackCount < 200) {
            sRunStack[nStackCount] = nStack;
            nStackCount++;
        }
    }

    public static int PopMoveRun() {
        if (nStackCount <= 0) {
            throw new AssertException(" PopMoveRun() called inappropriately");
        }
        return sRunStack[--nStackCount];
    }

    public static void SignalRun(int RunNum, int nStack) {
        int NxtPtr = RunNum;
        if (NxtPtr == RunChain && word_966BE) {
            PushMoveRun(nStack);
        } else {
            while (true) {
                word_966BE = true;
                if (NxtPtr >= 0) {
                    if (NxtPtr >= MAXRUN) {
                        throw new AssertException("NxtPtr<MAXRUN");
                    }
                    NxtPtr = RunData[NxtPtr].RunPtr;
                }
                while (true) {
                    int RunPtr = NxtPtr;
                    if (RunPtr < 0) {
                        break;
                    }
                    if (RunPtr >= MAXRUN) {
                        throw new AssertException("RunPtr<MAXRUN");
                    }
                    NxtPtr = RunData[NxtPtr].RunPtr;
                    if (RunData[RunPtr].getObject() >= 0) {
                        SendMessageToRunRec(RunPtr, nStack, 0);
                    }
                }

                word_966BE = false;
                if (nStackCount == 0) {
                    break;
                }
                nStack = PopMoveRun();
                NxtPtr = RunChain;
            }
        }
    }

    public static void InitChan() {
        ChannelList = -1;
        ChannelLast = -1;
        for (int i = 0; i < channel.length; i++) {
            if (channel[i] == null) {
                channel[i] = new Channel();
            }
            channel[i].head = HeadRun();
            channel[i].next = -1;
            channel[i].field_4 = -1;
            channel[i].field_6 = 0;
        }
    }

    public static void ChangeChannel(int ch, int a2) {
        if (channel[ch].next < 0) {
            int tmp =  ChannelList;
            ChannelList = ch;
            channel[ch].next = tmp;
        }

        channel[ch].field_4 =  a2;
        channel[ch].field_6 |= 2;
    }

    public static void ReadyChannel(int ch) {
        if (channel[ch].next < 0) {
            int tmp =  ChannelList;
            ChannelList = ch;
            channel[ch].next = tmp;
        }
        channel[ch].field_6 |= 1;
    }

    public static void ProcessChannels() {
        int ch1, ch2;
        do {
            ch2 = -1;
            ch1 = -1;
            while (ChannelList >= 0) {
                Channel v2 = channel[ChannelList];
                int v3 = v2.field_6;
                int next = v2.next;
                if ((v3 & 2) != 0) {
                    v2.field_6 ^= 2;
                    SignalRun(v2.head, ChannelList | nEvent1);
                }
                if ((v3 & 1) != 0) {
                    v2.field_6 ^= 1;
                    SignalRun(v2.head, nEvent3);
                }
                if (v2.field_6 != 0) {
                    if (ch1 == -1) {
                        ch1 = ChannelList;
                    } else {
                        channel[ch2].next =  ChannelList;
                    }
                    ch2 = ChannelList;
                } else {
                    channel[ChannelList].next = -1;
                }
                ChannelList = next;
            }
            ChannelList = ch1;
        } while (ch1 != -1);
    }

    public static int FindChannel(int ch) {
        for (int i = 0; i < channel.length; i++) {
            if (channel[i].field_4 == -1) {
                channel[i].field_4 =  ch;
                return i;
            }
        }
        return -1;
    }

    public static int AllocChannel(int ch) {
        if (ch != 0) {
            for (int i = 0; i < channel.length; i++) {
                if (channel[i].field_4 == ch) {
                    return i;
                }
            }
        }
        return FindChannel(ch);
    }

    public static void ExecObjects() {
        ProcessChannels();
        SignalRun(RunChain, nEventProcess);
    }
}
