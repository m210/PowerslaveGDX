package ru.m210projects.Powerslave.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

import static ru.m210projects.Powerslave.Globals.BACKGROUND;

public class PSMessageScreen extends MessageScreen {

    public PSMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(1), game.getFont(3), type);

        for (MenuItem item : messageItems) {
            item.y += 30;
        }

        for (MenuItem item : variantItems) {
            item.y += 20;
        }
    }

    @Override
    public void drawBackground(Renderer renderer) {
        renderer.clearview(96);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, BACKGROUND, -128,
                0, 2 | 8 | 64);
    }
}
