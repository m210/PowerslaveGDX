// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Powerslave.Config.PsKeys;
import ru.m210projects.Powerslave.Factory.PSMenuHandler;
import ru.m210projects.Powerslave.Main;
import ru.m210projects.Powerslave.Menus.MenuInterfaceSet;
import ru.m210projects.Powerslave.Sound;
import ru.m210projects.Powerslave.Type.EpisodeInfo;
import ru.m210projects.Powerslave.Type.Input;
import ru.m210projects.Powerslave.Type.PlayerStruct;

import java.util.Arrays;

import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Render.AbstractRenderer.DEFAULT_SCREEN_FADE;
import static ru.m210projects.Powerslave.Cinema.*;
import static ru.m210projects.Powerslave.Energy.DoEnergyTile;
import static ru.m210projects.Powerslave.Factory.PSInput.nPause;
import static ru.m210projects.Powerslave.Factory.PSMenuHandler.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.*;
import static ru.m210projects.Powerslave.LoadSave.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.*;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.checkEpisodeResources;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.resetEpisodeResources;
import static ru.m210projects.Powerslave.Object.*;
import static ru.m210projects.Powerslave.Palette.*;
import static ru.m210projects.Powerslave.Player.*;
import static ru.m210projects.Powerslave.RunList.CleanRunRecs;
import static ru.m210projects.Powerslave.RunList.ExecObjects;
import static ru.m210projects.Powerslave.PSSector.DoMovingSects;
import static ru.m210projects.Powerslave.Snake.SnakeList;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.SpiritHead.DoSpiritHead;
import static ru.m210projects.Powerslave.SpiritHead.nHeadStage;
import static ru.m210projects.Powerslave.Sprites.DoBubbleMachines;
import static ru.m210projects.Powerslave.View.*;
import static ru.m210projects.Powerslave.Weapons.DrawWeapons;
import static ru.m210projects.Powerslave.Weapons.bobangle;

public class GameScreen extends GameAdapter {

    public int gNameShowTime;
    private final Main game;
    protected final boolean[] gameKeyState;
    private int nonsharedtimer;

    public GameScreen(Main game) {
        super(game, gLoadingScreen);
        this.game = game;
        this.gameKeyState = new boolean[game.pCfg.getKeymap().length + 1];
    }

    public static void DoClockBeep() {
        for (ListNode<Sprite> node = boardService.getStatNode(407); node != null; node = node.getNext()) {
            PlayFX2(StaticSound[74], node.getIndex());
        }
    }

    public static void InitClockTile() {
        ArtEntry artEntry = engine.allocatepermanenttile(engine.getTile(3603));
        Arrays.fill(artEntry.getBytes(), (byte) 0xFF); // clear to mask color
    }

    public static void DrawClock() {
        int nCount = lCountDown / 30;
        if (nCount != nClockVal) {
            ArtEntry dst = engine.getTile(3603);
            if (!(dst instanceof DynamicArtEntry) || !dst.exists()) {
                dst = engine.allocatepermanenttile(dst);
                if (!dst.hasSize()) {
                    return;
                }
            }
            Arrays.fill(dst.getBytes(), (byte) 255);

            nClockVal = nCount;
            DoClockBeep();

            int x = 49;
            while (nCount != 0) {
                int nNumber = 3606 + (nCount & 0xF);
                ArtEntry pic = engine.getTile(nNumber);
                if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                    pic = engine.allocatepermanenttile(pic);
                    if (!pic.hasSize()) {
                        return;
                    }
                }

                CopyTileToBitmap((DynamicArtEntry) pic, (DynamicArtEntry) dst, x - (pic.getWidth() / 2), 32 - (pic.getHeight() / 2));
                x -= 15;
                nCount /= 16;
            }
            ((DynamicArtEntry) dst).invalidate();
        }

        DoEnergyTile();
    }

    private static void DoRedAlert(int a1) {
        if (a1 != 0) {
            nAlarmTicks = 69;
            nRedTicks = 30;
        }
        for (ListNode<Sprite> node = boardService.getStatNode(405); node != null; node = node.getNext()) {
            if (a1 != 0) {
                Sprite spr = node.get();
                PlayFXAtXYZ(StaticSound[73], spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());
                AddFlash(spr.getSectnum(), spr.getX(), spr.getY(), spr.getZ(), 192);
            }
        }
    }

    public static void CopyTileToBitmap(DynamicArtEntry src, DynamicArtEntry dst, int cx, int cy) {
        byte[] pSource = src.getBytes();
        byte[] pDest = dst.getBytes();

        int sptr = 0;
        int dptr = ((dst.getHeight() * cx) + cy);
        for (int i = 0, j; i < src.getWidth(); i++) {
            for (j = 0; j < src.getHeight(); j++) {
                byte col = pSource[sptr++];
                if (col != -1 && (dptr + j) < pDest.length) {
                    pDest[dptr + j] = col;
                }
            }
            dptr += dst.getHeight();
        }
    }

    @Override
    public void show() {
        super.show();

        gDemoScreen.onStopPlaying();

        sndPlayMusic();
    }

    protected void UpdateInputs(BuildNet net) {
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            Input src = (Input) net.gFifoInput[net.gNetFifoTail & 0xFF][i];
            sPlayerInput[i].Copy(src);

            if ((sPlayerInput[i].bits & nPause) != 0) {
                game.gPaused = !game.gPaused;
                sndHandlePause(game.gPaused);
            }
        }
        net.gNetFifoTail++;
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        FixPalette();
        UpdateInputs(net);


        if (game.gPaused || !DemoScreen.isDemoPlaying() && !DemoScreen.isDemoScreen(this) && (game.menu.gShowMenu || Console.out.isShowing())) {
            return;
        }

        UpdateSounds();
        if (levelnum == 20) {
            lCountDown--;
            DrawClock();
            if (nRedTicks != 0) {
                if (--nRedTicks <= 0) {
                    DoRedAlert(0);
                }
            }
            nAlarmTicks--;
            nButtonColor--;
            if (nAlarmTicks <= 0) {
                DoRedAlert(1);
            }

            if (lCountDown <= 0) {
                DoFailedFinalScene();
            }
        }

        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            PlayerList[i].UpdatePlayerLoc();
        }

        UndoFlashes();
        DoLights();
        if (nFreeze != 0) {
            if (gCurrentEpisode != gOriginalEpisode) {
                if (mUserFlag == UserFlag.UserMap) {
                    Gdx.app.postRunnable(() -> {
                        nPlayerLives[0] = 0;
                        game.EndGame();
                    });
                } else {
                    levelnew = levelnum + 1;
                }
            } else {
                if (nFreeze == 1 || nFreeze == 2) {
                    DoSpiritHead();

                    if (cfg.bSubtitles && nFreeze == 2 && nHeadStage == 5 && nHeight + nCrawlY > 0
                            && engine.getTotalClock() >= nextclock) {
                        nextclock = engine.getTotalClock() + 14;
                        nCrawlY--;
                    }
                }
            }
        } else {
            ExecObjects();
            CleanRunRecs();
        }
        MoveStatus();
        DoBubbleMachines();
        DoDrips();
        DoMovingSects();
        DoRegenerates();

        if (followmode) {
            followa += (int) followang;

            followx += (followvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
            followy += (followvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;

            followx += (followsvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;
            followy -= (followsvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
        }

        if (levelnum == 20) {
            DoFinale();
            if (lCountDown < 1800 && nDronePitch < 2400 && lFinaleStart == 0) {
                nDronePitch += 64;
                BendAmbientSound();
            }
        }

        if (totalvel[nLocalPlayer] != 0) {
            bobangle += 56;
            bobangle &= 0x7FF;
        } else {
            bobangle = 0;
        }

        totalmoves++;
        moveframes--;

        checkNextMap();
    }

    @Override
    public void DrawWorld(float smooth) {
        if (PlayerList[nLocalPlayer].spriteId == -1) {
            return;
        }

        Renderer renderer = game.getRenderer();
        Sprite pPlayer = boardService.getSprite(PlayerList[nLocalPlayer].spriteId);
        if (pPlayer == null) {
            return;
        }

        PlayerStruct pp = PlayerList[0]; // screenpeek XXX
        int nSector = -1;

        if (nSnakeCam >= 0) {
            Sprite pSnake = boardService.getSprite(SnakeList[nSnakeCam].nSprite[0]);
            if (pSnake != null) {
                nCamerax = pSnake.getX();
                nCameray = pSnake.getY();
                nCameraz = pSnake.getZ();
                nCameraa = pSnake.getAng();
                nCamerapan = 92;
                nSector = pSnake.getSectnum();

                ILoc oldLoc = game.pInt.getsprinterpolate(SnakeList[nSnakeCam].nSprite[0]);
                if (oldLoc != null) {
                    nCamerax = oldLoc.x + mulscale(nCamerax - oldLoc.x, (int) smooth, 16);
                    nCameray = oldLoc.y + mulscale(nCameray - oldLoc.y, (int) smooth, 16);
                    nCameraz = oldLoc.z + mulscale(nCameraz - oldLoc.z, (int) smooth, 16);
                    nCameraa = oldLoc.ang + mulscale(((pSnake.getAng() - oldLoc.ang + 1024) & 0x7FF) - 1024, (int) smooth, 16);
                }

                engine.getPaletteManager().setGreenPal();
            }
        } else {
            engine.getPaletteManager().restoreGreenPal();
            nCamerax = pPlayer.getX();
            nCameray = pPlayer.getY();
            nCameraz = pPlayer.getZ() + pp.eyelevel;
            nCameraa = pp.ang;
            nCamerapan = pp.horiz;
            nSector = nPlayerViewSect[nLocalPlayer];
        }

        if (nSector != -1 && initsect != -1) {
            UpdateMap();
        }

        if (bCamera) {
            if ((!game.menu.gShowMenu && !Console.out.isShowing()) || DemoScreen.isDemoPlaying()) {
                nCamerax = pp.prevView.x + mulscale((nCamerax - pp.prevView.x), (int) smooth, 16);
                nCameray = pp.prevView.y + mulscale((nCameray - pp.prevView.y), (int) smooth, 16);
                nCameraz = pp.prevView.z + mulscale((nCameraz - pp.prevView.z), (int) smooth, 16);
                nCameraa = pp.prevView.ang
                        + (BClampAngle(nCameraa + 1024 - pp.prevView.ang) - 1024) * smooth / 65536.0f;
            }

            engine.clipmove(nCamerax, nCameray, nCameraz, nSector, (int) (-2000 * BCosAngle(nCameraa)),
                    (int) (-2000 * BSinAngle(nCameraa)), 64, 0, 0, CLIPMASK1);

            nCamerapan = 92;
            if (clipmove_sectnum != -1) {
                nCamerax = clipmove_x;
                nCameray = clipmove_y;
                nCameraz = clipmove_z;
                nSector = clipmove_sectnum;
            }
        } else {
            if (nSnakeCam == -1) {
//				if (screenpeek == myconnectindex && numplayers > 1)
//		    	{
//		       	 	PSNetwork net = game.net;
//		       		nCamerax = net.predictOld.x+mulscale((net.predict.x-net.predictOld.x),smoothratio, 16);
//		       		nCameray = net.predictOld.y+mulscale((net.predict.y-net.predictOld.y),smoothratio, 16);
//		            nCameraa = net.predictOld.ang + (BClampAngle(net.predict.ang+1024-net.predictOld.ang)-1024) * smoothratio / 65536.0f;
//		      	}
//		     	else
                {
                    if ((!game.menu.gShowMenu && !Console.out.isShowing()) || DemoScreen.isDemoPlaying()) {
                        nCamerax = pp.prevView.x + mulscale((nCamerax - pp.prevView.x), (int) smooth, 16);
                        nCameray = pp.prevView.y + mulscale((nCameray - pp.prevView.y), (int) smooth, 16);
                        nCameraz = pp.prevView.z + mulscale((nCameraz - pp.prevView.z), (int) smooth, 16);
                        nCameraa = pp.prevView.ang
                                + (BClampAngle(nCameraa + 1024 - pp.prevView.ang) - 1024) * smooth / 65536.0f;
                        nCamerapan = pp.prevView.horiz + (pp.horiz - pp.prevView.horiz) * smooth / 65536.0f;
                    }
                }

                nCameraz += nQuake[nLocalPlayer];
                Sector sec = boardService.getSector(pPlayer.getSectnum());
                if (sec != null && nCameraz > sec.getFloorz()) {
                    nCameraz = sec.getFloorz();
                }
                nCameraa += (nQuake[nLocalPlayer] >> 7) % 31;
            }
        }

        Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return;
        }

        int cz = sec.getCeilingz() + 256 * 8;
        if (cz > nCameraz) {
            nCameraz = cz;
        }

        int fz = sec.getFloorz() - 256 * 8;
        if (fz < nCameraz) {
            nCameraz = fz;
        }

        nCamerapan += 8; // GDX 14.12.2019

        if (nFreeze != 0 || nOverhead != 2) {
            renderer.drawrooms(nCamerax, nCameray, nCameraz, nCameraa, nCamerapan, nSector);
            analyzesprites((int) smooth);
            renderer.drawmasks();
        }

        if (nFreeze == 0 && nOverhead > 0) {
            if (followmode) {
                nCamerax = followx;
                nCameray = followy;
                nCameraa = followa;
            }

            if (nOverhead == 2) {
                renderer.clearview(96);
                renderer.drawmapview(nCamerax, nCameray, zoom, (int) nCameraa);
            }
            renderer.drawoverheadmap(nCamerax, nCameray, zoom, (short) nCameraa);

            if (followmode) {
                game.getFont(0).drawTextScaled(renderer, 5, 25, "Follow mode", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
            }

            if (mUserFlag == UserFlag.UserMap) {
                game.getFont(0).drawTextScaled(renderer, 5, 15, "User map: " + boardfilename, 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
            } else if (levelnum > 0) {
                if (gCurrentEpisode.gMapInfo.get(levelnum - 1) != null) {
                    game.getFont(0).drawTextScaled(renderer, 5, 15, gCurrentEpisode.gMapInfo.get(levelnum - 1).title, 1.0f, 0, 7,
                            TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
                }
            }
        }
    }

    @Override
    public void DrawHud(float smooth) {
        Renderer renderer = game.getRenderer();
        if (game.gPaused) {
            game.getFont(1).drawTextScaled(renderer, 160, 80, "PAUSE", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        }

        if (nFreeze != 0) {
            nSnakeCam = -1;

            if (nFreeze == 2) {
                if (nHeadStage == 4) {
                    int spr = PlayerList[nLocalPlayer].spriteId;
                    Sprite pFreeze = boardService.getSprite(spr);
                    if (pFreeze == null) {
                        return;
                    }

                    pFreeze.setCstat(pFreeze.getCstat() | 0x8000);
                    int dang = (int) (nCameraa - pFreeze.getAng());
                    if (klabs(dang) > 10) {
                        inita -= dang >> 3;
                        return;
                    }

                    if (cfg.bSubtitles) {
                        if (levelnum == 1) {
                            ReadyCinemaText(1);
                        } else {
                            ReadyCinemaText(5);
                        }
                    }

                    nHeadStage = 5;
                } else {
                    if (cfg.bSubtitles && (!AdvanceCinemaText())) {
                        levelnew = levelnum + 1;
                    }
                }
            }
            return;
        }

        if (nSnakeCam >= 0) {
            game.getFont(0).drawTextScaled(renderer, 160, 5, "S E R P E N T C A M", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
            return;
        }

        if (nOverhead != 2) {
            DrawWeapons();
        }

        if (!game.menu.gShowMenu || (game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            DrawStatus();

            if (game.isCurrentScreen(gGameScreen) && engine.getTotalClock() < gNameShowTime) {
                Transparent transp = Transparent.None;
                if (engine.getTotalClock() > gNameShowTime - 20) {
                    transp = Transparent.Bit1;
                }
                if (engine.getTotalClock() > gNameShowTime - 10) {
                    transp = Transparent.Bit2;
                }

                if (cfg.showMapInfo != 0 && !game.menu.gShowMenu) {
                    if (mUserFlag != UserFlag.UserMap && levelnum > 0) {
                        if (gCurrentEpisode.gMapInfo.get(levelnum - 1) != null) {
                            game.getFont(1).drawTextScaled(renderer, 160, 100, gCurrentEpisode.gMapInfo.get(levelnum - 1).title, 1.0f, -128,
                                    0, TextAlign.Center, transp, ConvertType.Normal, true);
                        }
                    } else if (boardfilename != null) {
                        game.getFont(1).drawTextScaled(renderer, 160, 100, boardfilename.getName(), 1.0f, -128, 0,
                                TextAlign.Center, transp, ConvertType.Normal, true);
                    }
                }
            }

            int y = 155;
            if (cfg.nScreenSize == 0) {
                y = 195;
            }

            viewDrawStats(5, y, cfg.gStatSize);
        }
    }



    @Override
    public void processInput(GameProcessor processor) {
        if (nFreeze == 0 && nOverhead != 0) {
            int j = engine.getTotalClock() - nonsharedtimer;
            nonsharedtimer += j;

            if (processor.isGameKeyPressed(GameKeys.Enlarge_Screen)) {
                zoom += mulscale(j, Math.max(zoom, 256), 6);
            }
            if (processor.isGameKeyPressed(GameKeys.Shrink_Screen)) {
                zoom -= mulscale(j, Math.max(zoom, 256), 6);
            }

            if ((zoom > 2048)) {
                zoom = 2048;
            }
            if ((zoom < 48)) {
                zoom = 48;
            }
        }
    }

    @Override
    public void PostFrame(BuildNet net) {
        PSMenuHandler menu = game.menu;
        if (gAutosaveRequest) {
            if (gClassicMode) {
                if (nSaveName == null) {
                    if (!menu.isOpened(menu.mMenus[SAVE])) {
                        menu.mOpen(menu.mMenus[SAVE], -1);
                    }
                } else {
                    if (captBuffer != null) {
                        savegame(game.getUserDirectory(), nSaveName, nSaveFile.getName());
                        gAutosaveRequest = false;
                    } else {
                        gGameScreen.capture(160, 100);
                    }
                }
            } else {
                if (captBuffer != null) {
                    savegame(game.getUserDirectory(), "[autosave]", "autosave.sav");
                    gAutosaveRequest = false;
                } else {
                    gGameScreen.capture(160, 100);
                }
            }
        }

        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }

        if (nPalDiff != 0) {
            Renderer renderer = game.getRenderer();
            DEFAULT_SCREEN_FADE.set(min(63, rtint) << 2,min(63, gtint) << 2,min(63, btint) << 2, nPalDiff | 128);
            renderer.showScreenFade(DEFAULT_SCREEN_FADE);
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        PSMenuHandler menu = game.menu;

        if (!DemoScreen.isDemoPlaying()) {
            if (nFreeze != 0) {
                if (GameKeys.Menu_Toggle.equals(gameKey)
                        || GameKeys.Open.equals(gameKey)) {
                    levelnew = levelnum + 1;
                }
                return false;
            }
        }

        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            StopAllSounds();
            if (DemoScreen.isDemoPlaying()) {
                menu.mOpen(menu.mMenus[MAIN], -1);
            } else {
                menu.mOpen(menu.mMenus[GAME], -1);
            }
        }

        if (nOverhead != 0) {
            if (PsKeys.Map_Follow_Mode.equals(gameKey)) {
                followmode = !followmode;
                if (followmode) {
                    followx = initx;
                    followy = inity;
                    followa = inita;
                }
                StatusMessage(500, "Follow mode " + (followmode ? "ON" : "OFF"), nLocalPlayer);
            }
        } else {
            if (GameKeys.Enlarge_Screen.equals(gameKey)) {
                if (cfg.nScreenSize > 0) {
                    cfg.nScreenSize = BClipLow(cfg.nScreenSize - 1, 0);
                }
            }
            if (GameKeys.Shrink_Screen.equals(gameKey)) {
                if (cfg.nScreenSize < 2) {
                    cfg.nScreenSize = BClipHigh(cfg.nScreenSize + 1, 2);
                }
            }
        }

        if (GameKeys.Map_Toggle.equals(gameKey)) {
            setOverHead(nOverhead);
        }

        if (PsKeys.Show_SoundSetup.equals(gameKey)) {
            menu.mOpen(menu.mMenus[AUDIO], -1);
        }

        if (PsKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
        }

        if (PsKeys.Toggle_messages.equals(gameKey)) {
            cfg.gShowMessages = !cfg.gShowMessages;
            if (cfg.gShowMessages) {
                StatusMessage(500, "Messages on", nLocalPlayer);
            }
        }

        if (!DemoScreen.isDemoPlaying()) {
            if (PsKeys.Show_SaveMenu.equals(gameKey)) {
                if (numplayers > 1 || gClassicMode) {
                    return false;
                }
                if (PlayerList[nLocalPlayer].HealthAmount != 0) {
                    gGameScreen.capture(160, 100);
                    menu.mOpen(menu.mMenus[SAVE], -1);
                }
            }

            if (PsKeys.Show_LoadMenu.equals(gameKey)) {
                if (numplayers > 1) {
                    return false;
                }
                menu.mOpen(menu.mMenus[LOAD], -1);
            }

            if (PsKeys.Quicksave.equals(gameKey)) { // quick save
                quicksave();
            }

            if (PsKeys.Quickload.equals(gameKey)) { // quick load
                quickload();
            }

            if (PsKeys.AutoRun.equals(gameKey)) {
                cfg.gAutoRun = !cfg.gAutoRun;
                StatusMessage(500, "Autorun " + (cfg.gAutoRun ? "ON" : "OFF"), nLocalPlayer);
            }

            if (PsKeys.Third_View.equals(gameKey)) {
                bCamera = !bCamera;
                StatusMessage(500, "Third person view " + (bCamera ? "ON" : "OFF"), nLocalPlayer);
            }
        }

        if (PsKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
        }

        if (PsKeys.Gamma.equals(gameKey)) {
            menu.mOpen(menu.mMenus[COLORCORR], -1);
        }

        if (PsKeys.Make_Screenshot.equals(gameKey)) {
            makeScreenshot();
        }

        return true;
    }

    protected void setOverHead(int mode) {
        switch (mode) {
            case 0:
                if (cfg.gOverlayMap != 0) {
                    nOverhead = 1;
                } else {
                    nOverhead = 2;
                }
                break;
            case 1:
                if (cfg.gOverlayMap == 1) {
                    nOverhead = 0;
                } else {
                    nOverhead = 2;
                }
                break;
            default:
                nOverhead = 0;
                break;
        }
    }

    protected void makeScreenshot() {
        String name;
        if (mUserFlag == UserFlag.UserMap) {
            name = "scr-" + boardfilename.getName() + "-xxxx.png";
        } else {
            name = "scr-map" + levelnum + "-xxxx.png";
        }
        Renderer renderer = game.getRenderer();
        String filename = renderer.screencapture(game.cache.getGameDirectory(), name);
        if (filename != null) {
            StatusMessage(500, filename + " saved", nLocalPlayer);
        } else {
            StatusMessage(500, "Screenshot not saved. Access denied!", nLocalPlayer);
        }
    }

    @Override
    public void sndHandlePause(boolean pause) {
        Sound.sndHandlePause(pause);
    }

    @Override
    protected boolean prepareboard(Entry entry) {
        if (!LoadLevel(entry, levelnew)) {
            return false;
        }

        gNameShowTime = 500;
        StopAllSounds();

        levelnew = -1;
        lastlevel = levelnum;
        nOverhead = 0;
        game.gPaused = false;
        show2dsector.clear();
        show2dwall.clear();
        show2dsprite.clear();

        for (int i = 0; i < numplayers; i++) {
            SetSavePoint(i, initx, inity, initz, initsect, inita);
            RestartPlayer(i);
            InitPlayerKeys(i);
        }

        gDemoScreen.onPrepareboard(this);
        if (!DemoScreen.isDemoPlaying()) {
            StopMusic();
        }

        UpdateScreenSize();
        InitStatus();
        game.pNet.ResetNetwork();
        game.pNet.ResetTimers();
        game.pNet.ready2send = false;
//        ResetView(v64, v65);

        totalmoves = 0;
        GrabPalette();

        moveframes = 0;
        RefreshStatus();
        sndCheckUserMusic(entry);

        return true;
    }

    public void changemap(int num, Runnable prestart) {
        if (num < 1) {
            return;
        }
        boardfilename = game.getCache().getEntry(gCurrentEpisode.gMapInfo.get(num - 1).path, true);
        levelnew = num;
        String name = "Loading " + gCurrentEpisode.gMapInfo.get(num - 1).title;
        loadboard(boardfilename, prestart).setTitle(name);
    }

    public void training() {
        newgame(gTrainingEpisode, 1, true);
    }

    public void newgame(Object item, int nLevel, boolean classic) {
        userMusicEntry = null;
        nBestLevel = nLevel - 1;
        lastlevel = -1;
        zoom = 768;
        lastload = null;
        bCamera = false;
        pNet.ready2send = false;
        game.nNetMode = NetMode.Single;
        gClassicMode = classic;

        UserFlag flag = UserFlag.None;
        if (item instanceof EpisodeInfo) {
            EpisodeInfo game = (EpisodeInfo) item;
            if (!game.equals(gTrainingEpisode) && !game.equals(gOriginalEpisode)) {
                checkEpisodeResources(game);

                flag = UserFlag.Addon;
                Console.out.println("Start user episode: " + game.Title);
            } else {
                resetEpisodeResources(game);
            }
        } else if (item instanceof FileEntry) {
            flag = UserFlag.UserMap;
            boardfilename = (Entry) item;
            levelnum = 0;
            levelnew = 0;
            resetEpisodeResources(null);
            Console.out.println("Start user map: " + ((FileEntry) item).getName());
        }
        mUserFlag = flag;

        PlayerCount = 0;
        for (int i = 0; i < numplayers; i++) {
            int nPlayer = GrabPlayer();
            if (nPlayer == -1) {
                System.err.println("Can't create local player");
                return;
            }
            InitPlayerInventory(nPlayer);
        }

        if (!gClassicMode) {
            PlayerList[nLocalPlayer].HealthAmount = 800;
            if (nNetPlayerCount != 0) {
                PlayerList[nLocalPlayer].HealthAmount = 1600;
            }
        }

        if (mUserFlag == UserFlag.UserMap) {
            gGameScreen.loadboard(boardfilename, null).setTitle("Loading " + boardfilename);
        } else if (gCurrentEpisode != null) {
            if (gCurrentEpisode.equals(gOriginalEpisode)) {
                gMap.showMap(nLevel, nLevel, nBestLevel);
            } else {
                changemap(nLevel, null);
            }
        } else {
            game.show(); // can't restart the level
        }
    }

    public boolean isOriginal() {
        return false;
    }


}
