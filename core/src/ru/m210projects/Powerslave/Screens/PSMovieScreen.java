package ru.m210projects.Powerslave.Screens;

import com.badlogic.gdx.Input;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.CRC32;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Sound;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import static ru.m210projects.Build.Engine.MAXPALOOKUPS;
import static ru.m210projects.Build.Engine.RESERVEDPALS;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Powerslave.Main.cfg;

public class PSMovieScreen extends MovieScreen {

    private long lastCrc32;
    private int backgroundCol = 0;
    private int frameScale, frameAngle;
    private int currSub;
    private int oldAngle, oldScale;
    private final Subtitle[] subs = {
            new Subtitle(new String[]{"During the time of the pharaohs, the city of Karnak",
                    "was a shining example of a civilization that all", "others nations could only hope to emulate"},
                    35, 132),

            new Subtitle(new String[]{"Today Karnak lives on, surrounded by the spirits",
                    "of the past, however something has gone terribly wrong."}, 143, 228),

            new Subtitle(new String[]{"Unknown forces have seized the city and great",
                    "turmoil is spreading into neighboring lands."}, 242, 302),

            new Subtitle(new String[]{"World leaders from all parts of the globe have sent",
                    "forces into the Karnak Valley, but none have returned."}, 308, 384),

            new Subtitle(new String[]{"The great power of this new empire is quickly crushing",
                    "the best forces the human world has to offer."}, 392, 456),

            new Subtitle(new String[]{"The only known information about this crisis came from",
                    "a Karnak villager, found wandering through the desert",
                    "miles from his home, dazed, dehydrated and close to death."}, 477, 594),

            new Subtitle(new String[]{"In his final moments among the living, the villager told",
                    "horrifying stories of fierce alien creatures that invaded",
                    "the city, devoured the women and children, and made slaves", "of the men."}, 610, 733),

            new Subtitle(
                    new String[]{"Many of the unfortunate victims were", "skinned alive or brutally dismembered."},
                    741, 794),

            new Subtitle(new String[]{"Others were subjected to unbearable tortures, ",
                    "injected with strange substances and ", "then mummified while still alive."}, 803, 886),

            new Subtitle(new String[]{"According to the villager, even the mummified body",
                    "of the great King Ramses was ", "unearthed and taken away."}, 893, 967),

            new Subtitle(new String[]{"You have been chosen from a group of the best",
                    "operatives in the world to infiltrate Karnak and ", "destroy the threatening forces."}, 979, 1055),

            new Subtitle(new String[]{"But as your helicopter nears the Karnak Valley, ",
                    "it is shot down. You barely escape with your life."}, 1059, 1128),

            new Subtitle(new String[]{"With no possible contact to the outside world,",}, 1134, 1164),

            new Subtitle(new String[]{"you begin your adventure,",}, 1168, 1185),

            new Subtitle(new String[]{"ready to accomplish your mission...",}, 1190, 1208),

            new Subtitle(new String[]{"praying to return alive.",}, 1215, 1235)
    };

    public PSMovieScreen(BuildGame game) {
        super(game, 764);
    }

    @Override
    protected MovieFile GetFile(String file) {
        try {
            lastCrc32 = 0;
            currSub = -1;
            backgroundCol = 96;
            frameScale = 0;
            frameAngle = 1536;
            
            return new LmfFile(file);
        } catch (FileNotFoundException fnf) {
            Console.out.println(file + " is not found!", OsdColor.RED);
            return null;
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
            return null;
        }
    }

    @Override
    protected void StopAllSounds() {
        Sound.StopAllSounds();
        Sound.StopMusic();
    }

    @Override
    protected byte[] DoDrawFrame(int num) {
        return mvfil.getFrame(num);
    }

    @Override
    protected Font GetFont() {
        return game.getFont(0);
    }

    @Override
    protected void DrawEscText(Font font, int pal) {
        Renderer renderer = game.getRenderer();
        int shade = 16 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
        font.drawTextScaled(renderer, 160, 5, "Press ESC to skip", 1.0f, shade, MAXPALOOKUPS - RESERVEDPALS - 1,
                TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    @Override
    protected void changepalette(byte[] pal) {
        super.changepalette(pal);

        if (pal != null && pal.length == 768) {
            backgroundCol = 0;
            int k = 255;
            for (int i = 0; i < 256; i += 3) {
                int j = (pal[3 * i] & 0xFF) + (pal[3 * i + 1] & 0xFF) + (pal[3 * i + 2] & 0xFF);
                if (j < k) {
                    k = j;
                    backgroundCol = i;
                }
            }
        }
    }

    @Override
    protected boolean play() {
        if (game.getProcessor().isKeyJustPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        Renderer renderer = game.getRenderer();
        if (mvfil != null) {
            if (LastMS == -1) {
                LastMS = engine.getCurrentTimeMillis();
            }
            DynamicArtEntry pic = (DynamicArtEntry) renderer.getTile(TILE_MOVIE);

            long ms = engine.getCurrentTimeMillis();
            long dt = ms - LastMS;
            mvtime += dt;
            float tick = mvfil.getRate();
            if (mvtime >= tick) {
                try {
                    if (((LmfFile) mvfil).readFrame()) {
                        long crc32 = CRC32.getChecksum(((LmfFile) mvfil).framebuf);
                        if (lastCrc32 != crc32) {
                            pic.copyData(((LmfFile) mvfil).framebuf);
                            lastCrc32 = crc32;
                        }
                        frame++;
                        int nextSub = currSub + 1;
                        if (nextSub < subs.length && frame >= subs[currSub + 1].from) {
                            currSub++;
                        }
                    } else {
                        return false;
                    }
                } catch (IOException e) {
                    return false;
                }

                oldScale = frameScale;
                if (frameScale < 65536) {
                    frameScale += 2048;
                }

                oldAngle = frameAngle;
                if (frameAngle != 0) {
                    frameAngle += 16;
                    if (frameAngle == 2048) {
                        frameAngle = 0;
                    }
                }
                mvtime -= (long) tick;
            }
            LastMS = ms;

            if (!pic.hasSize()) {
                return false;
            }

            int ang = oldAngle;
            int scale = oldScale;

            if (frameAngle != oldAngle && frameScale != oldScale) {
                int smoothratio = (int) ((mvtime / tick) * 65536.0f);
                ang += mulscale(((frameAngle - oldAngle + 1024) & 0x7FF) - 1024, smoothratio, 16);
                scale += mulscale(frameScale - oldScale, smoothratio, 16);
            }

            renderer.rotatesprite(nPosX << 16, nPosY << 16, scale, ang, TILE_MOVIE, 0, 0, nFlags);

            if (cfg.bSubtitles && (currSub != -1 && frame < subs[currSub].to)) {
                String[] text = subs[currSub].text;
                int pos = 190 - 8 * text.length;
                for (String s : text) {
                    game.getFont(0).drawTextScaled(renderer, 160, pos += 8, s, 0.7f, 0, MAXPALOOKUPS - RESERVEDPALS - 1,
                            TextAlign.Center, Transparent.None, ConvertType.Normal, true);
                }
            }

            return true;
        }
        return false;
    }

    @Override
    public void draw(float delta) {
        game.getRenderer().clearview(backgroundCol);
        super.draw(delta);
    }

    @Override
    protected void close() {
        super.close();
        frameAngle = oldAngle = 0;
        oldScale = frameScale = 0;
    }

    @Override
    public boolean open(String fn) {
        return super.open(fn);
    }

    private static class Subtitle {
        public final String[] text;
        public final int from;
        public final int to;

        public Subtitle(String[] text, int from, int to) {
            this.text = text;
            this.from = from;
            this.to = to;
        }
    }

    private class LmfFile implements MovieFile {

        private final InputStream is;
        private final byte[] framebuf;
        private final ByteBuffer audbuf = ByteBuffer.allocateDirect(4096);

        public LmfFile(String fn) throws Exception {
            Entry entry = game.getCache().getEntry(fn, true);
            if (!entry.exists()) {
                throw new FileNotFoundException();
            }

            is = entry.getInputStream();
            int size = (int) entry.getSize();
            if (size < 32 || !StreamUtils.readString(is, 4).equals("LMF ")) {
                is.close();
                throw new Exception("Wrong file format");
            }

            StreamUtils.skip(is, 28);
            framebuf = new byte[200 * 320];
        }
        
        @Override
        public byte[] getFrame(int num) {
            return framebuf;
        }

        @Override
        public int getWidth() {
            return 200;
        }

        @Override
        public int getHeight() {
            return 320;
        }

        public boolean readFrame() throws IOException {
            if (is.available() == 0) {
                return false;
            }

            while (true) {
                byte id = StreamUtils.readByte(is);
                if (id == 0) {
                    return false;
                }

                int cSize = StreamUtils.readInt(is);
                switch (id) {
                    case 1:
                        byte[] pal = StreamUtils.readBytes(is, 768);
                        StreamUtils.readByte(is);

                        for (int i = 0; i < 768; i++) {
                            pal[i] <<= 2;
                        }

                        changepalette(pal);
                        continue;
                    case 2: // Audio chunk
                        audbuf.limit(cSize);
                        StreamUtils.readBuffer(is, audbuf);
                        Source hVoice = Sound.newSound(audbuf, 22050, 8, 255);
                        if (hVoice != null) {
                            hVoice.play(255);
                        }
                        continue;
                    case 3: // Video chunk
                        if (cSize != 0) {
                            int ptr = 200 * StreamUtils.readShort(is);
                            cSize -= 2;
                            while (cSize > 0) {
                                ptr += StreamUtils.readUnsignedByte(is);
                                int len = StreamUtils.readUnsignedByte(is);
                                cSize -= 2;
                                if (len != 0) {
                                    int rlen = is.read(framebuf, ptr, len);
                                    if (rlen == -1) {
                                        throw new EOFException();
                                    }
                                    ptr += len;
                                    cSize -= len;
                                }
                            }
                        }
                        continue;
                    case 4: // End of frame
                        return true;
                }
            }
        }

        @Override
        public void close() {
            try {
                is.close();
            } catch (IOException ignored) {
            }
        }

        @Override
        public float getRate() {
            return 1000f / 10f;
        }

        @Override
        public byte[] getPalette() {
            // unimplemented
            return null;
        }

        @Override
        public void playAudio() {
            // unimplemented        
        }

        @Override
        public int getFrames() {
            // unimplemented
            return -1;
        }
    }
}
