// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Powerslave.Globals.BACKGROUND;
import static ru.m210projects.Powerslave.Sound.StopAllSounds;

public class LoadingScreen extends LoadingAdapter {

    public LoadingScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();
        StopAllSounds();
    }

    @Override
    protected void draw(String title, float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(96);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, BACKGROUND, 0, 0, 2 | 8);

        game.getFont(1).drawTextScaled(renderer, 160, 100, title, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

}
