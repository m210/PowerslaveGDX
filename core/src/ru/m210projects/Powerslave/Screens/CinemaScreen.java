// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import com.badlogic.gdx.Input;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Powerslave.Main;

import static ru.m210projects.Build.filehandle.art.ArtFile.DUMMY_ART_FILE;
import static ru.m210projects.Powerslave.Cinema.*;
import static ru.m210projects.Powerslave.Globals.gString;
import static ru.m210projects.Powerslave.Main.cfg;
import static ru.m210projects.Powerslave.Palette.LoadCinemaPalette;
import static ru.m210projects.Powerslave.Palette.cinemapal;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomLong;
import static ru.m210projects.Powerslave.Screens.GameScreen.CopyTileToBitmap;
import static ru.m210projects.Powerslave.Seq.GetSeqPicnum;
import static ru.m210projects.Powerslave.Sound.*;

public class CinemaScreen extends SkippableAdapter {

    private static final int CINEMA_TILE = 3623;
    private DynamicArtEntry cinemaEntry = DUMMY_ART_FILE;
    private final byte[] cinemaData;

    private final Main game;
    private int num;
    private int cinematile;
    private int backgroundCol;
    private int cinematext;
    private long time;

    private int lastScene;
    private int nLastSceneTextIndex;
    private int nCurrentSceneTextIndex;
    private sceneStatus lastStatus;
    private int openClock;
    private int lx, ly;
    private char[] gMessage;
    private int symPosX, symPosY, symCur;
    private int creditsShade;
    private long creditsTime;

    private boolean anyKeyPressed = false;

    public CinemaScreen(Main game) {
        super(game);
        this.game = game;

        ArtEntry src = engine.getTile(CINEMA_TILE);
        cinemaData = src.getBytes();
    }

    @Override
    public void show() {
        byte[] palette = engine.getPaletteManager().getBasePalette();
        lastScene = 0;
        cinematext = -1;

        ArtEntry src = engine.getTile(CINEMA_TILE);
        if (!cinemaEntry.exists()) {
            cinemaEntry = engine.allocatepermanenttile(src);
        }

        switch (num) {
            case 1:
                cinematile = 3454;
                cinematext = 4;
                break;
            case 2:
                cinematile = 3452;
                cinematext = 0;
                break;
            case 3:
                cinematile = 3449;
                cinematext = 2;
                break;
            case 4:
                cinematile = 3445;
                cinematext = 7;
                break;
            case 5:
                cinematile = 3451;
                cinematext = 3;
                break;
            case 6:
                cinematile = 3448;
                cinematext = 8;
                break;
            case 7:
                cinematile = 3446;
                cinematext = 6;
                break;
            case 8:
                lastScene = 1;
                engine.setbrightness(cfg.getPaletteGamma(), palette);
                backgroundCol = 96;
                break;
            case 9:
                lastScene = 2;
                engine.setbrightness(cfg.getPaletteGamma(), palette);
                backgroundCol = 96;
                break;
        }

        StopAllSounds();
        if (lastScene == 0) {
            if (cinematext != -1) {
                playCDtrack(cinematext + 2, false);
            }
            LoadCinemaPalette(num);
            engine.setbrightness(cfg.getPaletteGamma(), cinemapal);

            backgroundCol = 0;
            int k = 255;
            for (int i = 0; i < 256; i += 3) {
                int j = (cinemapal[3 * i] & 0xFF) + (cinemapal[3 * i + 1] & 0xFF) + (cinemapal[3 * i + 2] & 0xFF);
                if (j < k) {
                    k = j;
                    backgroundCol = i;
                }
            }

            ReadyCinemaText(cinematext);
            time = System.currentTimeMillis();
        } else if (lastScene == 1) {
            playCDtrack(19, true);

            nCurrentSceneTextIndex = nLastSceneTextIndex = game.FindGString("LASTLEVEL");

            PlayLocalSound(StaticSound[75], 0);

            if (cinemaEntry.exists()) {
                cinemaEntry.copyData(cinemaData);
            }

            lastStatus = sceneStatus.Open;
            openClock = engine.getTotalClock() + 240;
            lx = 12;
            ly = 16;

            time = System.currentTimeMillis();
        } else if (lastScene == 2) {
            playCDtrack(19, false);

            nCurrentSceneTextIndex = nLastSceneTextIndex = game.FindGString("CREDITS");
            NextCredit();
        }
        game.getProcessor().resetPollingStates();
    }

    @Override
    public void hide() {
        StopAllSounds();
        StopMusic();
        byte[] palette = engine.getPaletteManager().getBasePalette();
        engine.setbrightness(cfg.getPaletteGamma(), palette);
    }

    public CinemaScreen setNum(int num) {
        this.num = num;
        return this;
    }

    @Override
    public void skip() {
        if (lastScene != 1) {
            super.skip();
        } else {
            if (lastStatus != sceneStatus.Close) {
                PlayLocalSound(StaticSound[75], 0);
                lastStatus = sceneStatus.Close;
            } else {
                super.skip();
            }
        }
    }

    @Override
    public void draw(float delta) {
        if (game.getProcessor().isKeyJustPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        Renderer renderer = game.getRenderer();
        renderer.clearview(backgroundCol);
        if (lastScene == 0) {
            renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, cinematile, 0, 0, 2 | 8);
            if (cinematext != -1) {
                if (nHeight + nCrawlY > 0 && System.currentTimeMillis() - time >= 120) {
                    nCrawlY--;
                    time = System.currentTimeMillis();
                }
                AdvanceCinemaText();

                if (nHeight + nCrawlY <= 0 && !MusicPlaying()) {
                    skip();
                }
            }
        } else if (lastScene == 1) {
            DynamicArtEntry dst = cinemaEntry;
            if (!dst.exists()) {
                skip();
                return;
            }

            switch (lastStatus) {
                case Open:
                    if (engine.getTotalClock() >= openClock) {
                        lastStatus = sceneStatus.Screen;

                        if (cinemaEntry.exists()) {
                            cinemaEntry.copyData(cinemaData);
                        }
                        NextScreen();
                        break;
                    }

                    if (System.currentTimeMillis() - time >= 15) {
                        if (ly < 116) {
                            ly += 20;
                        } else if (lx < 192) {
                            lx += 20;
                        }
                        time = System.currentTimeMillis();
                    }

                    DoStatic(dst, lx, ly);
                    break;
                case Close:
                    if (lx == 12 && ly == 16) {
                        skip();
                        break;
                    }

                    if (System.currentTimeMillis() - time >= 15) {
                        if (lx > 20) {
                            lx -= 20;
                        } else if (ly > 20) {
                            ly -= 20;
                        }
                        time = System.currentTimeMillis();
                    }
                    if (cinemaEntry.exists()) {
                        cinemaEntry.copyData(cinemaData);
                    }
                    DoStatic(dst, lx, ly);
                    break;
                case Screen:
                    if (System.currentTimeMillis() - time >= 40) {
                        if (symCur < gMessage.length) {
                            if (anyKeyPressed) {
                                while (nLastSceneTextIndex - nCurrentSceneTextIndex >= 1) {
                                    while (symCur < gMessage.length) {
                                        symPosX += CopyCharToBitmap(gMessage[symCur++], dst, symPosX, symPosY);
                                    }
                                    NextLine();
                                }
                                nCurrentSceneTextIndex -= 1; // to avoid skip method
                                anyKeyPressed = false;
                            } else {
                                char sym = gMessage[symCur++];
                                if (sym != ' ') {
                                    PlayLocalSound(StaticSound[71], 0);
                                }
                                symPosX += CopyCharToBitmap(sym, dst, symPosX, symPosY);
                            }

                            dst.invalidate();
                            time = System.currentTimeMillis();
                        } else {
                            if (nLastSceneTextIndex - nCurrentSceneTextIndex <= 1) {
                                if (System.currentTimeMillis() - time >= 10000 || anyKeyPressed) {
                                    if (cinemaEntry.exists()) {
                                        cinemaEntry.copyData(cinemaData);
                                    }
                                    NextScreen();
                                    anyKeyPressed = false;
                                }
                                if (nCurrentSceneTextIndex == nLastSceneTextIndex) {
                                    skip();
                                    break;
                                }
                            } else {
                                NextLine();
                            }
                        }
                    }

                    renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, CINEMA_TILE, 0, 0, 2 | 8);
                    break;
            }
        } else // Credits
        {
            if (System.currentTimeMillis() - time >= 30) {
                if (System.currentTimeMillis() - creditsTime <= 6000) {
                    if (creditsShade >= 0) {
                        creditsShade--;
                    }
                } else {
                    if (creditsShade < 64) {
                        creditsShade++;
                    } else if (!NextCredit()) {
                        if (!MusicPlaying()) {
                            skip();
                        }
                    }
                }
                time = System.currentTimeMillis();
            }

            int y = symPosY;
            for (int i = nCurrentSceneTextIndex; i < nLastSceneTextIndex; i++) {
                game.getFont(0).drawTextScaled(renderer, symPosX, y, gString[i], 1.0f, creditsShade, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                y += (game.getFont(0).getSize() + 1);
            }
        }
    }

    private boolean NextCredit() {
        int index = nLastSceneTextIndex + 1;
        if (gString[index].equals("END")) {
            return false;
        }

        while (!gString[index].isEmpty()) {
            index++;
        }

        creditsShade = 64;
        symPosX = 160;
        symPosY = 100 - ((game.getFont(0).getSize() + 1) * (index - nLastSceneTextIndex - 1)) / 2;

        nCurrentSceneTextIndex = nLastSceneTextIndex;
        nLastSceneTextIndex = index;
        creditsTime = time = System.currentTimeMillis();
        return true;
    }

    private void NextScreen() {
        int index = nLastSceneTextIndex;
        while (!gString[index].isEmpty()) {
            index++;
        }

        symCur = 0;
        symPosX = 70;
        symPosY = 81 - 4 * (index - nLastSceneTextIndex);
        gMessage = gString[nLastSceneTextIndex].toCharArray();
        nCurrentSceneTextIndex = nLastSceneTextIndex + 1;
        nLastSceneTextIndex = index + 1;
        time = System.currentTimeMillis();
    }

    private void NextLine() {
        symCur = 0;
        symPosX = 70;
        symPosY += 8;
        gMessage = gString[nCurrentSceneTextIndex].toCharArray();
        nCurrentSceneTextIndex++;
        time = System.currentTimeMillis();
    }

    private void DoStatic(DynamicArtEntry pic, int x, int y) {
        RandomLong();

        int x1 = 160 - x / 2;
        int y1 = 81 - y / 2;
        int x2 = x1 + x;
        int y2 = y1 + y;

        int ptr = 200 * x1 + y1;
        byte[] data = pic.getBytes();
        for (int i = x1; i < x2; i++) {
            int wptr = ptr;
            for (int j = y1; j < y2; j++) {
                data[wptr++] = (byte) (16 * RandomBit());
            }
            ptr += 200;
        }

        pic.invalidate();
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, CINEMA_TILE, 0, 0, 2 | 8);
    }

    private int CopyCharToBitmap(char ch, DynamicArtEntry dest, int x, int y) {
        if (ch == ' ') {
            return 4;
        }

        int pic = GetSeqPicnum(69, 0, Character.toUpperCase(ch) - ' ') + 102;
        ArtEntry src = engine.getTile(pic);
        if (!(src instanceof DynamicArtEntry) || !src.exists()) {
            src = engine.allocatepermanenttile(src);
            if (!src.exists()) {
                return 4;
            }
        }

        CopyTileToBitmap((DynamicArtEntry) src, dest, x, y);
        return engine.getTile(pic).getWidth() + 1;
    }

    public void anyKeyPressed() {
        game.getProcessor().prepareNext();
        anyKeyPressed = true;
    }

    private enum sceneStatus {
        Open, Close, Screen
    }
}
