// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import com.badlogic.gdx.Screen;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Config.PsKeys;
import ru.m210projects.Powerslave.Factory.PSMenuHandler;
import ru.m210projects.Powerslave.Main;
import ru.m210projects.Powerslave.Type.DemoFile;
import ru.m210projects.Powerslave.Type.Input;

import java.io.InputStream;
import java.util.*;

import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Powerslave.Factory.PSMenuHandler.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.LoadSave.gClassicMode;
import static ru.m210projects.Powerslave.LoadSave.lastload;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Player.*;
import static ru.m210projects.Powerslave.Random.InitRandom;
import static ru.m210projects.Powerslave.Sound.playCDtrack;
import static ru.m210projects.Powerslave.Weapons.CheckClip;

public class DemoScreen extends GameScreen {

    private static boolean bPlayback = false;

    public int nDemonum = -1;
    public final List<Entry> demofiles = new ArrayList<>();
    public DemoFile demfile;
    private int lockclock;

    public DemoScreen(Main game) {
        super(game);
    }

    @Override
    public void show() {
        lastload = null;

        playCDtrack(19, true);
    }

    public boolean showDemo(Entry entry) {
        demfile = null;
        try (InputStream is = entry.getInputStream()) {
            demfile = new DemoFile(is);
        } catch (Exception e) {
            Console.out.println("Can't play the demo file: " + entry.getName(), OsdColor.RED);
            return false;
        }

        game.nNetMode = NetMode.Single;
        levelnew = demfile.level;
        nPlayerWeapons[nLocalPlayer] = demfile.weapons;
        nPlayerClip[nLocalPlayer] = demfile.clip;
        nPlayerItem[nLocalPlayer] = demfile.items;
        nPistolClip[nLocalPlayer] =  Math.min(PlayerList[nLocalPlayer].AmmosAmount[1], 6);
        PlayerList[nLocalPlayer].copy(demfile.player);
        nPlayerLives[nLocalPlayer] = demfile.lives;
        SetPlayerItem(nLocalPlayer, nPlayerItem[nLocalPlayer]);
        CheckClip(nLocalPlayer);
        gClassicMode = true;

        InitRandom();
        InitPlayerInventory(GrabPlayer());

        PlayerCount = 1;
        if (levelnew == 0) {
            gCurrentEpisode = gTrainingEpisode;
        } else {
            gCurrentEpisode = gOriginalEpisode;
            levelnew--;
        }

        boardfilename = game.cache.getEntry(gCurrentEpisode.gMapInfo.get(levelnew).path, true);
        loadboard(boardfilename, null).setTitle("Loading " + gCurrentEpisode.gMapInfo.get(levelnew).title);
        bPlayback = true;
        Console.out.println("Playing demo " + entry.getName());
        return true;
    }

    @Override
    protected void startboard(final Runnable startboard) {
        game.doPrecache(() -> {
            startboard.run(); //call faketimehandler
            pNet.ResetTimers(); //reset ototalclock
            lockclock = 0;
            pNet.ready2send = false;
        });
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        PSMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            menu.mOpen(menu.mMenus[MAIN], -1);
        }

        if (GameKeys.Enlarge_Screen.equals(gameKey)) {
            if (cfg.nScreenSize > 0) {
                cfg.nScreenSize = BClipLow(cfg.nScreenSize - 1, 0);
            }
        } else if (GameKeys.Shrink_Screen.equals(gameKey)) {
            if (cfg.nScreenSize < 2) {
                cfg.nScreenSize = BClipHigh(cfg.nScreenSize + 1, 2);
            }
        } else if (PsKeys.Show_LoadMenu.equals(gameKey)) {
            if (game.nNetMode == NetMode.Single) {
                menu.mOpen(menu.mMenus[LOAD], -1);
            }
        } else if (PsKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
        } else if (PsKeys.Show_SoundSetup.equals(gameKey)) {
            menu.mOpen(menu.mMenus[AUDIO], -1);
        } else if (PsKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
        } else if (PsKeys.Gamma.equals(gameKey)) {
            menu.mOpen(menu.mMenus[COLORCORR], -1);
        } else if (PsKeys.Make_Screenshot.equals(gameKey)) {
            makeScreenshot();
        }

        return true;
    }

    @Override
    public void render(float delta) {
        DemoRender();

        float smoothratio = 65536;
        if (!game.gPaused) {
            smoothratio = pEngine.getTimer().getsmoothratio(delta);
            if (smoothratio < 0 || smoothratio > 0x10000) {
                smoothratio = BClipRange(smoothratio, 0, 0x10000);
            }
        }

        game.pInt.dointerpolations(smoothratio);
        DrawWorld(smoothratio);

        DrawHud(smoothratio);
        game.pInt.restoreinterpolations();

        if (pMenu.gShowMenu) {
            pMenu.mDrawMenu();
        }

        PostFrame(pNet);

        pEngine.nextpage(delta);
    }

    private void DemoRender() {
        pNet.ready2send = false;

        if (!game.isCurrentScreen(this)) {
            return;
        }

        if (!game.gPaused && demfile != null) {
            while (engine.getTotalClock() >= (lockclock + 4)) {
                Input pInput = demfile.ReadPlaybackInput();
                if (pInput != null) {
                    pNet.gFifoInput[pNet.gNetFifoHead[nLocalPlayer] & 0xFF][nLocalPlayer].Copy(pInput);
                    Ra[nLocalPlayer].nTarget = besttarget = pInput.nTarget;
                    pNet.gNetFifoHead[nLocalPlayer]++;
                } else {
                    if (!showDemo()) {
                        onStopPlaying();
                        game.changeScreen(gMenuScreen);
                    }
                    return;
                }

                game.pInt.clearinterpolations();
                while (moveframes > 0) {
                    ProcessFrame(pNet);
                    lockclock += engine.getTimer().getFrameTicks();
                }
                pNet.gNetFifoTail++;
            }
        } else {
            lockclock = engine.getTotalClock();
        }
    }

    @Override
    protected void UpdateInputs(BuildNet net) {
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            Input src = (Input) net.gFifoInput[net.gNetFifoTail & 0xFF][i];
            sPlayerInput[i].Copy(src);
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean showDemo() {
        switch (cfg.gDemoSeq) {
            case 0: //OFF
                return false;
            case 1: //Consistently
                if (nDemonum < (demofiles.size() - 1)) {
                    nDemonum++;
                } else {
                    nDemonum = 0;
                }
                break;
            case 2: //Accidentally
                int nextnum = nDemonum;
                if (demofiles.size() > 1) {
                    while (nextnum == nDemonum) {
                        nextnum = (int) (Math.random() * (demofiles.size()));
                    }
                }
                nDemonum = BClipRange(nextnum, 0, demofiles.size() - 1);
                break;
        }

        if (demofiles != null && !demofiles.isEmpty()) {
            boolean result = showDemo(demofiles.get(nDemonum));
            if (!result) {
                demofiles.remove(nDemonum);
                return false;
            }
            return result;
        }

        return false;
    }

    public void demoscan(Directory directory) {
        demofiles.clear();

        for (Entry file : directory.getEntries()) {
            if (file.isExtension("vcr")) {
                demofiles.add(file);
            }
        }

        if (!demofiles.isEmpty()) {
            demofiles.sort(Comparator.comparing(Entry::getName));
        }
        Console.out.println("There are " + demofiles.size() + " demo(s) in the loop", OsdColor.YELLOW);
    }

    @Override
    public boolean isOriginal() {
        return true;
    }


    public static boolean isDemoScreen(Screen screen) {
        return screen == gDemoScreen;
    }

    public static boolean isDemoPlaying() {
        return bPlayback;
    }

    public void onLoad() {
//        onStopRecord();
        demfile = null;
        onStopPlaying();
    }

    public void onStopPlaying() {
        demfile = null;
        bPlayback = false;
    }

    public void onPrepareboard(GameScreen screen) {
        if (screen != this && isDemoPlaying()) {
            onStopPlaying();
        }
    }
}
