// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Powerslave.Main;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Powerslave.Globals.BACKGROUND;
import static ru.m210projects.Powerslave.Globals.THEFONT;
import static ru.m210projects.Powerslave.Main.boardService;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        addQueue("Preload floor and ceiling tiles...", () -> {
            Sector[] sectors = boardService.getBoard().getSectors();
            for (Sector sec : sectors) {
                addTile(sec.getFloorpicnum());
                addTile(sec.getCeilingpicnum());
            }
            doprecache(0);
        });

        addQueue("Preload wall tiles...", () -> {
            for (Wall wall : boardService.getBoard().getWalls()) {
                addTile(wall.getPicnum());
                if (wall.getOverpicnum() >= 0) {
                    addTile(wall.getOverpicnum());
                }
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr != null && spr.getStatnum() < MAXSTATUS) {
                    cachespritenum(i);
                }
            }

            doprecache(1);
        });

        addQueue("Preload hud tiles...", () -> {
            for (int i = 3522; i <= 3554; i++) //small font
            {
                addTile(i);
            }
            for (int i = THEFONT; i <= THEFONT + 51; i++) //menu font
            {
                addTile(i);
            }

//				addTile(STATUSBAR);

            doprecache(1);
        });
    }

    @Override
    protected void draw(String title, int index) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(96);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, BACKGROUND, -128,
                0, 2 | 8 | 64);

        game.getFont(1).drawTextScaled(renderer, 160, 100, "Loading", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);

        game.getFont(1).drawTextScaled(renderer, 160, 114, title, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    private void cachespritenum(int i) {
        int maxc = 1;
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        for (int j = spr.getPicnum(); j < (spr.getPicnum() + maxc); j++) {
            addTile(j);
        }
    }

}
