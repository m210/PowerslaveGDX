// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Screens;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LogoScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Powerslave.Energy.DoPlasma;
import static ru.m210projects.Powerslave.Sound.*;

public class LogoScreen2 extends LogoScreen {

    private final int[] loc_10010 = {6, 25, 43, 50, 68, 78, 101, 111, 134, 158, 173, 230, 6000};
    private int clock, frame, anim, coord;

    public LogoScreen2(BuildGame game, float gShowTime) {
        super(game, gShowTime);
    }

    @Override
    public void show() {
        super.show();
        anim = frame = 0;
        clock = engine.getTotalClock();
        coord = 130;
        playCDtrack(19, true);
        if ((System.currentTimeMillis() & 0xF) != 0) {
            PlayGameOverSound();
        } else {
            PlayLocalSound(StaticSound[61], 0);
        }
    }

    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(96);
        DoPlasma(160, 40, 65536);

        int nFireTile = (engine.getTotalClock() / 16) & 3;
        renderer.rotatesprite(50 << 16, 150 << 16, 65536, 0, nFireTile + 3512, 0, 0, 10);
        renderer.rotatesprite(270 << 16, 150 << 16, 65536, 0, ((nFireTile + 2) & 3) + 3512, 0, 0, 10);

        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 3582, 0, 0, 10);

        int pic = 3437;
        if (LocalSoundPlaying()) {
            if (engine.getTotalClock() > clock + loc_10010[frame]) {
                anim ^= 1;
                frame++;
            }

            if (anim != 0) {
                if (coord >= 135) {
                    pic = 3583;
                } else {
                    coord += 5;
                }
            } else if (coord <= 130) {
                coord = 130;
            } else {
                coord -= 2;
            }

            if (pic == 3583) {
                coord = 131;
            } else if (coord > 135) {
                coord = 135;
            }
        } else {
            if ((gTicks += delta) >= gShowTime && callback != null) {
                Gdx.app.postRunnable(callback);
                callback = null;
            }
        }

        if (frame >= 1) {
            game.getFont(0).drawTextScaled(renderer, 160, 170, "Lobotomy software, Inc.", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            game.getFont(0).drawTextScaled(renderer, 160, 178, "3D Engine by 3D Realms", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }
        renderer.rotatesprite(161 << 16, coord << 16, 65536, 0, pic, 0, 0, 10);
    }
}
