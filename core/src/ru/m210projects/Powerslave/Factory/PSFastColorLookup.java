package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.Types.FastColorLookup;

public class PSFastColorLookup implements FastColorLookup {

    @Override
    public void invalidate() {

    }

    @Override
    public byte getClosestColorIndex(byte[] palette, int r, int g, int b) {
        int i;
        int dr, dg, db, dist, matchDist, match = 0;

        matchDist = 0x7FFFFFFF;

        for (i = 0; i < 256; i++) {
            dist = 0;

            dg = (palette[3 * i + 1] & 0xFF) - g;
            dist += dg * dg;
            if (dist >= matchDist) {
                continue;
            }

            dr = (palette[3 * i] & 0xFF) - r;
            dist += dr * dr;
            if (dist >= matchDist) {
                continue;
            }

            db = (palette[3 * i + 2] & 0xFF) - b;
            dist += db * db;
            if (dist >= matchDist) {
                continue;
            }

            matchDist = dist;
            match = i;

            if (dist == 0) {
                break;
            }
        }

        return (byte) match;
    }
}
