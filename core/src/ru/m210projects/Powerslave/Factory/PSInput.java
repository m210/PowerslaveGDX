// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Powerslave.Config.PsKeys;
import ru.m210projects.Powerslave.Main;
import ru.m210projects.Powerslave.Type.Input;
import ru.m210projects.Powerslave.Type.PlayerStruct;

import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.cfg;
import static ru.m210projects.Powerslave.View.*;


public class PSInput extends GameProcessor {

    public static final int nJump = 1;
    public static final int nPause = 1 << 1;
    public static final int nOpen = 1 << 2;
    public static final int nFire = 1 << 3;
    public static final int nCrouch = 1 << 4;
    public static final int nLookUp = 1 << 5;
    public static final int nLookDown = 1 << 6;
    public static final int nItemLeft = 1 << 7;
    public static final int nItemRight = 1 << 8;
    public static final int nItemUse = 1 << 9;
    public static final int nRun = 1 << 10;
    public static final int nTurnAround = 1 << 11;
    public static final int nAimCenter = 1 << 12;
    public static final int nAimUp = 1 << 17;

    // 1 << 13 - 1 << 16 nWeapons
    public static final int nAimDown = 1 << 18;
    public static final int nItemHand = 1 << 19;
    public static final int nItemEye = 1 << 20;
    public static final int nItemMask = 1 << 21;
    public static final int nItemHeart = 1 << 22;
    public static final int nItemTorch = 1 << 23;
    public static final int nItemScarab = 1 << 24;
    public final int NORMALKEYMOVE = 40;
    public final int MAXVEL = (NORMALKEYMOVE * 2);
    private int lPlayerXVel, lPlayerYVel;

    public PSInput(Main main) {
        super(main);
    }

    @Override
    public void fillInput(NetInput input) {
        mouseDelta.x /= 32.0f;
        mouseDelta.y /= 8.0f;

        Input gInput = (Input) input;

        gInput.reset();
        int vel;
        int svel = vel = 0;
        float angvel;
        float horiz = angvel = 0;

        gInput.bits |= isGameKeyJustPressed(GameKeys.Open) ? nOpen : 0;

        if (PlayerList[nLocalPlayer].HealthAmount == 0) {
            lPlayerYVel = 0;
            lPlayerXVel = 0;
            return;
        }

        if (Console.out.isShowing() || game.pMenu.gShowMenu) {
            return;
        }

        if (isGameKeyJustPressed(GameKeys.Mouse_Aiming)) {
            cfg.setgMouseAim(!cfg.isgMouseAim());
            StatusMessage(500, "Mouse aiming " + (cfg.isgMouseAim() ? "ON" : "OFF"), nLocalPlayer);
        }

        gInput.bits |= isGameKeyPressed(GameKeys.Jump) ? nJump : 0;
        gInput.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.PAUSE) ? nPause : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Weapon_Fire) ? nFire : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Crouch) ? nCrouch : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Look_Up) ? nLookUp : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Look_Down) ? nLookDown : 0;
        gInput.bits |= isGameKeyPressed(PsKeys.Aim_Up) ? nAimUp : 0;
        gInput.bits |= isGameKeyPressed(PsKeys.Aim_Down) ? nAimDown : 0;
        gInput.bits |= isGameKeyPressed(PsKeys.Aim_Center) ? nAimCenter : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Left) ? nItemLeft : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Right) ? nItemRight : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Use) ? nItemUse : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Run) ? nRun : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Turn_Around) ? nTurnAround : 0;

        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Hand) ? nItemHand : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Eye) ? nItemEye : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Mask) ? nItemMask : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Heart) ? nItemHeart : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Torch) ? nItemTorch : 0;
        gInput.bits |= isGameKeyJustPressed(PsKeys.Inventory_Scarab) ? nItemScarab : 0;

        PlayerStruct p = PlayerList[myconnectindex];

        if ((gInput.bits & nCrouch) != 0) {
            p.crouch_toggle = false;
        }

        boolean bUnderwater = (SectFlag[nPlayerViewSect[myconnectindex]] & 0x2000) != 0;
        boolean CrouchMode;
        if (bUnderwater) {
            CrouchMode = isGameKeyPressed(PsKeys.Crouch_toggle);
        } else {
            CrouchMode = isGameKeyJustPressed(PsKeys.Crouch_toggle);
        }

        if (bUnderwater) {
            p.crouch_toggle = CrouchMode;
        } else if (CrouchMode) {
            p.crouch_toggle = !p.crouch_toggle;
        }

        if (p.crouch_toggle) {
            gInput.bits |= nCrouch;
        }

        for (int i = 0; i < 7; i++) {
            if (isGameKeyJustPressed(cfg.getKeymap()[i + cfg.weaponIndex])) {
                gInput.bits |= (i + 1) << 13; // 8192
            }
        }
        if (isGameKeyJustPressed(GameKeys.Previous_Weapon)) {
            gInput.bits |= (8) << 13; // 65536
        }
        if (isGameKeyJustPressed(GameKeys.Next_Weapon)) {
            gInput.bits |= (9) << 13; // 73728
        }
        if (isGameKeyJustPressed(PsKeys.Last_Used_Weapon)) {
            gInput.bits |= (10) << 13; // 81920
        }

        int keymove = 40;
        int keyturn = 8;

        boolean running = ((!cfg.gAutoRun && (gInput.bits & nRun) != 0) || ((gInput.bits & nRun) == 0 && cfg.gAutoRun));

        if (running) {
            keymove = 80;
            keyturn = 12;
        }

        if (isGameKeyPressed(GameKeys.Strafe)) {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                svel -= -keymove;
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                svel -= keymove;
            }
            svel = (int) BClipRange(svel - 20 * ctrlGetMouseStrafe(), -keymove, keymove);
        } else {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                angvel -= keyturn;
            } else if (isGameKeyPressed(GameKeys.Turn_Right)) {
                angvel += keyturn;
            }
            angvel = BClipRange(angvel + ctrlGetMouseTurn(), -1024, 1024);
        }

        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            svel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            svel -= keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel -= keymove;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();
        if (cfg.isgMouseAim()) {
            horiz = BClipRange(ctrlGetMouseLook(!cfg.isgInvertmouse()), -(ydim >> 1), 100 + (ydim >> 1));
        } else {
            vel = (int) BClipRange(vel - ctrlGetMouseMove(), -4 * keymove, 4 * keymove);
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 8.0f;
            horiz = BClipRange(horiz - k * looky * cfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 8.0f;
            angvel = BClipRange(angvel + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            vel = (int) BClipRange(vel - (keymove * stick2.y), -4 * keymove, 4 * keymove);
        }
        if (stick2.x != 0) {
            svel = (int) BClipRange(svel - (keymove * stick2.x), -4 * keymove, 4 * keymove);
        }

        angvel = BClipRange(angvel, -127, 127);
        horiz = BClipRange(horiz, -127, 127);

        if (followmode && nOverhead != 0) {
            followvel = vel;
            followsvel = svel;
            followang = angvel;
            return;
        }

        float k = 0.7f;
        float angle = BClampAngle(inita);

        double xvel = (vel * BCosAngle(angle) + svel * BSinAngle(angle));
        double yvel = (vel * BSinAngle(angle) - svel * BCosAngle(angle));
        double len = Math.sqrt(xvel * xvel + yvel * yvel);
        if (len > (MAXVEL << 14)) {
            xvel = (xvel / len) * (MAXVEL << 14);
            yvel = (yvel / len) * (MAXVEL << 14);
        }

        lPlayerXVel += (int) (xvel * k);
        lPlayerYVel += (int) (yvel * k);

        lPlayerXVel = mulscale(lPlayerXVel, 0xD000, 16);
        lPlayerYVel = mulscale(lPlayerYVel, 0xD000, 16);

        if (klabs(lPlayerXVel) < 2048 && klabs(lPlayerYVel) < 2048) {
            lPlayerXVel = lPlayerYVel = 0;
        }

        gInput.xvel = lPlayerXVel;
        gInput.yvel = lPlayerYVel;
        gInput.avel = angvel;
        gInput.horiz = horiz;
        gInput.nTarget = besttarget;
        Ra[nLocalPlayer].nTarget = besttarget;
        gInput.nWeaponAim = (int) p.horiz;
    }

}
