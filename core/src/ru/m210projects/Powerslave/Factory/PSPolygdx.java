// This file is part of PowerslaveGDX.
// Copyright (C) 2021  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GdxRender.GDXOrtho;
import ru.m210projects.Build.Render.GdxRender.GDXRenderer;
import ru.m210projects.Build.settings.GameConfig;

public class PSPolygdx extends GDXRenderer {

    public PSPolygdx(GameConfig config) {
        super(config);
    }

    @Override
    protected GDXOrtho allocOrphoRenderer(Engine engine) {
        return new GDXOrtho(this, new PSMapSettings(engine.getBoardService()));
    }
}
