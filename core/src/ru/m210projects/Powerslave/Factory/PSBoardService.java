package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.SpriteMap;
import ru.m210projects.Build.Types.collections.ValueSetter;

import java.util.List;

public class PSBoardService extends BoardService {

    public PSBoardService() {
        registerBoard(6, SectorVer6.class, WallVer6.class, SpriteVer6.class);
    }

    @Override
    protected SpriteMap createSpriteMap(int listCount, List<Sprite> spriteList, int spriteCount, ValueSetter<Sprite> valueSetter) {
        return new SpriteMap(listCount, spriteList, spriteCount, valueSetter) {
            @Override
            protected Sprite getInstance() {
                return new SpriteVer6();
            }
        };
    }
}
