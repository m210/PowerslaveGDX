// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.FontHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;
import ru.m210projects.Powerslave.Font.FontA;
import ru.m210projects.Powerslave.Font.GameFont;
import ru.m210projects.Powerslave.Font.LargeFont;
import ru.m210projects.Powerslave.Font.MenuFont;
import ru.m210projects.Powerslave.Main;

import static ru.m210projects.Powerslave.Globals.BACKGROUND;

public class PSFactory extends BuildFactory {

    private final Main app;

    public PSFactory(Main app) {
        super("stuff.dat");
        this.app = app;

        OsdColor.DEFAULT.setPal(0);
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = app.getRenderer();
        renderer.rotatesprite(0, 0, 65536, 0, BACKGROUND, -128, 0, 10 | 16);
    }

    @Override
    public Engine engine() throws Exception {
        return Main.engine = new PSEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software) {
            return new PSSoftware(app.pCfg);
        } else if (type == RenderType.PolyGDX) {
            return new PSPolygdx(app.pCfg);
        } else {
            return new PSPolymost(app.pCfg);
        }
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new DefScript(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new PSOSDFunc();
    }

    @Override
    public MenuHandler menus() {
        return app.menu = new PSMenuHandler(app);
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(4) {
            @Override
            protected Font init(int i) {
                if (i == 0) {
                    return new GameFont(app.pEngine);
                }
                if (i == 1) {
                    return new MenuFont();
                }
                if (i == 2) {
                    return new FontA(app.pEngine);
                }
                return new LargeFont();
            }
        };
    }

    @Override
    public BuildNet net() {
        return new PSNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new PSSliderDrawable();
    }
}
