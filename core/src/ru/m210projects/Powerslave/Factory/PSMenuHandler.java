// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.CommonMenus.MenuVideoMode;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Powerslave.Menus.MenuInterfaceSet;
import ru.m210projects.Powerslave.Screens.MenuScreen;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Sound.*;

public class PSMenuHandler extends MenuHandler {

    public static final int MAIN = 0;
    public static final int GAME = 1;
    public static final int NEWGAME = 2;
    public static final int LOAD = 3;
    public static final int SAVE = 4;
    public static final int OPTIONS = 5;
    public static final int AUDIO = 6;
    public static final int COLORCORR = 7;
    public static final int QUIT = 8;
    public static final int QUITTITLE = 9;
    public static final int ENDGAME = 10;
    public static final int ADDON = 11;
    public static final int CORRUPTLOAD = 12;

    private final Engine engine;
    private final BuildGame app;
    private int oldValue;
    private long soundTime;

    public PSMenuHandler(BuildGame app) {
        super(app);
        mMenus = new BuildMenu[18];
        this.engine = app.pEngine;
        this.app = app;
    }

    @Override
    public int getShade(MenuItem item) {
        int shade = 32;
        if (item != null && item.isFocused()) {
            shade = 32 - (engine.getTotalClock() & 0x3F);
        }
        return shade;
    }

    @Override
    public int getPal(Font font, MenuItem item) {
        if (item != null) {
            if (!item.isEnabled()) {
                return 20;
            }

            return item.pal;
        }

        return 0;
    }

    @Override
    public void mDrawMenu() {
        Renderer renderer = game.getRenderer();
        if (!(app.getScreen() instanceof MenuScreen) && !(app.pMenu.getCurrentMenu() instanceof BuildMenuList)
                && !(app.pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            int tile = BACKGROUND;
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            ArtEntry pic = engine.getTile(tile);
            float kt = xdim / (float) ydim;
            float kv = pic.getWidth() / (float) pic.getHeight();
            float scale;
            if (kv >= kt) {
                scale = (ydim + 1) / (float) pic.getHeight();
            } else {
                scale = (xdim + 1) / (float) pic.getWidth();
            }

            renderer.rotatesprite(0, 0, (int) (scale * 65536), 0, tile, 127, 0, 8 | 16 | 1);
        }

        super.mDrawMenu();
    }

    @Override
    public void mDrawMouse(int x, int y) {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int zoom = scale(0x10000, ydim, 200);
        int czoom = mulscale(0x8000, mulscale(zoom, app.pCfg.getgMouseCursorSize(), 16), 16);
        int xoffset = 0; // 16;
        int yoffset = 0; // 23;
        int ang = 0;

        renderer.rotatesprite((x + xoffset) << 16, (y + yoffset) << 16, czoom, ang, MOUSECURSOR, 0, 0, 8);
    }

    @Override
    public void mDrawBackButton() {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }
        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(16384, ydim, 200);
        if (mCount > 1) {
            // Back button
            ArtEntry pic = engine.getTile(BACKBUTTON);

            int shade = 16 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
            renderer.rotatesprite(0, (ydim - mulscale(pic.getHeight(), zoom, 16)) << 16, zoom, 0, BACKBUTTON, shade,
                    0, 8 | 16, 0, 0, mulscale(zoom, pic.getWidth() - 1, 16), ydim - 1);
        }
    }

    @Override
    public boolean mCheckBackButton(int mx, int my) {
        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();
        int zoom = scale(16384, ydim, 200);
        ArtEntry pic = engine.getTile(BACKBUTTON);

        int size = mulscale(pic.getWidth(), zoom, 16);
        int bx = 0;
        int by = ydim - mulscale(pic.getHeight(), zoom, 16);
        if (mx >= bx && mx < bx + size) {
            return my >= by && my < by + size;
        }
        return false;
    }

    @Override
    public void mSound(MenuItem item, MenuOpt opt) {
        switch (opt) {
            case Open:
                PlayLocalSound(33, 0);
                break;
            case Close:
                PlayLocalSound(31, 0);
                break;
            case UP:
            case DW:
            case PGUP:
            case PGDW:
            case HOME:
            case END:
            case MCHANGE:
                PlayLocalSound(35, 0);
                break;
            case LEFT:
            case RIGHT:
            case ENTER:
            case MWUP:
            case MWDW:
                if (opt == MenuOpt.ENTER && item instanceof MenuConteiner
                        && item.getClass().getEnclosingClass() == MenuVideoMode.class) {
                    break;
                }
                if (item instanceof MenuSlider) {
                    if (GetLocalSound() != 23 || !LocalSoundPlaying()) {
                        PlayLocalSound(23, 0);
                    }
                } else {
                    PlayLocalSound(35, 0);
                }
                soundTime = System.currentTimeMillis();
                break;
            case LMB:
                if (item instanceof MenuSlider) {
                    MenuSlider slider = (MenuSlider) item;
                    if (oldValue != slider.value) {
                        oldValue = slider.value;
                        if (GetLocalSound() != 23 || !LocalSoundPlaying()) {
                            PlayLocalSound(23, 0);
                        }
                        soundTime = System.currentTimeMillis();
                    }
                } else {
                    if (!(item instanceof MenuScroller)
                            && !(item instanceof MenuFileBrowser)
                            && !(item instanceof MenuKeyboardList)) {
                        PlayLocalSound(35, 0);
                    }
                }
                break;
            default:
                break;
        }

        if (GetLocalSound() == 23 && (System.currentTimeMillis() - soundTime) >= 200) {
            StopLocalSound();
        }
    }

    @Override
    public void mPostDraw(MenuItem item) {
        Renderer renderer = game.getRenderer();
        int shade = 8 - (engine.getTotalClock() & 0x3F);
        if (item.isFocused()) {
            if (item instanceof MenuButton) {
                int py = item.y;
                int scale = 48000;
                if (item.align == 1) {
                    int centre = 320 >> 2;
                    if (item.font == app.getFont(1)) {
                        renderer.rotatesprite(((320 >> 1) + (centre >> 1) + 35) << 16, (py) << 16, scale, 1024, 3468,
                                shade, 0, 4 | 10);
                        renderer.rotatesprite(((320 >> 1) - (centre >> 1) - 35) << 16, (py) << 16, scale, 0, 3468, shade,
                                0, 10);
                    } else {
                        renderer.rotatesprite(((320 >> 1) - (item.font.getWidth(item.text, 1.0f) >> 1) - 12) << 16,
                                (item.y + 2) << 16, 16384, 0, 3468, shade, 0, 10);
                    }
                } else {
                    renderer.rotatesprite((item.x - 10) << 16, (item.y + 2) << 16, 16384, 0, 3468, shade, 0, 10);
                }
            } else if (item instanceof MenuList) {
                if (item instanceof MenuResolutionList) {
                    MenuList list = (MenuList) item;

                    int px = list.x;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());
                    renderer.rotatesprite((px + 45) << 16, (py + 2) << 16, 16384, 0, 3468, shade, 0, 10);
                } else if (item instanceof MenuKeyboardList || item instanceof MenuJoyList) {
                    MenuList list = (MenuList) item;
                    int px = list.x;
                    int py = list.y + (list.l_nFocus - list.l_nMin) * (list.mFontOffset());

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    renderer.rotatesprite((px - 10) << 16, (py + 2) << 16, 16384, 0, 3468, shade, 0, 10);
                }
            } else if (item instanceof MenuVariants) {
                renderer.rotatesprite((item.x - 20) << 16, (item.y - 1) << 16, 16384, 0, 3468, shade, 0, 10);
            } else if (!(item instanceof MenuFileBrowser)) {
                renderer.rotatesprite((item.x - 10) << 16, (item.y + 2) << 16, 16384, 0, 3468, shade, 0, 10);
            }
        }
    }

}
