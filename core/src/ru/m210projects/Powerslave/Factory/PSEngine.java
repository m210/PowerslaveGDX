// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Tables;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;

import java.io.IOException;

import static ru.m210projects.Powerslave.Bullet.IgniteSprite;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.engine;
import static ru.m210projects.Powerslave.Player.GetPlayerFromSprite;
import static ru.m210projects.Powerslave.PSSector.CheckSectorFloor;
import static ru.m210projects.Powerslave.PSSector.overridesect;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.*;

public class PSEngine extends Engine {

    private final short[] AngTable = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 30, 30, 30, 30, 30, 30, 30, 31, 31, 31, 31, 31, 31, 32, 32, 32, 32, 32, 32, 33, 33, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 34, 35, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 36, 36, 37, 37, 37, 37, 37, 37, 38, 38, 38, 38, 38, 38, 38, 39, 39, 39, 39, 39, 39, 40, 40, 40, 40, 40, 40, 41, 41, 41, 41, 41, 41, 41, 42, 42, 42, 42, 42, 42, 43, 43, 43, 43, 43, 43, 43, 44, 44, 44, 44, 44, 44, 45, 45, 45, 45, 45, 45, 46, 46, 46, 46, 46, 46, 46, 47, 47, 47, 47, 47, 47, 48, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 49, 49, 50, 50, 50, 50, 50, 50, 51, 51, 51, 51, 51, 51, 51, 52, 52, 52, 52, 52, 52, 53, 53, 53, 53, 53, 53, 53, 54, 54, 54, 54, 54, 54, 55, 55, 55, 55, 55, 55, 55, 56, 56, 56, 56, 56, 56, 57, 57, 57, 57, 57, 57, 57, 58, 58, 58, 58, 58, 58, 59, 59, 59, 59, 59, 59, 59, 60, 60, 60, 60, 60, 60, 61, 61, 61, 61, 61, 61, 61, 62, 62, 62, 62, 62, 62, 63, 63, 63, 63, 63, 63, 63, 64, 64, 64, 64, 64, 64, 65, 65, 65, 65, 65, 65, 65, 66, 66, 66, 66, 66, 66, 67, 67, 67, 67, 67, 67, 67, 68, 68, 68, 68, 68, 68, 69, 69, 69, 69, 69, 69, 69, 70, 70, 70, 70, 70, 70, 70, 71, 71, 71, 71, 71, 71, 72, 72, 72, 72, 72, 72, 72, 73, 73, 73, 73, 73, 73, 74, 74, 74, 74, 74, 74, 74, 75, 75, 75, 75, 75, 75, 75, 76, 76, 76, 76, 76, 76, 77, 77, 77, 77, 77, 77, 77, 78, 78, 78, 78, 78, 78, 78, 79, 79, 79, 79, 79, 79, 80, 80, 80, 80, 80, 80, 80, 81, 81, 81, 81, 81, 81, 81, 82, 82, 82, 82, 82, 82, 83, 83, 83, 83, 83, 83, 83, 84, 84, 84, 84, 84, 84, 84, 85, 85, 85, 85, 85, 85, 86, 86, 86, 86, 86, 86, 86, 87, 87, 87, 87, 87, 87, 87, 88, 88, 88, 88, 88, 88, 88, 89, 89, 89, 89, 89, 89, 90, 90, 90, 90, 90, 90, 90, 91, 91, 91, 91, 91, 91, 91, 92, 92, 92, 92, 92, 92, 92, 93, 93, 93, 93, 93, 93, 93, 94, 94, 94, 94, 94, 94, 94, 95, 95, 95, 95, 95, 95, 96, 96, 96, 96, 96, 96, 96, 97, 97, 97, 97, 97, 97, 97, 98, 98, 98, 98, 98, 98, 98, 99, 99, 99, 99, 99, 99, 99, 100, 100, 100, 100, 100, 100, 100, 101, 101, 101, 101, 101, 101, 101, 102, 102, 102, 102, 102, 102, 102, 103, 103, 103, 103, 103, 103, 103, 104, 104, 104, 104, 104, 104, 104, 105, 105, 105, 105, 105, 105, 105, 106, 106, 106, 106, 106, 106, 106, 107, 107, 107, 107, 107, 107, 107, 108, 108, 108, 108, 108, 108, 108, 109, 109, 109, 109, 109, 109, 109, 110, 110, 110, 110, 110, 110, 110, 111, 111, 111, 111, 111, 111, 111, 112, 112, 112, 112, 112, 112, 112, 113, 113, 113, 113, 113, 113, 113, 114, 114, 114, 114, 114, 114, 114, 115, 115, 115, 115, 115, 115, 115, 116, 116, 116, 116, 116, 116, 116, 117, 117, 117, 117, 117, 117, 117, 117, 118, 118, 118, 118, 118, 118, 118, 119, 119, 119, 119, 119, 119, 119, 120, 120, 120, 120, 120, 120, 120, 121, 121, 121, 121, 121, 121, 121, 122, 122, 122, 122, 122, 122, 122, 122, 123, 123, 123, 123, 123, 123, 123, 124, 124, 124, 124, 124, 124, 124, 125, 125, 125, 125, 125, 125, 125, 125, 126, 126, 126, 126, 126, 126, 126, 127, 127, 127, 127, 127, 127, 127, 128, 128, 128, 128, 128, 128, 128, 128, 129, 129, 129, 129, 129, 129, 129, 130, 130, 130, 130, 130, 130, 130, 131, 131, 131, 131, 131, 131, 131, 131, 132, 132, 132, 132, 132, 132, 132, 133, 133, 133, 133, 133, 133, 133, 133, 134, 134, 134, 134, 134, 134, 134, 135, 135, 135, 135, 135, 135, 135, 135, 136, 136, 136, 136, 136, 136, 136, 137, 137, 137, 137, 137, 137, 137, 137, 138, 138, 138, 138, 138, 138, 138, 139, 139, 139, 139, 139, 139, 139, 139, 140, 140, 140, 140, 140, 140, 140, 141, 141, 141, 141, 141, 141, 141, 141, 142, 142, 142, 142, 142, 142, 142, 142, 143, 143, 143, 143, 143, 143, 143, 144, 144, 144, 144, 144, 144, 144, 144, 145, 145, 145, 145, 145, 145, 145, 145, 146, 146, 146, 146, 146, 146, 146, 146, 147, 147, 147, 147, 147, 147, 147, 148, 148, 148, 148, 148, 148, 148, 148, 149, 149, 149, 149, 149, 149, 149, 149, 150, 150, 150, 150, 150, 150, 150, 150, 151, 151, 151, 151, 151, 151, 151, 151, 152, 152, 152, 152, 152, 152, 152, 153, 153, 153, 153, 153, 153, 153, 153, 154, 154, 154, 154, 154, 154, 154, 154, 155, 155, 155, 155, 155, 155, 155, 155, 156, 156, 156, 156, 156, 156, 156, 156, 157, 157, 157, 157, 157, 157, 157, 157, 158, 158, 158, 158, 158, 158, 158, 158, 159, 159, 159, 159, 159, 159, 159, 159, 160, 160, 160, 160, 160, 160, 160, 160, 161, 161, 161, 161, 161, 161, 161, 161, 162, 162, 162, 162, 162, 162, 162, 162, 162, 163, 163, 163, 163, 163, 163, 163, 163, 164, 164, 164, 164, 164, 164, 164, 164, 165, 165, 165, 165, 165, 165, 165, 165, 166, 166, 166, 166, 166, 166, 166, 166, 167, 167, 167, 167, 167, 167, 167, 167, 167, 168, 168, 168, 168, 168, 168, 168, 168, 169, 169, 169, 169, 169, 169, 169, 169, 170, 170, 170, 170, 170, 170, 170, 170, 170, 171, 171, 171, 171, 171, 171, 171, 171, 172, 172, 172, 172, 172, 172, 172, 172, 173, 173, 173, 173, 173, 173, 173, 173, 173, 174, 174, 174, 174, 174, 174, 174, 174, 175, 175, 175, 175, 175, 175, 175, 175, 175, 176, 176, 176, 176, 176, 176, 176, 176, 177, 177, 177, 177, 177, 177, 177, 177, 177, 178, 178, 178, 178, 178, 178, 178, 178, 178, 179, 179, 179, 179, 179, 179, 179, 179, 180, 180, 180, 180, 180, 180, 180, 180, 180, 181, 181, 181, 181, 181, 181, 181, 181, 181, 182, 182, 182, 182, 182, 182, 182, 182, 183, 183, 183, 183, 183, 183, 183, 183, 183, 184, 184, 184, 184, 184, 184, 184, 184, 184, 185, 185, 185, 185, 185, 185, 185, 185, 185, 186, 186, 186, 186, 186, 186, 186, 186, 186, 187, 187, 187, 187, 187, 187, 187, 187, 188, 188, 188, 188, 188, 188, 188, 188, 188, 189, 189, 189, 189, 189, 189, 189, 189, 189, 190, 190, 190, 190, 190, 190, 190, 190, 190, 191, 191, 191, 191, 191, 191, 191, 191, 191, 192, 192, 192, 192, 192, 192, 192, 192, 192, 192, 193, 193, 193, 193, 193, 193, 193, 193, 193, 194, 194, 194, 194, 194, 194, 194, 194, 194, 195, 195, 195, 195, 195, 195, 195, 195, 195, 196, 196, 196, 196, 196, 196, 196, 196, 196, 197, 197, 197, 197, 197, 197, 197, 197, 197, 197, 198, 198, 198, 198, 198, 198, 198, 198, 198, 199, 199, 199, 199, 199, 199, 199, 199, 199, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 201, 201, 201, 201, 201, 201, 201, 201, 201, 202, 202, 202, 202, 202, 202, 202, 202, 202, 202, 203, 203, 203, 203, 203, 203, 203, 203, 203, 204, 204, 204, 204, 204, 204, 204, 204, 204, 204, 205, 205, 205, 205, 205, 205, 205, 205, 205, 206, 206, 206, 206, 206, 206, 206, 206, 206, 206, 207, 207, 207, 207, 207, 207, 207, 207, 207, 207, 208, 208, 208, 208, 208, 208, 208, 208, 208, 209, 209, 209, 209, 209, 209, 209, 209, 209, 209, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 211, 211, 211, 211, 211, 211, 211, 211, 211, 211, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 213, 213, 213, 213, 213, 213, 213, 213, 213, 213, 214, 214, 214, 214, 214, 214, 214, 214, 214, 214, 215, 215, 215, 215, 215, 215, 215, 215, 215, 215, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 217, 217, 217, 217, 217, 217, 217, 217, 217, 217, 218, 218, 218, 218, 218, 218, 218, 218, 218, 218, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 220, 220, 220, 220, 220, 220, 220, 220, 220, 220, 221, 221, 221, 221, 221, 221, 221, 221, 221, 221, 222, 222, 222, 222, 222, 222, 222, 222, 222, 222, 222, 223, 223, 223, 223, 223, 223, 223, 223, 223, 223, 224, 224, 224, 224, 224, 224, 224, 224, 224, 224, 224, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 226, 226, 226, 226, 226, 226, 226, 226, 226, 226, 226, 227, 227, 227, 227, 227, 227, 227, 227, 227, 227, 228, 228, 228, 228, 228, 228, 228, 228, 228, 228, 228, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 229, 230, 230, 230, 230, 230, 230, 230, 230, 230, 230, 230, 231, 231, 231, 231, 231, 231, 231, 231, 231, 231, 231, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 233, 233, 233, 233, 233, 233, 233, 233, 233, 233, 233, 234, 234, 234, 234, 234, 234, 234, 234, 234, 234, 234, 235, 235, 235, 235, 235, 235, 235, 235, 235, 235, 235, 236, 236, 236, 236, 236, 236, 236, 236, 236, 236, 236, 237, 237, 237, 237, 237, 237, 237, 237, 237, 237, 237, 238, 238, 238, 238, 238, 238, 238, 238, 238, 238, 238, 238, 239, 239, 239, 239, 239, 239, 239, 239, 239, 239, 239, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 241, 241, 241, 241, 241, 241, 241, 241, 241, 241, 241, 242, 242, 242, 242, 242, 242, 242, 242, 242, 242, 242, 242, 243, 243, 243, 243, 243, 243, 243, 243, 243, 243, 243, 244, 244, 244, 244, 244, 244, 244, 244, 244, 244, 244, 244, 245, 245, 245, 245, 245, 245, 245, 245, 245, 245, 245, 245, 246, 246, 246, 246, 246, 246, 246, 246, 246, 246, 246, 246, 247, 247, 247, 247, 247, 247, 247, 247, 247, 247, 247, 248, 248, 248, 248, 248, 248, 248, 248, 248, 248, 248, 248, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 250, 250, 250, 250, 250, 250, 250, 250, 250, 250, 250, 250, 251, 251, 251, 251, 251, 251, 251, 251, 251, 251, 251, 251, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 256, 256, 256, 256, 256, 256};
    private int next = 1;

//	private boolean key = false;
//	@Override
//	public void getTimer().update() {
//		if (timerfreq == 0)
//			return;
//
//		long n = (getticks() * timerticspersec / timerfreq) - timerlastsample;
//		if (n > 0) {
//			if(game.isCurrentScreen(gDemoScreen)) {
////				engine.getTotalClock() += 8;
//				if(BuildGdx.input.isKeyPressed(com.badlogic.gdx.Input.Keys.UP)) {
//					if(!key) {
//						engine.getTotalClock() += 4;
//						if(BuildGdx.input.isKeyPressed(com.badlogic.gdx.Input.Keys.SHIFT_RIGHT)) {
//							key = true;
//						}
//					}
//				} else key = false;
//			} else engine.getTotalClock() += n;
//
//			timerlastsample += n;
//		}
//	}

    public PSEngine(BuildGame game) throws Exception {
        super(game);

        inittimer(game.pCfg.isLegacyTimer(), 120, 4);
    }

    @Override
    protected BoardService createBoardService() {
        return new PSBoardService();
    }

//	@Override
//	public short insertsprite(short sectnum, short statnum)
//	{
//		 short i = super.insertsprite(sectnum, statnum);
//		 boardService.getSprite(i).clipdist = 0;
//		 boardService.getSprite(i).xrepeat = 0;
//		 boardService.getSprite(i).yrepeat = 0;
//		 boardService.getSprite(i).extra = 0;
//		 boardService.getSprite(i).owner = 0;
//		 return i;
//	}

    @Override
    public int rand() {
        next = (int) ((next & 0xFFFFFFFFL) * 1103515245 + 12345);
        return (next >> 16) & 32767;
    }

    public int GetMyAngle(int a1, int a2) {
        int result;
        int v3 = -a1;
        int v4 = a2 << 11;
        if (a1 <= 0) {
            int v5 = -2048 * a1;
            if (a2 < 0) {
                int v7 = -a2;
                if (v3 == v7) {
                    return 1280;
                }
                if (v3 > v7) {
                    return (short) ((AngTable[(v7 << 11) / v3 & 0x7FF] + 1024) & 0x7FF);
                }
                result = (short) (1024 - AngTable[v5 / v7 & 0x7FF]);
            } else {
                if (v3 == a2) {
                    return 768;
                }
                if (a2 <= v3) {
                    result = (short) (512 - AngTable[v4 / v3 & 0x7FF]);
                } else {
                    result = AngTable[v5 / a2 & 0x7FF];
                }
            }
            result += 512;
            result &= 0x7FF;
            return result;
        }
        if (a2 < 0) {
            int v8 = -a2;
            if (a1 == -a2) {
                return 1792;
            }
            if (a1 < v8) {
                result = AngTable[(a1 << 11) / v8 & 0x7FF];
                result += 1536;
                result &= 0x7FF;
                return result;
            }
            result = (short) (1536 - AngTable[-2048 * a2 / a1 & 0x7FF]);
            result += 512;
            result &= 0x7FF;
            return result;
        }
        if (a1 == a2) {
            return 256;
        }
        if (a1 <= a2) {
            result = (short) (2048 - AngTable[(a1 << 11) / a2 & 0x7FF]);
            result += 512;
        } else {
            result = AngTable[v4 / a1 & 0x7FF];
            result += 2048;
        }

        result &= 0x7FF;
        return result;
    }

    public int GetWallNormal(int nWall) {
        Wall wal = boardService.getWall(nWall);
        if (wal == null) {
            return 0;
        }

        return (GetMyAngle(wal.getWall2().getX() - wal.getX(), wal.getWall2().getY() - wal.getY()) + 512) & 0x7FF;
    }

    public void mydeletesprite(int num) {
        if (num < 0 || num >= MAXSPRITES) {
            throw new AssertException("bad sprite value " + num + " handed to mydeletesprite");
        }

        game.pInt.clearspriteinterpolate(num);
        deletesprite(num);
        if (num == besttarget) {
            besttarget = -1;
        }
    }

    public void mychangespritesect(int spritenum, int newsectnum) {
        Sprite sprite = boardService.getSprite(spritenum);
        if (sprite == null || sprite.getSectnum() == newsectnum) {
            return;
        }

        DoKenTest();
        changespritesect(spritenum, newsectnum);
        DoKenTest();
    }

    private void DoKenTest() {
        Sprite spr = boardService.getSprite(PlayerList[0].spriteId);
        if (spr == null) {
            return;
        }

        for (ListNode<Sprite> node = boardService.getSectNode(spr.getSectnum()); /* nothing */ ; node = node.getNext()) {
            if (node == null) {
                return;
            }

            ListNode<Sprite> next = node.getNext();
            if (next != null && next.getIndex() == node.getIndex()) {
                break;
            }
        }
        throw new AssertException("ERROR in Ken's linked list!");
    }

    @Override
    public int clipmove(int x, int y, int z, int sectnum,
                        long xvect, long yvect, int walldist, int ceildist, int flordist, int cliptype) {
        return CONVERT_HIT(super.clipmove(x, y, z, sectnum, xvect, yvect, walldist, ceildist, flordist, cliptype));
    }

    @Override
    public void getzrange(int x, int y, int z, int sectnum,
                          int walldist, int cliptype) {

        super.getzrange(x, y, z, sectnum, walldist, cliptype);
        zr_ceilhit = CONVERT_HIT(zr_ceilhit);
        zr_florhit = CONVERT_HIT(zr_florhit);
    }

    public int movesprite(int spritenum, int dx, int dy, int dz, int ignoredceildist, int flordist, int cliptype) {
        if (cliptype == 1) {
            cliptype = CLIPMASK1;
        } else {
            cliptype = CLIPMASK0;
        }

        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        game.pInt.setsprinterpolate(spritenum, spr);

        bTouchFloor[0] = false;
        int sx = spr.getX();
        int sy = spr.getY();
        int sz = spr.getZ();
        int sectnum;
        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return 0;
        }

        int height = GetSpriteHeight(spritenum);
        if ((SectFlag[spr.getSectnum()] & 0x2000) != 0 || sec.getFloorz() < spr.getZ()) {
            dx >>= 1;
            dy >>= 1;
        }
        int zhit = movespritez(spritenum, dz, height, flordist, spr.getClipdist() * 4);
        sectnum = spr.getSectnum();

        if (spr.getStatnum() == 100) {
            int plr = GetPlayerFromSprite(spritenum);
            Vector2 floorz = CheckSectorFloor(overridesect, spr.getZ(), 0, 0);
            if (floorz != null) {
                nXDamage[plr] = (int) floorz.x;
                nYDamage[plr] = (int) floorz.y;
            }
            dx += nXDamage[plr];
            dy += nYDamage[plr];
        } else {
            Vector2 floorz = CheckSectorFloor(overridesect, spr.getZ(), dx, dy);
            if (floorz != null) {
                dx = (int) floorz.x;
                dy = (int) floorz.y;
            }
        }

        int movehit = engine.clipmove(spr.getX(), spr.getY(), spr.getZ(), sectnum, dx, dy, 4 * spr.getClipdist(), height, flordist, cliptype);
        spr.setX(clipmove_x);
        spr.setY(clipmove_y);
        spr.setZ(clipmove_z);
        sectnum = clipmove_sectnum;

        int sumhit = (movehit | zhit);
        if (sectnum != spr.getSectnum() && sectnum >= 0) {
            if ((sumhit & nEventProcess) != 0) {
                dz = 0;
            }

            sec = boardService.getSector(sectnum);
            if (sec == null) {
                return 0;
            }

            if (sec.getFloorz() - sz >= flordist + dz) {
                mychangespritesect(spritenum, sectnum);
                if (spr.getPal() < 5 && spr.getHitag() == 0) {
                    spr.setPal(sec.getCeilingpal());
                }
            } else {
                spr.setX(sx);
                spr.setY(sy);
            }
        }

        if ((sumhit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
            if (!boardService.isValidSprite(sumhit & PS_HIT_INDEX_MASK)) {
                sumhit = movehit;
            }
        }

        return sumhit;
    }

    private int movespritez(final int spritenum, int dz, int ceildist, int ignoredflordist, int cliptype) {
        Sprite pSprite = boardService.getSprite(spritenum);
        if (pSprite == null) {
            return 0;
        }

        int sectnum = pSprite.getSectnum();
        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec == null) {
            return 0;
        }


        overridesect = sectnum;
        int v6 = sectnum;
        int oldcstat = pSprite.getCstat();
        pSprite.setCstat(pSprite.getCstat() & ~1);

        int movehit = 0;
        if ((SectFlag[sectnum] & 0x2000) != 0) {
            dz >>= 1;
        }
        int sz = pSprite.getZ();
        int floorz = sec.getFloorz();
        int height = dz + sz;
        int v12 = (ceildist >> 1) + sec.getCeilingz();
        if ((SectFlag[sectnum] & 0x2000) != 0 && height < v12) {
            height = v12;
        }

        while (true) {
            int v13 = pSprite.getSectnum();
            sec = boardService.getSector(v13);
            if (sec == null) {
                return 0;
            }

            if (height <= sec.getFloorz() || SectBelow[v13] < 0) {
                break;
            }
            v6 = SectBelow[v13];
            mychangespritesect(spritenum, SectBelow[v13]);
        }

        if (v6 == sectnum) {
            while (true) {
                int v15 = pSprite.getSectnum();
                sec = boardService.getSector(v15);
                if (sec == null) {
                    return 0;
                }

                if (height >= sec.getCeilingz()) {
                    break;
                }
                if (SectAbove[v15] < 0) {
                    break;
                }
                v6 = SectAbove[v15];
                mychangespritesect(spritenum, v6);
            }
        } else {
            pSprite.setZ(height);
            if ((SectFlag[v6] & 0x2000) != 0) {
                if (spritenum == PlayerList[nLocalPlayer].spriteId) {
                    D3PlayFX(StaticSound[2], spritenum);
                }
                if (pSprite.getStatnum() <= 107) {
                    pSprite.setHitag(0);
                }
            }
        }

        getzrange(pSprite.getX(), pSprite.getY(), pSprite.getZ() - 256, pSprite.getSectnum(), 128, CLIPMASK0);
        int lohit = zr_florhit;
        int hihit = zr_ceilhit;
        int sprceiling = zr_ceilz;

        int v17 = zr_florz;
        if ((lohit & PS_HIT_TYPE_MASK) != PS_HIT_SPRITE) {
            v17 = (SectDepth[pSprite.getSectnum()] & 0xFFFF) + zr_florz;
        }
        if (height > v17) {
            if (dz > 0) {
                bTouchFloor[0] = true;
                if ((lohit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                    int lospr = lohit & PS_HIT_INDEX_MASK;
                    Sprite hitSpr = boardService.getSprite(lospr);
                    if (hitSpr != null) {
                        if (pSprite.getStatnum() == 100 && hitSpr.getStatnum() != 0 && hitSpr.getStatnum() < 100) {
                            if ((dz >> 9) != 0) {
                                DamageEnemy(lospr, spritenum, 2 * (dz >> 9));
                            }
                            pSprite.setZvel(-dz);
                        } else {
                            if (hitSpr.getStatnum() != 0 && hitSpr.getStatnum() <= 199) {
                                movehit = lohit;
                            } else {
                                movehit = nEventProcess;
                            }
                            pSprite.setZvel(0);
                        }
                    }
                } else if (SectBelow[pSprite.getSectnum()] == -1) {
                    movehit = nEventProcess;
                    if (SectDamage[pSprite.getSectnum()] != 0) {
                        if (pSprite.getHitag() < 15) {
                            IgniteSprite(spritenum);
                            pSprite.setHitag(20);
                        }
                        if (SectDamage[pSprite.getSectnum()] >> 2 != SectDamage[pSprite.getSectnum()] >> 4) {
                            DamageEnemy(spritenum, -1, (SectDamage[pSprite.getSectnum()] >> 2) - (SectDamage[pSprite.getSectnum()] >> 4));
                        }
                    }
                    pSprite.setZvel(0);
                }
            }
            height = v17;
            pSprite.setZ(v17);
        } else if (height - ceildist < sprceiling && ((hihit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE || SectAbove[pSprite.getSectnum()] == -1)) {
            height = sprceiling + ceildist;
            movehit = nEvent1;
        }

        if (sz <= floorz && height > floorz && (SectDepth[sectnum] != 0 || v6 != sectnum && (SectFlag[v6] & 0x2000) != 0)) {
            BuildSplash(pSprite, sectnum);
        }
        pSprite.setCstat(oldcstat);
        pSprite.setZ(height);
        if (pSprite.getStatnum() == 100) {
            BuildNear(pSprite.getX(), pSprite.getY(), cliptype / 2 + cliptype, pSprite.getSectnum());
            movehit |= BelowNear(pSprite, lohit);
        }
        return movehit;
    }

    @Override
    protected Tables loadtables() throws IOException {
        return new PSTables(game.getCache().getEntry("tables.dat", true));
    }

    @Override
    public PaletteManager loadpalette() throws IOException {
        return new PSPaletteManager(this, game.getCache().getEntry("palette.dat", true));
    }

    @Override
    public PSPaletteManager getPaletteManager() {
        return (PSPaletteManager) paletteManager;
    }
}