package ru.m210projects.Powerslave.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Powerslave.Cheats.IsCheatCode;
import static ru.m210projects.Powerslave.Cheats.cheatCode;
import static ru.m210projects.Powerslave.Globals.LOGO;
import static ru.m210projects.Powerslave.Main.gGameScreen;
import static ru.m210projects.Powerslave.Main.game;

public class PSOSDFunc extends DefaultOsdFunc {

    public PSOSDFunc() {
        super(game.getRenderer());

        BGTILE = 1551; //3669;
        BGCTILE = LOGO;
        BORDTILE = 3469;
        PALETTE = 0;

        BITSTH = 1 + 8 + 16;

        OsdColor.RED.setPal(166);
        OsdColor.BLUE.setPal(20);
        OsdColor.YELLOW.setPal(180); //175
        OsdColor.BROWN.setPal(37);
        OsdColor.GREEN.setPal(233);
    }

    @Override
    protected Font getFont() {
        return EngineUtils.getLargeFont();
    }

    @Override
    public void showOsd(boolean captured) {
        super.showOsd(captured);

        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!captured);
        }
    }

    @Override
    public boolean textHandler(String message) {
        if (numplayers > 1) {
            return false;
        }

        boolean isCheat = false;
        for (String s : cheatCode) {
            if (message.equalsIgnoreCase(s)) {
                isCheat = true;
                break;
            }
        }

        if (!game.isCurrentScreen(gGameScreen) && isCheat) {
            Console.out.println(message + ": not in a game");
            return true;
        }

        return IsCheatCode(message);
    }
}
