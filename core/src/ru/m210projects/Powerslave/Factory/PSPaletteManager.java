package ru.m210projects.Powerslave.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Types.Color;
import ru.m210projects.Build.Types.DefaultPaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.bTorch;
import static ru.m210projects.Powerslave.Main.engine;
import static ru.m210projects.Powerslave.Main.game;
import static ru.m210projects.Powerslave.Sound.PlayLocalSound;
import static ru.m210projects.Powerslave.View.StatusMessage;

public class PSPaletteManager extends DefaultPaletteManager {

    public static final Color WATER_FOG_COLOR = new Color(0, 10, 20, 0);

    public boolean bGreenPalette;

    public PSPaletteManager(Engine engine, Entry entry) throws IOException {
        super(engine, new PSPaletteLoader(entry));

        loadremaps();
    }

    private void loadremaps() throws IOException {
        String[] flookups = {"normal.rmp", "nodim.rmp", "torch.rmp", "notorch.rmp", "brite.rmp", "redbrite.rmp",
                "grnbrite.rmp", "normal.rmp", "nodim.rmp", "torch.rmp", "notorch.rmp", "brite.rmp"};

        for (int i = 0; i < 12; i++) {
            Entry entry = game.cache.getEntry(flookups[i], true);
            if (!entry.exists()) {
                throw new IOException("Error reading palette lookup " + "\"" + flookups[i] + "\"!");
            }

            byte[] palookup = makePalookup(i, null, 0, 0, 0, 0);
            try (InputStream is = entry.getInputStream()) {
                StreamUtils.readBytes(is, palookup, 0x4000);
            }

            if (i == 5) {
                setFogColor(i, new Color(63, 0, 0, 0));
            }
            origpalookup[i] = palookup;
        }
    }

    public void setGreenPal() {
        if (bGreenPalette) {
            return;
        }

        for (int i = 0; i < 12; i++) {
            palookup[i] = palookup[6];
            listener.onPalookupChanged(i);
        }
        palookup[5] = origpalookup[5];
        bGreenPalette = true;
    }

    public void restoreGreenPal() {
        if (!bGreenPalette) {
            return;
        }

        System.arraycopy(origpalookup, 0, palookup, 0, 12);

        for (int i = 0; i < 12; i++) {
            listener.onPalookupChanged(i);
        }
        bGreenPalette = false;
    }

    public void updateUnderwaterGLFog() {
        if (!game.pCfg.isPaletteEmulation() && (SectFlag[nPlayerViewSect[nLocalPlayer]] & 0x2000) != 0) {
            palookupfog[0] = WATER_FOG_COLOR;
        } else {
            palookupfog[0] = DEFAULT_COLOR;
        }
    }

    public void SetTorch(int nPlayer, int nTorch) {
        if (nTorch != bTorch) {
            if (nPlayer == nLocalPlayer) {
                // 2, 9 torch.rmp
                // 3, 10 notorch.rmp

                byte[][] palookup = engine.getPaletteManager().getPalookupBuffer();
                SwapPalookup(palookup, 2, 3);
                SwapPalookup(palookup, 9, 10);

                if (nTorch == 2) {
                    bTorch = (bTorch == 0) ? 1 : 0;
                } else {
                    bTorch = nTorch;
                }
                if (bTorch != 0) {
                    PlayLocalSound(12, 0);
                }

                StatusMessage(150, "TORCH IS " + ((bTorch != 0) ? "LIT" : "OUT"), nPlayer);
            }
        }
    }

    public void LoadTorch(int nTorch) {
        byte[][] palookup = engine.getPaletteManager().getPalookupBuffer();
        palookup[2] = origpalookup[2];
        palookup[3] = origpalookup[3];
        palookup[9] = origpalookup[9];
        palookup[10] = origpalookup[10];

        if (nTorch != 0) {
            SwapPalookup(palookup, 2, 3);
            SwapPalookup(palookup, 9, 10);
        }
    }

    private void SwapPalookup(byte[][] palookup, int num1, int num2) {
        byte[] tmp = palookup[num1];
        palookup[num1] = palookup[num2];
        palookup[num2] = tmp;

        listener.onPalookupChanged(num1);
        listener.onPalookupChanged(num2);
    }
}
