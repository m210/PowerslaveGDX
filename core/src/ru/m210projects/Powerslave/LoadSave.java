// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Menus.MenuCorruptGame;
import ru.m210projects.Powerslave.Type.EpisodeInfo;
import ru.m210projects.Powerslave.Type.LSInfo;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Powerslave.Anim.loadAnm;
import static ru.m210projects.Powerslave.Anim.saveAnm;
import static ru.m210projects.Powerslave.Bullet.loadBullets;
import static ru.m210projects.Powerslave.Bullet.saveBullets;
import static ru.m210projects.Powerslave.Enemies.Anubis.loadAnubis;
import static ru.m210projects.Powerslave.Enemies.Anubis.saveAnubis;
import static ru.m210projects.Powerslave.Enemies.Fish.loadFish;
import static ru.m210projects.Powerslave.Enemies.Fish.saveFish;
import static ru.m210projects.Powerslave.Enemies.LavaDude.loadLava;
import static ru.m210projects.Powerslave.Enemies.LavaDude.saveLava;
import static ru.m210projects.Powerslave.Enemies.Lion.loadLion;
import static ru.m210projects.Powerslave.Enemies.Lion.saveLion;
import static ru.m210projects.Powerslave.Enemies.Mummy.loadMummy;
import static ru.m210projects.Powerslave.Enemies.Mummy.saveMummy;
import static ru.m210projects.Powerslave.Enemies.Queen.loadQueen;
import static ru.m210projects.Powerslave.Enemies.Queen.saveQueen;
import static ru.m210projects.Powerslave.Enemies.Ra.loadRa;
import static ru.m210projects.Powerslave.Enemies.Ra.saveRa;
import static ru.m210projects.Powerslave.Enemies.Rat.loadRat;
import static ru.m210projects.Powerslave.Enemies.Rat.saveRat;
import static ru.m210projects.Powerslave.Enemies.Rex.loadRex;
import static ru.m210projects.Powerslave.Enemies.Rex.saveRex;
import static ru.m210projects.Powerslave.Enemies.Roach.loadRoach;
import static ru.m210projects.Powerslave.Enemies.Roach.saveRoach;
import static ru.m210projects.Powerslave.Enemies.Scorp.loadScorp;
import static ru.m210projects.Powerslave.Enemies.Scorp.saveScorp;
import static ru.m210projects.Powerslave.Enemies.Set.loadSet;
import static ru.m210projects.Powerslave.Enemies.Set.saveSet;
import static ru.m210projects.Powerslave.Enemies.Spider.loadSpider;
import static ru.m210projects.Powerslave.Enemies.Spider.saveSpider;
import static ru.m210projects.Powerslave.Enemies.Wasp.loadWasp;
import static ru.m210projects.Powerslave.Enemies.Wasp.saveWasp;
import static ru.m210projects.Powerslave.Energy.InitEnergyTile;
import static ru.m210projects.Powerslave.Factory.PSMenuHandler.CORRUPTLOAD;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Grenade.loadGrenades;
import static ru.m210projects.Powerslave.Grenade.saveGrenades;
import static ru.m210projects.Powerslave.Light.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.*;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.checkEpisodeResources;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.resetEpisodeResources;
import static ru.m210projects.Powerslave.Object.*;
import static ru.m210projects.Powerslave.Palette.GrabPalette;
import static ru.m210projects.Powerslave.Player.SetPlayerItem;
import static ru.m210projects.Powerslave.Random.*;
import static ru.m210projects.Powerslave.RunList.loadRunList;
import static ru.m210projects.Powerslave.RunList.saveRunList;
import static ru.m210projects.Powerslave.Screens.GameScreen.InitClockTile;
import static ru.m210projects.Powerslave.PSSector.*;
import static ru.m210projects.Powerslave.Slide.loadSlide;
import static ru.m210projects.Powerslave.Slide.saveSlide;
import static ru.m210projects.Powerslave.Snake.loadSnakes;
import static ru.m210projects.Powerslave.Snake.saveSnakes;
import static ru.m210projects.Powerslave.Sound.sndCheckUserMusic;
import static ru.m210projects.Powerslave.SpiritHead.nSpiritSprite;
import static ru.m210projects.Powerslave.Sprites.*;
import static ru.m210projects.Powerslave.Switch.loadSwitches;
import static ru.m210projects.Powerslave.Switch.saveSwitches;
import static ru.m210projects.Powerslave.View.*;
import static ru.m210projects.Powerslave.Weapons.CheckClip;
import static ru.m210projects.Powerslave.Weapons.SetWeaponStatus;

public class LoadSave {

    public static final String savsign = "LOBO";
    public static final int gdxSave = 100;
    public static final int currentGdxSave = 101; // v1.16 == 100
    public static final int SAVENAME = 32;
    public static final int SAVESCREENSHOTSIZE = 160 * 100;
    public static final char[] filenum = new char[4];
    public static FileEntry nSaveFile;
    public static String nSaveName;
    public static boolean gClassicMode = false;
    public static boolean gQuickSaving;
    public static boolean gAutosaveRequest;
    public static final SafeLoader loader = new SafeLoader();
    public static final LSInfo lsInf = new LSInfo();
    public static int quickslot = 0;
    public static FileEntry lastload;

    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxSave) {
                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 160, 100);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == currentGdxSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);

                    lsInf.read(is);

                    boolean hasCapt = StreamUtils.readBoolean(is);
                    if (hasCapt) {
                        ((DynamicArtEntry) pic).copyData(StreamUtils.readBytes(is, SAVESCREENSHOTSIZE));
                    }

                    lsInf.addonfile = null;
                    if (StreamUtils.readByte(is) == 2) {
                        String addon = FileUtils.getFullName(StreamUtils.readDataString(is).toLowerCase());
                        if (!addon.isEmpty()) {
                            lsInf.addonfile = "Addon: " + addon;
                        }
                    }

                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + currentGdxSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        lsInf.clear();
        return -1;
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        return StreamUtils.readShort(is);
    }

    public static boolean checkfile(InputStream is) throws IOException {
        int nVersion = checkSave(is);
        if (nVersion != currentGdxSave) {
            return false;
        }

        return loader.load(is);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean loadgame(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                Console.out.println("debug: start loadgame()", OsdColor.BLUE);
                boolean status = checkfile(is);
                if (status) {
                    load();
                    if (lastload == null || !lastload.exists()) {
                        lastload = fil;
                    }

                    if (gClassicMode) {
                        nSaveFile = fil;
                        nSaveName = loader.savename;
                    } else {
                        nSaveFile = DUMMY_ENTRY;
                        nSaveName = null;
                    }
                    return true;
                }

                StatusMessage(500, "Incompatible version of saved game found!", nLocalPlayer);
                return false;
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        return false;
    }

    private static void load() {
        gDemoScreen.onLoad();

        LoadGDXBlock();

        gClassicMode = loader.gClassicMode;
        nBestLevel = loader.best;

        if (gClassicMode) {
            zoom = 768;
            nPlayerWeapons[nLocalPlayer] = loader.nPlayerWeapons[nLocalPlayer];
            PlayerList[nLocalPlayer].currentWeapon = loader.PlayerList[nLocalPlayer].currentWeapon;
            nPlayerClip[nLocalPlayer] = loader.nPlayerClip[nLocalPlayer];

            int nAmmo = PlayerList[nLocalPlayer].AmmosAmount[1];
            if (nAmmo >= 6) {
                nAmmo = 6;
            }
            nPistolClip[nLocalPlayer] = nAmmo;

            nPlayerItem[nLocalPlayer] = loader.nPlayerItem[nLocalPlayer];
            PlayerList[nLocalPlayer].copy(loader.PlayerList[nLocalPlayer]);
            nPlayerLives[nLocalPlayer] = loader.nPlayerLives[nLocalPlayer];
            SetPlayerItem(nLocalPlayer, nPlayerItem[nLocalPlayer]);
            CheckClip(nLocalPlayer);
            game.nNetMode = NetMode.Single;
            gGameScreen.changemap(loader.level, null);
            StatusMessage(500, "Game loaded", nLocalPlayer);
            System.gc();
        } else {
            InitElev();
            loadFM();

            game.doPrecache(() -> {
                game.nNetMode = NetMode.Single;

                gGameScreen.gNameShowTime = 500;
                if (mUserFlag == UserFlag.Addon) {
                    boardfilename = game.getCache().getEntry(gCurrentEpisode.gMapInfo.get(levelnum - 1).path, true);
                }
                sndCheckUserMusic(boardfilename);
                game.changeScreen(gGameScreen);
                game.gPaused = false;

                engine.getPaletteManager().LoadTorch(bTorch);
                SetPlayerItem(nLocalPlayer, nPlayerItem[nLocalPlayer]);
                SetAirFrame();
                RefreshStatus();

                if (loader.getMessage() != null) {
                    StatusMessage(2000, loader.getMessage(), myconnectindex);
                } else {
                    StatusMessage(500, "Game loaded", nLocalPlayer);
                }
                System.gc();

                game.pNet.ResetTimers();
                game.pNet.ready2send = true;
            });
        }
    }

    public static void LoadGDXBlock() {
        boardfilename = loader.boardfilename;
        if (loader.warp_on == 0) {
            mUserFlag = UserFlag.None;
            resetEpisodeResources(gOriginalEpisode);
        } else if (loader.warp_on == 1) {
            mUserFlag = UserFlag.UserMap;
            resetEpisodeResources(null);
        } else if (loader.warp_on == 2) {
            mUserFlag = UserFlag.Addon;
            checkEpisodeResources(loader.addon);
        }
    }

    public static void loadMap() {
        boardService.setBoard(new Board(null, loader.sector, loader.wall, loader.sprite));
    }

    private static void loadPlayer() {
        PlayerCount = 1;
        PlayerList[nLocalPlayer].copy(loader.PlayerList[nLocalPlayer]);

        connecthead = loader.connecthead;
        System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, 8);

        bTorch = loader.bTorch;
        nFreeze = loader.nFreeze;
        nXDamage[nLocalPlayer] = loader.nXDamage;
        nYDamage[nLocalPlayer] = loader.nYDamage;
        nDoppleSprite[nLocalPlayer] = loader.nDoppleSprite;
        nPlayerClip[nLocalPlayer] = loader.nPlayerClip[nLocalPlayer];
        nPistolClip[nLocalPlayer] = loader.nPistolClip[nLocalPlayer];
        nPlayerTorch[nLocalPlayer] = loader.nPlayerTorch;
        nPlayerWeapons[nLocalPlayer] = loader.nPlayerWeapons[nLocalPlayer];
        nPlayerLives[nLocalPlayer] = loader.nPlayerLives[nLocalPlayer];
        nPlayerItem[nLocalPlayer] = loader.nPlayerItem[nLocalPlayer];
        nPlayerInvisible[nLocalPlayer] = loader.nPlayerInvisible;
        nPlayerDouble[nLocalPlayer] = loader.nPlayerDouble;
        nPlayerViewSect[nLocalPlayer] = loader.nPlayerViewSect;
        nPlayerFloorSprite[nLocalPlayer] = loader.nPlayerFloorSprite;
        nPlayerScore[nLocalPlayer] = loader.nPlayerScore;
        nPlayerGrenade[nLocalPlayer] = loader.nPlayerGrenade;
        nPlayerSnake[nLocalPlayer] = loader.nPlayerSnake;
        nDestVertPan[nLocalPlayer] = loader.nDestVertPan;
        dVertPan[nLocalPlayer] = loader.dVertPan;
        nDeathType[nLocalPlayer] = loader.nDeathType;
        nQuake[nLocalPlayer] = loader.nQuake;
        bTouchFloor[nLocalPlayer] = loader.bTouchFloor;

        initx = loader.initx;
        inity = loader.inity;
        initz = loader.initz;
        inita = loader.inita;
        initsect = loader.initsect;

        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);

        nPlayerPushSound[nLocalPlayer] = -1;
        nTauntTimer[nLocalPlayer] =  (RandomSize(3) + 3);
        nPlayerPushSect[nLocalPlayer] = -1;
        nPlayerSwear[nLocalPlayer] = 4;
        nTemperature[nLocalPlayer] = 0;

        SetWeaponStatus(nLocalPlayer);
    }

    private static void loadSprites() {
        loadEnemies();

        loadBullets(loader);
        loadGrenades(loader);
        loadBubbles(loader);
        loadSnakes(loader);
        loadObjects(loader);
        loadTraps(loader);
        loadDrips(loader);

        nCreaturesLeft = loader.nCreaturesLeft;
        nCreaturesMax = loader.nCreaturesMax;
        nSpiritSprite = loader.nSpiritSprite;
        nMagicCount = loader.nMagicCount;
        nRegenerates = loader.nRegenerates;
        nFirstRegenerate = loader.nFirstRegenerate;
        nNetStartSprites = loader.nNetStartSprites;
        nCurStartSprite = loader.nCurStartSprite;
        nNetPlayerCount = loader.nNetPlayerCount;

        System.arraycopy(loader.nNetStartSprite, 0, nNetStartSprite, 0, 8);
        System.arraycopy(loader.nChunkSprite, 0, nChunkSprite, 0, nChunkSprite.length);
        System.arraycopy(loader.nBodyGunSprite, 0, nBodyGunSprite, 0, nBodyGunSprite.length);
        System.arraycopy(loader.nBodySprite, 0, nBodySprite, 0, nBodySprite.length);

        nCurChunkNum = loader.nCurChunkNum;
        nCurBodyNum = loader.nCurBodyNum;
        nCurBodyGunNum = loader.nCurBodyGunNum;
        nBodyTotal = loader.nBodyTotal;
        nChunkTotal = loader.nChunkTotal;

        nRadialSpr = loader.nRadialSpr;
        nRadialDamage = loader.nRadialDamage;
        nDamageRadius = loader.nDamageRadius;
        nRadialOwner = loader.nRadialOwner;
        nRadialBullet = loader.nRadialBullet;

        nDronePitch = loader.nDronePitch;
        nFinaleSpr = loader.nFinaleSpr;
        nFinaleStage = loader.nFinaleStage;
        lFinaleStart = loader.lFinaleStart;
        nSmokeSparks = loader.nSmokeSparks;
    }

    private static void loadEnemies() {
        loadAnubis(loader);
        loadFish(loader);
        loadLava(loader);
        loadLion(loader);
        loadMummy(loader);
        loadQueen(loader);
        loadRa(loader);
        loadRat(loader);
        loadRex(loader);
        loadRoach(loader);
        loadScorp(loader);
        loadSet(loader);
        loadSpider(loader);
        loadWasp(loader);
    }

    private static void loadSectors() {
        loadLights(loader);
        loadElevs(loader);
        loadBobs(loader);
        loadMoves(loader);
        loadTrails(loader);
        loadPushBlocks(loader);
        loadSlide(loader);
        loadSwitches(loader);
        loadLinks(loader);
        loadSecExtra(loader);

        nEnergyChan = loader.nEnergyChan;
        nEnergyBlocks = loader.nEnergyBlocks;
        nEnergyTowers = loader.nEnergyTowers;

        nSwitchSound = loader.nSwitchSound;
        nStoneSound = loader.nStoneSound;
        nElevSound = loader.nElevSound;
        nStopSound = loader.nStopSound;

        lCountDown = loader.lCountDown;
        nAlarmTicks = loader.nAlarmTicks;
        nRedTicks = loader.nRedTicks;
        nClockVal = loader.nClockVal;
        nButtonColor = loader.nButtonColor;

        pskyoff[0] = 0;
        pskyoff[1] = 0;
        pskyoff[2] = 0;
        pskyoff[3] = 0;
        parallaxtype = 0;
        visibility = 1024; // 2048;
        Renderer renderer = game.getRenderer();
        renderer.setParallaxOffset(256);
        pskybits = 2;
    }

    private static void loadFM() {
        levelnum = loader.level;
        lastlevel = loader.lastlevel;

        loadMap();
        loadAnm(loader);

        loadSprites();
        loadSectors();

        loadWallFaces(loader);
        loadRunList(loader);
        loadPlayer();
        loadRandom(loader);

        GrabPalette();
        if (levelnum == 20) {
            InitEnergyTile();
            InitClockTile();
        }

        totalmoves = loader.totalmoves;
        moveframes = loader.moveframes;
    }

    public static void quicksave() {
        if (numplayers > 1 || gClassicMode) {
            return;
        }

        if (PlayerList[nLocalPlayer].HealthAmount != 0) {
            gQuickSaving = true;
        }
    }

    public static boolean canLoad(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                if (nVersion != currentGdxSave) {
                    if (nVersion >= gdxSave) {
                        final EpisodeInfo addon = loader.LoadGDXHeader(is);
                        if (loader.level <= 20 && loader.warp_on != 1) { // not usermap
                            MenuCorruptGame menu = (MenuCorruptGame) game.menu.mMenus[CORRUPTLOAD];
                            menu.setRunnable(() -> {
                                EpisodeInfo game = addon != null ? addon : gOriginalEpisode;
                                levelnew = loader.level;
                                if (gClassicMode) {
                                    nSaveFile = fil;
                                    nSaveName = loader.savename;
                                } else {
                                    nSaveFile = DUMMY_ENTRY;
                                    nSaveName = null;
                                }
                                gGameScreen.newgame(game, levelnew, loader.gClassicMode);
                                nBestLevel = loader.best;
                            });
                            game.menu.mOpen(menu, -1);
                        }
                    }
                }
                return nVersion == currentGdxSave;
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    public static void quickload() {
        if (numplayers > 1 || gClassicMode) {
            return;
        }

        final FileEntry loadFile = game.pSavemgr.getLast();
        if (canLoad(loadFile)) {
            game.changeScreen(gLoadingScreen);
            gLoadingScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.setPrevScreen();
                    if (game.isCurrentScreen(gGameScreen)) {
                        game.pNet.ready2send = true;
                        StatusMessage(500, "Incompatible version of saved game found!", nLocalPlayer);
                    }
                }
            });
        }
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static void SaveVersion(OutputStream os, int nVersion) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, nVersion);
    }

    public static void SaveHeader(OutputStream os, String savename, long time, int level, int best, boolean gClassicMode) throws IOException {
        SaveVersion(os, currentGdxSave);

        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);

        StreamUtils.writeByte(os, gClassicMode ? 1 : 0);
        StreamUtils.writeShort(os, level);
        StreamUtils.writeShort(os, best);
    }

    public static void SaveGDXBlock(OutputStream os) throws IOException {
        boolean hasCapt = gGameScreen != null && gGameScreen.captBuffer != null;
        StreamUtils.writeByte(os, hasCapt ? (byte) 1 : 0);
        if (hasCapt) {
            StreamUtils.writeBytes(os, gGameScreen.captBuffer);
            gGameScreen.captBuffer = null;
        }

        byte warp_on = 0;
        if (mUserFlag == UserFlag.UserMap) {
            warp_on = 1;
        }
        if (mUserFlag == UserFlag.Addon) {
            warp_on = 2;
        }

        StreamUtils.writeByte(os, warp_on);
        if (warp_on == 2) {// user episode
            StreamUtils.writeDataString(os, gCurrentEpisode.getPath());
        }

        if (boardfilename != null && boardfilename.exists()) {
            if (boardfilename instanceof FileEntry) {
                StreamUtils.writeDataString(os, ((FileEntry) boardfilename).getPath().toString());
            } else {
                StreamUtils.writeDataString(os, boardfilename.getName());
            }
        } else {
            StreamUtils.writeDataString(os, "");
        }
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                StatusMessage(500, "Game not saved. Access denied!", nLocalPlayer);
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time, levelnum, nBestLevel, gClassicMode);
            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                StatusMessage(500, "Game saved", nLocalPlayer);
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            StatusMessage(500, "Game not saved. " + e, nLocalPlayer);
        }

    }

    private static void save(OutputStream os, String savename, long time, int level, int best, boolean classic) throws IOException {
        SaveHeader(os, savename, time, level, best, classic);
        SaveGDXBlock(os);

        if (classic) {
            StreamUtils.writeShort(os, nPlayerWeapons[nLocalPlayer]);
            StreamUtils.writeShort(os, PlayerList[nLocalPlayer].currentWeapon);
            StreamUtils.writeShort(os, nPlayerClip[nLocalPlayer]);
            StreamUtils.writeShort(os, nPlayerItem[nLocalPlayer]);
            PlayerList[nLocalPlayer].writeObject(os, false);
            StreamUtils.writeShort(os, nPlayerLives[nLocalPlayer]);
        } else {
            saveFM(os);
        }
    }

    private static void saveFM(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, lastlevel);

        saveMap(os);
        saveAnm(os);
        saveSprites(os);
        saveSectors(os);

        saveWallFaces(os);
        saveRunList(os);
        savePlayer(os);
        saveRandom(os);

        StreamUtils.writeInt(os, totalmoves);
        StreamUtils.writeInt(os, moveframes);
    }

    private static void saveMap(OutputStream os) throws IOException {
        Board board = boardService.getBoard();
        Sector[] sectors = board.getSectors();
        StreamUtils.writeInt(os, sectors.length);
        for (Sector s : sectors) {
            s.writeObject(os);
        }

        Wall[] walls = board.getWalls();
        StreamUtils.writeInt(os, walls.length);
        for (Wall wal : walls) {
            wal.writeObject(os);
        }

        List<Sprite> sprites = board.getSprites();
        StreamUtils.writeInt(os, sprites.size());
        for (Sprite s : sprites) {
            s.writeObject(os);
        }
    }

    private static void savePlayer(OutputStream os) throws IOException {
        PlayerList[nLocalPlayer].writeObject(os,true);
        StreamUtils.writeShort(os, connecthead);
        for (short val : connectpoint2) {
            StreamUtils.writeShort(os, val);
        }

        StreamUtils.writeByte(os, bTorch);
        StreamUtils.writeInt(os, nFreeze);
        StreamUtils.writeInt(os, nXDamage[nLocalPlayer]);
        StreamUtils.writeInt(os, nYDamage[nLocalPlayer]);
        StreamUtils.writeShort(os, nDoppleSprite[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerClip[nLocalPlayer]);
        StreamUtils.writeShort(os, nPistolClip[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerTorch[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerWeapons[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerLives[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerItem[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerInvisible[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerDouble[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerViewSect[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerFloorSprite[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerScore[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerGrenade[nLocalPlayer]);
        StreamUtils.writeShort(os, nPlayerSnake[nLocalPlayer]);
        StreamUtils.writeShort(os, (short) nDestVertPan[nLocalPlayer]);
        StreamUtils.writeShort(os, dVertPan[nLocalPlayer]);
        StreamUtils.writeShort(os, nDeathType[nLocalPlayer]);
        StreamUtils.writeShort(os, nQuake[nLocalPlayer]);
        StreamUtils.writeByte(os, bTouchFloor[nLocalPlayer] ? 1 : 0);

        StreamUtils.writeInt(os, initx);
        StreamUtils.writeInt(os, inity);
        StreamUtils.writeInt(os, initz);
        StreamUtils.writeShort(os, inita);
        StreamUtils.writeShort(os, initsect);

        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);
    }

    private static void saveEnemies(OutputStream os) throws IOException {
        saveAnubis(os);
        saveFish(os);
        saveLava(os);
        saveLion(os);
        saveMummy(os);
        saveQueen(os);
        saveRa(os);
        saveRat(os);
        saveRex(os);
        saveRoach(os);
        saveScorp(os);
        saveSet(os);
        saveSpider(os);
        saveWasp(os);
    }

    private static void saveSprites(OutputStream os) throws IOException {
        saveEnemies(os);

        saveBullets(os);
        saveGrenades(os);
        saveBubbles(os);
        saveSnakes(os);
        saveObjects(os);
        saveTraps(os);
        saveDrips(os);

        StreamUtils.writeShort(os, nCreaturesLeft);
        StreamUtils.writeShort(os, nCreaturesMax);
        StreamUtils.writeShort(os, nSpiritSprite);
        StreamUtils.writeShort(os, nMagicCount);
        StreamUtils.writeShort(os, nRegenerates);
        StreamUtils.writeShort(os, nFirstRegenerate);
        StreamUtils.writeShort(os, nNetStartSprites);
        StreamUtils.writeShort(os, nCurStartSprite);
        StreamUtils.writeShort(os, nNetPlayerCount);
        for (int v : nNetStartSprite) {
            StreamUtils.writeShort(os, v);
        }

        for (int v : nChunkSprite) {
            StreamUtils.writeShort(os, v);
        }

        for (int v : nBodyGunSprite) {
            StreamUtils.writeShort(os, v);
        }

        for (int v : nBodySprite) {
            StreamUtils.writeShort(os, v);
        }

        StreamUtils.writeShort(os, nCurChunkNum);
        StreamUtils.writeShort(os, nCurBodyNum);
        StreamUtils.writeShort(os, nCurBodyGunNum);
        StreamUtils.writeShort(os, nBodyTotal);
        StreamUtils.writeShort(os, nChunkTotal);

        StreamUtils.writeShort(os, nRadialSpr);
        StreamUtils.writeShort(os, nRadialDamage);
        StreamUtils.writeShort(os, nDamageRadius);
        StreamUtils.writeShort(os, nRadialOwner);
        StreamUtils.writeShort(os, nRadialBullet);

        StreamUtils.writeShort(os, nDronePitch);
        StreamUtils.writeShort(os, nFinaleSpr);
        StreamUtils.writeShort(os, nFinaleStage);
        StreamUtils.writeShort(os, lFinaleStart);
        StreamUtils.writeShort(os, nSmokeSparks);
    }

    private static void saveSectors(OutputStream os) throws IOException {
        saveLights(os);
        saveElevs(os);
        saveBobs(os);
        saveMoves(os);
        saveTrails(os);
        savePushBlocks(os);
        saveSlide(os);
        saveSwitches(os);
        saveLinks(os);
        saveSecExtra(os);

        StreamUtils.writeShort(os, nEnergyChan);
        StreamUtils.writeShort(os, nEnergyBlocks);
        StreamUtils.writeShort(os, nEnergyTowers);

        StreamUtils.writeShort(os, nSwitchSound);
        StreamUtils.writeShort(os, nStoneSound);
        StreamUtils.writeShort(os, nElevSound);
        StreamUtils.writeShort(os, nStopSound);

        StreamUtils.writeInt(os, lCountDown);
        StreamUtils.writeShort(os, nAlarmTicks);
        StreamUtils.writeShort(os, nRedTicks);
        StreamUtils.writeShort(os, nClockVal);
        StreamUtils.writeShort(os, nButtonColor);
    }

}
