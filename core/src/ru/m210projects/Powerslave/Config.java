// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.settings.*;
import ru.m210projects.Build.input.GameKey;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.IntStream;

import static ru.m210projects.Build.Gameutils.BClipRange;

public class Config extends GameConfig {

    public static final int[] defclassickeys = {
            Keys.UP,//			GameKeys.Move_Forward,
            Keys.DOWN,//			GameKeys.Move_Backward,
            Keys.LEFT,//			GameKeys.Turn_Left,
            Keys.RIGHT,//			GameKeys.Turn_Right,
            Keys.BACKSPACE,//			GameKeys.Turn_Around,
            Keys.ALT_LEFT,//			GameKeys.Strafe,
            Keys.COMMA,//			GameKeys.Strafe_Left,
            Keys.PERIOD, //			GameKeys.Strafe_Right,
            Keys.A, //			GameKeys.Jump,
            Keys.Z, //			GameKeys.Crouch,
            Keys.SHIFT_LEFT, //			GameKeys.Run,
            Keys.SPACE,//			GameKeys.Open,
            Keys.CONTROL_LEFT,//			GameKeys.Weapon_Fire,
            Keys.APOSTROPHE,    //			GameKeys.Next_Weapon,
            Keys.SEMICOLON,    //			GameKeys.Previous_Weapon,
            Keys.PAGE_UP,//			GameKeys.Look_Up,
            Keys.PAGE_DOWN,//			GameKeys.Look_Down,
            Keys.TAB,    //			GameKeys.Map_Toggle,
            Keys.EQUALS,    //			GameKeys.Enlarge_Screen,
            Keys.MINUS,    //			GameKeys.Shrink_Screen,
            0, // Send_Message
            Keys.U,    //			GameKeys.Mouse_Aiming,
            Keys.ESCAPE,    //			GameKeys.Menu_Toggle,
            Keys.GRAVE,    //			GameKeys.Show_Console,

            0, //			PsKeys.Crouch_toggle,
            Keys.CAPS_LOCK, //			PsKeys.AutoRun,
            Keys.HOME,//			PsKeys.Aim_Up,
            Keys.END,//			PsKeys.Aim_Down,
            Keys.NUMPAD_5,//			PsKeys.Aim_Center,
            Keys.NUM_1,//			PsKeys.Weapon_1,
            Keys.NUM_2,//			PsKeys.Weapon_2,
            Keys.NUM_3,//			PsKeys.Weapon_3,
            Keys.NUM_4,//			PsKeys.Weapon_4,
            Keys.NUM_5,//			PsKeys.Weapon_5,
            Keys.NUM_6,//			PsKeys.Weapon_6,
            Keys.NUM_7,//			PsKeys.Weapon_7,
            Keys.ENTER,//			PsKeys.Inventory_Use,
            Keys.LEFT_BRACKET, //			PsKeys.Inventory_Left,
            Keys.RIGHT_BRACKET,//			PsKeys.Inventory_Right,
            0, //Inventory_Hand,
            0, //Inventory_Eye,
            0, //Inventory_Mask,
            0, //Inventory_Heart,
            0, //Inventory_Torch,
            0, //Inventory_Scarab,
            Keys.F7,            //PsKeys.Third_View,
            Keys.F,    //			PsKeys.Map_Follow_Mode,
            Keys.I,//			PsKeys.Toggle_Crosshair,
            0,        //			PsKeys.Last_Used_Weapon,
            Keys.F2,    //			PsKeys.Show_SaveMenu,
            Keys.F3,    //			PsKeys.Show_LoadMenu,
            Keys.F4,    //			PsKeys.Show_SoundSetup,
            Keys.F5,    //			PsKeys.Show_Options,
            Keys.F6,    //			PsKeys.Quicksave,
            Keys.F8,    //			PsKeys.Toggle_messages,
            Keys.F9,    //			PsKeys.Quickload,
            Keys.F10,    //			PsKeys.Quit,
            Keys.F11,    //			PsKeys.Gamma,
            Keys.F12    //			PsKeys.Make_Screenshot,
    };
    public static final int[] defkeys = {
            Keys.W,//			GameKeys.Move_Forward,
            Keys.S,//			GameKeys.Move_Backward,
            Keys.LEFT,//			GameKeys.Turn_Left,
            Keys.RIGHT,//			GameKeys.Turn_Right,
            Keys.BACKSPACE,//			GameKeys.Turn_Around,
            Keys.ALT_LEFT,//			GameKeys.Strafe,
            Keys.A,//			GameKeys.Strafe_Left,
            Keys.D, //			GameKeys.Strafe_Right,
            Keys.SPACE, //			GameKeys.Jump,
            Keys.CONTROL_LEFT, //			GameKeys.Crouch,
            Keys.SHIFT_LEFT, //			GameKeys.Run,
            Keys.E,//			GameKeys.Open,
            0,//			GameKeys.Weapon_Fire,
            Keys.APOSTROPHE,    //			GameKeys.Next_Weapon,
            Keys.SEMICOLON,    //			GameKeys.Previous_Weapon,
            Keys.PAGE_UP,//			GameKeys.Look_Up,
            Keys.PAGE_DOWN,//			GameKeys.Look_Down,
            Keys.TAB,    //			GameKeys.Map_Toggle,
            Keys.EQUALS,    //			GameKeys.Enlarge_Screen,
            Keys.MINUS,    //			GameKeys.Shrink_Screen,
            0, // Send_Message
            Keys.U,    //			GameKeys.Mouse_Aiming,
            Keys.ESCAPE,    //			GameKeys.Menu_Toggle,
            Keys.GRAVE,    //			GameKeys.Show_Console,


            0, //			PsKeys.Crouch_toggle,
            Keys.CAPS_LOCK, //			PsKeys.AutoRun,
            Keys.HOME,//			PsKeys.Aim_Up,
            Keys.END,//			PsKeys.Aim_Down,
            Keys.NUMPAD_5,//			PsKeys.Aim_Center,
            Keys.NUM_1,//			PsKeys.Weapon_1,
            Keys.NUM_2,//			PsKeys.Weapon_2,
            Keys.NUM_3,//			PsKeys.Weapon_3,
            Keys.NUM_4,//			PsKeys.Weapon_4,
            Keys.NUM_5,//			PsKeys.Weapon_5,
            Keys.NUM_6,//			PsKeys.Weapon_6,
            Keys.NUM_7,//			PsKeys.Weapon_7,
            Keys.ENTER,//			PsKeys.Inventory_Use,
            Keys.LEFT_BRACKET, //			PsKeys.Inventory_Left,
            Keys.RIGHT_BRACKET,//			PsKeys.Inventory_Right,
            0, //Inventory_Hand,
            0, //Inventory_Eye,
            0, //Inventory_Mask,
            0, //Inventory_Heart,
            0, //Inventory_Torch,
            0, //Inventory_Scarab,
            Keys.F7,            //PsKeys.Third_View,
            Keys.F,    //			PsKeys.Map_Follow_Mode,
            Keys.I,//			PsKeys.Toggle_Crosshair,
            Keys.Q,        //			PsKeys.Last_Used_Weapon,
            Keys.F2,    //			PsKeys.Show_SaveMenu,
            Keys.F3,    //			PsKeys.Show_LoadMenu,
            Keys.F4,    //			PsKeys.Show_SoundSetup,
            Keys.F5,    //			PsKeys.Show_Options,
            Keys.F6,    //			PsKeys.Quicksave,
            0,    //			PsKeys.Toggle_messages,
            Keys.F9,    //			PsKeys.Quickload,
            Keys.F10,    //			PsKeys.Quit,
            Keys.F11,    //			PsKeys.Gamma,
            Keys.F12    //			PsKeys.Make_Screenshot,
    };

    public boolean gShowMessages = true;
    public boolean gCrosshair = true;
    public boolean gAutoRun = false;
    public int gCrossSize = 65536;
    public int nScreenSize = 2;
    public int gOverlayMap = 1;
    public boolean gAutoAim = true;
    public int showMapInfo = 1;
    public int gStatSize = 65536;
    public int gShowStat = 1;
    public boolean bNewShadows = true;
    public int gDemoSeq = 1;
    public boolean bSubtitles = true;
    public boolean bGrenadeFix = false;
    public boolean bWaspSound = true;
    public int weaponIndex = -1;
    
    public Config(Path path) {
        super(path);
    }
    
    public GameKey[] getKeyMap() {
        return new GameKey[]{
                GameKeys.Move_Forward,
                GameKeys.Move_Backward,
                GameKeys.Turn_Left,
                GameKeys.Turn_Right,
                GameKeys.Turn_Around,
                GameKeys.Strafe,
                GameKeys.Strafe_Left,
                GameKeys.Strafe_Right,
                GameKeys.Jump,
                GameKeys.Crouch,
                PsKeys.Crouch_toggle,
                GameKeys.Run,
                PsKeys.AutoRun,
                GameKeys.Open,
                GameKeys.Weapon_Fire,
                PsKeys.Aim_Up,
                PsKeys.Aim_Down,
                PsKeys.Aim_Center,
                GameKeys.Look_Up,
                GameKeys.Look_Down,
                PsKeys.Weapon_1,
                PsKeys.Weapon_2,
                PsKeys.Weapon_3,
                PsKeys.Weapon_4,
                PsKeys.Weapon_5,
                PsKeys.Weapon_6,
                PsKeys.Weapon_7,
                PsKeys.Inventory_Use,
                PsKeys.Inventory_Left,
                PsKeys.Inventory_Right,
                PsKeys.Inventory_Hand,
                PsKeys.Inventory_Eye,
                PsKeys.Inventory_Mask,
                PsKeys.Inventory_Heart,
                PsKeys.Inventory_Torch,
                PsKeys.Inventory_Scarab,
                PsKeys.Third_View,
                PsKeys.Toggle_Crosshair,
                PsKeys.Last_Used_Weapon,
                GameKeys.Next_Weapon,
                GameKeys.Previous_Weapon,
                GameKeys.Map_Toggle,
                PsKeys.Map_Follow_Mode,
                GameKeys.Shrink_Screen,
                GameKeys.Enlarge_Screen,
                GameKeys.Mouse_Aiming,
                PsKeys.Toggle_messages,
                GameKeys.Show_Console,
                PsKeys.Show_SaveMenu,
                PsKeys.Show_LoadMenu,
                PsKeys.Show_SoundSetup,
                PsKeys.Show_Options,
                PsKeys.Quicksave,
                PsKeys.Quickload,
                PsKeys.Quit,
                PsKeys.Gamma,
                PsKeys.Make_Screenshot,
                GameKeys.Menu_Toggle,
        };

    }

    @Override
    protected InputContext createDefaultInputContext() {
        return new InputContext(getKeyMap(), defkeys, defclassickeys) {

            @Override
            protected void clearInput() {
                super.clearInput();
                weaponIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(PsKeys.Weapon_1)).findFirst().orElse(-1);
            }
        };
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    gShowMessages = prop.getBooleanValue("ShowMessages", gShowMessages);
                    gCrosshair = prop.getBooleanValue("Crosshair", gCrosshair);
                    gAutoRun = prop.getBooleanValue("AutoRun", gAutoRun);
                    gCrossSize = Math.max(16384, prop.getIntValue("CrossSize", gCrossSize));
                    nScreenSize = BClipRange(prop.getIntValue("ScreenSize", nScreenSize), 0, 2);
                    gOverlayMap = BClipRange(prop.getIntValue("OverlayMap", gOverlayMap), 0, 2);
                    showMapInfo = prop.getIntValue("showMapInfo", showMapInfo);
                    gStatSize = Math.max(16384, prop.getIntValue("StatSize", gStatSize));
                    gShowStat = prop.getIntValue("ShowStat", gShowStat);
                    gAutoAim = prop.getBooleanValue("AutoAim", gAutoAim);
                    gDemoSeq = prop.getIntValue("DemoSequence", gDemoSeq);
                    bNewShadows = prop.getBooleanValue("NewShadows", bNewShadows);
                    bSubtitles = prop.getBooleanValue("Subtitles", bSubtitles);
                    bGrenadeFix = prop.getBooleanValue("GrenadeFix", bGrenadeFix);
                    bWaspSound = prop.getBooleanValue("WaspSound", bWaspSound);
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options
                putBoolean(os, "ShowMessages", gShowMessages);
                putBoolean(os, "Crosshair", gCrosshair);
                putBoolean(os, "AutoRun", gAutoRun);
                putInteger(os, "CrossSize", gCrossSize);
                putInteger(os, "ScreenSize", nScreenSize);
                putInteger(os, "OverlayMap", gOverlayMap);
                putInteger(os, "showMapInfo", showMapInfo);
                putInteger(os, "StatSize", gStatSize);
                putInteger(os, "ShowStat", gShowStat);
                putBoolean(os, "AutoAim", gAutoAim);
                putInteger(os, "DemoSequence", gDemoSeq);
                putBoolean(os, "NewShadows", bNewShadows);
                putBoolean(os, "Subtitles", bSubtitles);
                putBoolean(os, "GrenadeFix", bGrenadeFix);
                putBoolean(os, "WaspSound", bWaspSound);
            }
        };
    }


    public enum PsKeys implements GameKey {
        Crouch_toggle,
        AutoRun,
        Aim_Up,
        Aim_Down,
        Aim_Center,
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Weapon_4,
        Weapon_5,
        Weapon_6,
        Weapon_7,
        Inventory_Use,
        Inventory_Left,
        Inventory_Right,
        Inventory_Hand,
        Inventory_Eye,
        Inventory_Mask,
        Inventory_Heart,
        Inventory_Torch,
        Inventory_Scarab,
        Third_View,
        Map_Follow_Mode,
        Toggle_Crosshair,
        Last_Used_Weapon,
        Show_SaveMenu,
        Show_LoadMenu,
        Show_SoundSetup,
        Show_Options,
        Quicksave,
        Toggle_messages,
        Quickload,
        Quit,
        Gamma,
        Make_Screenshot;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }

}
