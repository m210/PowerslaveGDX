// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BulletInfo;
import ru.m210projects.Powerslave.Type.BulletStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Anim.GetAnimSprite;
import static ru.m210projects.Powerslave.Enemies.Enemy.MoveCreature;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Player.GetPlayerFromSprite;
import static ru.m210projects.Powerslave.Random.RandomBit;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.StopSpriteSound;
import static ru.m210projects.Powerslave.Sprites.*;
import static ru.m210projects.Powerslave.Weapons.SetNewWeapon;

public class Bullet {
    public static final BulletInfo[] BulletInfo = {
            new BulletInfo(25, 1, 20, -1, -1, 13, 0, 0, -1),
            new BulletInfo(25, -1, 65000, -1, 31, 73, 0, 0, -1),
            new BulletInfo(15, -1, 60000, -1, 31, 73, 0, 0, -1),
            new BulletInfo(5, 15, 2000, -1, 14, 38, 4, 5, 3),
            new BulletInfo(250, 100, 2000, -1, 33, 34, 4, 20, -1),
            new BulletInfo(200, -1, 2000, -1, 20, 23, 4, 10, -1),
            new BulletInfo(200, -1, 60000, 68, 68, -1, -1, 0, -1),
            new BulletInfo(300, 1, 0, -1, -1, -1, 0, 50, -1),
            new BulletInfo(18, -1, 2000, -1, 18, 29, 4, 0, -1),
            new BulletInfo(20, -1, 2000, 37, 11, 30, 4, 0, -1),
            new BulletInfo(25, -1, 3000, -1, 44, 36, 4, 15, 90),
            new BulletInfo(30, -1, 1000, -1, 52, 53, 4, 20, 48),
            new BulletInfo(20, -1, 3500, -1, 54, 55, 4, 30, -1),
            new BulletInfo(10, -1, 5000, -1, 57, 76, 4, 0, -1),
            new BulletInfo(40, -1, 1500, -1, 63, 38, 4, 10, 40),
            new BulletInfo(20, -1, 2000, -1, 60, 12, 0, 0, -1),
            new BulletInfo(5, -1, 60000, -1, 31, 76, 0, 0, -1)};
    public static int lasthitx, lasthity, lasthitz;
    public static int lasthitsect, lasthitwall, lasthitsprite;
    private static final Vector2 tmpVec = new Vector2();
    private static final BulletStruct tmpBullet = new BulletStruct();
    private static final BulletStruct.BulletResult bulletResult = new BulletStruct.BulletResult();
    private static int nBulletCount;
    private static int nBulletsFree;
    private static final int[] BulletFree = new int[500];
    private static final int[] nBulletEnemy = new int[500];
    private static final BulletStruct[] BulletList = new BulletStruct[500];

    public static void InitBullets() {
        nBulletCount = 0;
        nBulletsFree = 500;
        for (int i = 0; i < 500; i++) {
            BulletFree[i] =  i;
            BulletList[i] = new BulletStruct();
        }
        Arrays.fill(nBulletEnemy,  -1);
    }

    public static void saveBullets(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nBulletCount);
        StreamUtils.writeShort(os,  nBulletsFree);
        for (int i = 0; i < 500; i++) {
            StreamUtils.writeShort(os, BulletFree[i]);
            StreamUtils.writeShort(os, nBulletEnemy[i]);
            BulletList[i].save(os);
        }

        StreamUtils.writeInt(os, lasthitx);
        StreamUtils.writeInt(os, lasthity);
        StreamUtils.writeInt(os, lasthitz);
        StreamUtils.writeShort(os,  lasthitsect);
        StreamUtils.writeShort(os,  lasthitwall);
        StreamUtils.writeShort(os,  lasthitsprite);
    }

    public static void loadBullets(SafeLoader loader) {
        nBulletCount = loader.nBulletCount;
        nBulletsFree = loader.nBulletsFree;
        System.arraycopy(loader.BulletFree, 0, BulletFree, 0, 500);
        System.arraycopy(loader.nBulletEnemy, 0, nBulletEnemy, 0, 500);

        for (int i = 0; i < 500; i++) {
            if (BulletList[i] == null) {
                BulletList[i] = new BulletStruct();
            }
            BulletList[i].copy(loader.BulletList[i]);
        }

        lasthitx = loader.lasthitx;
        lasthity = loader.lasthity;
        lasthitz = loader.lasthitz;
        lasthitsect = loader.lasthitsect;
        lasthitwall = loader.lasthitwall;
        lasthitsprite = loader.lasthitsprite;
    }

    public static void loadBullets(SafeLoader loader, InputStream is) throws IOException {
        loader.nBulletCount = StreamUtils.readShort(is);
        loader.nBulletsFree = StreamUtils.readShort(is);
        for (int i = 0; i < 500; i++) {
            loader.BulletFree[i] =  StreamUtils.readShort(is);
            loader.nBulletEnemy[i] =  StreamUtils.readShort(is);
            if (loader.BulletList[i] == null) {
                loader.BulletList[i] = new BulletStruct();
            }
            loader.BulletList[i].load(is);
        }

        loader.lasthitx = StreamUtils.readInt(is);
        loader.lasthity = StreamUtils.readInt(is);
        loader.lasthitz = StreamUtils.readInt(is);
        loader.lasthitsect = StreamUtils.readShort(is);
        loader.lasthitwall = StreamUtils.readShort(is);
        loader.lasthitsprite = StreamUtils.readShort(is);
    }

    public static int GrabBullet() {
        return BulletFree[--nBulletsFree];
    }

    public static void DestroyBullet(int nBullet) {
        int nSprite = BulletList[nBullet].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        DoSubRunRec(BulletList[nBullet].bull_6);
        DoSubRunRec(pSprite.getLotag() - 1);
        SubRunRec(BulletList[nBullet].bull_8);
        StopSpriteSound(nSprite);
        engine.mydeletesprite(nSprite);
        BulletFree[nBulletsFree] =  nBullet;
        nBulletsFree++;
    }

    public static void IgniteSprite(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        pSprite.setHitag(pSprite.getHitag() + 2);
        int nAnimSprite = GetAnimSprite(BuildAnim(-1, 38, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(),
                pSprite.getSectnum(), 0x28, 20));
        Sprite pAnimSprite = boardService.getSprite(nAnimSprite);
        if (pAnimSprite == null) {
            return;
        }

        pAnimSprite.setHitag(nSprite);
        engine.changespritestat(nAnimSprite,  404);
        pAnimSprite.setYrepeat(Math.max(32 * engine.getTile(pAnimSprite.getPicnum()).getHeight() / nFlameHeight, 1));
    }

    public static void BulletHitsSprite(BulletStruct pBullet, int nSource, int nDest, int x, int y, int z, int sectnum) {
        BulletInfo pBulletInfo = BulletInfo[pBullet.type];
        Sprite pDest = boardService.getSprite(nDest);
        if (pDest == null) {
            return;
        }

        int type = pBullet.type;
        int statnum = pDest.getStatnum();

        switch (type) {
            case 14:
                if (statnum > 107 || statnum == 98) {
                    return;
                }
            case 1:
            case 2:
            case 8:
            case 9:
            case 12:
            case 13:
            case 15:
            case 16:
                if (statnum != 0 && statnum <= 98) {
                    Sprite pSprite = boardService.getSprite(pBullet.nSprite);
                    if (pSprite == null) {
                        break;
                    }

                    if (statnum == 98) {
                        int v20 = (pSprite.getAng() + 256) - RandomSize(9);
                        pDest.setXvel( (2 * EngineUtils.sin((v20 + 512) & 0x7FF)));
                        pDest.setYvel( (2 * EngineUtils.sin(v20 & 0x7FF)));
                        pDest.setZvel( (-256 * (RandomSize(3) + 1)));
                    } else {
                        short v15 = pDest.getXvel();
                        short v17 = pDest.getYvel();
                        pDest.setXvel( (EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 2));
                        pDest.setYvel( (EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 2));
                        MoveCreature(nDest);
                        pDest.setXvel(v15);
                        pDest.setYvel(v17);
                    }
                }
                break;
            case 3:
                if (statnum <= 107 && statnum != 98) {
                    pDest.setHitag(pDest.getHitag() + 1);
                    if (pDest.getHitag() == 15) {
                        IgniteSprite(nDest);
                    }
                    if (RandomSize(2) == 0) {
                        BuildAnim(-1, pBulletInfo.inf_C, 0, x, y, z, sectnum, 40, pBulletInfo.flags);
                        return;
                    }
                }
                return;
        }

        int damage = pBulletInfo.force;
        if (pBullet.bull_13 > 1) {
            damage *= 2;
        }
        DamageEnemy(nDest, nSource, damage);
        if (statnum > 90 && statnum < 199) {
            switch (statnum) {
                case 97:
                    return;
                case 98:
                case 102:
                case 141:
                case 152:
                    BuildAnim(-1, 12, 0, x, y, z, sectnum, 40, 0);
                    return;
                default:
                    BuildAnim(-1, 39, 0, x, y, z, sectnum, 40, 0);
                    if (pBullet.type <= 2) {
                        return;
                    }
                    BuildAnim(-1, pBulletInfo.inf_C, 0, x, y, z, sectnum, 40, pBulletInfo.flags);
                    return;
            }
        }
        BuildAnim(-1, pBulletInfo.inf_C, 0, x, y, z, sectnum, 40, pBulletInfo.flags);
    }

    public static Vector2 BackUpBullet(int a1, int a2, int a3) {
        a1 -= EngineUtils.sin((a3 + 512) & 0x7FF) >> 11;
        a2 -= EngineUtils.sin(a3 & 0x7FF) >> 11;

        return tmpVec.set(a1, a2);
    }

    public static void SetBulletEnemy(int a1, int a2) {
        if (a1 >= 0) {
            nBulletEnemy[a1] =  a2;
        }
    }

    public static BulletStruct.BulletResult BuildBullet(int nSprite, int nType, int wz, int a6, int zAngle, int a8) {
        BulletInfo pBullet = BulletInfo[nType];
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return bulletResult.setResult(-1, -1);
        }

        if (pBullet.inf_4 > 30000) {
            if (zAngle >= 10000) {
                int v88 = zAngle - 10000;
                Sprite v11 = boardService.getSprite(v88);
                if (v11 != null) {
                    if ((v11.getCstat() & 0x101) != 0 && boardService.isValidSector(v11.getSectnum())) {
                        int i = engine.insertsprite(pSprite.getSectnum(), 200);
                        Sprite spr = boardService.getSprite(i);
                        if (spr != null) {
                            spr.setAng(a6);
                            tmpBullet.nSprite = i;
                            tmpBullet.type = nType;
                            tmpBullet.bull_13 = (byte) a8;
                            BulletHitsSprite(tmpBullet, nSprite, v88, v11.getX(), v11.getY(), v11.getZ() - (GetSpriteHeight(v88) >> 1),
                                    v11.getSectnum());
                            engine.mydeletesprite(i);
                            return bulletResult.setResult(-1, -1);
                        }
                    }
                    zAngle = v11.getCstat() & 0x101;
                }
            }
        }
        if (nBulletsFree == 0) {
            return bulletResult.setResult(-1, -1);
        }

        int v19 = pSprite.getSectnum();
        if (pSprite.getStatnum() == 100) {
            v19 = nPlayerViewSect[GetPlayerFromSprite(nSprite)];
        }

        final int spr = engine.insertsprite(v19, 200);
        Sprite pSprite2 = boardService.getSprite(spr);
        if (pSprite2 == null) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
            // return bulletResult.setResult(-1, -1);
        }

        int v21 = GetSpriteHeight(nSprite);
        if (wz == -1) {
            wz = -(v21 - (v21 >> 2));
        }

        pSprite2.setX(pSprite.getX());
        pSprite2.setY(pSprite.getY());
        pSprite2.setZ(pSprite.getZ());

        int nBullet = GrabBullet();
        BulletStruct v35 = BulletList[nBullet];

        nBulletEnemy[nBullet] = -1;

        pSprite2.setCstat(0);
        pSprite2.setShade(-64);
        if ((pBullet.flags & 4) != 0) {
            pSprite2.setPal(4);
        } else {
            pSprite2.setPal(0);
        }

        pSprite2.setClipdist(25);
        int v31 = pBullet.inf_12;
        if (v31 < 0) {
            v31 = 30;
        }

        pSprite2.setXrepeat( v31);
        pSprite2.setYrepeat( v31);
        pSprite2.setXoffset(0);
        pSprite2.setYoffset(0);
        pSprite2.setAng( a6);
        pSprite2.setXvel(0);
        pSprite2.setYvel(0);
        pSprite2.setZvel(0);
        pSprite2.setOwner( nSprite);

        pSprite2.setLotag( (HeadRun() + 1));
        pSprite2.setExtra(-1);
        pSprite2.setHitag(0);

        v35.bull_10 = 0;
        v35.bull_E = pBullet.inf_2;
        v35.frmOffset = 0;
        if (pBullet.inf_8 == -1) {
            v35.bull_12 = 1;
            v35.baseSeq = pBullet.inf_A;
        } else {
            v35.bull_12 = 0;
            v35.baseSeq = pBullet.inf_8;
        }

        pSprite2.setPicnum(GetSeqPicnum(v35.baseSeq, 0, 0));
        if (v35.baseSeq == 31) {
            pSprite2.setCstat(pSprite2.getCstat() | 0x8000);
        }

        v35.zang =  zAngle;
        v35.type =  nType;
        v35.nSprite = spr;
        v35.bull_6 =  AddRunRec(pSprite2.getLotag() - 1, nEvent11 | nBullet);
        v35.bull_8 =  AddRunRec(NewRun, nEvent11 | nBullet);
        v35.bull_13 = (byte) a8;
        pSprite2.setZ(pSprite2.getZ() + wz);
        int sect = pSprite2.getSectnum();
        while (true) {
            ru.m210projects.Build.Types.Sector sec = boardService.getSector(sect);
            if (sec == null) {
                break;
            }

            if (pSprite2.getZ() >= sec.getCeilingz()) {
                break;
            }

            if (SectAbove[sect] == -1) {
                pSprite2.setZ(sec.getCeilingz());
                break;
            }

            engine.mychangespritesect(spr, SectAbove[sect]);
            sect = SectAbove[sect];
        }

        int zvec = 0;
        if (zAngle >= 10000) {
            int v89 = zAngle - 10000;
            if (pBullet.inf_4 > 0x7530) {
                nBulletEnemy[nBullet] =  v89;
            } else {
                int v51 = GetSpriteHeight(v89), v53;
                Sprite pSprite89 = boardService.getSprite(v89);
                if (pSprite89 == null) {
                    return bulletResult.setResult(-1, nBullet);
                }

                if (pSprite89.getStatnum() == 100) {
                    v53 = v51 >> 2;
                } else {
                    v53 = v51 >> 1;
                }
                int dx, dy, dz = pSprite89.getZ() - (v51 - v53);
                if (nSprite == -1 || pSprite.getStatnum() == 100) {
                    dx = pSprite89.getX() - pSprite2.getX();
                    dy = pSprite89.getY() - pSprite2.getY();
                } else {

                    int sx = pSprite89.getX();
                    int sy = pSprite89.getY();
                    if (pSprite89.getStatnum() == 100) {
                        int plr = GetPlayerFromSprite(v89);
                        if (plr > -1) {
                            int pxvel = 15 * nPlayerDX[plr];
                            int pyvel = 15 * nPlayerDY[plr];

                            if (!isOriginal()) {
                                pxvel = BClipRange(pxvel, -1000, 1000);
                                pyvel = BClipRange(pyvel, -1000, 1000);
                            }

                            sx += pxvel;
                            sy += pyvel;
                        }
                    } else {
                        sx += 20 * pSprite89.getXvel() >> 6;
                        sy += 20 * pSprite89.getYvel() >> 6;
                    }

                    dy = sy - pSprite2.getY();
                    dx = sx - pSprite2.getX();
                    a6 = engine.GetMyAngle(dx, dy);
                    pSprite.setAng( a6);
                }

                int v67 = EngineUtils.sqrt(dx * dx + dy * dy);
                if (v67 != 0) {
                    zvec = (dz - pSprite2.getZ()) * pBullet.inf_4 / v67;
                }
            }
        } else {
            zvec = isOriginal() ? (pBullet.inf_4 * -EngineUtils.sin(zAngle & 0x7FF) >> 11) : -(zAngle / 32) * pBullet.inf_4;
        }

        int dist = 4 * pSprite.getClipdist();
        v35.xvec = dist * EngineUtils.sin((a6 + 512) & 0x7FF);
        v35.yvec = dist * EngineUtils.sin(a6 & 0x7FF);
        v35.zvec = 0;

        nBulletEnemy[nBullet] = -1;
        if (MoveBullet(nBullet)) {
            return bulletResult.setResult(-1, nBullet);
        } else {
            v35.bull_10 =  pBullet.inf_4;
            v35.xvec = pBullet.inf_4 * (EngineUtils.sin((a6 + 512) & 0x7FF) >> 3);
            v35.yvec = pBullet.inf_4 * (EngineUtils.sin(a6 & 0x7FF) >> 3);
            v35.zvec = zvec >> 3;
        }

        return bulletResult.setResult(spr, nBullet);
    }

    private static boolean MoveBullet(int nBullet) {
        BulletStruct pBullet = BulletList[nBullet];
        int type = pBullet.type;
        BulletInfo pBulletInfo = BulletInfo[type];
        final int nSprite = pBullet.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null || !boardService.isValidSector(pSprite.getSectnum())) {
            return false;
        }

        int nSectFlag = SectFlag[pSprite.getSectnum()];

        int hitsec = -1, hitwall = -1, hitspr = -1;
        int hitx = 0, hity = 0, hitz = 0;
        boolean out;

        if ((pBullet.bull_10 & 0xFFFF) >= 30000) {
            if (nBulletEnemy[nBullet] == -1) {
                int zvel = isOriginal() ? -8 * EngineUtils.sin(pBullet.zang & 0x7FF) : -16 * pBullet.zang * 32;
                engine.hitscan(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF),
                        EngineUtils.sin(pSprite.getAng()), zvel, pHitInfo, CLIPMASK1);

                hitx = pHitInfo.hitx;
                hity = pHitInfo.hity;
                hitz = pHitInfo.hitz;
                hitsec = pHitInfo.hitsect;
                hitwall = pHitInfo.hitwall;
                hitspr = pHitInfo.hitsprite;
            } else {
                int nEnemy = nBulletEnemy[nBullet];
                Sprite pEnemy = boardService.getSprite(nEnemy);
                if (pEnemy != null) {
                    hitx = pEnemy.getX();
                    hity = pEnemy.getY();
                    hitz = pEnemy.getZ() - (GetSpriteHeight(nEnemy) >> 1);
                    hitsec = pEnemy.getSectnum();
                    hitspr = nEnemy;
                }
            }
            lasthitx = hitx;
            lasthity = hity;
            lasthitz = hitz;
            lasthitsect = hitsec;
            lasthitwall = hitwall;
            lasthitsprite = hitspr;
        } else {
            int v9 = nBulletEnemy[nBullet];
            Sprite pEnemy = boardService.getSprite(v9);
            int moveHit;
            if (pEnemy == null || (pEnemy.getCstat() & 0x101) == 0) {
                nBulletEnemy[nBullet] = -1;
                if (type == 3) {
                    if (pBullet.bull_E >= 8) {
                        pSprite.setXrepeat(pSprite.getXrepeat() + 4);
                        pSprite.setYrepeat(pSprite.getYrepeat() + 4);
                    } else {
                        pSprite.setXrepeat(pSprite.getXrepeat() - 1);
                        pSprite.setYrepeat(pSprite.getYrepeat() + 8);
                        pBullet.zvec -= 200;
                        if (pSprite.getShade() < 90) {
                            pSprite.setShade(pSprite.getShade() + 35);
                        }
                        if (pBullet.bull_E == 3) {
                            pBullet.baseSeq = 45;
                            pBullet.frmOffset = 0;
                            pSprite.setXrepeat(40);
                            pSprite.setYrepeat(40);
                            pSprite.setShade(0);
                            pSprite.setZ(pSprite.getZ() + 512);
                        }
                    }
                }

                moveHit = engine.movesprite(nSprite, pBullet.xvec, pBullet.yvec, pBullet.zvec,
                        pSprite.getClipdist() >> 1, pSprite.getClipdist() >> 1, 1);
            } else {
                moveHit = AngleChase(nSprite, v9, (pBullet.bull_10 & 0xFFFF), 0, 16);
            }

            out = moveHit != 0;
            if (out) {
                hitx = pSprite.getX();
                hity = pSprite.getY();
                hitz = pSprite.getZ();
                hitsec = pSprite.getSectnum();

                if ((moveHit & nEvent3) != 0 || (moveHit & PS_HIT_TYPE_MASK) == PS_HIT_WALL) {
                    hitwall = moveHit & PS_HIT_INDEX_MASK;
                } else if ((moveHit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
                    hitspr = moveHit & PS_HIT_INDEX_MASK;
                }
            }

            if (hitwall == -1 && hitspr == -1) {
                if ((((nSectFlag ^ SectFlag[pSprite.getSectnum()]) >> 8) & 0x20) != 0) {
                    DestroyBullet(nBullet);
                    out = true;
                }

                if (!out && type != 15 && type != 3) {
                    AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                    if (pSprite.getPal() != 5) {
                        pSprite.setPal(1);
                    }
                }
                return out;
            }
        }

        if (hitspr != -1) {
            Sprite pHit = boardService.getSprite(hitspr);
            if (pHit != null) {
                if (pSprite.getPal() != 5 || pHit.getStatnum() != 100) {
                    BulletHitsSprite(pBullet, pSprite.getOwner(), hitspr, hitx, hity, hitz, hitsec);
                } else {
                    int plr = GetPlayerFromSprite(hitspr);
                    if (PlayerList[plr].mummified == 0) {
                        PlayerList[plr].mummified = 1;
                        SetNewWeapon(plr, 7);
                    }
                }
            }
        } else if (hitwall != -1) {
            Wall pHit = boardService.getWall(hitwall);
            if (pHit != null && pHit.getPicnum() == 3604) {
                ru.m210projects.Build.Types.Sector nextSector = boardService.getSector(pHit.getNextsector());
                if (nextSector != null) {
                    int v31 = BulletInfo[pBullet.type].force;
                    if (pBullet.bull_13 > 1) {
                        v31 *= 2;
                    }

                    DamageEnemy(nextSector.getExtra(), engine.GetWallNormal(hitwall) & 0x7FF, v31);
                }
            }
        }

        if (hitsec != -1) {
            if (hitwall != -1 || hitspr != -1) {
                if (hitwall == -1) {
                    pSprite.setX(hitx);
                    pSprite.setY(hity);
                    pSprite.setZ(hitz);
                    engine.mychangespritesect( nSprite,  hitsec);
                } else {
                    Vector2 vec = BackUpBullet(hitx, hity, pSprite.getAng());
                    hitx = (int) vec.x;
                    hity = (int) vec.y;
                    if (type != 3 || RandomSize(2) == 0) {
                        int v38 = RandomSize(8);
                        int v37 = RandomBit();
                        int v39 = 8 * v38;
                        if (v37 == 0) {
                            v39 = -v39;
                        }

                        BuildAnim(-1, pBulletInfo.inf_C, 0, hitx, hity, hitz + v39, hitsec, 0x28, pBulletInfo.flags);
                    }
                }
                if (pBulletInfo.inf_10 != 0) {
                    nRadialBullet =  type;
                    RadialDamageEnemy(nSprite, pBulletInfo.force, pBulletInfo.inf_10);
                    nRadialBullet = -1;
                    AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 128);
                }
            } else {
                if (SectBelow[hitsec] >= 0 && (SectFlag[SectBelow[hitsec]] & 0x2000) != 0 || SectDepth[hitsec] != 0) {
                    pSprite.setX(hitx);
                    pSprite.setY(hity);
                    pSprite.setZ(hitz);
                    BuildSplash(pSprite, hitsec);
                } else {
                    BuildAnim(-1, pBulletInfo.inf_C, 0, hitx, hity, hitz, hitsec, 0x28, pBulletInfo.flags);
                }
            }
        }
        DestroyBullet(nBullet);
        return true;
    }

    public static void FuncBullet(int nStack, int ignored, int RunPtr) {
        int nBullet = RunData[RunPtr].getObject();
        if (nBullet < 0 || nBullet >= 500) {
            throw new AssertException("nBullet>=0 && nBullet<MAX_BULLETS");
        }

        BulletStruct pBullet = BulletList[nBullet];
        int nSprite = pBullet.nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if ((nStack & EVENT_MASK) == nEventProcess) {
            int v17 = SeqOffsets[pBullet.baseSeq];
            int v8 = pBullet.frmOffset;
            MoveSequence(nSprite, v17, v8);
            if ((FrameFlag[v8 + SeqBase[v17]] & 0x80) != 0) {
                BuildAnim(-1, 45, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(),
                        pSprite.getXrepeat(), 0);
            }
            pBullet.frmOffset++;
            if (pBullet.frmOffset >= SeqSize[v17]) {
                if (pBullet.bull_12 == 0) {
                    pBullet.baseSeq = BulletInfo[pBullet.type].inf_A;
                    pBullet.bull_12++;
                }
                pBullet.frmOffset = 0;
            }
            if (pBullet.bull_E == -1 || (--pBullet.bull_E) != 0) {
                MoveBullet(nBullet);
            } else {
                DestroyBullet(nBullet);
            }
        } else if ((nStack & EVENT_MASK) == nEventView) {
            int spr = (short) (nStack & 0xFFFF);
            Renderer renderer = game.getRenderer();
            RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();
            TSprite tsp = renderedSpriteList.get(spr);
            tsp.setStatnum(1000);
            if (pBullet.type == 15) {
                PlotArrowSequence(tsp, SeqOffsets[pBullet.baseSeq], pBullet.frmOffset);
            } else {
                PlotSequence(tsp, SeqOffsets[pBullet.baseSeq], pBullet.frmOffset, 0);
                tsp.setOwner(-1);
            }
        }
    }
}
