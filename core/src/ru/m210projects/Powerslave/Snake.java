// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;
import ru.m210projects.Powerslave.Type.SnakeStruct;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Powerslave.Anim.BuildAnim;
import static ru.m210projects.Powerslave.Bullet.BackUpBullet;
import static ru.m210projects.Powerslave.Bullet.BulletInfo;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.AddFlash;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.Sprites.*;

public class Snake {

    public static final int MAX_SNAKES = 50;
    public static final SnakeStruct[] SnakeList = new SnakeStruct[MAX_SNAKES];
    private static int nSnakesFree;
    private static final int[] SnakeFree = new int[MAX_SNAKES];
    private static final int[] nSnakePlayer = new int[MAX_SNAKES];

    public static void InitSnakes() {
        for (short i = 0; i < MAX_SNAKES; i++) {
            SnakeFree[i] = i;
        }
        nSnakesFree = MAX_SNAKES;
        Arrays.fill(nPlayerSnake,  0);
    }

    public static void saveSnakes(OutputStream os) throws IOException {
        int nSnakes = 0;
        for (int i = 0; i < MAX_SNAKES; i++) {
            if (SnakeList[i] != null && SnakeList[i].nFunc != -1) {
                nSnakes++;
            }
        }

        StreamUtils.writeShort(os,  nSnakesFree);
        for (int i = 0; i < MAX_SNAKES; i++) {
            StreamUtils.writeShort(os, SnakeFree[i]);
            StreamUtils.writeShort(os, nSnakePlayer[i]);
        }

        if (nSnakes != 0) {
            for (int i = 0; i < MAX_SNAKES; i++) {
                if (SnakeList[i] != null && SnakeList[i].nFunc != -1) {
                    StreamUtils.writeShort(os,  i);
                    SnakeList[i].save(os);
                }
            }
        }
    }

    public static void loadSnakes(SafeLoader loader) {
        nSnakesFree = loader.nSnakesFree;

        System.arraycopy(loader.SnakeFree, 0, SnakeFree, 0, MAX_SNAKES);
        System.arraycopy(loader.nSnakePlayer, 0, nSnakePlayer, 0, MAX_SNAKES);

        for (int i = 0; i < MAX_SNAKES; i++) {
            if (SnakeList[i] != null) {
                SnakeList[i].nFunc = -1;
            }

            if (loader.SnakeList[i] != null && loader.SnakeList[i].nFunc != -1) {
                if (SnakeList[i] == null) {
                    SnakeList[i] = new SnakeStruct();
                }
                SnakeList[i].copy(loader.SnakeList[i]);
            }
        }
    }

    public static void loadSnakes(SafeLoader loader, InputStream is) throws IOException {
        for (int i = 0; i < MAX_SNAKES; i++) {
            if (loader.SnakeList[i] != null) {
                loader.SnakeList[i].nFunc = -1;
            }
        }

        loader.nSnakesFree = StreamUtils.readShort(is);
        for (int i = 0; i < MAX_SNAKES; i++) {
            loader.SnakeFree[i] =  StreamUtils.readShort(is);
            loader.nSnakePlayer[i] =  StreamUtils.readShort(is);
        }

        int nSnakes = (MAX_SNAKES - loader.nSnakesFree);
        while (nSnakes > 0) {
            short i =  StreamUtils.readShort(is);
            if (loader.SnakeList[i] == null) {
                loader.SnakeList[i] = new SnakeStruct();
            }
            loader.SnakeList[i].load(is);
            nSnakes--;
        }
    }

    public static int GrabSnake() {
        return SnakeFree[--nSnakesFree];
    }

    public static void DestroySnake(int a1) {
        SubRunRec(SnakeList[a1].nFunc);
        for (int i = 0; i < 8; i++) {
            int spr = SnakeList[a1].nSprite[i];
            Sprite sp = boardService.getSprite(spr);
            if (sp != null) {
                DoSubRunRec(sp.getLotag() - 1);
                DoSubRunRec(sp.getOwner());
                engine.mydeletesprite(spr);
            }
        }
        SnakeList[a1].nFunc = -1;
        SnakeFree[nSnakesFree] =  a1;
        nSnakesFree++;

        if (a1 == nSnakeCam) {
            nSnakeCam = -1;
        }
    }

    public static void ExplodeSnakeSprite(int nSprite, int a2) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int v3 = BulletInfo[5].force;
        if (nPlayerDouble[a2] > 0) {
            v3 = 2 * BulletInfo[5].force;
        }

        short old = pSprite.getOwner();
        pSprite.setOwner(PlayerList[a2].spriteId);
        RadialDamageEnemy(nSprite, v3, BulletInfo[5].inf_10);
        pSprite.setOwner(old);
        BuildAnim(-1, 23, 0, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 0x28, 4);
        AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 128);
        StopSpriteSound(nSprite);
    }

    public static void BuildSnake(int a1, int a2) {
        if (nSnakesFree != 0) {
            int nSprite = PlayerList[a1].spriteId;
            Sprite pSprite = boardService.getSprite(nSprite);
            if (pSprite == null) {
                return;
            }

            int sect = nPlayerViewSect[a1];
            int picnum = GetSeqPicnum(21, 0, 0);
            int v40 = a2 - 1280;

            engine.hitscan(pSprite.getX(), pSprite.getY(), v40 + pSprite.getZ() - 2560, pSprite.getSectnum(),
                    EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF), EngineUtils.sin(pSprite.getAng() & 0x7FF), 0, pHitInfo,
                    CLIPMASK1);

            if (EngineUtils.sqrt((pHitInfo.hitx - pSprite.getX()) * (pHitInfo.hitx - pSprite.getX())
                    + (pHitInfo.hity - pSprite.getY()) * (pHitInfo.hity - pSprite.getY())) >= (EngineUtils.sin(512) >> 4)) {

                Sprite pTarget = boardService.getSprite(pHitInfo.hitsprite);
                int nTarget;
                if (pTarget == null || pTarget.getStatnum() < 90 || pTarget.getStatnum() > 199) {
                    nTarget = sPlayerInput[a1].nTarget;
                } else {
                    nTarget = pHitInfo.hitsprite;
                }

                int nSnake = GrabSnake();
                if (SnakeList[nSnake] == null) {
                    SnakeList[nSnake] = new SnakeStruct();
                }

                int v55 = 0;
                int peer = -1;
                for (int i = 0; i < 8; i++) {
                    int spr = engine.insertsprite(sect,  202);
                    Sprite sp = boardService.getSprite(spr);
                    if (sp == null) {
                        throw new AssertException("spr>=0 && spr<MAXSPRITES");
                    }

                    sp.setOwner(nSprite);
                    sp.setPicnum(picnum);
                    if (i != 0) {
                        Sprite pPeer = boardService.getSprite(peer);
                        if (pPeer != null) {
                            sp.setX(pPeer.getX());
                            sp.setY(pPeer.getY());
                            sp.setZ(pPeer.getZ());
                            int siz = (40 - (i + v55));
                            sp.setXrepeat(siz);
                            sp.setYrepeat(siz);
                        }
                    } else {
                        sp.setX(pSprite.getX());
                        sp.setY(pSprite.getY());
                        sp.setZ(v40 + pSprite.getZ());
                        sp.setXrepeat(32);
                        sp.setYrepeat(32);
                        peer = spr;
                        sect = sp.getSectnum();
                    }
                    sp.setClipdist(10);
                    sp.setCstat(0);
                    sp.setShade(-64);
                    sp.setPal(0);
                    sp.setXoffset(0);
                    sp.setYoffset(0);
                    sp.setAng(pSprite.getAng());
                    sp.setXvel(0);
                    sp.setYvel(0);
                    sp.setZvel(0);
                    sp.setLotag( (HeadRun() + 1));
                    sp.setHitag(0);
                    sp.setExtra(-1);

                    SnakeList[nSnake].nSprite[i] =  spr;
                    sp.setOwner( AddRunRec(sp.getLotag() - 1, nEvent17 | (i << 8) | nSnake));
                    game.pInt.setsprinterpolate(spr, sp);
                    v55 += 2;
                }

                SnakeList[nSnake].nFunc =  AddRunRec(NewRun, nEvent17 | nSnake);
                SnakeList[nSnake].field_14[1] = 2;
                SnakeList[nSnake].field_14[2] = 4;
                SnakeList[nSnake].field_14[3] = 6;
                SnakeList[nSnake].field_14[4] = 7;
                SnakeList[nSnake].field_14[5] = 5;
                SnakeList[nSnake].field_14[6] = 6;
                SnakeList[nSnake].field_14[7] = 7;
                SnakeList[nSnake].nTarget =  nTarget;
                SnakeList[nSnake].field_10 = 1200;
                SnakeList[nSnake].zvec = 0;
                nSnakePlayer[nSnake] =  a1;
                nPlayerSnake[a1] =  nSnake;
                if (bSnakeCam && nSnakeCam < 0) {
                    nSnakeCam = nSnake;
                }
                D3PlayFX(StaticSound[6], peer);
            } else {
                BackUpBullet(pHitInfo.hitx, pHitInfo.hity, pSprite.getAng());
                int i = engine.insertsprite(pHitInfo.hitsect,  202);
                Sprite s = boardService.getSprite(i);
                if (s != null) {
                    s.setX(pHitInfo.hitx);
                    s.setY(pHitInfo.hity);
                    s.setZ(pHitInfo.hitz);
                    ExplodeSnakeSprite(i, a1);
                    engine.mydeletesprite(i);
                }
            }
        }
    }

    public static void FindSnakeEnemy(int nSnake) {
        int pspr = PlayerList[nSnakePlayer[nSnake]].spriteId;
        int spr = SnakeList[nSnake].nSprite[0];
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            SnakeList[nSnake].nTarget = -1;
            return;
        }

        int v15 = -1;
        int v5 = 2048;
        for (ListNode<Sprite> node = boardService.getSectNode(pSprite.getSectnum()); node != null; node = node.getNext()) {
            int i = node.getIndex();
            Sprite nodeSprite = node.get();
            if (nodeSprite.getStatnum() >= 90 && nodeSprite.getStatnum() < 150) {
                if ((nodeSprite.getCstat() & 0x101) != 0) {
                    if (i != pspr && ((nodeSprite.getCstat() >> 8) & 0x80) == 0) {
                        int v9 = (pSprite.getAng() - GetAngleToSprite(pSprite, nodeSprite)) & 0x7FF;
                        if (v9 < v5) {
                            v15 = i;
                            v5 = v9;
                        }
                    }
                }
            }
        }

        if (v15 != -1) {
            SnakeList[nSnake].nTarget =  v15;
            return;
        }

        if (--SnakeList[nSnake].nTarget < -25) {
            SnakeList[nSnake].nTarget = pspr;
        }
    }

    public static void FuncSnake(int nStack, int ignored, int RunPtr) {
        byte nSnake = (byte) (RunData[RunPtr].getObject() & 0xFF);
        if (nSnake < 0 || nSnake >= MAX_SNAKES) {
            throw new AssertException("nSnake>=0 && nSnake<MAX_SNAKES");
        }

        int v37 = 0, nHitMove = 0;
        short nObject = (short) (nStack & 0xFFFF);
        final int nSprite = SnakeList[nSnake].nSprite[0];
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                MoveSequence(nSprite, SeqOffsets[20], 0);
                int nTarget = SnakeList[nSnake].nTarget;
                Sprite pTarget = boardService.getSprite(nTarget);
                if (pTarget != null) {
                    if ((pTarget.getCstat() & 0x101) != 0) {
                        nHitMove = AngleChase(nSprite, nTarget, 1200, SnakeList[nSnake].zvec, 32);
//                        v37 = pTarget.getZ() - pSprite.getZ();
                        break;
                    }
                    SnakeList[nSnake].nTarget = -1;
                }

                nHitMove = engine.movesprite(nSprite, 600 * EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF),
                        600 * EngineUtils.sin(pSprite.getAng() & 0x7FF), EngineUtils.sin(SnakeList[nSnake].zvec & 0x7FF) >> 5, 0,
                        0, 1);
                FindSnakeEnemy(nSnake);
                break;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get(nObject);
                if ((RunData[RunPtr].getObject() & 0xFF00) != 0) {
                    PlotSequence(tsp, SeqOffsets[21], 0, 0);
                } else {
                    PlotSequence(tsp, SeqOffsets[20], 0, 0);
                }
                tsp.setOwner(-1);
                return;
        }

        if (nHitMove != 0) {
            ExplodeSnakeSprite(SnakeList[nSnake].nSprite[0], nSnakePlayer[nSnake]);
            nPlayerSnake[nObject] = -1;
            nSnakePlayer[nSnake] = -1;
            DestroySnake(nSnake);
        } else {
            int ang = pSprite.getAng();

            int v14 = -64 * EngineUtils.sin(ang & 0x7FF);
            int dx = -576 * EngineUtils.sin((ang + 512) & 0x7FF);

            int v36 = -64 * EngineUtils.sin((ang + 512) & 0x7FF);
            int v15 = -512 * EngineUtils.sin(ang & 0x7FF);
            int dy = v36 + v15;

            int v39 = SnakeList[nSnake].zvec;
            SnakeList[nSnake].zvec =  ((v39 + 64) & 0x7FF);

            short sectnum = pSprite.getSectnum();
            int sx = pSprite.getX();
            int sy = pSprite.getY();
            int sz = pSprite.getZ();
            int dz = 0; //-7 * v37;

            for (int i = 7; i > 0; i--) {
                int spr = SnakeList[nSnake].nSprite[i];
                Sprite tailSprite = boardService.getSprite(spr);
                if (tailSprite == null) {
                    continue;
                }

                game.pInt.setsprinterpolate(spr, tailSprite);
                tailSprite.setAng( ang);
                tailSprite.setX(sx);
                tailSprite.setY(sy);
                tailSprite.setZ(sz);
                engine.mychangespritesect(spr, sectnum);
                dx -= v36;
                dy -= v14;
                dz += v37;

                int vel = SnakeList[nSnake].field_14[i] * EngineUtils.sin(v39 & 0x7FF) >> 9;
                engine.movesprite( spr, vel * EngineUtils.sin((pSprite.getAng() + 1024) & 0x7FF) + dx,
                        EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) * vel + dy, dz, 0, 0, 1);
                v39 = (v39 + 128) & 0x7FF;
            }
        }
    }

}
