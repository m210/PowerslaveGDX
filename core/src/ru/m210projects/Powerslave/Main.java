// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ByteArray;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.MemLog;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdValueRange;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Powerslave.Factory.PSEngine;
import ru.m210projects.Powerslave.Factory.PSFactory;
import ru.m210projects.Powerslave.Factory.PSInput;
import ru.m210projects.Powerslave.Factory.PSMenuHandler;
import ru.m210projects.Powerslave.Menus.MainMenu;
import ru.m210projects.Powerslave.Menus.MenuCorruptGame;
import ru.m210projects.Powerslave.Menus.MenuGame;
import ru.m210projects.Powerslave.Menus.NewAddon;
import ru.m210projects.Powerslave.Screens.*;
import ru.m210projects.Powerslave.Type.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Powerslave.Factory.PSMenuHandler.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.LoadSave.*;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.*;
import static ru.m210projects.Powerslave.Player.sPlayerSave;
import static ru.m210projects.Powerslave.Random.InitRandom;
import static ru.m210projects.Powerslave.Seq.GetSeqPicnum;
import static ru.m210projects.Powerslave.Seq.LoadSequences;
import static ru.m210projects.Powerslave.Sound.*;

public class Main extends BuildGame {

    public static final String appdef = "psgdx.def";

    public static Main game;
    public static PSEngine engine;
    public static BoardService boardService;
    public static Config cfg;
    public static LoadingScreen gLoadingScreen;
    public static GameScreen gGameScreen;
    public static DemoScreen gDemoScreen;
    public static MapScreen gMap;
    public static PSLogoScreen gLogoScreen;
    public static LogoScreen2 gLogoScreen2;
    public static PSMovieScreen gMovieScreen;
    public static MenuScreen gMenuScreen;
    public static CinemaScreen gCinemaScreen;
    private final int LOGO1 = 3349; //3368
    public PSMenuHandler menu;
    public final Runnable rMenu = new Runnable() {
        @Override
        public void run() {
            resetState();

            engine.setbrightness(cfg.getPaletteGamma(), engine.getPaletteManager().getBasePalette());

            if (numplayers > 1 || gDemoScreen.demofiles.isEmpty() || cfg.gDemoSeq == 0 || !gDemoScreen.showDemo()) {
                changeScreen(gMenuScreen);
            }

            if (!menu.gShowMenu) {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
        }
    };

    public final Runnable toLogo3 = new Runnable() {
        @Override
        public void run() {
            game.changeScreen(gLogoScreen2.setCallback(rMenu));
        }
    };
    public final Runnable logo = new Runnable() {
        final Runnable cutscene = new Runnable() {
            @Override
            public void run() {
                if (gMovieScreen.open("book.mov")) {
                    game.changeScreen(gMovieScreen.setCallback(toLogo3).escSkipping(true));
                } else {
                    game.changeScreen(gCinemaScreen.setNum(2).setSkipping(toLogo3));
                }
            }
        };

        @Override
        public void run() {
            changeScreen(gLogoScreen.setTime(2.0f).setTile(GetSeqPicnum(59, 0, 0)).setCallback(cutscene).setSkipping(toLogo3));
        }
    };

    public Main(List<String> args, GameConfig bcfg, String name, String version, boolean isRelease) throws IOException {
        super(args, bcfg, name, version, isRelease);
        game = this;
        cfg = (Config) bcfg;
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new PSMessageScreen(this, header, text, type);
    }

    @Override
    public GameProcessor createGameProcessor() {
        return new PSInput(this);
    }

    @Override
    public BuildFactory getFactory() {
        return new PSFactory(this);
    }

    @Override
    public boolean init() {
        boardService = engine.getBoardService();
        ConsoleInit();

        for (int i = 0; i < 8; i++) {
            PlayerList[i] = new PlayerStruct();
            sPlayerSave[i] = new PlayerSave();
            sPlayerInput[i] = new Input();
        }
        sndInit();
        InitRandom();
        LoadFX();
        LoadSequences();
        InitOriginalEpisodes();

        byte[] remapbuf = new byte[256];
        for (int i = 0; i < 255; i++) {
            remapbuf[i] = (byte) (99 + (i & 8)); // gray palette #20
        }
        engine.getPaletteManager().makePalookup(20, remapbuf, 0, 0, 0, 1);
        Gdx.app.postRunnable(Palette::GrabPalette);

        nItemTextIndex = FindGString("ITEMS");

        Arrays.fill(nPlayerLives,  3);
        nBestLevel = 0;
        game.pNet.ResetTimers();

        Directory gameDir = cache.getGameDirectory();
        Console.out.println("Initializing def-scripts...");
        cache.loadGdxDef(baseDef, appdef, "psgdx.dat");

        if (pCfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP": {
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(appdef);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        }
                    }
                    break;
                    case "DEF":
                        baseDef.loadScript(file);
                        break;
                }
            }
        }

        FindSaves(getUserDirectory());
        searchCDtracks();

        FileEntry filgdx = gameDir.getEntry(appdef);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }
        this.setDefs(baseDef);

        menu.mMenus[MAIN] = new MainMenu(this);
        menu.mMenus[GAME] = new MenuGame(this);
        menu.mMenus[ADDON] = new NewAddon(this);
        menu.mMenus[CORRUPTLOAD] = new MenuCorruptGame(this);

        gLoadingScreen = new LoadingScreen(this);
        gGameScreen = new GameScreen(this);
        gDemoScreen = new DemoScreen(this);
        gMap = new MapScreen(this);
        gLogoScreen = new PSLogoScreen(this);
        gLogoScreen2 = new LogoScreen2(this, 1.0f);
        gMovieScreen = new PSMovieScreen(this);
        gMenuScreen = new MenuScreen(this);
        gCinemaScreen = new CinemaScreen(this);

        gDemoScreen.demoscan(gameDir);

        System.gc();
        MemLog.log("create");
        return true;
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (!entry.isExtension("map")) {
            return;
        }

        Console.out.println("Start dropped map: " + entry.getName());
        gGameScreen.newgame(entry, 0, false);
    }

    private void InitOriginalEpisodes() {
        Directory dir = cache.getGameDirectory();
        gTrainingEpisode = new EpisodeInfo("Training", new EpisodeEntry.Addon(dir, dir.getDirectoryEntry()));
        gTrainingEpisode.gMapInfo.add(new MapInfo("lev0.map", "Training"));

        final String[] sMapName = {"Abu Simbel", "Dendur", "Kalabsh", "El Subua", "El Derr",
                "Abu Ghurab", "Philae", "El Kab", "Aswan", "Set Boss", "Qubbet el Hawa", "Abydos", "Edufu", "West Bank",
                "Luxor", "Karnak", "Saqqara", "Mittrahn", "Kilmaatikahn Boss", "Alien Mothership"};

        gOriginalEpisode = new EpisodeInfo("Original", new EpisodeEntry.Addon(dir, dir.getDirectoryEntry()));
        for (int i = 0; i < 20; i++) {
            gOriginalEpisode.gMapInfo.add(new MapInfo("lev" + (i + 1) + ".map", sMapName[i]));
        }

        episodeManager.putEpisode(gTrainingEpisode);
        episodeManager.putEpisode(gOriginalEpisode);
    }

    private void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");
//		Console.out.getPrompt().setParameters(0, 0, 0, 0, 0, 0);
        Console.out.getPrompt().setVersion(getTitle(), OsdColor.YELLOW, 10);

        Console.out.registerCommand(new OsdValueRange("changemap",
                "", levelnum, 0, 20) {
            @Override
            public float getValue() {
                return levelnum;
            }

            @Override
            protected void setCheckedValue(float value) {
                levelnum = (int) value;
            }
        });
    }

    public void resetState() {
        menu.mClose();
        menu.mOpen(menu.mMenus[MAIN], -1);

        resetEpisodeResources(gOriginalEpisode);

        gAutosaveRequest = false;
        levelnum = 0;
        levelnew = 0;
        nBestLevel = -1;
        mUserFlag = UserFlag.None;
        boardfilename = DUMMY_ENTRY;
        lastload = null;

        StopAllSounds();
        if (MusicPlaying() && currTrack != 19) {
            StopMusic();
        }
    }

    public int FindGString(String string) {
        for (int i = 0; ; i++) {
            if (gString[i].equals(string)) {
                return i + 1;
            }
            if (gString[i].equals("EOF")) {
                break;
            }
        }
        return -1;
    }

    @Override
    public void show() {
        if (usecustomarts) {
            resetEpisodeResources(null);
        }

        changeScreen(gLogoScreen.setTime(2.0f).setTile(LOGO1).setCallback(logo).setSkipping(toLogo3));
    }

    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }


    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                byte[] out;

                String report = "Mapname: " + boardfilename;

                report += "\r\n";
                report += "UserFlag: " + mUserFlag;
                report += "\r\n";

                if (PlayerList[myconnectindex] != null && boardService.isValidSprite(PlayerList[myconnectindex].spriteId)) {
                    Sprite pSprite = boardService.getSprite(PlayerList[myconnectindex].spriteId);
                    if (pSprite != null) {
                        report += "PlayerX " + pSprite.getX();
                        report += "\r\n";
                        report += "PlayerY " + pSprite.getY();
                        report += "\r\n";
                        report += "PlayerZ " + pSprite.getZ();
                        report += "\r\n";
                        report += "PlayerAng " + pSprite.getAng();
                        report += "\r\n";
                        report += "PlayerSect: " + pSprite.getSectnum();
                        report += "\r\n";
                    }
                }

                if (mUserFlag == UserFlag.UserMap) {
                    ByteArray array = new ByteArray();
                    byte[] data = boardfilename.getBytes();
                    report += "\r\n<------Start Map data------->\r\n";
                    array.addAll(report.getBytes());
                    array.addAll(data);
                    array.addAll("\r\n<------End Map data------->\r\n".getBytes());

                    out = Arrays.copyOf(array.items, array.size);
                } else {
                    out = report.getBytes();
                }

                return out;
            }
        };
    }

    public void EndGame() {
        StopAllSounds();
        StopMusic();
        changeScreen(gLogoScreen.setTime(2.0f).setTile(LOGO1).setCallback(logo).setSkipping(toLogo3));
    }

    public enum UserFlag {
        None, UserMap, Addon
    }

}
