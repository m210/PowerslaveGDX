// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import java.io.InputStream;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Powerslave.Type.Channel;
import ru.m210projects.Powerslave.Type.SafeLoader;
import ru.m210projects.Powerslave.Type.SwitchStruct;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.boardService;
import static ru.m210projects.Powerslave.Map.nSwitchSound;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.PSSector.LinkMap;
import static ru.m210projects.Powerslave.Sound.PlayFXAtXYZ;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.View.StatusMessage;

public class Switch {

    public static final int MAXSWITCH = 1024;

    public static int SwitchCount;
    public static final SwitchStruct[] SwitchData = new SwitchStruct[MAXSWITCH];

    public static void InitSwitch() {
        SwitchCount = MAXSWITCH;
        for (int i = 0; i < MAXSWITCH; i++) {
            if (SwitchData[i] == null) {
                SwitchData[i] = new SwitchStruct();
            }
            SwitchData[i].clear();
        }
    }

    public static void saveSwitches(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  SwitchCount);
        for (int i = SwitchCount; i < MAXSWITCH; i++) {
            SwitchData[i].save(os);
        }
    }

    public static void loadSwitches(SafeLoader loader, InputStream is) throws IOException {
        loader.SwitchCount = StreamUtils.readShort(is);
        for (int i = loader.SwitchCount; i < MAXSWITCH; i++) {
            if (loader.SwitchData[i] == null) {
                loader.SwitchData[i] = new SwitchStruct();
            }
            loader.SwitchData[i].load(is);
        }
    }

    public static void loadSwitches(SafeLoader loader) {
        SwitchCount = loader.SwitchCount;
        for (int i = loader.SwitchCount; i < MAXSWITCH; i++) {
            if (SwitchData[i] == null) {
                SwitchData[i] = new SwitchStruct();
            }
            SwitchData[i].copy(loader.SwitchData[i]);
        }
    }

    public static int BuildSwStepOn(int a1, int a2, int a3) {
        if (SwitchCount <= 0 || a2 < 0 || a3 < 0) {
            throw new AssertException("Too many switches!");
        }
        SwitchCount--;
        SwitchData[SwitchCount].nChannel =  a1;
        SwitchData[SwitchCount].nLink =  a2;
        SwitchData[SwitchCount].nSector =  a3;
        SwitchData[SwitchCount].field_C = -1;
        return SwitchCount | nEvent3;
    }

    public static void FuncSwStepOn(int nStack, int ignored, int a3) {
        int sw = RunData[a3].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        if (pChannel.field_4 == -1) {
            return;
        }

        int v16 = LinkMap[pSwitch.nLink][pChannel.field_4];
        int nSector = pSwitch.nSector;
        Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return;
        }

        if ((nStack & EVENT_MASK) == nEvent1) {
            if (pSwitch.field_C >= 0) {
                SubRunRec(pSwitch.field_C);
                pSwitch.field_C = -1;
            }
            if (v16 >= 0) {
                pSwitch.field_C =  AddRunRec(sec.getLotag() - 1, RunData[a3].getEvent());
            }
        } else if ((nStack & EVENT_MASK) == nEvent5 && v16 != pChannel.field_4) {
            Wall wall = boardService.getWall(sec.getWallptr());
            if (wall != null) {
                PlayFXAtXYZ(StaticSound[nSwitchSound], wall.getX(), wall.getY(), sec.getFloorz(), nSector);
            }
            ChangeChannel(pSwitch.nChannel, LinkMap[pSwitch.nLink][pChannel.field_4]);
        }
    }

    public static int BuildSwReady(int a1, int a2) {
        if (SwitchCount <= 0 || a2 < 0) {
            throw new AssertException("Too many switch readys!");
        }
        SwitchCount--;
        SwitchData[SwitchCount].nChannel =  a1;
        SwitchData[SwitchCount].nLink =  a2;
        return nEvent1 | SwitchCount;
    }

    public static void FuncSwReady(int nStack, int ignored, int a3) {
        int sw = RunData[a3].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        if ((nStack & EVENT_MASK) == nEvent3) {
            if (LinkMap[pSwitch.nLink][pChannel.field_4] >= 0) {
                ChangeChannel(pSwitch.nChannel, LinkMap[pSwitch.nLink][pChannel.field_4]);
            }
        }
    }

    public static int BuildSwNotOnPause(int nChannel, int nLink, int nSector, int nPause) {
        if (SwitchCount <= 0 || nLink < 0 || nSector < 0) {
            throw new AssertException("Too many switches!");
        }
        SwitchCount--;

        SwitchData[SwitchCount].nChannel =  nChannel;
        SwitchData[SwitchCount].nLink =  nLink;
        SwitchData[SwitchCount].nPause =  nPause;
        SwitchData[SwitchCount].nSector =  nSector;
        SwitchData[SwitchCount].field_8 = -1;
        SwitchData[SwitchCount].field_C = -1;
        return SwitchCount | nEvent4;
    }

    public static void FuncSwNotOnPause(int nStack, int ignored, int RunPtr) {
        int sw = RunData[RunPtr].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        switch (nStack & EVENT_MASK) {
            case nEvent1:
                if (pSwitch.field_C >= 0) {
                    SubRunRec(pSwitch.field_C);
                    pSwitch.field_C = -1;
                }
                if (pSwitch.field_8 >= 0) {
                    SubRunRec(pSwitch.field_8);
                    pSwitch.field_8 = -1;
                }
                break;
            case nEventProcess:
                pSwitch.field_0 -= 4;
                if (pSwitch.field_0 <= 0) {
                    ChangeChannel(pSwitch.nChannel, LinkMap[pSwitch.nLink][pChannel.field_4]);
                }
                break;
            case nEvent3:
                if (LinkMap[pSwitch.nLink][pChannel.field_4] >= 0 && pSwitch.field_8 == -1) {
                    pSwitch.field_8 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent3
                    pSwitch.field_0 = pSwitch.nPause;
                    Sector sec = boardService.getSector(pSwitch.nSector);
                    if (sec != null) {
                        pSwitch.field_C = AddRunRec(sec.getLotag() - 1, RunData[RunPtr].getEvent()); // nEvent3
                    }
                }
                break;
            case nEvent5:
                pSwitch.field_0 = pSwitch.nPause;
                break;
        }
    }

    public static int BuildSwPause(int a1, int a2, int a3) {
        for (int i = MAXSWITCH - 1; i >= SwitchCount; --i) {
            if (a1 == SwitchData[i].nChannel && SwitchData[i].nPause != 0) {
                return i | nEventProcess;
            }
        }
        if (SwitchCount <= 0 || a2 < 0) {
            throw new AssertException("Too many switches!");
        }

        SwitchCount--;
        SwitchData[SwitchCount].nChannel =  a1;
        SwitchData[SwitchCount].nLink =  a2;
        SwitchData[SwitchCount].nPause =  a3;
        SwitchData[SwitchCount].field_8 = -1;
        return SwitchCount | nEventProcess;
    }

    public static void FuncSwPause(int nStack, int ignored, int a3) {
        int sw = RunData[a3].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        switch (nStack & EVENT_MASK) {
            case nEvent1:
                if (pSwitch.field_8 >= 0) {
                    SubRunRec(pSwitch.field_8);
                    pSwitch.field_8 = -1;
                }
                break;
            case nEventProcess:
                if (--pSwitch.field_0 <= 0) {
                    SubRunRec(pSwitch.field_8);
                    pSwitch.field_8 = -1;
                    ChangeChannel(pSwitch.nChannel, LinkMap[pSwitch.nLink][pChannel.field_4]);
                }
                break;
            case nEvent3:
                if (LinkMap[pSwitch.nLink][pChannel.field_4] >= 0 && pSwitch.field_8 < 0) {
                    pSwitch.field_8 =  AddRunRec(NewRun, RunData[a3].getEvent()); // nEvent3
                    int nPause = pSwitch.nPause;
                    if (nPause <= 0) {
                        nPause = 100;
                    }
                    pSwitch.field_0 = nPause;
                }
                break;
        }
    }

    public static int BuildSwPressSector(int a1, int a2, int a3, int a4) {
        if (SwitchCount <= 0 || a2 < 0 || a3 < 0) {
            throw new AssertException("Too many switches!");
        }

        SwitchCount--;
        SwitchData[SwitchCount].nChannel =  a1;
        SwitchData[SwitchCount].nLink =  a2;
        SwitchData[SwitchCount].nSector =  a3;
        SwitchData[SwitchCount].field_12 =  a4;
        SwitchData[SwitchCount].field_C = -1;
        return SwitchCount | nEvent5;
    }

    public static void FuncSwPressSector(int nStack, int ignored, int a3) {
        int sw = RunData[a3].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        short plr = (short) (nStack & 0xFFFF);
        if ((nStack & EVENT_MASK) == nEvent4) {
            if ((pSwitch.field_12 & PlayerList[plr].KeysBitMask) == pSwitch.field_12) {
                ChangeChannel(pSwitch.nChannel, LinkMap[pSwitch.nLink][pChannel.field_4]);
                return;
            }

            if (pSwitch.field_12 != 0) {
                Sprite spr = boardService.getSprite(PlayerList[plr].spriteId);
                if (spr != null) {
                    PlayFXAtXYZ(StaticSound[nSwitchSound], spr.getX(),
                            spr.getY(), 0, spr.getSectnum());
                }
                StatusMessage(300, "YOU NEED THE KEY FOR THIS DOOR", plr);
            }
            return;
        }

        if (pSwitch.field_C >= 0) {
            SubRunRec(pSwitch.field_C);
            pSwitch.field_C = -1;
        }

        if (LinkMap[pSwitch.nLink][pChannel.field_4] >= 0) {
            Sector sec = boardService.getSector(pSwitch.nSector);
            if (sec != null) {
                pSwitch.field_C = AddRunRec(sec.getLotag() - 1, RunData[a3].getEvent());
            }
        }
    }

    public static int BuildSwPressWall(int a1, int a2, int a3) {
        if (SwitchCount <= 0 || a2 < 0 || a3 < 0) {
            throw new AssertException("Too many switches!");
        }
        SwitchCount--;
        SwitchData[SwitchCount].nChannel =  a1;
        SwitchData[SwitchCount].nLink =  a2;
        SwitchData[SwitchCount].nWall =  a3;
        SwitchData[SwitchCount].field_10 = -1;
//		SwitchData[SwitchCount].field_14 = 0;

        return SwitchCount | nEvent6;
    }

    public static void FuncSwPressWall(int nStack, int ignored, int a3) {
        int sw = RunData[a3].getObject();
        if (sw < 0 || sw >= MAXSWITCH) {
            throw new AssertException("sw>=0 && sw<MAXSWITCH");
        }

        SwitchStruct pSwitch = SwitchData[sw];
        Channel pChannel = channel[pSwitch.nChannel];

        if (pChannel.field_4 == -1) {
            return;
        }

        Wall wall = boardService.getWall(pSwitch.nWall);
        byte pLinkMap = LinkMap[pSwitch.nLink][pChannel.field_4];

        if ((nStack & EVENT_MASK) == nEvent3) {
            if (pSwitch.field_10 >= 0) {
                SubRunRec(pSwitch.field_10);
                pSwitch.field_10 = -1;
            }
            if (pLinkMap >= 0 && wall != null) {
                pSwitch.field_10 =  AddRunRec(wall.getLotag() - 1, RunData[a3].getEvent());
            }
        } else if ((nStack & EVENT_MASK) == nEvent4 && wall != null) {
            ChangeChannel(pSwitch.nChannel, pLinkMap);
            if (pLinkMap < 0 || LinkMap[pSwitch.nLink][pLinkMap] < 0) {
                SubRunRec(pSwitch.field_10);
                pSwitch.field_10 = -1;
            }
            PlayFXAtXYZ(StaticSound[nSwitchSound], wall.getX(), wall.getY(), 0,
                    pSwitch.nSector);
        }
    }

}
