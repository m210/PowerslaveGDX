// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Type.StatusAnim;

import java.util.Arrays;
import java.util.Objects;

import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Player.*;
import static ru.m210projects.Powerslave.RunList.SignalRun;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Snake.SnakeList;
import static ru.m210projects.Powerslave.Sprites.GetSpriteHeight;
import static ru.m210projects.Powerslave.Type.StatusAnim.*;
import static ru.m210projects.Powerslave.Weapons.ammodelay;

public class View {

    private static final int[] gItemPic = {736, 752, 754, 726, 744};
    private static final char[] statbuffer = new char[80];
    public static final int[] nDigit = new int[3];
    public static int nOverhead = 0;
    public static int zoom = 768;

    public static void SetHealthFrame(int var) {
        if (healthperline == 0) {
            return;
        }

        nHealthLevel = (800 - PlayerList[nLocalPlayer].HealthAmount) / healthperline;
        if (nHealthLevel >= nMeterRange) {
            nHealthLevel = nMeterRange - 1;
        }
        if (nHealthLevel < 0) {
            nHealthLevel = 0;
        }

        if (var < 0) {
            BuildStatusAnim(4, 0);
        }
    }

    public static void SetMagicFrame() {
        if (magicperline == 0) {
            return;
        }

        nMagicLevel = (1000 - PlayerList[nLocalPlayer].MagicAmount) / magicperline;
        if (nMagicLevel >= nMeterRange) {
            nMagicLevel = nMeterRange - 1;
        }
        if (nMagicLevel < 0) {
            nMagicLevel = 0;
        }
    }

    public static void UpdateScreenSize() {
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        renderer.setview(0, 0, xdim - 1, ydim - 1);
        RefreshStatus();
    }

    public static void StatusMessage(int time, String text, int ignoredPlayer) {
        message_timer = time;
        message_text = text;
        Console.out.println(text);
    }

    public static void InitStatus() {
        nStatusSeqOffset = SeqOffsets[26];
        nHealthFrames = SeqSize[SeqOffsets[26] + 1];
        int tile = GetSeqPicnum(26, 1, 0);
        nMagicFrames = SeqSize[nStatusSeqOffset + 129];

        nHealthFrame = 0;
        nMagicFrame = 0;
        nHealthLevel = 0;
        nMagicLevel = 0;
        nMeterRange = engine.getTile(tile).getHeight();
        if (nMeterRange <= 0) {
            throw new AssertException("Error: Tile #" + tile + " is not found");
        }
        magicperline = 1000 / nMeterRange;
        healthperline = 800 / nMeterRange;
        nAirFrames = SeqSize[nStatusSeqOffset + 133];
        if (nAirFrames != 0) {
            airperline = 100 / nAirFrames;
        }
        nCounter = 0;
        nCounterDest = 0;
        Arrays.fill(nDigit, 0);

        SetCounter(0);
        SetHealthFrame(0);
        SetMagicFrame();
        SetAirFrame();

        for (byte i = 0; i < 50; i++) {
            StatusAnimsFree[i] = i;
            if (StatusAnim[i] == null) {
                StatusAnim[i] = new StatusAnim();
            }
        }

        nLastAnim = -1;
        nFirstAnim = -1;
        nItemSeq = -1;
        nAnimsFree_X_1 = 50;

        message_timer = 0;
    }

    public static void RefreshStatus() {
        BuildStatusAnim(2 * nPlayerLives[nLocalPlayer] + 145, 0);
        int keys = PlayerList[nLocalPlayer].KeysBitMask >> 12;
        for (int i = 0, s = 37; i < 4; i++, s += 2) {
            if ((keys & (1 << i)) != 0) {
                BuildStatusAnim(s, 0);
            }
        }

        if (nPlayerItem[nLocalPlayer] != -1) {
            if (nItemMagic[nPlayerItem[nLocalPlayer]] <= PlayerList[nLocalPlayer].MagicAmount) {
                nItemAltSeq = 0;
            } else {
                nItemAltSeq = 2;
            }
            nItemSeq = nItemAltSeq + nItemSeqOffset[nPlayerItem[nLocalPlayer]];
            nItemFrames = SeqSize[nStatusSeqOffset + nItemSeq];
            BuildStatusAnim(2 * PlayerList[nLocalPlayer].ItemsAmount[nPlayerItem[nLocalPlayer]] + 156, 0);
        }

        SetHealthFrame(0);
        SetMagicFrame();
    }

    public static void SetAirFrame() {
        if (airperline == 0) {
            return;
        }

        airframe = PlayerList[nLocalPlayer].AirAmount / airperline;
        if (airframe < nAirFrames) {
            if (airframe < 0) {
                airframe = 0;
            }
        } else {
            airframe = nAirFrames - 1;
        }
    }

    public static void MoveStatus() {
        if (++nHealthFrame >= nHealthFrames) {
            nHealthFrame = 0;
        }
        if (++nMagicFrame >= nMagicFrames) {
            nMagicFrame = 0;
        }

        if (nItemSeq >= 0 && ++nItemFrame >= nItemFrames) {
            if (nItemSeq == 67) {
                SetItemSeq();
            } else {
                nItemSeq -= nItemAltSeq;
                if (nItemAltSeq != 0 || (totalmoves & 0x1F) != 0) {
                    if (nItemAltSeq < 2) {
                        nItemAltSeq = 0;
                    }
                } else {
                    nItemAltSeq = 1;
                }
                nItemFrame = 0;
                nItemSeq += nItemAltSeq;
                nItemFrames = SeqSize[(nItemAltSeq + nItemSeq) + nStatusSeqOffset];
            }
        }
        int v1 = message_timer;
        if (message_timer != 0) {
            message_timer -= 4;
            if ((v1 - 4) <= 0) {
                message_timer = 0;
            }
        }
        MoveStatusAnims();
        if (nCounter == nCounterDest) {
            ammodelay = 3;
            return;
        }
        if (--ammodelay == 0) {
            return;
        }

        int v2 = nCounterDest - nCounter;
        if (nCounterDest - nCounter <= 0) {
            if (v2 < -30) {
                nCounter += (nCounterDest - nCounter) >> 1;
                SetCounterDigits();
                return;
            }

            for (int i = 0; i < 3; ++i) {
                int v8 = nDigit[i] - 1;
                nDigit[i] = v8;
                if (v8 < 0) {
                    nDigit[i] = v8 + 30;
                }
                if (nDigit[i] < 27) {
                    break;
                }
            }
        } else {
            if (v2 > 30) {
                nCounter += (nCounterDest - nCounter) >> 1;
                SetCounterDigits();
                return;
            }
            for (int j = 0; j < 3; ++j) {
                int v5 = nDigit[j] + 1;
                nDigit[j] = v5;
                if (v5 <= 27) {
                    break;
                }
                if (v5 >= 30) {
                    nDigit[j] = v5 - 30;
                }
            }
        }
        if ((nDigit[0] % 3) == 0) {
            nCounter = nDigit[0] / 3 + 100 * (nDigit[2] / 3) + 10 * (nDigit[1] / 3);
        }
        int v9 = nCounterDest - nCounter;
        if (nCounterDest - nCounter < 0) {
            v9 = -v9;
        }
        ammodelay = 4 - (v9 >> 1);
        if (ammodelay >= 1) {
            return;
        }

        ammodelay = 1;

    }

    public static void MoveStatusAnims() {
        int v7;
        for (int i = nFirstAnim; ; i = StatusAnim[v7].field_6) {
            v7 = i;
            if (i == -1) {
                break;
            }
            int v2 = (StatusAnim[i].field_0 + nStatusSeqOffset);
            MoveSequence(-1, StatusAnim[i].field_0 + nStatusSeqOffset, StatusAnim[i].field_2);
            int v4 = StatusAnim[i].field_2 + 1;
            int v5 = SeqSize[v2];
            StatusAnim[i].field_2 =  v4;
            if (v4 >= v5) {
                int v6;
                if ((StatusAnimFlags[i] & 0x10) != 0) {
                    v6 = 0;
                } else {
                    v6 = v5 - 1;
                }
                StatusAnim[i].field_2 =  v6;
            }
        }
    }

    public static void DrawStatus() {
        Renderer renderer = game.getRenderer();
        int gView = cfg.nScreenSize;
        if (gView == 2) {
            DrawStatusSequence(nStatusSeqOffset, 0, 0);

            DrawStatusSequence(nStatusSeqOffset + 128, 0, 0); // black hole health
            DrawStatusSequence(nStatusSeqOffset + 127, 0, 0); // black hole mana

            DrawStatusSequence(nStatusSeqOffset + 1, nHealthFrame, nHealthLevel); // health
            DrawStatusSequence(nStatusSeqOffset + 129, nMagicFrame, nMagicLevel); // manna
            DrawStatusSequence(nStatusSeqOffset + 125, 0, 0); // health icon

            DrawStatusSequence(nStatusSeqOffset + 130, 0, 0); // health border
            DrawStatusSequence(nStatusSeqOffset + 131, 0, 0); // mana border

            if (nItemSeq >= 0) {
                DrawStatusSequence((nItemSeq + nStatusSeqOffset), nItemFrame, 0);
            }
            DrawStatusAnims(false); // air and lives
            if ((SectFlag[nPlayerViewSect[nLocalPlayer]] & 0x2000) != 0) {
                DrawStatusSequence((nStatusSeqOffset + 133), airframe, 0);
            }

            DrawCustomSequence(nStatusSeqOffset + 35, ((inita + 128) & 0x7FF) >> 8,
                    (inita >= 128 && inita <= 892) ? 1 : 0, 0, 0, 0, 0); // compass

            DrawStatusSequence((nStatusSeqOffset + 44), nDigit[2], 0);
            DrawStatusSequence((nStatusSeqOffset + 45), nDigit[1], 0);
            DrawStatusSequence((nStatusSeqOffset + 46), nDigit[0], 0);
        } else if (gView == 1) // GDX HUD
        {
            renderer.rotatesprite(66 << 16, 179 << 16, 65536, 0, HUDLEFT, 0, 0, 10 | 256);
            renderer.rotatesprite(253 << 16, 179 << 16, 65536, 0, HUDRIGHT, 0, 0, 10 | 512);

            DrawCustomSequence(nStatusSeqOffset + 1, nHealthFrame, 0, nHealthLevel, 0, 0, 512); // health
            DrawCustomSequence(nStatusSeqOffset + 129, nMagicFrame, 0, nMagicLevel, 0, 0, 256); // manna
            DrawCustomSequence(nStatusSeqOffset + 125, 0, 0, 0, 0, 0, 512); // health icon
            if (nItemSeq >= 0) {
                DrawCustomSequence((nItemSeq + nStatusSeqOffset), nItemFrame, 0, 0, 0, 0, 256);
            }

            DrawCustomSequence(nStatusSeqOffset + 130, 0, 0, 0, 0, 0, 512); // health border
            DrawCustomSequence(nStatusSeqOffset + 131, 0, 0, 0, 0, 0, 256); // mana border

            renderer.rotatesprite(76 << 16, 186 << 16, 65536, 0, 883, 0, 0, 10 | 256); // first
            // air
            // icon

            DrawStatusAnims(true); // air and lives
            if ((SectFlag[nPlayerViewSect[nLocalPlayer]] & 0x2000) != 0) {
                DrawCustomSequence((nStatusSeqOffset + 133), airframe, -7, 0, 0, 0, 256);
            }

            DrawCustomSequence((nStatusSeqOffset + 44), nDigit[2], -13, 0, 0, 0, 256);
            DrawCustomSequence((nStatusSeqOffset + 45), nDigit[1], -13, 0, 0, 0, 256);
            DrawCustomSequence((nStatusSeqOffset + 46), nDigit[0], -13, 0, 0, 0, 256);
        }

        DrawItemTimer(300, 20, nLocalPlayer);

        if (cfg.gShowMessages) {
            if (nSnakeCam < 0) {
                if (message_timer != 0) {
                    if (Objects.equals(message_text, deathMessage1)) {
                        game.getFont(2).drawText(renderer, 0, 5, deathMessage2, 2, 0, 0, TextAlign.Left, Transparent.None, true);
                        game.getFont(2).drawText(renderer, 0, 25, deathMessage1, 2, 0, 0, TextAlign.Left, Transparent.None, true);
                    } else {
                        game.getFont(2).drawText(renderer, 0, 5, message_text, 2, 0, 0, TextAlign.Left, Transparent.None, true);
                    }
                }
            } else {
                game.getFont(2).drawTextScaled(renderer, 2, 5, "S E R P E N T   C A M", 2, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
            }
        }

        if (cfg.gCrosshair) {
            int col = 187;
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            renderer.drawline256((xdim - mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11,
                    (xdim - mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11, col);
            renderer.drawline256((xdim + mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11,
                    (xdim + mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11, col);
            renderer.drawline256(xdim << 11, (ydim - mulscale(cfg.gCrossSize, 16, 16)) << 11, xdim << 11,
                    (ydim - mulscale(cfg.gCrossSize, 4, 16)) << 11, col);
            renderer.drawline256(xdim << 11, (ydim + mulscale(cfg.gCrossSize, 4, 16)) << 11, xdim << 11,
                    (ydim + mulscale(cfg.gCrossSize, 16, 16)) << 11, col);
        }

        RefreshStatus();
    }

    private static void DrawItemTimer(int posx, int posy, int plr) {
        Renderer renderer = game.getRenderer();
        for (int i = 0; i < 5; i++) {
            int value = ItemTimer(i, plr);
            if (value > 0) {
                int tile = gItemPic[i];
                ArtEntry pic = engine.getTile(tile);
                int x = (posx - (pic.getWidth() / 8));
                int y = (posy - (pic.getHeight() / 8));
                renderer.rotatesprite(x << 16, y << 16, 32768, 0, gItemPic[i], 0, 0, 10 | 512);

                int offs = Bitoa(value, statbuffer);
                buildString(statbuffer, offs, "%");
                game.getFont(2).drawTextScaled(renderer, x, posy + (pic.getHeight() / 8) + 4, statbuffer, 0.5f, 24, 0, TextAlign.Center, Transparent.None, ConvertType.AlignRight, true);
                posy += 25;
            }
        }
    }

    private static int ItemTimer(int num, int plr) {
        switch (num) {
            case 0: //Scarab item
                return (PlayerList[plr].invisibility * 100) / 900;
            case 1: //Hand item
                return (nPlayerDouble[plr] * 100) / 1350;
            case 2: //Mask
                return (PlayerList[plr].AirMaskAmount * 100) / 1350;
            case 3: //Invisible
                return (nPlayerInvisible[plr] * 100) / 900;
            case 4: //Torch
                return (nPlayerTorch[plr] * 100) / 900;
        }

        return -1;
    }

    public static void DrawStatusAnims(boolean nCustomHud) {
        for (int a1 = nFirstAnim; a1 != -1; a1 = StatusAnim[a1].field_6) {
            int v2 = StatusAnim[a1].field_0 + nStatusSeqOffset;
            if (nCustomHud) {
                int stat = 0, xoffs = 0;
                switch (StatusAnim[a1].field_0) {
                    case 36:
                    case 37: // power key
                        xoffs = 12;
                        stat = 512;
                        break;
                    case 38:
                    case 39: // time key
                        xoffs = 9;
                        stat = 512;
                        break;
                    case 40:
                    case 41: // war key
                        xoffs = 7;
                        stat = 512;
                        break;
                    case 42:
                    case 43: // earth key
                        xoffs = 3;
                        stat = 512;
                        break;
                    case 4: // damage border
                    case 7:
                    case 10:
                    case 13:
                    case 19:
                    case 145: // no lives
                    case 147: // lives count 1
                    case 149: // lives count 2
                    case 151: // lives count 3
                    case 152:
                    case 153: // lives count 4
                    case 154:
                    case 155: // lives count 5
                        stat = 512;
                        break;
                    case 132: // air
                        xoffs = -7;
                    case 156: // item count 0
                    case 158: // item count 1
                    case 160: // item count 2
                    case 162: // item count 3
                    case 164: // item count 4
                    case 166: // item count 5
                        stat = 256;
                        break;
                    default:
                        Console.out.println("Anim " + StatusAnim[a1].field_0, OsdColor.RED);
                        break;
                }

                DrawCustomSequence(v2, StatusAnim[a1].field_2, xoffs, 0, 0, 0, stat);
            } else {
                DrawStatusSequence(v2, StatusAnim[a1].field_2, 0);
            }

            if (StatusAnim[a1].field_2 >= SeqSize[v2] - 1 && (StatusAnimFlags[a1] & 0x10) == 0) {
                StatusAnim[a1].field_4--;
                if (StatusAnim[a1].field_4 <= 0) {
                    DestroyStatusAnim(a1);
                }
            }
        }
    }

    public static void DestroyStatusAnim(int result) {
        int v1 = StatusAnim[result].field_6;
        int v2 = StatusAnim[result].field_7;
        if (v2 >= 0) {
            StatusAnim[StatusAnim[result].field_7].field_6 = (byte) v1;
        }
        if (v1 >= 0) {
            StatusAnim[v1].field_7 = (byte) v2;
        }
        if (result == nFirstAnim) {
            nFirstAnim = v1;
        }
        if (result == nLastAnim) {
            nLastAnim = v2;
        }
        int v3 = nAnimsFree_X_1 + 1;
        StatusAnimsFree[nAnimsFree_X_1] = (byte) result;
        nAnimsFree_X_1 = v3;
    }

    public static void analyzesprites(int smoothratio) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (int i = 0; i < renderedSpriteList.getSize(); i++) {
            Sprite pTSprite = renderedSpriteList.get(i);
            if (pTSprite.getOwner() == -1) {
                continue;
            }

            if ((pTSprite.getPicnum() == 338 || pTSprite.getPicnum() == 350) && (pTSprite.getCstat() & 128) == 0) { // Torch bouncing fix
                pTSprite.setCstat(pTSprite.getCstat() | 128);
                pTSprite.setZ(pTSprite.getZ() - ((engine.getTile(pTSprite.getPicnum()).getHeight() * pTSprite.getYrepeat()) << 1));
            }

            if (nSnakeCam >= 0) {
                int enemy = SnakeList[nSnakeCam].nTarget;
                if (enemy > -1 && pTSprite.getOwner() == enemy && (totalmoves & 1) == 0) {
                    pTSprite.setPal(5);
                }
            }

            ILoc oldLoc = game.pInt.getsprinterpolate(pTSprite.getOwner());
            if (oldLoc != null) {
                int x = oldLoc.x;
                int y = oldLoc.y;
                int z = oldLoc.z;
                short nAngle = oldLoc.ang;

                // interpolate sprite position
                x += mulscale(pTSprite.getX() - oldLoc.x, smoothratio, 16);
                y += mulscale(pTSprite.getY() - oldLoc.y, smoothratio, 16);
                z += mulscale(pTSprite.getZ() - oldLoc.z, smoothratio, 16);
                nAngle += (short) mulscale(((pTSprite.getAng() - oldLoc.ang + 1024) & 0x7FF) - 1024, smoothratio, 16);

                pTSprite.setX(x);
                pTSprite.setY(y);
                pTSprite.setZ(z);
                pTSprite.setAng(nAngle);
            }

            Sector sec = boardService.getSector(pTSprite.getSectnum());
            if (sec != null) {
                int shade = (sec.getFloorshade() + 10);
                if ((sec.getCeilingstat() & 1) != 0) {
                    shade = (sec.getCeilingshade() + 10);
                }
                pTSprite.setShade((byte) BClipRange(pTSprite.getShade() + shade, -127, 127));
            }
        }

        int nPlayer = PlayerList[nLocalPlayer].spriteId;
        int v29 = 20;
        int v31 = 30000;

        Sprite pPlayer = boardService.getSprite(nPlayer);
        if (pPlayer == null) {
            return;
        }

        besttarget = -1;

        int px = pPlayer.getX();
        int py = pPlayer.getY();
        int pz = pPlayer.getZ() - GetSpriteHeight(nPlayer) / 2;
        short nSector = pPlayer.getSectnum();
        int pang = (2048 - pPlayer.getAng()) & 0x7FF;

        int spr = renderedSpriteList.getSize();
        while (--spr >= 0) {
            Sprite pTSprite = renderedSpriteList.get(spr);
            short nOwner = pTSprite.getOwner();
            Sprite pSprite = boardService.getSprite(nOwner);
            if (pSprite == null) {
                continue;
            }

            if (!bCamera && (nOwner == nPlayer || nOwner == nDoppleSprite[nLocalPlayer])) {
                pTSprite.setOwner(-1);
                continue;
            }

            if (pSprite.getStatnum() > 0) {
                SignalRun((pSprite.getLotag() - 1), nEventView | spr);
                if (pSprite.getStatnum() < 150 && (pSprite.getCstat() & 0x101) != 0 && nOwner != nPlayer) {
                    int dx = pSprite.getX() - px;
                    int dy = pSprite.getY() - py;
                    int cos = EngineUtils.cos(pang);
                    int sin = EngineUtils.sin(pang);

                    int v17 = klabs((cos * dx - sin * dy) >> 14);
                    if (v17 != 0) {
                        int v20 = 32 * klabs((sin * dx + dy * cos) >> 14) / v17;
                        if (v17 >= 1000 || v17 >= v31 || v20 >= 10) {
                            if (v17 < 30000) {
                                int v22 = v29 - v20;
                                if (v22 > 3 || v17 < v31 && klabs(v22) < 5) {
                                    v29 = v20;
                                    v31 = v17;
                                    besttarget = nOwner;
                                }
                            }
                        } else {
                            v29 = v20;
                            v31 = v17;
                            besttarget = nOwner;
                        }
                    }
                }
            }
        }

        Sprite pBest = boardService.getSprite(besttarget);
        if (pBest != null) {
            nCreepyTimer = nCreepyTime;
            if (!engine.cansee(px, py, pz, nSector, pBest.getX(), pBest.getY(),
                    pBest.getZ() - GetSpriteHeight(besttarget), pBest.getSectnum())) {
                besttarget = -1;
            }
        }
    }

    public static void viewDrawStats(int x, int y, int zoom) {
        if (cfg.gShowStat == 0 || cfg.gShowStat == 2 && nOverhead == 0) {
            return;
        }

        Renderer renderer = game.getRenderer();
        float viewzoom = (zoom / 65536.0f);
        Font f = game.getFont(2);

        buildString(statbuffer, 0, "K: ");
        int alignx = f.getWidth(statbuffer, viewzoom);

        int yoffset = (int) (2.5f * f.getSize() * viewzoom);
        y -= yoffset;

        int staty = y;

        f.drawTextScaled(renderer, x, staty, statbuffer, viewzoom, 0, 20, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);

        int offs = Bitoa((nCreaturesMax - nCreaturesLeft), statbuffer);
        buildString(statbuffer, offs, " / ", nCreaturesMax);
        f.drawTextScaled(renderer, x + (alignx + 2), staty, statbuffer, viewzoom, 0, 15, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);

        staty = y + (int) (8 * viewzoom);

        buildString(statbuffer, 0, "T: ");
        f.drawTextScaled(renderer, x, staty, statbuffer, viewzoom, 0, 20, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
        alignx = f.getWidth(statbuffer, viewzoom);

        int sec = (totalmoves / 30) % 60;
        int minutes = (totalmoves / (30 * 60)) % 60;
        int hours = (totalmoves / (30 * 3600)) % 60;

        offs = Bitoa(hours, statbuffer, 2);
        offs = buildString(statbuffer, offs, ":", minutes, 2);
        buildString(statbuffer, offs, ":", sec, 2);
        f.drawTextScaled(renderer, x + (alignx + 2), staty, statbuffer, viewzoom, 0, 15, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
    }
}
