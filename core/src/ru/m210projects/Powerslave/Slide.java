// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.nStopSound;
import static ru.m210projects.Powerslave.Object.LongSeek;
import static ru.m210projects.Powerslave.Object.longSeek_out;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Sound.D3PlayFX;
import static ru.m210projects.Powerslave.Sound.StaticSound;

public class Slide {

    public static int PointCount;
    public static final int[] PointFree = new int[1024];
    public static int SlideCount;
    public static final int[] SlideFree = new int[128];
    public static final PointStruct[] PointList = new PointStruct[1024];
    public static final SlideStruct[] SlideData = new SlideStruct[128];
    public static final SlideStruct2[] SlideData2 = new SlideStruct2[128];

    public static void InitPoint() {
        PointCount = 0;
        for (int i = 0; i < 1024; i++) {
            PointFree[i] =  i;
            if (PointList[i] == null) {
                PointList[i] = new PointStruct();
            }
        }
    }

    public static void saveSlide(OutputStream os) throws IOException {
        saveData(os);
        savePoints(os);
    }

    public static void loadSlide(SafeLoader loader, InputStream is) throws IOException {
        loadData(loader, is);
        loadPoints(loader, is);
    }

    public static void loadSlide(SafeLoader loader) {
        loadData(loader);
        loadPoints(loader);
    }

    private static void saveData(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  SlideCount);
        for (int i = SlideCount; i < 128; i++) {
            SlideData[i].save(os);
            SlideData2[i].save(os);
        }

        for (int i = 0; i < 128; i++) {
            StreamUtils.writeShort(os, SlideFree[i]);
        }
    }

    private static void loadData(SafeLoader loader, InputStream is) throws IOException {
        loader.SlideCount = StreamUtils.readShort(is);
        for (int i = loader.SlideCount; i < 128; i++) {
            if (loader.SlideData[i] == null) {
                loader.SlideData[i] = new SlideStruct();
            }
            loader.SlideData[i].load(is);
            if (loader.SlideData2[i] == null) {
                loader.SlideData2[i] = new SlideStruct2();
            }
            loader.SlideData2[i].load(is);
        }
        for (int i = 0; i < 128; i++) {
            loader.SlideFree[i] =  StreamUtils.readShort(is);
        }
    }

    private static void loadData(SafeLoader loader) {
        SlideCount = loader.SlideCount;
        for (int i = loader.SlideCount; i < 128; i++) {
            if (SlideData[i] == null) {
                SlideData[i] = new SlideStruct();
            }
            SlideData[i].copy(loader.SlideData[i]);
            if (SlideData2[i] == null) {
                SlideData2[i] = new SlideStruct2();
            }
            SlideData2[i].copy(loader.SlideData2[i]);
        }
        System.arraycopy(loader.SlideFree, 0, SlideFree, 0, 128);
    }

    private static void savePoints(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  PointCount);
        for (int i = 0; i < PointCount; i++) {
            PointList[i].save(os);
        }
        for (int i = 0; i < 1024; i++) {
            StreamUtils.writeShort(os, PointFree[i]);
        }
    }

    private static void loadPoints(SafeLoader loader, InputStream is) throws IOException {
            loader.PointCount = StreamUtils.readShort(is);
            for (int i = 0; i < loader.PointCount; i++) {
                if (loader.PointList[i] == null) {
                    loader.PointList[i] = new PointStruct();
                }
                loader.PointList[i].load(is);
            }
            for (int i = 0; i < 1024; i++) {
                loader.PointFree[i] =  StreamUtils.readShort(is);
            }
    }

    private static void loadPoints(SafeLoader loader) {
        PointCount = loader.PointCount;
        for (int i = 0; i < loader.PointCount; i++) {
            if (PointList[i] == null) {
                PointList[i] = new PointStruct();
            }
            PointList[i].copy(loader.PointList[i]);
        }
        System.arraycopy(loader.PointFree, 0, PointFree, 0, 1024);
    }

    public static int GrabPoint() {
        return PointFree[PointCount++];
    }

    public static void InitSlide() {
        SlideCount = 128;
        for (int i = 0; i < SlideCount; i++) {
            SlideFree[i] =  i;
            if (SlideData2[i] == null) {
                SlideData2[i] = new SlideStruct2();
            }
            if (SlideData[i] == null) {
                SlideData[i] = new SlideStruct();
            }
        }
    }

    public static int IdentifySector(int a1) {
        for (int i = 0, j; i < boardService.getSectorCount(); ++i) {
            Sector sec = boardService.getSector(i);
            if (sec != null) {
                for (j = 0; j < sec.getWallnum(); j++) {
                    if (sec.getWallptr() + j == a1) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public static int BuildSlide(int a1, int a2, int a3, int a4, int a5, int a6, int a7) {
        if (SlideCount <= 0) {
            throw new AssertException("Too many slides!");
        }

        SlideCount--;
        final int sect =  IdentifySector(a2);
        Sector sec = boardService.getSector(sect);
        if (sec == null) {
            return -1;
        }

        SlideData2[SlideCount].channel =  a1;
        SlideData2[SlideCount].field_4 = -1;

        int v14 = GrabPoint();
        SlideData2[SlideCount].field_2 =  v14;

        PointList[v14].field_E = -1;
        PointList[v14].field_0 = sect;

        int startWall = sec.getWallptr();
        int endWall = sec.getWallptr() + sec.getWallnum();
        NEXT_WALL:
        for (int w = startWall; w < endWall; w++) {
            Wall pWall = boardService.getWall(w);
            if (pWall == null) {
                continue;
            }

            for (int i = SlideData2[SlideCount].field_2; i >= 0; i = PointList[i].field_E) {
                if (pWall.getNextsector() == PointList[i].field_0) {
                    break NEXT_WALL;
                }
            }

            if (pWall.getNextsector() >= 0) {
                int v24 = GrabPoint();
                PointList[v24].field_E = SlideData2[SlideCount].field_2;
                PointList[v24].field_0 = pWall.getNextsector();
                SlideData2[SlideCount].field_2 =  v24;
            }
        }

        SlideData[SlideCount].nWall1 = a2;
        SlideData[SlideCount].nWall2 = a3;
        SlideData[SlideCount].nWall3 = a5;
        SlideData[SlideCount].nWall4 = a6;

        Wall w2 = boardService.getWall(a2);
        Wall w3 = boardService.getWall(a3);
        Wall w4 = boardService.getWall(a4);
        Wall w5 = boardService.getWall(a5);
        Wall w6 = boardService.getWall(a6);
        Wall w7 = boardService.getWall(a7);

        if (w2 != null && w3 != null && w4 != null
                && w5 != null && w6 != null && w7 != null) {
            SlideData[SlideCount].x1 = w2.getX();
            SlideData[SlideCount].y1 = w2.getY();
            SlideData[SlideCount].x2 = w5.getX();
            SlideData[SlideCount].y2 = w5.getY();
            SlideData[SlideCount].x3 = w3.getX();
            SlideData[SlideCount].y3 = w3.getY();
            SlideData[SlideCount].x4 = w6.getX();
            SlideData[SlideCount].y4 = w6.getY();
            SlideData[SlideCount].x5 = w4.getX();
            SlideData[SlideCount].y5 = w4.getY();
            SlideData[SlideCount].x6 = w7.getX();
            SlideData[SlideCount].y6 = w7.getY();

            int spr = engine.insertsprite(sect, 899);
            Sprite pSprite = boardService.getSprite(spr);
            if (pSprite != null) {
                SlideData2[SlideCount].field_6 = spr;
                pSprite.setCstat(32768);
                pSprite.setX(w2.getX());
                pSprite.setY(w2.getY());
                pSprite.setZ(sec.getFloorz());
            }
        }
        SlideData2[SlideCount].field_8 = 0;
        return nEventDamage | SlideCount;
    }

    public static void FuncSlide(int nStack, int ignored, int RunPtr) {
        int nSlide = RunData[RunPtr].getObject();
        if (nSlide < 0 || nSlide >= 128) {
            throw new AssertException("slide>=0 && slide<MAXSLIDE");
        }

        int nChannel = channel[SlideData2[nSlide].channel].field_4;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                int v4 = 0;
                if (nChannel == 1) {
                    final int nWall =  SlideData[nSlide].nWall2;
                    Wall pWall = boardService.getWall(nWall);
                    if (pWall == null) {
                        break;
                    }

                    int wx = pWall.getX();
                    int wy = pWall.getY();
                    int xvel = LongSeek(wx, SlideData[nSlide].x5, 20, 20);
                    wx = longSeek_out;
                    int yvel = LongSeek(wy, SlideData[nSlide].y5, 20, 20);
                    wy = longSeek_out;
                    engine.dragpoint(nWall, wx, wy);
                    engine.movesprite(SlideData2[nSlide].field_6, xvel << 14, yvel << 14, 0, 0, 0, 1);
                    if (xvel == 0 && yvel == 0) {
                        v4 = 1;
                    }

                    final int nWall1 = SlideData[nSlide].nWall1;
                    Wall pWall1 = boardService.getWall(nWall1);
                    if (pWall1 == null) {
                        break;
                    }

                    engine.dragpoint(nWall1, xvel + pWall1.getX(), yvel + pWall1.getY());

                    final int nWall4 = SlideData[nSlide].nWall4;
                    Wall pWall4 = boardService.getWall(nWall4);
                    if (pWall4 == null) {
                        break;
                    }

                    wx = pWall4.getX();
                    wy = pWall4.getY();
                    xvel = LongSeek(wx, SlideData[nSlide].x6, 20, 20);
                    wx = longSeek_out;
                    yvel = LongSeek(wy, SlideData[nSlide].y6, 20, 20);
                    wy = longSeek_out;
                    engine.dragpoint(nWall4, wx, wy);

                    if (yvel == 0 && xvel == 0) {
                        v4++;
                    }

                    final int nWall3 = SlideData[nSlide].nWall3;
                    Wall pWall3 = boardService.getWall(nWall3);
                    if (pWall3 == null) {
                        break;
                    }

                    engine.dragpoint(nWall3, xvel + pWall3.getX(), yvel + pWall3.getY());
                } else if (nChannel == 0) {
                    final int nWall =  SlideData[nSlide].nWall1;
                    Wall pWall = boardService.getWall(nWall);
                    if (pWall == null) {
                        break;
                    }

                    int wx = pWall.getX();
                    int wy = pWall.getY();
                    int xvel = LongSeek(wx, SlideData[nSlide].x1, 20, 20);
                    wx = longSeek_out;
                    int yvel = LongSeek(wy, SlideData[nSlide].y1, 20, 20);
                    wy = longSeek_out;
                    engine.dragpoint(nWall, wx, wy);
                    if (xvel == 0 && yvel == 0) {
                        v4 = 1;
                    }

                    final int nWall2 = SlideData[nSlide].nWall2;
                    Wall pWall2 = boardService.getWall(nWall2);
                    if (pWall2 == null) {
                        break;
                    }

                    engine.dragpoint(nWall2, xvel + pWall2.getX(), yvel + pWall2.getY());

                    final int nWall3 = SlideData[nSlide].nWall3;
                    Wall pWall3 = boardService.getWall(nWall3);
                    if (pWall3 == null) {
                        break;
                    }

                    wx = pWall3.getX();
                    wy = pWall3.getY();
                    xvel = LongSeek(wx, SlideData[nSlide].x2, 20, 20);
                    wx = longSeek_out;
                    yvel = LongSeek(wy, SlideData[nSlide].y2, 20, 20);
                    wy = longSeek_out;
                    engine.dragpoint(nWall3, wx, wy);
                    if (yvel == 0 && xvel == 0) {
                        v4++;
                    }

                    final int nWall4 = SlideData[nSlide].nWall4;
                    Wall pWall4 = boardService.getWall(nWall4);
                    if (pWall4 == null) {
                        break;
                    }

                    engine.dragpoint(nWall4, xvel + pWall4.getX(), yvel + pWall4.getY());
                }

                if (v4 >= 2) {
                    SubRunRec(SlideData2[nSlide].field_4);
                    SlideData2[nSlide].field_4 = -1;
                    D3PlayFX(StaticSound[nStopSound], SlideData2[nSlide].field_6);
                    ReadyChannel(SlideData2[nSlide].channel);
                }
                return;
            case nEvent1:
                if (SlideData2[nSlide].field_4 >= 0) {
                    SubRunRec(SlideData2[nSlide].field_4);
                    SlideData2[nSlide].field_4 = -1;
                }
                if (nChannel == 0 || nChannel == 1) {
                    SlideData2[nSlide].field_4 =  AddRunRec(NewRun, RunData[RunPtr].getEvent()); // nEvent1
                    if (SlideData2[nSlide].field_8 != nChannel) {
                        D3PlayFX(StaticSound[23], SlideData2[nSlide].field_6);
                        SlideData2[nSlide].field_8 = nChannel;
                    }
                }
        }
    }

    public static class SlideStruct {

        public static final int size = 64;

        public int nWall1;
        public int nWall2;
        public int nWall3;
        public int nWall4;
        public int x1, y1;
        public int x2, y2;
        public int x3, y3;
        public int x4, y4;
        public int x5, y5;
        public int x6, y6;

        public void save(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, nWall1);
            StreamUtils.writeInt(os, nWall2);
            StreamUtils.writeInt(os, nWall3);
            StreamUtils.writeInt(os, nWall4);

            StreamUtils.writeInt(os, x1);
            StreamUtils.writeInt(os, y1);
            StreamUtils.writeInt(os, x2);
            StreamUtils.writeInt(os, y2);

            StreamUtils.writeInt(os, x3);
            StreamUtils.writeInt(os, y3);
            StreamUtils.writeInt(os, x4);
            StreamUtils.writeInt(os, y4);

            StreamUtils.writeInt(os, x5);
            StreamUtils.writeInt(os, y5);
            StreamUtils.writeInt(os, x6);
            StreamUtils.writeInt(os, y6);
        }

        public void load(InputStream is) throws IOException {
            nWall1 = StreamUtils.readInt(is);
            nWall2 = StreamUtils.readInt(is);
            nWall3 = StreamUtils.readInt(is);
            nWall4 = StreamUtils.readInt(is);

            x1 = StreamUtils.readInt(is);
            y1 = StreamUtils.readInt(is);
            x2 = StreamUtils.readInt(is);
            y2 = StreamUtils.readInt(is);

            x3 = StreamUtils.readInt(is);
            y3 = StreamUtils.readInt(is);
            x4 = StreamUtils.readInt(is);
            y4 = StreamUtils.readInt(is);

            x5 = StreamUtils.readInt(is);
            y5 = StreamUtils.readInt(is);
            x6 = StreamUtils.readInt(is);
            y6 = StreamUtils.readInt(is);
        }

        public void copy(SlideStruct src) {
            nWall1 = src.nWall1;
            nWall2 = src.nWall2;
            nWall3 = src.nWall3;
            nWall4 = src.nWall4;

            x1 = src.x1;
            y1 = src.y1;
            x2 = src.x2;
            y2 = src.y2;

            x3 = src.x3;
            y3 = src.y3;
            x4 = src.x4;
            y4 = src.y4;

            x5 = src.x5;
            y5 = src.y5;
            x6 = src.x6;
            y6 = src.y6;
        }
    }

    public static class SlideStruct2 {

        public static final int size = 10;

        public int channel;
        public int field_2;
        public int field_4;
        public int field_6;
        public int field_8;

        public void save(OutputStream os) throws IOException {
            StreamUtils.writeShort(os, channel);
            StreamUtils.writeShort(os, field_2);
            StreamUtils.writeShort(os, field_4);
            StreamUtils.writeShort(os,  field_6);
            StreamUtils.writeShort(os, field_8);
        }

        public void load(InputStream is) throws IOException {
            channel =  StreamUtils.readShort(is);
            field_2 =  StreamUtils.readShort(is);
            field_4 =  StreamUtils.readShort(is);
            field_6 = StreamUtils.readShort(is);
            field_8 =  StreamUtils.readShort(is);
        }

        public void copy(SlideStruct2 src) {
            channel = src.channel;
            field_2 = src.field_2;
            field_4 = src.field_4;
            field_6 = src.field_6;
            field_8 = src.field_8;
        }
    }

    public static class PointStruct {

        public int field_0;
        public int field_2;
        public int field_4;
        public int field_6;
        public int field_8;
        public int field_A;
        public int field_C;
        public int field_E;

        public void save(OutputStream os) throws IOException {
            StreamUtils.writeShort(os, field_0);
            StreamUtils.writeShort(os, field_2);
            StreamUtils.writeShort(os, field_4);
            StreamUtils.writeShort(os, field_6);
            StreamUtils.writeShort(os, field_8);
            StreamUtils.writeShort(os, field_A);
            StreamUtils.writeShort(os, field_C);
            StreamUtils.writeShort(os, field_E);
        }

        public void load(InputStream is) throws IOException {
            field_0 =  StreamUtils.readShort(is);
            field_2 =  StreamUtils.readShort(is);
            field_4 =  StreamUtils.readShort(is);
            field_6 =  StreamUtils.readShort(is);
            field_8 =  StreamUtils.readShort(is);
            field_A =  StreamUtils.readShort(is);
            field_C =  StreamUtils.readShort(is);
            field_E =  StreamUtils.readShort(is);
        }

        public void copy(PointStruct src) {
            field_0 = src.field_0;
            field_2 = src.field_2;
            field_4 = src.field_4;
            field_6 = src.field_6;
            field_8 = src.field_8;
            field_A = src.field_A;
            field_C = src.field_C;
            field_E = src.field_E;
        }
    }
}
