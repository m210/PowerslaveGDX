// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;

import java.io.IOException;
import java.io.InputStream;

import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.AnimType;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Powerslave.Enemies.Ra.FreeRa;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Grenade.ThrowGrenade;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.BuildItemAnim;
import static ru.m210projects.Powerslave.Player.nActionEyeLevel;
import static ru.m210projects.Powerslave.RunList.SignalRun;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.Type.StatusAnim.BuildStatusAnim;
import static ru.m210projects.Powerslave.View.InitStatus;
import static ru.m210projects.Powerslave.Weapons.*;

public class Seq {

    public static final int SEQMAX = 4096;
    public static final int FRAMEMAX = 18000;
    public static final int CHUNKMAX = 21000;
    public static int sequences;
    public static final int[] SeqBase = new int[SEQMAX];
    public static final int[] SeqSize = new int[SEQMAX];
    public static final int[] SeqFlag = new int[SEQMAX];
    public static final int[] SeqOffsets = new int[78];
    public static int frames;
    public static final int[] FrameBase = new int[FRAMEMAX];
    public static final int[] FrameSize = new int[FRAMEMAX];
    public static final int[] FrameFlag = new int[FRAMEMAX]; // 4(shade - 100) 16 64 128
    public static final int[] FrameSound = new int[FRAMEMAX];
    public static int chunks;
    public static final int[] ChunkXpos = new int[CHUNKMAX];
    public static final int[] ChunkYpos = new int[CHUNKMAX];
    public static final int[] ChunkPict = new int[CHUNKMAX];
    public static final int[] ChunkFlag = new int[CHUNKMAX];
    public static int centerx;
    public static int centery;
    private static final int[] nGunPicnum = {488, 490, 491, 878, 899, 3455};

    public static void LoadSequences() {
        String[] SeqNames = {"rothands.seq", "sword.seq", "pistol.seq", "m_60.seq", "flamer.seq", "grenade.seq",
                "cobra.seq", "bonesaw.seq", "scramble.seq", "glove.seq", "mummy.seq", "skull.seq", "poof.seq",
                "kapow.seq", "fireball.seq", "bubble.seq", "spider.seq", "anubis.seq", "anuball.seq", "fish.seq",
                "snakehed.seq", "snakbody.seq", "wasp.seq", "cobrapow.seq", "scorp.seq", "joe.seq", "status.seq",
                "dead.seq", "deadex.seq", "anupoof.seq", "skulpoof.seq", "bullet.seq", "shadow.seq", "grenroll.seq",
                "grenboom.seq", "splash.seq", "grenpow.seq", "skulstrt.seq", "firepoof.seq", "bloodhit.seq", "lion.seq",
                "items.seq", "lavag.seq", "lsplash.seq", "lavashot.seq", "smokebal.seq", "firepot.seq", "rex.seq",
                "set.seq", "queen.seq", "roach.seq", "hawk.seq", "setghost.seq", "setgblow.seq", "bizztail.seq",
                "bizzpoof.seq", "queenegg.seq", "roacshot.seq", "backgrnd.seq", "screens.seq", "arrow.seq", "fonts.seq",
                "drips.seq", "firetrap.seq", "magic2.seq", "creepy.seq", "slider.seq", "ravolt.seq", "eyehit.seq",
                "font2.seq", "seebubbl.seq", "blood.seq", "drum.seq", "poof2.seq", "deadbrn.seq", "grenbubb.seq",
                "rochfire.seq", "rat.seq"};

        for (int i = 0; i < 78; i++) {
            SeqOffsets[i] = sequences;
            if (!ReadSequence(SeqNames[i])) {
                Console.out.println("Error loading " + SeqNames[i]);
            }
        }

        nShadowPic = GetFirstSeqPicnum(32);
        nShadowWidth = engine.getTile(nShadowPic).getWidth();
        nFlameHeight = engine.getTile(GetFirstSeqPicnum(38)).getHeight(); // firepoof
        nBackgroundPic = GetFirstSeqPicnum(58); // backgrnd

        nPilotLightBase = SeqBase[SeqOffsets[4] + 3]; // flamer
        nPilotLightCount = SeqSize[SeqOffsets[4] + 3];
        nPilotLightFrame = 0;
        nFontFirstChar = GetFirstSeqPicnum(69); // font2
        for (int i = 0; i < SeqSize[SeqOffsets[69]]; i++) {
            engine.getTile(i + nFontFirstChar).setOffset(0, 0);
//            engine.getTile(i + nFontFirstChar).anm &= 0xFF0000FF;
        }

        InitStatus();
    }

    public static int GetSeqPicnum(int seq, int offset, int a3) {
        if ((SeqBase[SeqOffsets[seq] + offset] + a3) == -1) {
            return 0;
        }
        return ChunkPict[FrameBase[SeqBase[SeqOffsets[seq] + offset] + a3]];
    }

    public static int GetSeqPicnum2(int seq, int a2) {
        if ((SeqBase[seq] + a2) == -1) {
            return 0;
        }
        return ChunkPict[FrameBase[SeqBase[seq] + a2]];
    }

    public static int GetFirstSeqPicnum(int seq) {
        if (SeqBase[seq] == -1) {
            return 0;
        }
        return ChunkPict[FrameBase[SeqBase[SeqOffsets[seq]]]];
    }

    public static void DrawStatusSequence(int seq, int frameOffs, int yOffset) {
        Renderer renderer = game.getRenderer();
        int v3 = (SeqBase[seq] + frameOffs);
        if (v3 == -1) {
            return;
        }

        int chunkNum = FrameBase[v3];
        int count = FrameSize[v3];

        while (--count >= 0) {
            int ang = 0;
            int stat = 2 | 8;
            int orientation = ChunkFlag[chunkNum];
            int x = ChunkXpos[chunkNum] + 160;
            int y = ChunkYpos[chunkNum] + 100 + yOffset;

            if ((orientation & 3) != 0) {
                if ((orientation & 1) != 0) {
                    ang = 1024;
                }
                if (orientation != 3) {
                    stat |= 4;
                }
            }

            renderer.rotatesprite(x << 16, y << 16, 65536, ang, ChunkPict[chunkNum++], 0, 0, stat);
        }
    }

    public static void DrawCustomSequence(int baseSeq, int frameOffs, int xoffs, int yoffs, int shade, int pal,
                                          int dastat) {
        Renderer renderer = game.getRenderer();
        if (baseSeq == -1) {
            return;
        }

        int v3 = (SeqBase[baseSeq] + frameOffs);
        if (v3 == -1) {
            return;
        }

        int chunkNum = FrameBase[v3];
        int count = FrameSize[v3];

        while (--count >= 0) {
            int ang = 0;
            int stat = 2 | 8 | dastat;
            int orientation = ChunkFlag[chunkNum];
            int x = ChunkXpos[chunkNum] + 160 + xoffs;
            int y = ChunkYpos[chunkNum] + 100 + yoffs;

            if ((orientation & 3) != 0) {
                if ((orientation & 1) != 0) {
                    ang = 1024;
                }
                if (orientation != 3) {
                    stat |= 4;
                }
            }

            renderer.rotatesprite(x << 16, y << 16, 65536, ang, ChunkPict[chunkNum++], shade, pal, stat);
        }
    }

    public static void DrawGunSequence(int baseSeq, int frameOffs, int xoffs, int yoffs, int shade, int pal, int extstat) {
        if (baseSeq == -1) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int v3 = (SeqBase[baseSeq] + frameOffs);
        if (v3 == -1) {
            return;
        }

        int chunkNum = FrameBase[v3];
        int count = FrameSize[v3];
        int flags = FrameFlag[v3];

        while (--count >= 0) {
            int ang = 0;
            int stat = 2 | 8 | extstat;
            int orientation = ChunkFlag[chunkNum];
            int x = ChunkXpos[chunkNum] + 160 + xoffs;
            int y = ChunkYpos[chunkNum] + 100 + yoffs;

            if ((orientation & 3) != 0) {
                if ((orientation & 1) != 0) {
                    ang = 1024;
                }
                if (orientation != 3) {
                    stat |= 4;
                }
            }

            if ((flags & 4) != 0) {
                shade -= 100;
            }

            if (nPlayerInvisible[nLocalPlayer] != 0) {
                stat |= 33;
            }

            renderer.rotatesprite(x << 16, y << 16, 65536, ang, ChunkPict[chunkNum++], shade, pal, stat);
        }
    }

    public static void PlotArrowSequence(TSprite tsp, int baseSeq, int frameOffs) {
        int ang = ((tsp.getAng() + 512
                - engine.GetMyAngle(nCamerax - tsp.getX(), nCameray - tsp.getY()) + 128) & 0x7FF) >> 8;
        int seq = SeqBase[baseSeq + ang] + frameOffs;
        if (seq == -1) {
            return;
        }
        int frm = FrameBase[seq];

        int cstat = tsp.getCstat() | 0x80;
        byte shade = tsp.getShade();

        if ((ang & 3) != 0) {
            cstat |= 0x18;
        } else {
            cstat &= ~0x18;
        }
        if ((FrameFlag[seq] & 4) != 0) {
            shade -= 100;
        }

        tsp.setCstat( cstat);
        tsp.setShade(shade);
        tsp.setStatnum(FrameSize[seq]);
        if ((ChunkFlag[frm] & 1) != 0) {
            tsp.setXoffset(ChunkXpos[frm]);
            tsp.setCstat(tsp.getCstat() | 4);
        } else {
            tsp.setXoffset( -ChunkXpos[frm]);
        }

        tsp.setYoffset( -ChunkYpos[frm]);
        tsp.setPicnum(ChunkPict[frm]);
    }

    public static void PlotSequence(Sprite pTSprite, int a2, int a3, int a4) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        int dang = engine.GetMyAngle(nCamerax - pTSprite.getX(), nCameray - pTSprite.getY());
        int nShade = pTSprite.getShade();
        if ((SeqBase[a2] + a3) == -1) {
            return;
        }
        if ((FrameFlag[SeqBase[a2] + a3] & 4) != 0) {
            nShade -= 100;
        }

        int v8 = ((a4 & 1) == 0) ? ((((pTSprite.getAng()) - dang + 128) & 0x7FF) >> 8) : 0;
        int seq = (SeqBase[v8 + a2] + a3);
        int frm = FrameBase[seq];
        int frmSize = FrameSize[seq];
        int nStatnum = 100;
        if ((a4 & 0x100) != 0) {
            nStatnum = -3;
        }

        int nOwner = pTSprite.getOwner();
        int nTile = ChunkPict[frm];
        int xoffs, yoffs;
        for (int i = frmSize; i > 0; i--) {
            TSprite tsp = renderedSpriteList.obtain();
            xoffs = mulscale(frmSize - i, EngineUtils.sin((dang + 512) & 0x7FF), 10);
            yoffs = mulscale(frmSize - i, EngineUtils.sin(dang & 0x7FF), 10);

            tsp.setX(pTSprite.getX() + xoffs);
            tsp.setY(pTSprite.getY() + yoffs);
            tsp.setZ(pTSprite.getZ());
            tsp.setShade((byte) nShade);
            tsp.setPal(pTSprite.getPal());
            tsp.setXrepeat(pTSprite.getXrepeat());
            tsp.setYrepeat(pTSprite.getYrepeat());
            tsp.setAng( (ChunkPict[frm] == 736 ? dang : pTSprite.getAng()));
            tsp.setOwner(pTSprite.getOwner());
            tsp.update(pTSprite.getX() + xoffs, pTSprite.getY() + yoffs, pTSprite.getZ(), pTSprite.getSectnum());
            tsp.setCstat( (pTSprite.getCstat() | 0x80));
            tsp.setStatnum( (i + nStatnum + 1));
            if ((ChunkFlag[frm] & 1) != 0) {
                tsp.setXoffset(ChunkXpos[frm]);
                tsp.setCstat(tsp.getCstat() | 4);
            } else {
                tsp.setXoffset( -ChunkXpos[frm]);
            }
            tsp.setYoffset( -ChunkYpos[frm]);
            tsp.setPicnum(ChunkPict[frm]);
            frm++;
        }

        Sprite pOwner = boardService.getSprite(nOwner);
        if ((pTSprite.getCstat() & 0x101) != 0 && ((pOwner != null && pOwner.getStatnum() != 100) || nNetPlayerCount == 0)) {
            Sector sec = boardService.getSector(pTSprite.getSectnum());
            if (sec == null) {
                pTSprite.setOwner(-1);
                return;
            }

            int fz = sec.getFloorz();
            if (fz > initz + PlayerList[nLocalPlayer].eyelevel) {
                if (!cfg.bNewShadows) {
                    int siz =  Math.max(((32 * engine.getTile(nTile).getWidth() / nShadowWidth) - ((fz - pTSprite.getZ()) >> 10)), 1);
                    pTSprite.setPicnum( nShadowPic);
                    pTSprite.setCstat(2 | 32);
                    pTSprite.setXrepeat(siz);
                    pTSprite.setYrepeat(siz);
                } else {
                    pTSprite.setX(pTSprite.getX() - mulscale(EngineUtils.sin((dang + 512) & 2047), 100, 16));
                    pTSprite.setY(pTSprite.getY() + mulscale(EngineUtils.sin((dang + 1024) & 2047), 100, 16));

                    int siz =  Math.max((48 - ((fz - pTSprite.getZ()) >> 10)), 1);
                    pTSprite.setPicnum( nTile);
                    pTSprite.setXrepeat(siz);
                    pTSprite.setYrepeat( (pTSprite.getYrepeat() >> 3));
                    if (pTSprite.getYrepeat() < 4) {
                        pTSprite.setYrepeat(4);
                    }

                    pTSprite.setShade(127);
                    pTSprite.setCstat(pTSprite.getCstat() | 2);
                }

                pTSprite.setZ(fz + 1);
                pTSprite.setStatnum(-3);
                pTSprite.setPal(0);
                return;
            }
        }

        pTSprite.setOwner(-1);
    }

    public static void StartDeathSeq(int player, int seq) {
        FreeRa(player);
        PlayerList[player].HealthAmount = 0;
        int nSprite = PlayerList[player].spriteId;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        Sector sec = boardService.getSector(pSprite.getSectnum());
        if (sec != null) {
            int lotag = sec.getLotag();
            if (lotag > 0) {
                SignalRun((lotag - 1), nEvent7 | player);
            }
        }

        if (nPlayerGrenade[player] < 0) {
            if (nNetPlayerCount != 0) {
                if (PlayerList[player].currentWeapon > 0 && PlayerList[player].currentWeapon <= 6) {
                    int sect = pSprite.getSectnum();
                    if (SectBelow[sect] > -1) {
                        sect = SectBelow[sect];
                    }

                    int spr = GrabBodyGunSprite();
                    Sprite sprite = boardService.getSprite(spr);
                    Sector sec2 = boardService.getSector(sect);
                    if (sprite != null && sec2 != null) {
                        engine.changespritesect(spr, sect);
                        sprite.setX(pSprite.getX());
                        sprite.setY(pSprite.getY());
                        sprite.setZ(sec2.getFloorz() - 512);
                        engine.changespritestat(spr, (nActionEyeLevel[PlayerList[player].currentWeapon + 20] + 900));
                        sprite.setPicnum(nGunPicnum[PlayerList[player].currentWeapon - 1]);
                        BuildItemAnim(spr);
                    }
                }
            }
        } else {
            ThrowGrenade(player, 0, 0, 0, -10000);
        }

        StopFiringWeapon(player);
        PlayerList[player].horiz = 92;
        PlayerList[player].eyelevel = -14080;
        nPlayerInvisible[player] = 0;
        dVertPan[player] = 15;
        pSprite.setCstat(pSprite.getCstat() & ~0x8101);
        SetNewWeaponImmediate(player, -2);
        if (SectDamage[pSprite.getSectnum()] <= 0) {
            nDeathType[player] =  seq;
        } else {
            nDeathType[player] = 2;
        }

        if (seq != 0 || (SectFlag[pSprite.getSectnum()] & 0x2000) == 0) {
            PlayerList[player].anim_ =  (2 * seq + 17);
        } else {
            PlayerList[player].anim_ = 16;
        }
        PlayerList[player].animCount = 0;

        if (numplayers == 1) {
            if (nPlayerLives[player] > 0) {
                BuildStatusAnim(3 * (nPlayerLives[player] - 1) + 7, 0);
            }
            if (levelnum > 0 || mUserFlag == UserFlag.UserMap) {
                nPlayerLives[player]--;
            }

            if (nPlayerLives[player] < 0) {
                nPlayerLives[player] = 0;
            }
        }
        totalvel[player] = 0;
    }

    public static void DrawPilotLightSeq(int x, int y) {
        Renderer renderer = game.getRenderer();
        if ((SectFlag[nPlayerViewSect[nLocalPlayer]] & 0x2000) == 0) {
            int v3 = nPilotLightBase + nPilotLightFrame;
            int seq = FrameBase[v3];
            int count = FrameSize[v3];
            while (--count >= 0) {
                renderer.rotatesprite((x + 160 + ChunkXpos[seq]) << 16, (y + 100 + ChunkYpos[seq]) << 16, 65536,
                        -8 * (int) sPlayerInput[nLocalPlayer].avel, ChunkPict[seq], -127, 1, 2 | 8);
                seq++;
            }
        }
    }

    public static boolean ReadSequence(String name) {
        int sequence = 0;
        Entry entry = game.getCache().getEntry(name, true);
        if (!entry.exists()) {
            System.err.println("Unable to open " + name);
            return false;
        }

        try (InputStream is = entry.getInputStream()) {
            String sign = StreamUtils.readString(is, 2); // DS or IH

            centerx = StreamUtils.readShort(is);
            centery = StreamUtils.readShort(is);
            int nSeqs = StreamUtils.readShort(is);

            if (nSeqs <= 0 || nSeqs + sequences >= SEQMAX) {
                System.err.println("Not enough sequences available!  Increase array");
                return false;
            }

            for (int i = 0; i < nSeqs; i++) {
                SeqBase[i + sequences] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nSeqs; i++) {
                SeqSize[i + sequences] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nSeqs; i++) {
                SeqFlag[i + sequences] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nSeqs; i++) {
                SeqBase[i + sequences] += frames;
            }
            int oldSeqFrameOffset = frames;

            int nFrames = StreamUtils.readShort(is); // 778
            if (nFrames <= 0 || frames + nFrames >= FRAMEMAX) {
                System.err.println("Not enough frames available!  Increase FRAMEMAX");
                return false;
            }

            for (int i = 0; i < nFrames; i++) {
                FrameBase[i + frames] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nFrames; i++) {
                FrameSize[i + frames] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nFrames; i++) {
                FrameFlag[i + frames] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nFrames; i++) {
                FrameSound[i + frames] = -1;
            }

            for (int i = 0; i < nFrames; i++) {
                FrameBase[i + frames] += chunks;
            }

            int nChunks = StreamUtils.readShort(is); // 1043
            if (nChunks < 0 || chunks + nChunks >= CHUNKMAX) {
                System.err.println("Not enough chunks available!  Increase CHUNKMAX");
                return false;
            }

            for (int i = 0; i < nChunks; i++) {
                ChunkXpos[i + chunks] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nChunks; i++) {
                ChunkYpos[i + chunks] =  StreamUtils.readShort(is);
            }
            for (int i = 0; i < nChunks; i++) {
                ChunkPict[i + chunks] =  StreamUtils.readShort(is);
            }

            for (int i = 0; i < nChunks; i++) {
                if ((engine.getTile(ChunkPict[i + chunks]).getType() != AnimType.NONE)) {
                    System.err.println(
                            "sequence " + sequence + " tile " + ChunkPict[i + chunks] + " has Ken animation attached!");
                }
            }

            for (int i = 0; i < nChunks; i++) {
                ChunkFlag[i + chunks] =  StreamUtils.readShort(is);
            }

            for (int i = 0; i < nChunks; i++) {
                ChunkXpos[i + chunks] -= centerx;
                ChunkYpos[i + chunks] -= centery;
            }

            sequences += nSeqs;
            frames += nFrames;
            chunks += nChunks;
            SeqBase[sequences] =  frames;
            FrameBase[frames] =  chunks;

            if (sign.equals("DS")) {
                short numSounds =  StreamUtils.readShort(is);

                String[] sounds = new String[20];
                byte[] data = new byte[8];

                for (int i = 0; i < numSounds; i++) {
                    StreamUtils.readBytes(is, data);
                    sounds[i] = new String(data).trim();
                }

                int v52 = StreamUtils.readShort(is);
                for (int i = 0; i < v52; i++) {
                    int index = StreamUtils.readShort(is);
                    short v46 =  StreamUtils.readShort(is);

                    FrameSound[index + oldSeqFrameOffset] =  ((v46 & 0xFE00) | LoadSound(sounds[v46 & 0x1FF]));
                }
            }

        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static int GetFrameFlag(int a1, int a2) {
        return FrameFlag[a2 + SeqBase[a1]];
    }

    public static void MoveSequence(int nSprite, int a2, int a3) {
        int v4 = GetFrameSound(a2, a3);
        if (v4 != -1) {
            if (nSprite <= -1) {
                PlayLocalSound(v4, 0);
            } else {
                D3PlayFX(v4, nSprite);
            }
        }
    }

}
