// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BubbleMachineStruct;
import ru.m210projects.Powerslave.Type.BubbleStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Anim.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.BuildSpark;
import static ru.m210projects.Powerslave.Player.GetPlayerFromSprite;
import static ru.m210projects.Powerslave.Player.SetPlayerItem;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.Random.RandomWord;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.PSSector.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.*;


public class Sprites {

    public static final int[] nBodySprite = new int[50];
    public static int nCurBodyNum, nBodyTotal;

    public static final int[] nChunkSprite = new int[75];
    public static final int[] nBodyGunSprite = new int[50];
    public static int nCurChunkNum, nCurBodyGunNum, nChunkTotal;
    public static int word_96760;
    private static final int[] wheresMyMouth = new int[4];

    public static void InitChunks() {
        nCurChunkNum = 0;
        Arrays.fill(nChunkSprite,  -1);
        Arrays.fill(nBodyGunSprite,  -1);
        Arrays.fill(nBodySprite,  -1);
        nCurBodyNum = 0;
        nCurBodyGunNum = 0;
        nBodyTotal = 0;
        nChunkTotal = 0;
    }

    public static int GrabBody() {
        int nSprite;
        Sprite spr;
        do {
            nSprite = nBodySprite[nCurBodyNum];
            spr = boardService.getSprite(nSprite);
            if (spr == null) {
                nSprite = engine.insertsprite(0, 899);
                nBodySprite[nCurBodyNum] = nSprite;
                spr = boardService.getSprite(nSprite);
                if (spr == null) {
                    return -1;
                }

                spr.setCstat(32768);
            }

            if (++nCurBodyNum >= 50) {
                nCurBodyNum = 0;
            }
        } while ((spr.getCstat() & (1 | 256)) != 0);

        if (nBodyTotal < 50) {
            ++nBodyTotal;
        }

        spr.setCstat(0);
        return nSprite;
    }

    public static boolean GrabItem(int nPlayer, int nItem) {
        if (PlayerList[nPlayer].ItemsAmount[nItem] < 5) {
            PlayerList[nPlayer].ItemsAmount[nItem]++;
            if (nPlayerItem[nPlayer] < 0 || nItem == nPlayerItem[nPlayer]) {
                SetPlayerItem(nPlayer, nItem);
            }
            return true;
        }
        return false;
    }

    public static int CheckRadialDamage(int a1) {
        int v13 = 0;
        if (a1 != nRadialSpr) {
            Sprite v2 = boardService.getSprite(a1);
            if (v2 != null && (v2.getCstat() & 0x101) != 0) {
                int v3 = v2.getStatnum();
                Sprite v4 = boardService.getSprite(nRadialSpr);
                if (v4 != null && v3 < 1024 && v4.getStatnum() < 1024 && (v3 == 100 || a1 != nRadialOwner)) {
                    int v5 = (v2.getX() - v4.getX()) >> 8;
                    int v18 = (v2.getY() - v4.getY()) >> 8;
                    int v8 = (v2.getZ() - v4.getZ()) >> 12;
                    if (klabs(v5) <= nDamageRadius && klabs(v18) <= nDamageRadius && klabs(v8) <= nDamageRadius) {
                        int v19 = EngineUtils.sqrt(v5 * v5 + v18 * v18);
                        if (v19 < nDamageRadius) {
                            short oldcstat = v2.getCstat();
                            v2.setCstat(257);
                            if ((152 - v2.getStatnum()) <= 1 || engine.cansee(v4.getX(), v4.getY(), v4.getZ() - 512, v4.getSectnum(), v2.getX(), v2.getY(), v2.getZ() - 0x2000, v2.getSectnum())) {
                                v13 = nRadialDamage * (nDamageRadius - v19) / nDamageRadius;
                                if (v13 > 20) {
                                    int ang = engine.GetMyAngle(v5, v18);
                                    v2.setXvel(v2.getXvel() + (v13 * EngineUtils.sin((ang + 512) & 0x7FF) >> 3));
                                    v2.setYvel(v2.getYvel() + (v13 * EngineUtils.sin(ang & 0x7FF) >> 3));
                                    v2.setZvel( BClipLow(v2.getZvel() - 24 * v13, -3584));
                                }
                            }
                            v2.setCstat(oldcstat);
                        } // else v13 = 32767;
                    }
                }
            }
        }

        return v13;
    }

    public static void DestroyItemAnim(int nItem) {
        Sprite pItem = boardService.getSprite(nItem);
        if (pItem == null || pItem.getOwner() < 0 || pItem.getOwner() >= MAX_LIL_ANIM) {
            return;
        }

        DestroyAnim(pItem.getOwner());
    }

    public static void ExplodeScreen(int a1) {
        Sprite spr = boardService.getSprite(a1);
        if (spr == null) {
            return;
        }

        spr.setZ(spr.getZ() - GetSpriteHeight(a1) / 2);
        for (int i = 0; i < 30; i++) {
            BuildSpark(a1, 0);
        }

        spr.setCstat( 32768);
        PlayFX2(StaticSound[78], a1);
    }

    public static int GrabChunkSprite() {
        int nSprite = nChunkSprite[nCurChunkNum];
        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            nSprite = engine.insertsprite(0, 899);
            spr = boardService.getSprite(nSprite);
            nChunkSprite[nCurChunkNum] = nSprite;
        } else if (spr.getStatnum() != 0) {
            System.err.println("too many chunks being used at once!");
            return -1;
        }

        engine.changespritestat(nSprite,  899);
        if (++nCurChunkNum >= 75) {
            nCurChunkNum = 0;
        }
        if (nChunkTotal < 75) {
            ++nChunkTotal;
        }

        if (spr != null) {
            spr.setCstat(128);
        }
        return nSprite;
    }

    public static int BuildCreatureChunk(int nSprite, int a1) {
        int spr = GrabChunkSprite();
        Sprite v6 = boardService.getSprite(spr);
        boolean v15 = false;
        if (v6 != null) {
            if ((nSprite & PS_HIT_FLAG) != 0) {
                nSprite &= ~PS_HIT_FLAG;
                v15 = true;
            }

            Sprite v7 = boardService.getSprite(nSprite);
            if (v7 == null) {
                return -1;
            }

            v6.setX(v7.getX());
            v6.setY(v7.getY());
            v6.setZ(v7.getZ());
            engine.mychangespritesect(spr, v7.getSectnum());
            v6.setCstat(128);
            v6.setShade(-12);
            v6.setPal(0);
            v6.setXvel((((RandomSize(5) - 16) & 0xFFFF) << 7));
            v6.setYvel((((RandomSize(5) - 16) & 0xFFFF) << 7));
            v6.setZvel((-8 * (RandomSize(8) + 512)));

            if (v15) {
                v6.setXvel(v6.getXvel() * 4);
                v6.setYvel(v6.getYvel() * 4);
                v6.setZvel(v6.getZvel() * 2);
            }
            v6.setXrepeat(64);
            v6.setYrepeat(64);
            v6.setXoffset(0);
            v6.setYoffset(0);
            v6.setPicnum( a1);
            v6.setLotag( (HeadRun() + 1));
            v6.setClipdist(40);
            v6.setExtra(-1);
            v6.setOwner( AddRunRec(v6.getLotag() - 1, nEvent13 | spr));
            v6.setHitag( AddRunRec(NewRun, nEvent13 | spr));

        }
        return spr;
    }

    public static void FuncCreatureChunk(int nStack, int ignored, int RunPtr) {
        final int spr = RunData[RunPtr].getObject();
        Sprite sprite = boardService.getSprite(spr);
        if (sprite == null) {
            throw new AssertException("spr>=0 && spr<MAXSPRITES");
        }

        if ((nStack & EVENT_MASK) == nEventProcess) {
            ru.m210projects.Build.Types.Sector sec = boardService.getSector(sprite.getSectnum());
            if (sec != null) {
                Gravity(spr);
                sprite.setPal(sec.getCeilingpal());
                int hitMove = engine.movesprite(spr, sprite.getXvel() << 10, sprite.getYvel() << 10, sprite.getZvel(), 2560, -2560, 1);

                if (sprite.getZ() < sec.getFloorz()) {
                    if (hitMove == 0) {
                        return;
                    }

                    if ((hitMove & nEventProcess) == 0) {
                        int ang;
                        switch (hitMove & 0x3C000) {
                            default:
                                return;
                            case PS_HIT_WALL:
                                ang = engine.GetWallNormal(hitMove & PS_HIT_INDEX_MASK);
                                break;
                            case PS_HIT_SPRITE:
                                Sprite hitSprite = boardService.getSprite(hitMove & PS_HIT_INDEX_MASK);
                                if (hitSprite == null) {
                                    return;
                                }

                                ang = hitSprite.getAng();
                                break;
                            case nEvent1:
                                sprite.setXvel(sprite.getXvel() >> 1);
                                sprite.setYvel(sprite.getYvel() >> 1);
                                sprite.setZvel( -sprite.getZvel());
                                return;
                        }

                        int v20 = EngineUtils.sqrt(((sprite.getYvel() >> 10) * (sprite.getYvel() >> 10) + (sprite.getXvel() >> 10) * (sprite.getXvel() >> 10)) >> 8) >> 1;
                        sprite.setXvel( (v20 * EngineUtils.sin((ang + 512) & 0x7FF)));
                        sprite.setYvel( (v20 * EngineUtils.sin(ang & 0x7FF)));
                        return;
                    } else {
                        sprite.setCstat( 32768);
                    }
                } else {
                    sprite.setXvel(0);
                    sprite.setYvel(0);
                    sprite.setZvel(0);
                    sprite.setZ(sec.getFloorz());
                }
            }

            DoSubRunRec(sprite.getOwner());
            if (sprite.getLotag() > 0) {
                FreeRun(sprite.getLotag() - 1);
            }
            SubRunRec(sprite.getHitag());
            engine.changespritestat(spr,  0);
            sprite.setHitag(0);
            sprite.setLotag(0);
        }
    }

    public static void Gravity(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (boardService.isValidSector(pSprite.getSectnum()) && (SectFlag[pSprite.getSectnum()] & 0x2000) != 0) {
            if (pSprite.getStatnum() == 100) {
                if (pSprite.getZvel() > 0) {
                    pSprite.setZvel(pSprite.getZvel() - 64);
                    if (pSprite.getZvel() < 0) {
                        pSprite.setZvel(0);
                    }
                } else if (pSprite.getZvel() < 0) {
                    pSprite.setZvel(pSprite.getZvel() + 64);
                    if (pSprite.getZvel() > 0) {
                        pSprite.setZvel(0);
                    }
                }
            } else {
                if (pSprite.getZvel() <= 1024) {
                    pSprite.setZvel(pSprite.getZvel() + 512);
                } else {
                    pSprite.setZvel(pSprite.getZvel() - 64);
                }
            }
        } else {
            pSprite.setZvel(pSprite.getZvel() + 512);
            pSprite.setZvel( BClipHigh(pSprite.getZvel(), 0x4000));
        }
    }

    public static void DamageEnemy(int nDest, int nSource, int nDamage) {
        int left = nCreaturesLeft;
        Sprite pDest = boardService.getSprite(nDest);

        if (pDest != null && pDest.getStatnum() < MAXSTATUS && pDest.getOwner() > -1) {
            // FIXME: nSource & 0xFFFF - is very important
            SendMessageToRunRec(pDest.getOwner(), nEventDamage | (nSource & 0xFFFF), 4 * nDamage);
            if (left > nCreaturesLeft) {
                Sprite pSource = boardService.getSprite(nSource);
                if (pSource != null && pSource.getStatnum() == 100) {
                    int plr = GetPlayerFromSprite(nSource);
                    nTauntTimer[plr]--;
                    if (nTauntTimer[plr] <= 0) {
                        if (SectFlag[PlayerList[plr].getSprite().getSectnum() & 0x2000] == 0) {
                            D3PlayFX(StaticSound[RandomSize(3) % 5 + 53], nDoppleSprite[plr] | (plr == nLocalPlayer ? 0x6000 : PS_HIT_FLAG));
                        }
                        nTauntTimer[plr] =  (RandomSize(3) + 3);
                    }
                }
            }
        }
    }

    public static void RadialDamageEnemy(int nSprite, int damage, int radius) {
        if (radius != 0) {
            ++word_96760;
            if (nRadialSpr == -1) {
                nRadialDamage = 4 * damage;
                nDamageRadius = radius;
                nRadialSpr = nSprite;
                Sprite pRadialSpr = boardService.getSprite(nSprite);
                nRadialOwner = (pRadialSpr != null ? pRadialSpr.getOwner() : -1);
                ExplodeSignalRun();
                nRadialSpr = -1;
                word_96760--;
            }
        }
    }

    public static int GetAngleToSprite(Sprite spr1, Sprite spr2) {
        if (spr1 != null && spr2 != null) {
            return engine.GetMyAngle(spr2.getX() - spr1.getX(), spr2.getY() - spr1.getY());
        }

        return -1;
    }

    public static int GetSpriteHeight(int num) {
        Sprite spr = boardService.getSprite(num);
        if (spr == null) {
            return 0;
        }
        return 4 * spr.getYrepeat() * engine.getTile(spr.getPicnum()).getHeight();
    }

    public static void BuildSplash(Sprite pSprite, int sectnum) {
        if (pSprite == null) {
            return;
        }

        int v5, v6, v9, v10;
        if (pSprite.getStatnum() == 200) {
            v5 = 20;
            v6 = 1;
        } else {
            v5 = (RandomWord() % pSprite.getXrepeat()) + pSprite.getXrepeat();
            v6 = 0;
        }

        if ((SectFlag[sectnum] & PS_HIT_FLAG) != 0) {
            v9 = 43;
            v10 = 4;
        } else {
            v9 = 35;
            v10 = 0;
        }

        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sectnum);
        if (sec != null) {
            int v11 = AnimList[BuildAnim(-1, v9, 0, pSprite.getX(), pSprite.getY(), sec.getFloorz(), sectnum, v5, v10)].nSprite;
            if ((SectFlag[sectnum] & PS_HIT_FLAG) == 0) {
                D3PlayFX(StaticSound[v6] | 0xA00, v11);
            }
        }
    }

    public static int AngleChase(int nSprite, int nTarget, int a3, int a4, int a5) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return 0;
        }

        int nNewAngle = pSprite.getAng();
        Sprite pTarget = boardService.getSprite(nTarget);
        if (pTarget != null) {
            int zTop = 2 * pTarget.getYrepeat() * engine.getTile(pTarget.getPicnum()).getHeight();

            int dx = pTarget.getX() - pSprite.getX();
            int dy = pTarget.getY() - pSprite.getY();
            int dz = pTarget.getZ() - pSprite.getZ();

            int nGoalAngle = AngleDelta(pSprite.getAng(), engine.GetMyAngle(dx, dy), 1024);
            if (klabs(nGoalAngle) > 63) {
                a3 /= klabs(nGoalAngle >> 6);
                if (a3 < 5) {
                    a3 = 5;
                }
            }
            if (klabs(nGoalAngle) > a5) {
                if (nGoalAngle >= 0) {
                    nGoalAngle = a5;
                } else {
                    nGoalAngle = -a5;
                }
            }

            nNewAngle =  ((pSprite.getAng() + nGoalAngle) & 0x7FF);
            pSprite.setZvel( (pSprite.getZvel() + (AngleDelta(pSprite.getZvel(), engine.GetMyAngle(EngineUtils.sqrt(dx * dx + dy * dy), (dz - zTop) >> 8), 24)) & 0x7FF));
        } else {
            pSprite.setZvel(0);
        }
        pSprite.setAng(nNewAngle);

        int v28 = klabs(EngineUtils.sin((pSprite.getZvel() + 512) & 0x7FF));
        int xvel = v28 * (a3 * EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) >> 14);
        int yvel = v28 * (a3 * EngineUtils.sin(pSprite.getAng() & 0x7FF) >> 14);
        int v31 = EngineUtils.sqrt((xvel >> 8) * (xvel >> 8) + (yvel >> 8) * (yvel >> 8));

        return engine.movesprite( nSprite, xvel >> 2, yvel >> 2, (EngineUtils.sin(a4 & 0x7FF) >> 5) + (v31 * EngineUtils.sin(pSprite.getZvel() & 0x7FF) >> 13), 0, 0, pSprite.getStatnum() != 107 ? 1 : 0);
    }

    public static int AngleDelta(int ang1, int ang2, int a3) {
        int dang = ang2 - ang1;
        if (dang >= 0) {
            if (dang > 1024) {
                dang = -(2048 - dang);
            }
        } else if (dang < -1024) {
            dang += 2048;
        }

        if (klabs(dang) > a3) {
            return dang < 0 ? -a3 : a3;
        }
        return dang;
    }

    public static int AngleDiff(int a1, int a2) {
        int result = (a2 - a1) & 0x7FF;
        if (result > 1024) {
            result = 2048 - result;
        }
        return result;
    }

    public static int BelowNear(Sprite pSprite, int lohit) {
        int nSector = pSprite.getSectnum();
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return 0;
        }

        int z = 0;
        int v10;
        if ((lohit & PS_HIT_TYPE_MASK) == PS_HIT_SPRITE) {
            v10 = PS_HIT_SPRITE;
            Sprite pHit = boardService.getSprite(lohit & PS_HIT_INDEX_MASK);
            if (pHit != null) {
                z = pHit.getZ();
            }
        } else {
            v10 = nEventProcess;
            z = (SectDepth[pSprite.getSectnum()] & 0xFFFF) + sec.getFloorz();
            if (NearCount > 0) {
                int a2 = NearSector[0];
                for (int i = 0; i < NearCount; ++i) {
                    for (int j = NearSector[i]; j >= 0; j = SectBelow[j]) {
                        a2 = j;
                    }

                    ru.m210projects.Build.Types.Sector s = boardService.getSector(a2);
                    if (s != null) {
                        int v7 = (SectDepth[a2] & 0xFFFF) + s.getFloorz();
                        int v8 = v7 - pSprite.getZ();
                        if (v8 < 0 && v8 >= -5120) {
                            z = v7;
                            nSector = a2;
                        }
                    }
                }
            }
        }

        if (z >= pSprite.getZ()) {
            v10 = 0;
        } else {
            pSprite.setZ(z);
            overridesect = nSector;
            pSprite.setZvel(0);
            bTouchFloor[0] = true;
        }

        return v10;
    }

    public static void BuildNear(int x, int y, int walldist, short sectnum) {
        NearSector[0] = sectnum;
        int j = 0;
        NearCount = 1;
        while (j < NearCount) {
            ru.m210projects.Build.Types.Sector s = boardService.getSector(NearSector[j]);
            if (s != null) {
                int nWall = s.getWallptr();
                int nWalls = s.getWallnum();
                while (--nWalls >= 0) {
                    Wall wall = boardService.getWall(nWall);
                    if (wall != null) {
                        int v8 = wall.getNextsector();
                        if (v8 >= 0) {
                            int i;
                            for (i = 0; i < NearCount; ++i) {
                                if (v8 == NearSector[i]) {
                                    break;
                                }
                            }

                            if (i >= NearCount && engine.clipinsidebox(x, y, nWall, walldist) != 0) {
                                NearSector[NearCount] = v8;
                                NearCount++;
                            }
                        }
                    }
                    ++nWall;
                }
            }
            ++j;
        }
    }

    public static void InitBubbles() {
        nMachineCount = 0;
        for (int i = 0; i < 200; i++) {
            nBubblesFree[i] = (byte) i;
            if (BubbleList[i] != null) {
                BubbleList[i].nSprite = -1;
            }
        }
        nFreeCount = 200;
    }

    public static void saveBubbles(OutputStream os) throws IOException {
        int nBubbles = 0;
        for (int i = 0; i < 200; i++) {
            if (BubbleList[i] != null && BubbleList[i].nSprite != -1) {
                nBubbles++;
            }
        }

        StreamUtils.writeShort(os,  nMachineCount);
        StreamUtils.writeShort(os,  nFreeCount);

        for (int i = 0; i < 200; i++) {
            StreamUtils.writeByte(os, nBubblesFree[i]);
        }

        if (nBubbles != 0) {
            for (int i = 0; i < 200; i++) {
                if (BubbleList[i] != null && BubbleList[i].nSprite != -1) {
                    StreamUtils.writeShort(os,  i);
                    BubbleList[i].save(os);
                }
            }
        }

        for (int i = 0; i < nMachineCount; i++) {
            Machine[i].save(os);
        }
    }

    public static void loadBubbles(SafeLoader loader) {
        nMachineCount = loader.nMachineCount;
        nFreeCount = loader.nFreeCount;

        System.arraycopy(loader.nBubblesFree, 0, nBubblesFree, 0, nBubblesFree.length);

        for (int i = 0; i < 200; i++) {
            if (BubbleList[i] != null) {
                BubbleList[i].nSprite = -1;
            }

            if (loader.BubbleList[i] != null && loader.BubbleList[i].nSprite != -1) {
                if (BubbleList[i] == null) {
                    BubbleList[i] = new BubbleStruct();
                }
                BubbleList[i].copy(loader.BubbleList[i]);
            }
        }

        for (int i = 0; i < loader.nMachineCount; i++) {
            if (Machine[i] == null) {
                Machine[i] = new BubbleMachineStruct();
            }
            Machine[i].copy(loader.Machine[i]);
        }
    }

    public static void loadBubbles(SafeLoader loader, InputStream is) throws IOException {
        for (int i = 0; i < 200; i++) {
            if (loader.BubbleList[i] != null) {
                loader.BubbleList[i].nSprite = -1;
            }
        }

        loader.nMachineCount = StreamUtils.readShort(is);
        loader.nFreeCount = StreamUtils.readShort(is);
        StreamUtils.readBytes(is, loader.nBubblesFree);

        int nBubbles = 200 - loader.nFreeCount;
        while (nBubbles > 0) {
            short i =  StreamUtils.readShort(is);
            if (loader.BubbleList[i] == null) {
                loader.BubbleList[i] = new BubbleStruct();
            }
            loader.BubbleList[i].load(is);
            nBubbles--;
        }

        for (int i = 0; i < loader.nMachineCount; i++) {
            if (loader.Machine[i] == null) {
                loader.Machine[i] = new BubbleMachineStruct();
            }
            loader.Machine[i].load(is);
        }
    }

    public static void DoBubbleMachines() {
        for (int i = 0; i < nMachineCount; i++) {
            if (--Machine[i].field_0 <= 0) {
                Sprite spr = boardService.getSprite(Machine[i].field_2);
                Machine[i].field_0 =  ((RandomWord() % Machine[i].field_4) + 30);
                if (spr != null) {
                    BuildBubble(spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());
                }
            }
        }
    }

    private static BubbleStruct BuildBubble(int x, int y, int z, int sectnum) {
        int v6 = RandomSize(3);
        if (v6 > 4) {
            v6 -= 4;
        }
        if (nFreeCount > 0) {
            int nBubble = nBubblesFree[--nFreeCount] & 0xFF;
            int spr = engine.insertsprite( sectnum,  402);
            Sprite pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                throw new AssertException("spr>=0 && spr<MAXSPRITES");
            }

            pSprite.setX(x);
            pSprite.setY(y);
            pSprite.setZ(z);
            pSprite.setCstat(0);
            pSprite.setShade(-32);
            pSprite.setPal(0);
            pSprite.setClipdist(5);
            pSprite.setXrepeat(40);
            pSprite.setYrepeat(40);
            pSprite.setXoffset(0);
            pSprite.setYoffset(0);
            pSprite.setPicnum(1);
            pSprite.setAng( inita);
            pSprite.setXvel(0);
            pSprite.setYvel(0);
            pSprite.setZvel(-1200);
            pSprite.setLotag( (HeadRun() + 1));
            pSprite.setHitag(0);
            pSprite.setExtra(0);

            if (BubbleList[nBubble] == null) {
                BubbleList[nBubble] = new BubbleStruct();
            }

            BubbleList[nBubble].nSprite =  spr;
            BubbleList[nBubble].field_0 = 0;
            BubbleList[nBubble].nSeq =  (v6 + SeqOffsets[15]);

            pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEvent20 | nBubble));
            BubbleList[nBubble].field_6 =  AddRunRec(NewRun, nEvent20 | nBubble);
            return BubbleList[nBubble];
        }

        return null;
    }

    public static void FuncBubble(int nStack, int ignored, int RunPtr) {
        int nBubble = RunData[RunPtr].getObject();
        if (nBubble < 0 || nBubble >= 200) {
            throw new AssertException("Bubble>=0 && Bubble<MAX_BUBBLES");
        }

        int nSprite = BubbleList[nBubble].nSprite;
        Sprite sprite = boardService.getSprite(nSprite);
        if (sprite == null) {
            return;
        }

        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sprite.getSectnum());
        if (sec == null) {
            return;
        }

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                MoveSequence(nSprite, BubbleList[nBubble].nSeq, BubbleList[nBubble].field_0);
                if (++BubbleList[nBubble].field_0 >= SeqSize[BubbleList[nBubble].nSeq]) {
                    BubbleList[nBubble].field_0 = 0;
                }

                sprite.setZ(sprite.getZ() + sprite.getZvel());
                game.pInt.setsprinterpolate(nSprite, sprite);
                if (sprite.getZ() <= sec.getCeilingz()) {
                    int v13 = SectAbove[sprite.getSectnum()];
                    ru.m210projects.Build.Types.Sector sec13 = boardService.getSector(v13);
                    if (sprite.getHitag() > -1 && sec13 != null) {
                        BuildAnim(-1, 70, 0, sprite.getX(), sprite.getY(), sec13.getFloorz(), v13, 0x40, 0);
                    }
                    DestroyBubble(nBubble);
                }
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, BubbleList[nBubble].nSeq, BubbleList[nBubble].field_0, 1);
                tsp.setOwner(-1);
                break;
        }
    }

    public static void DestroyBubble(int a1) {
        int nSprite = BubbleList[a1].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        DoSubRunRec(pSprite.getLotag() - 1);
        DoSubRunRec(pSprite.getOwner());
        SubRunRec(BubbleList[a1].field_6);
        engine.mydeletesprite(nSprite);
        BubbleList[a1].nSprite = -1;
        nBubblesFree[nFreeCount] = (byte) a1;
        nFreeCount++;
    }

    public static int GetBubbleSprite(BubbleStruct pBubble) {
        if (pBubble != null) {
            return pBubble.nSprite;
        }
        return -1;
    }

    public static void DoBubbles(int a1) {
        int[] out = WheresMyMouth(a1);
        if (out[3] != -1) {
            Sprite sprite = boardService.getSprite(GetBubbleSprite(BuildBubble(out[0], out[1], out[2], out[3])));
            if (sprite != null) {
                sprite.setHitag( a1);
            }
        }
    }

    public static int[] WheresMyMouth(int plr) {
        int nSprite = PlayerList[plr].spriteId;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            engine.clipmove(pSprite.getX(),
                    pSprite.getY(),
                    pSprite.getZ() - GetSpriteHeight(nSprite) >> 1,
                    pSprite.getSectnum(),
                    (long) EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF) << 7,
                    (long) EngineUtils.sin(pSprite.getAng() & 0x7FF) << 7,
                    5120,
                    1280,
                    1280,
                    CLIPMASK1);

            wheresMyMouth[0] = clipmove_x;
            wheresMyMouth[1] = clipmove_y;
            wheresMyMouth[2] = clipmove_z;
            wheresMyMouth[3] = clipmove_sectnum;
        }

        return wheresMyMouth;
    }

}
