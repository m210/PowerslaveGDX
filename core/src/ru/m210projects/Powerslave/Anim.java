// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.AnimStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.StopSpriteSound;
import static ru.m210projects.Powerslave.Sprites.DamageEnemy;

public class Anim {

    public static final int MAX_LIL_ANIM = 400;

    public static int nAnimsFree;
    public static final byte[] AnimFlags = new byte[MAX_LIL_ANIM];
    public static final AnimStruct[] AnimList = new AnimStruct[MAX_LIL_ANIM];
    private static final int[] AnimsFree = new int[MAX_LIL_ANIM];
    private static final int[] AnimRunRec = new int[MAX_LIL_ANIM];

    public static void InitAnims() {

        for (short i = 0; i < MAX_LIL_ANIM; i++) {
            AnimsFree[i] = i;
            if (AnimList[i] != null) {
                AnimList[i].nSprite = -1;
            }
        }

        nAnimsFree = MAX_LIL_ANIM;
        nMagicSeq = SeqOffsets[41] + 21;
        nPreMagicSeq = SeqOffsets[64];
        nSavePointSeq = SeqOffsets[41] + 12;
    }

    public static void saveAnm(OutputStream os) throws IOException {
        int nAnims = 0;
        for (int i = 0; i < MAX_LIL_ANIM; i++) {
            if (AnimList[i] != null && AnimList[i].nSprite != -1) {
                nAnims++;
            }
        }

        StreamUtils.writeShort(os,  nAnimsFree);

        for (int i = 0; i < MAX_LIL_ANIM; i++) {
            StreamUtils.writeShort(os, AnimsFree[i]);
            StreamUtils.writeShort(os,  AnimRunRec[i]);
            StreamUtils.writeByte(os, AnimFlags[i]);
        }

        if (nAnims != 0) {
            for (int i = 0; i < MAX_LIL_ANIM; i++) {
                if (AnimList[i] != null && AnimList[i].nSprite != -1) {
                    StreamUtils.writeShort(os,  i);
                    AnimList[i].save(os);
                }
            }
        }
    }

    public static void loadAnm(SafeLoader loader) {
        nAnimsFree = loader.nAnimsFree;

        System.arraycopy(loader.AnimsFree, 0, AnimsFree, 0, MAX_LIL_ANIM);
        System.arraycopy(loader.AnimRunRec, 0, AnimRunRec, 0, MAX_LIL_ANIM);
        System.arraycopy(loader.AnimFlags, 0, AnimFlags, 0, MAX_LIL_ANIM);

        for (int i = 0; i < MAX_LIL_ANIM; i++) {
            if (AnimList[i] != null) {
                AnimList[i].nSprite = -1;
            }

            if (loader.AnimList[i] != null && loader.AnimList[i].nSprite != -1) {
                if (AnimList[i] == null) {
                    AnimList[i] = new AnimStruct();
                }
                AnimList[i].copy(loader.AnimList[i]);
            }
        }

        nMagicSeq = SeqOffsets[41] + 21;
        nPreMagicSeq = SeqOffsets[64];
        nSavePointSeq = SeqOffsets[41] + 12;
    }

    public static void loadAnm(SafeLoader loader, InputStream is) throws IOException {
        for (int i = 0; i < MAX_LIL_ANIM; i++) {
            if (loader.AnimList[i] != null) {
                loader.AnimList[i].nSprite = -1;
            }
        }

        loader.nAnimsFree = StreamUtils.readShort(is);
        for (int i = 0; i < MAX_LIL_ANIM; i++) {
            loader.AnimsFree[i] =  StreamUtils.readShort(is);
            loader.AnimRunRec[i] = StreamUtils.readShort(is);
            loader.AnimFlags[i] = StreamUtils.readByte(is);
        }

        int nAnims = (MAX_LIL_ANIM - loader.nAnimsFree);
        while (nAnims > 0) {
            short i =  StreamUtils.readShort(is);
            if (loader.AnimList[i] == null) {
                loader.AnimList[i] = new AnimStruct();
            }
            loader.AnimList[i].load(is);
            nAnims--;
        }
    }

    public static int BuildAnim(int spr, int a2, int a3, int x, int y, int z, int sectnum, int size, int flags) {
        if (--nAnimsFree != 0) {
            int nAnim = AnimsFree[nAnimsFree];
            Sprite pSprite = boardService.getSprite(spr);
            if (pSprite == null) {
                spr = engine.insertsprite( sectnum,  500);
                pSprite = boardService.getSprite(spr);
            }

            if (pSprite == null) {
                return -1;
            }

            pSprite.setY(y);
            pSprite.setZ(z);
            pSprite.setX(x);
            pSprite.setCstat(0);
            if ((flags & 4) != 0) {
                pSprite.setPal(4);
                pSprite.setShade(-64);
            } else {
                pSprite.setPal(0);
                pSprite.setShade(-12);
            }
            pSprite.setClipdist(10);
            pSprite.setXrepeat( size);
            pSprite.setYrepeat( size);
            pSprite.setPicnum(1);
            pSprite.setAng(0);
            pSprite.setXoffset(0);
            pSprite.setYoffset(0);
            pSprite.setXvel(0);
            pSprite.setZvel(0);
            pSprite.setYvel(0);
            if (pSprite.getStatnum() < 900) {
                pSprite.setHitag(-1);
            }
            pSprite.setLotag( (HeadRun() + 1));
            pSprite.setOwner(-1);
            pSprite.setExtra( AddRunRec(pSprite.getLotag() - 1, nEvent16 | nAnim));
            AnimRunRec[nAnim] = AddRunRec(NewRun, nEvent16 | nAnim);
            AnimFlags[nAnim] = (byte) flags;
            if ((flags & 0x80) != 0) {
                pSprite.setCstat(pSprite.getCstat() | 2);
            }

            if (AnimList[nAnim] == null) {
                AnimList[nAnim] = new AnimStruct();
            }
            AnimList[nAnim].nAction =  (a3 + SeqOffsets[a2]);
            AnimList[nAnim].nSeq = 0;
            AnimList[nAnim].nSprite =  spr;

            return nAnim;
        }

        return -1;
    }

    public static int GetAnimSprite(int a1) {
        return AnimList[a1].nSprite;
    }

    public static void DestroyAnim(int a1) {
        int nSprite = AnimList[a1].nSprite;
        if (nSprite >= 0) {
            StopSpriteSound(nSprite);
            SubRunRec(AnimRunRec[a1]);
            Sprite pSprite = boardService.getSprite(nSprite);
            if (pSprite == null) {
                return;
            }

            DoSubRunRec(pSprite.getExtra());
            FreeRun(pSprite.getLotag() - 1);
        }

        AnimList[a1].nSprite = -1;
        if (nAnimsFree == MAX_LIL_ANIM) {
            return;
        }
        AnimsFree[nAnimsFree] =  a1;
        nAnimsFree++;
    }

    public static void FuncAnim(int nStack, int ignored, int a3) {
        int nAnim = RunData[a3].getObject();
        if (nAnim < 0 || nAnim >= MAX_LIL_ANIM) {
            throw new AssertException("nAnim>=0 && nAnim<MAX_LIL_ANIM");
        }

        int nSprite = AnimList[nAnim].nSprite;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        int nSeq = AnimList[nAnim].nAction;

        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if ((pSprite.getCstat() & 0x8000) == 0) {
                    MoveSequence(nSprite, nSeq, AnimList[nAnim].nSeq);
                }

                if (pSprite.getStatnum() != 404) {
                    break;
                }

                int nSource = pSprite.getHitag();
                Sprite pSource = boardService.getSprite(nSource);
                if (pSource == null) {
                    break;
                }

                pSprite.setX(pSource.getX());
                pSprite.setY(pSource.getY());
                pSprite.setZ(pSource.getZ());
                int v11 = pSource.getSectnum();
                if (v11 != pSprite.getSectnum()) {
                    if (v11 < 0 || v11 >= 1024) {
                        DestroyAnim(nAnim);
                        engine.mydeletesprite( nSprite);
                        return;
                    }
                    engine.mychangespritesect( nSprite,  v11);
                }
                if (AnimList[nAnim].nSeq != 0) {
                    break;
                }

                int v13 = pSource.getHitag();
                if (pSource.getCstat() != (short) 32768) {
                    pSource.setHitag(pSource.getHitag() - 1);
                }

                if (pSource.getCstat() == (short) 32768 || (v13 < 15)) {
                    pSource.setHitag(1);
                    DestroyAnim(nAnim);
                    engine.mydeletesprite( nSprite);
                    break;
                }

                DamageEnemy(nSource, -1, 2 * (pSource.getHitag() - 14));
                if (pSource.getShade() < 100) {
                    pSource.setPal(0);
                    pSource.setShade(pSource.getShade() + 1);
                }
                if ((pSource.getCstat() & 0x101) != 0) {
                    break;
                }

                DestroyAnim(nAnim);
                engine.mydeletesprite( nSprite);
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, nSeq, AnimList[nAnim].nSeq, 257);
                return;
            default:
                return;
        }

        if (++AnimList[nAnim].nSeq < SeqSize[nSeq]) {
            return;
        }
        if ((AnimFlags[nAnim] & 0x10) != 0) {
            AnimList[nAnim].nSeq = 0;
            return;
        }
        if (nSeq == nPreMagicSeq) {
            AnimList[nAnim].nSeq = 0;
            AnimList[nAnim].nAction =  nMagicSeq;
            AnimFlags[nAnim] |= 0x10;
            Sprite spr = boardService.getSprite(AnimList[nAnim].nSprite);
            if (spr != null) {
                spr.setCstat(spr.getCstat() | 2);
            }
            return;
        }
        if (nSeq == nSavePointSeq) {
            AnimList[nAnim].nSeq =  (nSavePointSeq ^ nSeq);
            AnimList[nAnim].nAction++;
            AnimFlags[nAnim] |= 0x10;
            return;
        }

        DestroyAnim(nAnim);
        engine.mydeletesprite( nSprite);
    }

}
