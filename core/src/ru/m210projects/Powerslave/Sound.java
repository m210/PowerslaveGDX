// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.audio.Music;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.SoundData;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Architecture.common.audio.SourceListener;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Script.CueScript;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Type.SoundResource;
import ru.m210projects.Powerslave.Type.VOCDecoder;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.nElevSound;
import static ru.m210projects.Powerslave.Map.nStoneSound;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Snake.SnakeList;


public class Sound {

    public static final String[] cdTrackList = {"", "track02.ogg", "track03.ogg", "track04.ogg", "track05.ogg",
            "track06.ogg", "track07.ogg", "track08.ogg", "track09.ogg", "track10.ogg", "track11.ogg", "track12.ogg",
            "track13.ogg", "track14.ogg", "track15.ogg", "track16.ogg", "track17.ogg", "track18.ogg", "track19.ogg"};
    private static final List<Entry> cdTrackEntries = new ArrayList<>();
    public static final String[] SoundFiles = {"spl_big", "spl_smal", "bubble_l", "grn_drop", "p_click", "grn_roll",
            "cosprite", "m_chant0", "anu_icu", "item_reg", "item_spe", "item_key", "torch_on", "jon_bnst", "jon_gasp",
            "jon_land", "jon_gags", "jon_fall", "jon_drwn", "jon_air1", "jon_glp1", "jon_bbwl", "jon_pois", "amb_ston",
            "cat_icu", "bubble_h", "set_land", "jon_hlnd", "jon_laf2", "spi_jump", "jon_scub", "item_use", "tr_arrow",
            "swi_foot", "swi_ston", "swi_wtr1", "tr_fire", "m_skull5", "spi_atak", "anu_hit", "fishdies", "scrp_icu",
            "jon_wade", "amb_watr", "tele_1", "wasp_stg", "res", "drum4", "rex_icu", "m_hits_u", "q_tail", "vatr_mov",
            "jon_hit3", "jon_t_2", "jon_t_1", "jon_t_5", "jon_t_6", "jon_t_8", "jon_t_4", "rasprit1", "jon_fdie",
            "wijaf1", "ship_1", "saw_on", "ra_on", "amb_ston", "vatr_stp", "mana1", "mana2", "ammo", "pot_pc1",
            "pot_pc2", "weapon", "alarm", "tick1", "scrp_zap", "jon_t_3", "jon_laf1", "blasted", "jon_air2"};
    public static int nWaspSound = -1;
    public static final int[] StaticSound = new int[80];
    public static final SoundResource[] SoundBufs = new SoundResource[200];
    public static final String[] szSoundName = new String[200];
    public static int nSoundCount;
    public static int soundx, soundy, soundz, soundsect;
    public static int nLocalSectFlags;
    public static int nAmbientChannel = -1;
    public static int nLocalChan;
    public static ActiveSound[] sActiveSound;
    public static int nNextFreq;
    public static int nSwirlyFrames;
    public static int currTrack = -1;
    public static Music currMusic = null;
    public static Entry userMusicEntry;

    private static BuildAudio audio;

    private static int pSoundVolume, pSoundPitch;

    public static void sndInit() {
        cfg.setAudioDriver(cfg.getAudioDriver());
        cfg.setMidiDevice(cfg.getMidiDevice());
        Sound.audio = cfg.getAudio();
        Sound.audio.registerDecoder("VOC", new VOCDecoder());
        nCreepyTimer = nCreepyTime;
        InitSoundInfo(cfg.getMaxvoices());
    }

    private static void InitSoundInfo(int voices) {
        sActiveSound = new ActiveSound[voices];
        for (int i = 0; i < sActiveSound.length; i++) {
            sActiveSound[i] = new ActiveSound();
            sActiveSound[i].ambchannel = i;
        }
    }

    public static void GetSpriteSoundPitch(int nSector, int pVolume, int pPitch) {
        if (!boardService.isValidSector(nSector)) {
            return;
        }
        pSoundVolume = pVolume;
        pSoundPitch = pPitch;
        if (((nLocalSectFlags ^ SectFlag[nSector]) & 0x2000) != 0) {
            pSoundVolume >>= 1;
            pSoundPitch -= 1200;
        }
    }

    @Deprecated
    public static boolean midRestart() {
        StopMusic();
        return false;
    }

    public static int GetFrameSound(int a1, int a2) {
        if (a1 < 0 || a1 >= SeqBase.length ||  (a2 + SeqBase[a1]) == -1) {
            return -1;
        }
        return FrameSound[a2 + SeqBase[a1]];
    }

    public static void LoadFX() {
        for (int i = 0; i < 80; i++) {
            StaticSound[i] = LoadSound(SoundFiles[i]);
        }
    }

    public static void StartElevSound(int a1, int a2) {
        if ((a2 & 2) != 0) {
            D3PlayFX(StaticSound[nElevSound], a1);
        } else {
            D3PlayFX(StaticSound[nStoneSound], a1);
        }
    }

    public static int LoadSound(String name) {
        for (int i = 0; i < nSoundCount; i++) {
            if (szSoundName[i].equalsIgnoreCase(name)) {
                return i;
            }
        }

        if (nSoundCount >= 200) {
            throw new AssertException("Too many sounds being loaded... increase array size");
        }

        Entry entry = game.getCache().getEntry(name + ".voc", false);
        if (!entry.exists()) {
            Console.out.println("Sound " + name + " not found.", OsdColor.RED);
            return -1;
        }

        if (name.equals("WASP_WNG")) {
            nWaspSound = nSoundCount;
        }

        szSoundName[nSoundCount] = name;
        SoundData soundData = audio.getSoundDecoder(entry.getExtension()).decode(entry);
        boolean loop = false;
        if (soundData != null) {
            if (soundData instanceof VOCDecoder.PSSoundData) {
                loop = ((VOCDecoder.PSSoundData) soundData).isLoop();
            }
        } else {
            Console.out.println("Could not open sound " + name, OsdColor.RED);
            soundData = new SoundData(8000, 1, 8, ByteBuffer.allocateDirect(0));
        }
        SoundBufs[nSoundCount] = new SoundResource(soundData, nSoundCount, loop);
        return nSoundCount++;
    }

    public static void sndHandlePause(boolean gPaused) {
        if (gPaused) {
            if (currMusic != null) {
                currMusic.pause();
            }
            StopAllSounds();
        } else {
            if (!cfg.isMuteMusic() && currMusic != null) {
                currMusic.play();
            }
        }
    }

    public static void searchCDtracks() {
        CueScript cueScript;

        cdTrackEntries.clear();
        Directory gameDir = game.getCache().getGameDirectory();
        // search cue scripts at first
        for (Entry file : gameDir) {
            if (file.isExtension("cue")) {
                cueScript = new CueScript(file.getName(), file);
                for (String track : cueScript.getTracks()) {
                    Entry entry = game.getCache().getEntry(track, true);
                    if (entry.exists()) {
                        cdTrackEntries.add(entry);
                    }
                }

                if (!cdTrackEntries.isEmpty()) {
                    break;
                }
            }
        }

        // if not found try to find in music directory
        if (cdTrackEntries.isEmpty()) {
            for (String cdTrack : cdTrackList) {
                Entry entry = gameDir.getEntry(FileUtils.getPath("music", cdTrack));
                if (entry.exists()) {
                    cdTrackEntries.add(entry);
                }
            }
        }

        if (!cdTrackEntries.isEmpty()) {
            Console.out.println(cdTrackEntries.size() + " cd tracks found...");
        } else {
            Console.out.println("Cd tracks not found.");
        }
    }

    public static boolean MusicPlaying() {
        return currMusic != null && currMusic.isPlaying();
    }

    public static void playCDtrack(int nTrack, boolean loop) {
        if ( /*cfg.musicType == 0 ||*/ nTrack < 0 || cfg.isMuteMusic()) {
            return;
        }

        if (MusicPlaying() && currTrack == nTrack) {
            return;
        }

        if (userMusicEntry != null) {
            Music mus;
            if ((mus = newMusic(userMusicEntry)) != null) {
                StopMusic();
                currMusic = mus;
                currTrack = nTrack;
                currMusic.setLooping(loop);
                currMusic.play();
                return;
            }
        }

//		if (game.currentDef != null) { // music from def file
//			String CD = game.currentDef.audInfo.getDigitalInfo(track[nTrack]);
//			if (CD != null && !CD.isEmpty()
//					&& (currMusic = newMusic(MusicType.Digital, CD)) != null) {
//				StopMusic();
//				currTrack = nTrack;
//				currMusic.play(true);
//				return true;
//			}
//		}

        Music mus;
        if (nTrack > 0 && nTrack <= cdTrackEntries.size() && (mus = newMusic(cdTrackEntries.get(nTrack - 1))) != null) {
            StopMusic();
            currMusic = mus;
            currTrack = nTrack;
            currMusic.setLooping(loop);
            currMusic.play();
        }

    }

    public static void sndCheckUserMusic(Entry map) {
        userMusicEntry = null;
        if (map != null && map.exists()) {
            String mMapName = map.getName();
            userMusicEntry = map.getParent().getEntry(mMapName.substring(0, mMapName.lastIndexOf('.')) + ".ogg");
        }
    }

    public static void sndPlayMusic() {
//        cfg.setMusicVolume(!cfg.isMuteMusic() ? cfg.getMusicVolume() : 0);

//		if ( cfg.musicType == 1 && game.currentDef != null) { //music from def file
//			String himus = game.currentDef.audInfo.getDigitalInfo(pGameInfo.zLevelSong);
//			if(himus != null)
//			{
//				if(currMusic != null && currMusic.isPlaying() && currSong == himus)
//					return;
//
//				StopMusic();
//				if((currMusic = BuildGdx.audio.newMusic(MusicType.Digital, himus)) != null) {
//					currSong = himus;
//					currMusic.play(true);
//					return;
//				}
//			}
//		}

        if (mUserFlag != UserFlag.UserMap) {
            int num = levelnum;
            if (levelnum != 0) {
                num = levelnum - 1;
            }
            playCDtrack(num % 8 + 11, true);
        } else {
            if (!MusicPlaying()) {
                int num = (int) ((Math.random() * 8) + 11);
                playCDtrack(num, true);
            }
        }
    }

    public static void StopMusic() {
        if (currMusic != null) {
            currMusic.stop();
        }

        currTrack = -1;
        currMusic = null;
    }

    public static void StopAllSounds() {
        audio.stopAllSounds();
        for (ActiveSound activeSound : sActiveSound) {
            if (activeSound.pHandle != null) {
                activeSound.pHandle.stop();
            }
            activeSound.pHandle = null;
            activeSound.rate = 0;
        }
        nAmbientChannel = -1;
    }

    public static void PlayLocalSound(int soundid, int rateoffs) {
        if (cfg.isNoSound() || soundid < 0 || soundid >= SoundBufs.length) {
            return;
        }

        if (nAmbientChannel == nLocalChan) {
            nAmbientChannel = -1;
        }

        ActiveSound pSound = sActiveSound[nLocalChan];
        SoundResource res = SoundBufs[soundid];
        if (res == null) {
            return;
        }

        int rate = res.getRate();
        if (rateoffs != 0) {
            rate += rateoffs;
        }

        // #GDX 31.12.2024 Stop local sound before create new one
        if (LocalSoundPlaying()) {
            StopLocalSound();
        }

        Source hVoice = newSound(res.getBuffer(), rate, res.getBits(), 255);
        if (hVoice != null) {
//            hVoice.setCallback(callback);
            if (res.loop) {
                hVoice.loop(255.0f);
            } else {
                hVoice.play(255.0f);
            }

            pSound.pHandle = hVoice;
            pSound.rate = rate;
            pSound.soundnum = soundid;
            SetLocalChan(0);
        }

    }

    public static void CheckAmbience(int sect) {
        if (boardService.isValidSector(sect) && SectSound[sect] != -1) {
            int nSourceSect = SectSoundSect[sect];
            Sector sec = boardService.getSector(nSourceSect);
            if (sec == null || sec.getWallNode() == null) {
                return;
            }

            Wall pWall = sec.getWallNode().get();
            if (nAmbientChannel < 0) {
                PlayFXAtXYZ(SectSound[sect] | PS_HIT_FLAG, pWall.getX(), pWall.getY(), sec.getFloorz(), sect);
                return;
            }
            ActiveSound pSound = sActiveSound[nAmbientChannel];
            if (sect == nSourceSect) {
                Sprite pPlayer = boardService.getSprite(PlayerList[nLocalPlayer].spriteId);
                if (pPlayer != null) {
                    pSound.x = pPlayer.getX();
                    pSound.y = pPlayer.getY();
                    pSound.z = pPlayer.getZ();
                }
            } else {
                pSound.x = pWall.getX();
                pSound.y = pWall.getY();
                pSound.z = sec.getFloorz();
            }
            return;
        }

        if (nAmbientChannel != -1) {
            if (sActiveSound[nAmbientChannel].pHandle != null) {
                sActiveSound[nAmbientChannel].pHandle.stop();
            }
            sActiveSound[nAmbientChannel].pHandle = null;
            sActiveSound[nAmbientChannel].rate = 0;
            nAmbientChannel = -1;
        }
    }

    public static void StopLocalSound() {
        if (nLocalChan == nAmbientChannel) {
            nAmbientChannel = -1;
        }
        if (LocalSoundPlaying()) {
            sActiveSound[nLocalChan].pHandle.stop();
        }
        sActiveSound[nLocalChan].pHandle = null;
        sActiveSound[nLocalChan].rate = 0;
    }

    public static void StopSpriteSound(int nSprite) {
        if (cfg.isNoSound()) {
            return;
        }

        int i = 0;
        while (i < sActiveSound.length) {
            if (SoundPlaying(i) && nSprite == sActiveSound[i].spr) {
                sActiveSound[i].pHandle.stop();
                sActiveSound[i].pHandle = null;
                sActiveSound[i].rate = 0;
                break;
            }
            i++;
        }
    }

    public static void BendAmbientSound() {
        if (cfg.isNoSound()) {
            return;
        }

        if (nAmbientChannel >= 0) {
            ActiveSound pSound = sActiveSound[nAmbientChannel];
            if (pSound.pHandle != null) {
                pSound.pHandle.setPitch((nDronePitch + 11000) / (float) pSound.rate);
            }
        }
    }

    public static void PlayFXAtXYZ(int soundid, int x, int y, int z, int sect) {
        if (cfg.isNoSound()) {
            return;
        }

        soundx = x;
        soundy = y;
        soundz = z;
        soundsect = sect & (MAXSECTORS - 1);

        int num = PlayFX2(soundid, -1);
        if (num != -1) {
            if ((sect & PS_HIT_FLAG) != 0) {
                sActiveSound[num].asound_C = 2000;
            }
        }
    }

    public static void D3PlayFX(int soundid, int nSprite) {
        PlayFX2(soundid, nSprite);
    }

    public static int PlayFX2(int soundid, int nSprite) {
        if (cfg.isNoSound()) {
            return -1;
        }

        nLocalSectFlags = SectFlag[nPlayerViewSect[nLocalPlayer]];
        boolean v33 = false;
        boolean v11 = false;
        if (nSprite >= 0) {
            v33 = (nSprite & 0x2000) != 0;
            v11 = (nSprite & PS_HIT_FLAG) != 0;
            nSprite &= 0xFFF;
            Sprite pSprite = boardService.getSprite(nSprite);
            if (pSprite != null) {
                soundx = pSprite.getX();
                soundy = pSprite.getY();
                soundz = pSprite.getZ();
                soundsect = pSprite.getSectnum();
            }
        }

        int dx = (initx - soundx) >> 8;
        int dy = (inity - soundy) >> 8;

        int dist = GetDistFromDXDY(dx, dy);
        int vol = v33 ? 255 : BClipRange(255 + 10 - (EngineUtils.sin((2 * dist) & 0x7FF) >> 6), 0, 255);

        if (dist >= 255 || vol == 0) {
            if (nSprite >= 0) {
                StopSpriteSound(nSprite);
            }
            return -1;
        }

        int v37 = soundid & 0xFE00;
        boolean v39 = (soundid & 0x2000) != 0;
        boolean v38 = (soundid & 0x1000) != 0;
        boolean v35 = (soundid & PS_HIT_FLAG) != 0;
        int v29 = 0x7FFFFFFF;
        int v36 = (soundid & 0xE00) >> 9;
        soundid &= 0x1FF;

        int v32 = 0;
        if (v38 || v35) {
            v32 = 1000;
        } else if (nSprite != -1 && v11) {
            v32 = 2000;
        }

        int nFree = 0;
        int v30 = 0;
        int v26 = 0;

        if (soundid >= SoundBufs.length) {
            return -1;
        }

        for (int i = 1; i < sActiveSound.length; i++) {
            if (SoundPlaying(i)) {
                ActiveSound pSound = sActiveSound[i];
                if (v32 >= pSound.asound_C) {
                    if (v29 > pSound.clock && pSound.asound_C == v32) {
                        v30 = i;
                        v29 = pSound.clock;
                    }

                    if (!v38) {
                        if (soundid == pSound.soundnum) {
                            if (!v39 && nSprite == pSound.spr) {
                                return -1;
                            }
                            v26 = i;
                        } else if (nSprite == pSound.spr) {
                            nFree = i;
                            break;
                        }
                    }
                }
            } else {
                nFree = i;
            }
        }

        while (true) {
            if (nFree != 0) {
                ActiveSound pFree = sActiveSound[nFree];
                if (pFree.pHandle != null && pFree.pHandle.isActive()) {
                    pFree.pHandle.stop();
                    if (pFree.ambchannel == nAmbientChannel) {
                        nAmbientChannel = -1;
                    }
                    pFree.pHandle = null;
                    pFree.rate = 0;
                }

                SoundResource res = SoundBufs[soundid];
                if (res == null) {
                    return -1;
                }

                if (!cfg.bWaspSound && soundid == nWaspSound) {
                    return -1;
                }

                int rate = res.getRate();
                int pitch = 0;
                if (v36 != 0) {
                    pitch = -16 * (totalmoves & ((1 << v36) - 1));
                }
                if (pitch != 0) {
                    rate += pitch;
                }
                pitch += rate;

                pFree.oldpitch = pitch;
                pFree.oldvolume = vol;
                if (nSprite < 0) {
                    pFree.x = soundx;
                    pFree.y = soundy;
                    pFree.z = soundz;
                    pFree.sectnum = soundsect;
                }
                GetSpriteSoundPitch(soundsect, vol, pitch);
                pFree.pitch = pSoundPitch;
                pFree.volume = pSoundVolume;
//	                pFree.panning = v34;
                pFree.spr = nSprite;
                pFree.soundnum = soundid;
                pFree.clock = engine.getTotalClock();
                pFree.asound_C = v32;
                pFree.ambient = v37;

                boolean isPlayer = nSprite == PlayerList[nLocalPlayer].spriteId;
                Source hVoice = newSound(res.getBuffer(), rate, res.getBits(), isPlayer ? 255 : dist);
                if (hVoice != null) {
                    if (!isPlayer) {
                        hVoice.setPosition(soundx, soundz >> 4, soundy);
                    }
                    hVoice.setListener(pFree);

                    if (res.loop) {
                        hVoice.loop(vol / 255.0f);
                    } else {
                        hVoice.play(vol / 255.0f);
                    }

                    if (v35) {
                        nAmbientChannel = pFree.ambchannel;
                    }
                    Sprite pSprite = boardService.getSprite(nSprite);
                    if (!isPlayer && pSprite != null && (pSprite.getCstat() & 0x101) != 0) {
                        nCreepyTimer = nCreepyTime;
                    }

                    pFree.pHandle = hVoice;
                    pFree.rate = rate;
                    return pFree.ambchannel;
                }
                return -1;
            } else {
                if (v26 != 0) {
                    nFree = v26;
                    continue;
                }
                if (v30 != 0) {
                    nFree = v30;
                    continue;
                }
                break;
            }
        }
        return -1;

    }

    public static int GetDistFromDXDY(int dx, int dy) {
        return ((dx * dx + dy * dy) >> 3) - ((dx * dx + dy * dy) >> 5);
    }

    public static void StartSwirlies() {
        if (cfg.isNoSound()) {
            return;
        }

        StopAllSounds();
        nNextFreq = 19000;
        nSwirlyFrames = 0;

        for (int i = 1; i <= 4; i++) {
            StartSwirly(i);
        }
    }

    private static void StartSwirly(int nSwirly) {
        ActiveSound pSound = sActiveSound[nSwirly];
        pSound.panning &= 0x7ff;
        pSound.oldpitch = nNextFreq - RandomSize(9);
        nNextFreq = Math.min(25000 - 6 * RandomSize(10), 32000);
        SoundResource res = SoundBufs[StaticSound[67]];
        if (res == null) {
            return;
        }

        Source pHandle = pSound.pHandle = newSound(res.getBuffer(), pSound.oldpitch, res.getBits(), 255);
        if (pHandle != null) {
            pHandle.setPosition(pSound.panning * 6000, 0, 0);
            pHandle.setListener(pSound);
            pHandle.play(Math.min(nSwirlyFrames + 1, 220) / 220.0f);
            pSound.rate = pSound.oldpitch;
        }
    }

    public static void UpdateSwirlies() {
        if (cfg.isNoSound()) {
            return;
        }

        nSwirlyFrames++;

        int nSound = 1;
        int nShift = 5;
        for (int i = 1; i <= 4; i++) {
            ActiveSound pSound = sActiveSound[nSound++];
            if (pSound.pHandle == null || !pSound.pHandle.isActive()) {
                StartSwirly(i);
            }

            if (pSound.pHandle != null) {
                pSound.panning = (EngineUtils.sin((engine.getTotalClock() << nShift) & 0x7FF) >> 8) * 6000;
                pSound.pHandle.setVolume(Math.min(nSwirlyFrames + 1, 220) / 220.0f);
                pSound.pHandle.setPosition(pSound.panning, 0, pSound.panning);
            }
            nShift++;
        }
    }

    public static void SoundBigEntrance() {
        if (cfg.isNoSound()) {
            return;
        }

        StopAllSounds();
        int rate = -1200;

        for (int i = 0; i < 4; i++) {
            SoundResource res = SoundBufs[12];
            if (res == null) {
                return;
            }

            ActiveSound pSound = sActiveSound[i];
            Source pHandle = pSound.pHandle = newSound(res.getBuffer(), rate + 11000, res.getBits(), 255);
            if (pHandle == null) {
                return;
            }

            pSound.oldpitch = rate;
//            pHandle.setGlobal(1); XXX
            pHandle.setPosition(63 - 127 * (i & 1), 0, 0);
            pHandle.setListener(pSound);
            pHandle.play(200);
            pSound.rate = rate + 11000;
            rate += 512;
        }
    }

    private static boolean SoundPlaying(int num) {
        return num >= 0 && num < sActiveSound.length && sActiveSound[num].pHandle != null && sActiveSound[num].pHandle.isActive();
    }

    public static boolean LocalSoundPlaying() {
        return SoundPlaying(nLocalChan);
    }

    public static int GetLocalSound() {
        if (!LocalSoundPlaying()) {
            return -1;
        }

        return sActiveSound[nLocalChan].soundnum & 0x1ff;
    }

    public static void SetLocalChan(int channel) {
        if (channel >= 0 && channel < sActiveSound.length) {
            nLocalChan = channel;
        }
    }

    public static void PlayLogoSound() {
        PlayLocalSound(StaticSound[28], 7000);
    }

    public static void PlayGameOverSound() {
        PlayLocalSound(StaticSound[28], 0);
    }

    public static void UpdateSounds() {
        if (cfg.isNoSound() || nFreeze != 0) {
            if (nFreeze != 0) {
                audio.setListener(0, 0, 0, 0);
            }
            return;
        }

        nLocalSectFlags = SectFlag[nPlayerViewSect[nLocalPlayer]];
        int earx = initx, eary = inity, earz = initz, earang = inita;
        if (nSnakeCam >= 0) {
            Sprite pSnake = boardService.getSprite(SnakeList[nSnakeCam].nSprite[0]);
            if (pSnake != null) {
                earx = pSnake.getX();
                eary = pSnake.getY();
                earz = pSnake.getZ();
                earang = pSnake.getAng();
            }
        }

        audio.setListener(earx, earz >> 4, eary, earang);

        int dx, dy;
        for (int i = 1; i < sActiveSound.length; i++) {
            if (SoundPlaying(i)) {
                ActiveSound pSound = sActiveSound[i];
                Source pHandle = pSound.pHandle;

                Sprite spr = (pSound.spr >= 0) ? boardService.getSprite(pSound.spr) : null;
                if (spr != null) {
                    dx = earx - spr.getX();
                    dy = eary - spr.getY();

                    if (spr != boardService.getSprite(PlayerList[nLocalPlayer].spriteId)) {
                        pHandle.setPosition(spr.getX(), spr.getZ() >> 4, spr.getY());
                    }
                } else {
                    dx = earx - pSound.x;
                    dy = eary - pSound.y;
                    pHandle.setPosition(pSound.x, pSound.z >> 4, pSound.y);
                }

                int dist = GetDistFromDXDY(dx >> 8, dy >> 8);
                if (dist >= 255) {
                    pHandle.stop();
                    if ((pSound.ambient & PS_HIT_FLAG) != 0) {
                        nAmbientChannel = -1;
                    }
                    return;
                }

                int vol = BClipRange(255 + 10 - (EngineUtils.sin((2 * dist) & 0x7FF) >> 6), 0, 255);
                int pitch = pSound.oldpitch;
                int sectnum = pSound.sectnum;
                if (spr != null) {
                    if (spr == boardService.getSprite(PlayerList[nLocalPlayer].spriteId)) {
                        sectnum = nPlayerViewSect[nLocalPlayer];
                    } else {
                        sectnum = spr.getSectnum();
                    }
                }

                GetSpriteSoundPitch(sectnum, vol, pitch);
                vol = pSoundVolume;
                pitch = pSoundPitch;
                if (pSound.pitch != pitch) {
                    pSound.pitch = pitch;
                }

                if (vol != pSound.volume) {
                    if (pitch < 0) {
                        pitch = 7000;
                    }

                    pHandle.setPitch(pitch / (float) pSound.rate);
                    pHandle.setVolume(vol / 255.0f);
                    pSound.volume = vol;
                }
            }
        }

        if (nFreeze != 0 || levelnum == 20 || --nCreepyTimer > 0) {
            return;
        }

        if (nCreaturesLeft > 0 && (SectFlag[nPlayerViewSect[nLocalPlayer]] & 0x2000) == 0) {
            Sprite pPlayer = boardService.getSprite(PlayerList[nLocalPlayer].spriteId);
            if (pPlayer != null) {
                int soundId = GetFrameSound(SeqOffsets[65], totalmoves % SeqSize[SeqOffsets[65]]);
                int sx = (totalmoves + 32) & 0x1F;
                if ((totalmoves & 1) != 0) {
                    sx = -sx;
                }
                int sy = (totalmoves + 32) & 0x3F;
                if ((totalmoves & 2) != 0) {
                    sy = -sy;
                }

                PlayFXAtXYZ(soundId, pPlayer.getX() + sx, sy + pPlayer.getY(), pPlayer.getZ(), pPlayer.getSectnum());
            }
        }
        nCreepyTimer = nCreepyTime;
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, priority);
    }

    public static Music newMusic(Entry file) {
        return audio.newMusic(file);
    }

    public static class ActiveSound implements SourceListener {
        public Source pHandle;
        public int rate;
        public int spr;
        public int x, y, z;
        public int oldvolume, volume;
        public int panning;
        public int oldpitch, pitch;
        public int soundnum, asound_C, clock;
        public int ambient, ambchannel;
        public int sectnum;

        @Override
        public void onStop() {
            if (pHandle != null) {
                pHandle.stop();
            }
            pHandle = null;
            rate = 0;
        }
    }
}
