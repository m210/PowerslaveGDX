// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TSprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Powerslave.Screens.DemoScreen;
import ru.m210projects.Powerslave.Type.PlayerSave;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Anim.*;
import static ru.m210projects.Powerslave.Cinema.DoFailedFinalScene;
import static ru.m210projects.Powerslave.Cinema.DoGameOverScene;
import static ru.m210projects.Powerslave.Enemies.Ra.BuildRa;
import static ru.m210projects.Powerslave.Factory.PSInput.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Light.*;
import static ru.m210projects.Powerslave.LoadSave.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.FinishLevel;
import static ru.m210projects.Powerslave.Map.GrabMap;
import static ru.m210projects.Powerslave.Object.StartRegenerate;
import static ru.m210projects.Powerslave.Palette.*;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.PSSector.MoveSector;
import static ru.m210projects.Powerslave.PSSector.feebtag;
import static ru.m210projects.Powerslave.Seq.*;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.SpiritHead.InitSpiritHead;
import static ru.m210projects.Powerslave.SpiritHead.nSpiritSprite;
import static ru.m210projects.Powerslave.Sprites.*;
import static ru.m210projects.Powerslave.Type.StatusAnim.BuildStatusAnim;
import static ru.m210projects.Powerslave.View.*;
import static ru.m210projects.Powerslave.Weapons.*;

public class Player {

    public static final String deathMessage1 = "or \"enter\" to load last saved game";
    public static final String deathMessage2 = "Press \"use\" to restart the level";

    public static final int MAXHORIZ = 300;
    public static final int MAXOHORIZ = 180;
    public static final int MINHORIZ = -130;
    public static final int MINOHORIZ = 0;
    public static final Action[] ActionSeq = new Action[]{new Action(18, false), new Action(0, false),
            new Action(9, false), new Action(27, false), new Action(63, false), new Action(72, false),
            new Action(54, false), new Action(45, false), new Action(54, false), new Action(81, false),
            new Action(90, false), new Action(99, false), new Action(108, false), new Action(8, false),
            new Action(0, false), new Action(139, false), new Action(117, true), new Action(119, true),
            new Action(120, true), new Action(121, true), new Action(122, true)};
    public static final short[] nActionEyeLevel = new short[]{-14080, -14080, -14080, -14080, -14080, -14080, -8320, -8320,
            -8320, -8320, -8320, -8320, -8320, -14080, -14080, -14080, -14080, -14080, -14080, -14080, -14080};
    public static final int[] nHeightTemplate = new int[]{0, 0, 0, 0, 0, 0, 7, 7, 7, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0};
    public static int nLocalEyeSect;
    public static final PlayerSave[] sPlayerSave = new PlayerSave[8];

    public static int GetPlayerFromSprite(int num) {
        Sprite spr = boardService.getSprite(num);
        if (spr != null) {
            return RunData[spr.getOwner()].getObject();
        }
        return -1;
    }

    public static void InitPlayerKeys(int num) {
        PlayerList[num].KeysBitMask = 0;
    }

    public static void RestartPlayer(int num) {
        int nSprite = PlayerList[num].spriteId;
        int nDopple = nDoppleSprite[num];
        Sprite pPlayerSpr = boardService.getSprite(nSprite);
        if (pPlayerSpr != null) {
            DoSubRunRec(pPlayerSpr.getOwner());
            FreeRun(pPlayerSpr.getLotag() - 1);
            engine.changespritestat(nSprite,  0);
            PlayerList[num].spriteId = -1;
            if (nPlayerFloorSprite[num] > -1) {
                engine.mydeletesprite(nPlayerFloorSprite[num]);
            }

            Sprite pDopple = boardService.getSprite(nDopple);
            if (pDopple != null) {
                DoSubRunRec(pDopple.getOwner());
                FreeRun(pDopple.getLotag() - 1);
                engine.mydeletesprite(nDopple);
            }
        }

        final int spr = GrabBody();
        final Sprite pSprite = boardService.getSprite(spr);

        PlayerSave pSave = sPlayerSave[num];
        engine.changespritesect(spr, pSave.sect);
        engine.changespritestat(spr,  100);
        if (pSprite == null) {
            throw new AssertException("Player sprite == null");
        }

        int dspr = engine.insertsprite(pSprite.getSectnum(),  100);
        Sprite pDSprite = boardService.getSprite(dspr);
        if (pDSprite == null) {
            throw new AssertException("(dspr>=0) && (dspr<MAXSPRITES)");
        }

        nDoppleSprite[num] = dspr;
        int floorspr;
        if (numplayers <= 1) {
            pSprite.setX(pSave.x);
            pSprite.setY(pSave.y);
            Sector sec = boardService.getSector(pSave.sect);
            if (sec != null) {
                pSprite.setZ(sec.getFloorz());
            }
            PlayerList[num].ang = pSave.ang;
            pSprite.setAng(pSave.ang);
            floorspr = -1;
        } else {
            int nNetStart = nNetStartSprite[nCurStartSprite++];
            Sprite pNetStart = boardService.getSprite(nNetStart);
            if (pNetStart == null) {
                throw new AssertException("pNetStart != null");
            }

            if (nCurStartSprite >= nNetStartSprites) {
                nCurStartSprite = 0;
            }

            pSprite.setX(pNetStart.getX());
            pSprite.setY(pNetStart.getY());
            pSprite.setZ(pNetStart.getZ());
            engine.mychangespritesect(spr, pNetStart.getSectnum());
            PlayerList[num].ang = pNetStart.getAng();
            pSprite.setAng((int) PlayerList[num].ang);
            floorspr = engine.insertsprite(pSprite.getSectnum(),  0);
            Sprite pFloorspr = boardService.getSprite(floorspr);
            if (pFloorspr == null) {
                throw new AssertException("(floorspr>=0 && floorspr<MAXSPRITES");
            }
            pFloorspr.setX(pSprite.getX());
            pFloorspr.setY(pSprite.getY());
            pFloorspr.setZ(pSprite.getZ());
            pFloorspr.setYrepeat(64);
            pFloorspr.setXrepeat(64);
            pFloorspr.setCstat(32);
            pFloorspr.setPicnum( (num + 3571));
        }

        nPlayerFloorSprite[num] =  floorspr;
        pSprite.setCstat(257);
        pSprite.setShade(-12);
        pSprite.setClipdist(58);
        pSprite.setPal(0);
        pSprite.setXrepeat(40);
        pSprite.setYrepeat(40);
        pSprite.setXoffset(0);
        pSprite.setYoffset(0);
        pSprite.setPicnum(GetSeqPicnum(25, 18, 0));
        nStandHeight = GetSpriteHeight(spr);
        pSprite.setXvel(0);
        pSprite.setYvel(0);
        pSprite.setZvel(0);
        pSprite.setLotag( (HeadRun() + 1));
        pSprite.setHitag(0);
        pSprite.setExtra(-1);

        pDSprite.setX(pSprite.getX());
        pDSprite.setY(pSprite.getY());
        pDSprite.setZ(pSprite.getZ());
        pDSprite.setXrepeat(pSprite.getXrepeat());
        pDSprite.setYrepeat(pSprite.getYrepeat());
        pDSprite.setYoffset(0);
        pDSprite.setXoffset(0);
        pDSprite.setShade(pSprite.getShade());
        pDSprite.setAng(pSprite.getAng());
        pDSprite.setCstat(pSprite.getCstat());
        pDSprite.setLotag( (HeadRun() + 1));

        PlayerList[num].anim_ = 0;
        PlayerList[num].animCount = 0;
        PlayerList[num].spriteId = spr;
        PlayerList[num].invisibility = 0;
        PlayerList[num].mummified = 0;
        engine.getPaletteManager().SetTorch(num, 0);
        nPlayerInvisible[num] = 0;
        PlayerList[num].weaponFire = 0;
        PlayerList[num].seqOffset = 0;
        nPlayerViewSect[num] = pSave.sect;
        PlayerList[num].weaponState = 0;
        nPlayerDouble[num] = 0;
        PlayerList[num].seq = 25;
        nPlayerPushSound[num] = -1;
        PlayerList[num].newWeapon = -1;
        PlayerList[num].lastWeapon = 0;
        PlayerList[num].AirAmount = 100;
        nPlayerGrenade[num] = -1;
        nPlayerTorch[num] = 0;
        PlayerList[num].AirMaskAmount = 0;

        if (gClassicMode) {
            PlayerList[num].HealthAmount = 800;
            if (nNetPlayerCount != 0) {
                PlayerList[num].HealthAmount = 1600;
            }
            if (PlayerList[num].currentWeapon == 7) {
                PlayerList[num].currentWeapon = PlayerList[num].lastWeapon;
            }
        }

        if (levelnum <= 20) {
            if (!gClassicMode) {
                RestoreMinAmmo(num);
            } else {
                CheckClip(num);
            }
        } else {
            ResetPlayerWeapons(num);
            PlayerList[num].MagicAmount = 0;
        }

        PlayerList[num].eyelevel = -14080;
        dVertPan[num] = 0;
        nTemperature[num] = 0;
        nYDamage[num] = 0;
        nXDamage[num] = 0;
        PlayerList[num].horiz = 92;
        nDestVertPan[num] = 92;
        nBreathTimer[num] = 90;
        nTauntTimer[num] =  (RandomSize(3) + 3);

        pDSprite.setOwner( AddRunRec(pDSprite.getLotag() - 1, nEventRadialDamage | num));
        pSprite.setOwner( AddRunRec(pSprite.getLotag() - 1, nEventRadialDamage | num));

        if (PlayerList[num].RunFunc < 0) {
            PlayerList[num].RunFunc =  AddRunRec(NewRun, nEventRadialDamage | num);
        }

        BuildRa(num);
        if (num == nLocalPlayer) {
//			nLocalSpr = spr;
            SetMagicFrame();
            engine.getPaletteManager().restoreGreenPal();
            bPlayerPan = false;
            bLockPan = false;
        }

        // name
        totalvel[num] = 0;
        nDeathType[num] = 0;
        nQuake[num] = 0;
        if (num == nLocalPlayer) {
            SetHealthFrame(0);
        }
    }

//    public static void RestoreSavePoint(int num) {
//        PlayerSave ps = sPlayerSave[num];
//
//        initx = ps.x;
//        inity = ps.y;
//        initz = ps.z;
//        initsect = ps.sect;
//        inita = ps.ang;
//        PlayerList[num].ang = inita;
//    }

    public static void SetSavePoint(int num, int x, int y, int z, int sect, int ang) {
        PlayerSave ps = sPlayerSave[num];
        ps.x = x;
        ps.y = y;
        ps.z = z;
        ps.sect =  sect;
        ps.ang =  ang;
    }

    public static int GrabPlayer() {
        if (PlayerCount >= 8) {
            return -1;
        }
        return PlayerCount++;
    }

    public static void InitPlayerInventory(int nPlayer) {
        PlayerList[nPlayer].reset();
        PlayerList[nPlayer].nPlayer = nPlayer;
        nPlayerItem[nPlayer] = -1;
        nPlayerSwear[nPlayer] = 4;
        ResetPlayerWeapons(nPlayer);
        if (gClassicMode) {
            nPlayerLives[nPlayer] = 3;
        } else {
            nPlayerLives[nPlayer] = 0;
        }
        PlayerList[nPlayer].spriteId = -1;
        PlayerList[nPlayer].RunFunc = -1;
        nPistolClip[nPlayer] = 6;
        nPlayerClip[nPlayer] = 0;
        PlayerList[nPlayer].currentWeapon = 0;
        if (nPlayer == nLocalPlayer) {
            bMapMode = false;
        }
        nPlayerScore[nPlayer] = 0;
//        engine.loadtile(nPlayer + 3571);
//		  nPlayerColor[v9] = *(_BYTE *)(waloff[v10 + 3571] + tilesizx[v10 + 3571] * tilesizy[v10 + 3571] / 2); XXX
    }

    public static void SetItemSeq() {
        int item = nPlayerItem[nLocalPlayer];
        if (item < 0) {
            nItemSeq = -1;
        } else {
            if (nItemMagic[nPlayerItem[nLocalPlayer]] <= PlayerList[nLocalPlayer].MagicAmount) {
                nItemAltSeq = 0;
            } else {
                nItemAltSeq = 2;
            }
            nItemFrame = 0;
            nItemSeq = nItemAltSeq + nItemSeqOffset[item];
            nItemFrames = SeqSize[nStatusSeqOffset + nItemSeq];
        }
    }

    public static void SetPlayerItem(int nPlayer, int nItem) {
        nPlayerItem[nPlayer] =  nItem;
        if (nPlayer == nLocalPlayer) {
            SetItemSeq();
            if (nItem >= 0) {
                BuildStatusAnim(2 * PlayerList[nPlayer].ItemsAmount[nItem] + 156, 0);
            }
        }
    }

    public static void FuncPlayer(int nStack, int a2, int RunPtr) {
        final int player = RunData[RunPtr].getObject();
        if (player < 0 || player >= 8) {
            throw new AssertException("player>=0 && player<MAXPLAYER");
        }
        if (PlayerList[player].network == -1) {
            return;
        }

        int v233 = 0;
        final int spr = PlayerList[player].spriteId;
        int dspr = nDoppleSprite[player];
        Sprite pDSprite = boardService.getSprite(dspr);
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null || pDSprite == null) {
            throw new AssertException("Player sprite == null");
        }

        int anim = PlayerList[player].anim_;

        int v229 = -1;
        switch (nStack & EVENT_MASK) {
            case nEventProcess:
                if ((sPlayerInput[player].bits & nItemUse) != 0) {
                    UseCurItem(player, nPlayerItem[player]);
                }
                if ((sPlayerInput[player].bits & nItemLeft) != 0) {
                    SetPrevItem(player);
                }
                if ((sPlayerInput[player].bits & nItemRight) != 0) {
                    SetNextItem(player);
                }

                if ((sPlayerInput[player].bits & nItemHand) != 0) {
                    UseCurItem(player, 3);
                }
                if ((sPlayerInput[player].bits & nItemEye) != 0) {
                    UseCurItem(player, 4);
                }
                if ((sPlayerInput[player].bits & nItemMask) != 0) {
                    UseCurItem(player, 5);
                }
                if ((sPlayerInput[player].bits & nItemHeart) != 0) {
                    UseCurItem(player, 0);
                }
                if ((sPlayerInput[player].bits & nItemTorch) != 0) {
                    UseCurItem(player, 2);
                }
                if ((sPlayerInput[player].bits & nItemScarab) != 0) {
                    UseCurItem(player, 1);
                }

                int next_anim = PlayerList[player].anim_;

                pSprite.setXvel( (sPlayerInput[player].xvel >> 14));
                pSprite.setYvel( (sPlayerInput[player].yvel >> 14));
                if (sPlayerInput[player].field_F > -1) {
                    UseItem(player, sPlayerInput[player].field_F);
                    sPlayerInput[player].field_F = -1;
                }

                int picnum = GetSeqPicnum(PlayerList[player].seq,
                        ActionSeq[nHeightTemplate[anim]].seq, PlayerList[player].animCount);
                pDSprite.setPicnum(picnum);
                pSprite.setPicnum(picnum);

                int torch = nPlayerTorch[player];
                if (torch > 0) {
                    nPlayerTorch[player] =  (torch - 1);
                    if (torch == 1) {
                        engine.getPaletteManager().SetTorch(player, 0);
                    } else if (player != nLocalPlayer) {
                        nFlashDepth = 5;
                        AddFlash(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                        nFlashDepth = 2;
                    }
                } else if (player == nLocalPlayer) {
                    bTorch = 0;
                }

                short dble = (short) nPlayerDouble[player];
                if (dble > 0) {
                    nPlayerDouble[player] =  (dble - 1);
                    if (nPlayerDouble[player] == 150 && player == nLocalPlayer) {
                        PlayAlert("WEAPON POWER IS ABOUT TO EXPIRE", player);
                    }
                }

                int invis = nPlayerInvisible[player];
                if (invis > 0) {
                    nPlayerInvisible[player] = (invis - 1);
                    if (invis == 1) {
                        pSprite.setCstat(pSprite.getCstat() & 0x7FFF);
                        int fl = nPlayerFloorSprite[player];
                        Sprite floorSprite = boardService.getSprite(fl); // was player instead of fl
                        if (floorSprite != null) {
                            floorSprite.setCstat(floorSprite.getCstat() & 0x7FFF);
                        }
                    } else if (nPlayerInvisible[player] == 150 && player == nLocalPlayer) {
                        PlayAlert("INVISIBILITY IS ABOUT TO EXPIRE", player);
                    }
                }

                int god = PlayerList[player].invisibility;
                if (god > 0) {
                    PlayerList[player].invisibility =  (god - 1);
                    if (PlayerList[player].invisibility == 150 && player == nLocalPlayer) {
                        PlayAlert("INVINCIBILITY IS ABOUT TO EXPIRE", player);
                    }
                }

                int quake = nQuake[player];
                if (quake != 0) {
                    nQuake[player] =  -quake;
                    if (nQuake[player] >= 0 && nQuake[player] != 0) {
                        nQuake[player] =  (nQuake[player] - 512);
                        if ((-quake) - 512 < 0) {
                            nQuake[player] = 0;
                        }
                    }
                }

                PlayerList[player].ang = BClampAngle(PlayerList[player].ang + (4 * sPlayerInput[player].avel));
                pSprite.setAng((int) PlayerList[player].ang);

                short ozvel = pSprite.getZvel();
                Gravity(spr);

                if (pSprite.getZvel() >= 6500 && ozvel < 6500) {
                    D3PlayFX(StaticSound[17], spr);
                }

                int osectnum = pSprite.getSectnum();
                boolean oUnderwater = (SectFlag[nPlayerViewSect[player]] & 0x2000) != 0;

                int sx = pSprite.getX();
                int sy = pSprite.getY();
                int sz = pSprite.getZ();

                int xvel = 4 * sPlayerInput[player].xvel >> 2;
                int yvel = 4 * sPlayerInput[player].yvel >> 2;
                int zvel = 4 * pSprite.getZvel() >> 2;

                if (pSprite.getZvel() > 0x2000) {
                    pSprite.setZvel(0x2000);
                }
                ozvel = pSprite.getZvel();

                if (PlayerList[player].mummified != 0) {
                    xvel /= 2;
                    yvel /= 2;
                }

                int moveHit = 0;
                if (bSlipMode) {
                    pSprite.setX(pSprite.getX() + (xvel >> 14));
                    pSprite.setY(pSprite.getY() + (yvel >> 14));
                    engine.setsprite(spr, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                    Sector sec = boardService.getSector(pSprite.getSectnum());
                    if (sec != null) {
                        pSprite.setZ(sec.getFloorz());
                    }
                } else {
                    moveHit = engine.movesprite(spr, xvel, yvel, zvel, 5120, -5120, 0);
                    engine.pushmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 4 * pSprite.getClipdist(), 5120, -5120, 0);

                    pSprite.setX(pushmove_x);
                    pSprite.setY(pushmove_y);
                    pSprite.setZ(pushmove_z);

                    if (boardService.isValidSector(pushmove_sectnum) && pSprite.getSectnum() != pushmove_sectnum) {
                        engine.mychangespritesect(spr, pushmove_sectnum);
                    }

                    if (engine.inside(pSprite.getX(), pSprite.getY(), pSprite.getSectnum()) == 0) {
                        engine.mychangespritesect(spr, osectnum);
                        pSprite.setX(sx);
                        pSprite.setY(sy);
                        if (ozvel < pSprite.getZvel()) {
                            pSprite.setZvel(ozvel);
                        }
                    }
                }

                boolean bUnderwater = (SectFlag[pSprite.getSectnum()] & 0x2000) != 0;

                if (bUnderwater) {
                    int v48 = nXDamage[player]; // XXX WTF?
                    nXDamage[player] = (v48 - (v48 & 0xFF00)) >> 1;
                    int v49 = nYDamage[player];
                    nYDamage[player] = (v49 - (v49 & 0xFF00)) >> 1;
                }

                if ((SectFlag[pSprite.getSectnum()] & 0x8000) != 0 && bTouchFloor[0]) {
                    if (numplayers <= 1) {
                        PlayerList[player].ang = GetAngleToSprite(pSprite, boardService.getSprite(nSpiritSprite));
                        pSprite.setAng((int) PlayerList[player].ang);
                        pSprite.setZvel(0);
                        pSprite.setYvel(0);
                        pSprite.setXvel(pSprite.getYvel());
                        if (nFreeze < 1) {
                            nFreeze = 1;
                            StopAllSounds();
                            StopLocalSound();
                            InitSpiritHead();
                            nDestVertPan[player] = 92;
                            if (levelnum == 11) {
                                nDestVertPan[player] = 138;
                            } else {
                                nDestVertPan[player] = 103;
                            }
                        }
                    } else {
                        FinishLevel();
                    }
                    return;
                }

                if ((moveHit & (PS_HIT_SPRITE | nEvent3)) != 0) {
                    if (bTouchFloor[0]) {
                        int v53 = nXDamage[player]; // XXX
                        nXDamage[player] = (v53 - (v53 & 0xFF00)) >> 1;
                        int v54 = nYDamage[player];
                        nYDamage[player] = (v54 - (v54 & 0xFF00)) >> 1;
                        if (player == nLocalPlayer) {
                            if (!cfg.isgMouseAim() && klabs(ozvel) > 512) {
                                nDestVertPan[player] = 92;
                            }
                        }

                        if (ozvel >= 6500) {
                            pSprite.setXvel(pSprite.getXvel() >> 2);
                            pSprite.setYvel(pSprite.getYvel() >> 2);
                            DamageEnemy(spr, -1, ((ozvel - 6500) >> 7) + 10);
                            if (PlayerList[player].HealthAmount > 0) {
                                D3PlayFX(StaticSound[27] | 0x2000, spr);
                            } else {
                                pSprite.setXvel(0);
                                pSprite.setYvel(0);
                                StopSpriteSound(spr);
                                PlayFXAtXYZ(StaticSound[60], pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum() | PS_HIT_FLAG);
                            }
                        }
                    }

                    int hitobject = -1;
                    switch (moveHit & PS_HIT_TYPE_MASK) {
                        case PS_HIT_SECTOR: // hit a ceiling or floor
                            hitobject = moveHit & PS_HIT_INDEX_MASK;
                            break;
                        case PS_HIT_WALL:
                            Wall wall = boardService.getWall(moveHit & PS_HIT_INDEX_MASK);
                            if (wall != null) {
                                hitobject = wall.getNextsector();
                            }
                            break;
                    }

                    Sector pHitSector = boardService.getSector(hitobject);
                    if (pHitSector != null && pHitSector.getHitag() == 45 && bTouchFloor[0] && klabs(
                            AngleDiff(engine.GetWallNormal(moveHit & PS_HIT_INDEX_MASK), (pSprite.getAng() + 1024) & 0x7FF)) <= 256) {
                        nPlayerPushSect[player] = hitobject;
                        int xv = sPlayerInput[player].xvel;
                        int yv = sPlayerInput[player].yvel;
                        Vector2 out = MoveSector(hitobject, engine.GetMyAngle(xv, yv), xv, yv);
                        xv = (int) out.x;
                        yv = (int) out.y;

                        if (nPlayerPushSound[player] == -1) {
                            nPlayerPushSound[player] = pHitSector.getExtra();
                            D3PlayFX(StaticSound[23], sBlockInfo[pHitSector.getExtra()].sprite | PS_HIT_FLAG);
                        } else {
                            pSprite.setX(sx);
                            pSprite.setY(sy);
                            pSprite.setZ(sz);
                            engine.mychangespritesect(spr, osectnum);
                        }
                        engine.movesprite(spr, xv, yv, zvel, 5120, -5120, 0);
                    } else {
                        if (nPlayerPushSound[player] != -1) {
                            if (nPlayerPushSect[player] != -1) {
                                StopSpriteSound(sBlockInfo[nPlayerPushSound[player]].sprite);
                            }
                            nPlayerPushSound[player] = -1;
                        }
                    }
                }

                if (!bPlayerPan && !bLockPan) {
                    int v74 = ((sz - pSprite.getZ()) / 32) + 92;
                    nDestVertPan[player] =  BClipRange(v74, 0, 183);
                }

                int dx = sx - pSprite.getX();
                int dy = sy - pSprite.getY();
                totalvel[player] = EngineUtils.sqrt(dx * dx + dy * dy);

                int sectnum = pSprite.getSectnum();
                int z = nQuake[player] + pSprite.getZ() + PlayerList[player].eyelevel;

                Sector s = boardService.getSector(sectnum);
                if (s != null && z < s.getCeilingz() && SectAbove[sectnum] > -1) {
                    sectnum = SectAbove[sectnum];
                }

                if (bUnderwater) {
                    short osect = pSprite.getSectnum();
                    if (sectnum != osect && (moveHit & PS_HIT_TYPE_MASK) == PS_HIT_WALL) {
                        int ox = pSprite.getX();
                        int oy = pSprite.getY();
                        int oz = pSprite.getZ();
                        engine.mychangespritesect(spr, sectnum);
                        pSprite.setX(sx);
                        pSprite.setY(sy);
                        Sector sec = boardService.getSector(sectnum);
                        if (sec != null) {
                            int fz = -5120 + sec.getFloorz();
                            pSprite.setZ(fz);
                            if ((engine.movesprite(spr, xvel, yvel, 0, 5120, 0, 0) & PS_HIT_TYPE_MASK) == PS_HIT_WALL) {
                                engine.mychangespritesect(spr, osect);
                                pSprite.setX(ox);
                                pSprite.setY(oy);
                                pSprite.setZ(oz);
                            } else {
                                pSprite.setZ(fz - 256);
                                D3PlayFX(StaticSound[42], spr);
                            }
                        }
                    }
                }

                nPlayerViewSect[player] = sectnum;
                nPlayerDX[player] = pSprite.getX() - sx;
                nPlayerDY[player] = pSprite.getY() - sy;

                boolean bViewUnderwater = (SectFlag[sectnum] & 0x2000) != 0;
                if (PlayerList[player].HealthAmount <= 0) {
                    if (!gClassicMode && anim >= 16) {
                        if (message_timer <= 0) {
                            if (game.isCurrentScreen(gGameScreen) && lastload != null && lastload.exists()) {
                                StatusMessage(5000, deathMessage1, player);
                            } else {
                                StatusMessage(5000, deathMessage2, player);
                            }
                        }

                        if (game.isCurrentScreen(gGameScreen) && lastload != null && lastload.exists()) {
                            if (Gdx.input.isKeyPressed(Keys.ENTER)) {
                                game.changeScreen(gLoadingScreen.setTitle(lastload.getName()));
                                gLoadingScreen.init(() -> {
                                    if (!loadgame(lastload)) {
                                        game.GameMessage("Can't load game!");
                                    }
                                });
                                return;
                            }
                        }
                    }

                    if ((sPlayerInput[player].bits & nOpen) != 0) {
                        if (anim >= 16) {
                            if (!gClassicMode) {
                                Gdx.app.postRunnable(() -> gGameScreen.newgame(mUserFlag == UserFlag.UserMap ? boardfilename : gCurrentEpisode, levelnum, false));
                                return;
                            } else {
                                if (player == nLocalPlayer) {
                                    StopAllSounds();
                                    StopLocalSound();
                                    GrabPalette();
                                }
                                PlayerList[player].currentWeapon = nPlayerOldWeapon[player];
                                if (nPlayerLives[player] != 0 && nNetTime != 0) {
                                    if (anim != 20) {
                                        pSprite.setPicnum(GetSeqPicnum(25, 120, 0));
                                        pSprite.setCstat(0);
                                        Sector sec = boardService.getSector(pSprite.getSectnum());
                                        if (sec != null) {
                                            pSprite.setZ(sec.getFloorz());
                                        }
                                    }
                                    RestartPlayer(player);
                                    dspr = nDoppleSprite[player];
                                    pSprite = boardService.getSprite(PlayerList[player].spriteId);
                                    pDSprite = boardService.getSprite(nDoppleSprite[player]);
                                    if (pSprite == null || pDSprite == null) {
                                        throw new AssertException("Player sprite == null");
                                    }
                                } else {
                                    if (levelnum == 20) {
                                        DoFailedFinalScene();
                                    } else {
                                        DoGameOverScene();
                                    }
                                }
                            }
                        }
                    }
                } else {

                    if ((sPlayerInput[player].bits & nTurnAround) != 0) {
                        if (PlayerList[player].turnAround == 0) {
                            PlayerList[player].turnAround = -1024;
                        }
                    }

                    if (PlayerList[player].turnAround < 0) {
                        PlayerList[player].turnAround =  BClipHigh(PlayerList[player].turnAround + 128, 0);
                        PlayerList[player].ang = BClampAngle(PlayerList[player].ang + 128);
                        pSprite.setAng((int) PlayerList[player].ang);
                    }

                    if (PlayerList[player].AirMaskAmount > 0) {
                        PlayerList[player].AirMaskAmount--;
                        if (PlayerList[player].AirMaskAmount == 150 && player == nLocalPlayer) {
                            PlayAlert("MASK IS ABOUT TO EXPIRE", player);
                        }
                    }

                    if (PlayerList[player].invisibility == 0) {
                        nBreathTimer[player]--;
                        if (nBreathTimer[player] <= 0) {
                            nBreathTimer[player] = 90;
                            if (bViewUnderwater) {
                                if (PlayerList[player].AirMaskAmount <= 0) {
                                    PlayerList[player].AirAmount -= 25;
                                    if (PlayerList[player].AirAmount <= 0) {
                                        PlayerList[player].HealthAmount += 4 * PlayerList[player].AirAmount;
                                        if (PlayerList[player].HealthAmount <= 0) {
                                            PlayerList[player].HealthAmount = 0;
                                            StartDeathSeq(player, 0);
                                        }
                                        if (player == nLocalPlayer) {
                                            SetHealthFrame(-1);
                                        }
                                        PlayerList[player].AirAmount = 0;
                                        if (PlayerList[player].HealthAmount >= 300) {
                                            D3PlayFX(StaticSound[19], spr);
                                        } else {
                                            D3PlayFX(StaticSound[79], spr);
                                        }
                                    } else {
                                        D3PlayFX(StaticSound[25], spr);
                                    }
                                } else {
                                    if (player == nLocalPlayer) {
                                        BuildStatusAnim(132, 0);
                                    }
                                    D3PlayFX(StaticSound[30] | 0x1000, spr);
                                    PlayerList[player].AirAmount = 100;
                                }
                                DoBubbles(player);
                                SetAirFrame();
                            } else {
                                if (player == nLocalPlayer) {
                                    BuildStatusAnim(132, 0);
                                }
                            }
                        }
                    }

                    if (bViewUnderwater) {
                        if (nPlayerTorch[player] > 0) {
                            nPlayerTorch[player] = 0;
                            engine.getPaletteManager().SetTorch(player, 0);
                        }
                    } else {
                        if (totalvel[player] > 25) {
                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null && pSprite.getZ() > sec.getFloorz() && SectDepth[pSprite.getSectnum()] != 0
                                    && SectSpeed[pSprite.getSectnum()] == 0 && SectDamage[pSprite.getSectnum()] == 0) {
                                D3PlayFX(StaticSound[42], spr);
                            }
                        }

                        if (oUnderwater) {
                            if (PlayerList[player].AirAmount < 50) {
                                D3PlayFX(StaticSound[14], spr);
                            }
                            nBreathTimer[player] = 1;
                        }
                        nBreathTimer[player]--;
                        if (nBreathTimer[player] <= 0) {
                            nBreathTimer[player] = 90;
                            if (player == nLocalPlayer) {
                                BuildStatusAnim(132, nLocalPlayer ^ player);
                            }
                        }
                        if (PlayerList[player].AirAmount < 100) {
                            PlayerList[player].AirAmount = 100;
                            SetAirFrame();
                        }
                    }
                    if (numplayers > 1) {
                        Sprite fspr = boardService.getSprite(nPlayerFloorSprite[player]);
                        if (fspr != null) {
                            fspr.setX(pSprite.getX());
                            fspr.setY(pSprite.getY());
                            if (fspr.getSectnum() != pSprite.getSectnum()) {
                                engine.mychangespritesect(nPlayerFloorSprite[player], pSprite.getSectnum());
                            }
                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null) {
                                fspr.setZ(sec.getFloorz());
                            }
                        }
                    }

                    int flags = 0;
                    if (PlayerList[player].HealthAmount >= 800) {
                        flags = 2;
                    }
                    if (PlayerList[player].MagicAmount >= 1000) {
                        flags |= 1;
                    }

                    engine.neartag(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getAng(), neartag, 1024, 2);
                    int v231 = feebtag(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), flags, 768);
                    Sprite s231 = boardService.getSprite(v231);

                    if (s231 != null) {
                        int nSnd = 9;
                        int tintGreen = 16;
                        int tintRed = 0;
                        int pickFlag = 0;
                        boolean magicPickuped = false;
                        int healthPickuped = -65536;

                        int statnum = s231.getStatnum();
                        if (statnum >= 900) {
                            int statBase = s231.getStatnum() - 900;
                            switch (statnum - 906) {
                                case 0:
                                    if (!AddAmmo(player, 1, s231.getHitag())) {
                                        break;
                                    }
                                    nSnd = StaticSound[69];
                                    pickFlag = 3;
                                    break;
                                case 1:
                                    if (!AddAmmo(player, 3, s231.getHitag())) {
                                        break;
                                    }
                                    nSnd = StaticSound[69];
                                    pickFlag = 3;
                                    break;
                                case 2:
                                    if (!AddAmmo(player, 2, s231.getHitag())) {
                                        break;
                                    }
                                    nSnd = StaticSound[69];
                                    pickFlag = 3;
                                    break;
                                case 4:
                                case 9:
                                case 10:
                                case 18:
                                case 25:
                                case 28:
                                case 29:
                                case 30:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 45:
                                case 52:
                                    pickFlag = 3;
                                    break;
                                case 3:
                                case 21:
                                case 49:
                                    if (!AddAmmo(player, 4, 1)) {
                                        break;
                                    }

                                    nSnd = StaticSound[69];
                                    if ((nPlayerWeapons[player] & 0x10) == 0) {
                                        nPlayerWeapons[player] |= 0x10;
                                        SetNewWeaponIfBetter(player, 4);
                                    }
                                    if (statBase != 55) {
                                        pickFlag = 3;
                                        break;
                                    }

                                    s231.setCstat( 32768);
                                    DestroyItemAnim(v231);
                                    pickFlag = 2;
                                    break;
                                case 5:
                                    GrabMap();
                                    pickFlag = 3;
                                    break;
                                case 6:
                                case 7:
                                case 8:
                                    if (s231.getHitag() == 0) {
                                        break;
                                    }

                                    nSnd = 20;
                                    int v123 = 40;
                                    int v1151 = 1;
                                    switch (statnum - 906) {
                                        case 7:
                                            v123 = 160;
                                            break;
                                        case 8:
                                            v1151 = -1;
                                            v123 = -200;
                                            break;
                                    }
                                    if (v123 > 0 && (flags & 2) != 0) {
                                        break;
                                    }

                                    if (PlayerList[player].invisibility == 0 || v123 > 0) {
                                        PlayerList[player].HealthAmount += v123;
                                        healthPickuped = v123;
                                        if (PlayerList[player].HealthAmount <= 800) {
                                            if (PlayerList[player].HealthAmount <= 0) {
                                                nSnd = -1;
                                                StartDeathSeq(player, 0);
                                            }
                                        } else {
                                            PlayerList[player].HealthAmount = 800;
                                        }
                                    }
                                    if (player == nLocalPlayer) {
                                        SetHealthFrame(v1151);
                                    }
                                    if (statBase == 12) {
                                        s231.setHitag(0);
                                        s231.setPicnum(s231.getPicnum() + 1);
                                        engine.changespritestat( v231,  0);
                                        pickFlag = 2;
                                        break;
                                    }
                                    if (statBase == 14) {
                                        tintRed = tintGreen;
                                        tintGreen = 0;
                                        nSnd = 22;
                                    } else {
                                        nSnd = 21;
                                    }
                                    pickFlag = 3;
                                    break;
                                case 11:
                                    PlayerList[player].AirAmount =  BClipHigh(PlayerList[player].AirAmount + 10, 100);
                                    SetAirFrame();
                                    if (nBreathTimer[player] < 89) {
                                        D3PlayFX(StaticSound[13], spr);
                                    }
                                    nBreathTimer[player] = 90;
                                    break;
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    int v130 = 0;
                                    switch (statnum - 906) {
                                        case 13:
                                            v130 = 1;
                                            break;
                                        case 14:
                                            v130 = 3;
                                            break;
                                        case 15:
                                            v130 = 4;
                                            break;
                                        case 16:
                                            v130 = 2;
                                            break;
                                        case 17:
                                            v130 = 5;
                                            break;
                                    }
                                    if (GrabItem(player, v130)) {
                                        pickFlag = 3;
                                    }
                                    break;
                                case 19:
                                    nSnd = -1;
                                    if (nPlayerLives[player] >= 5) {
                                        break;
                                    }
                                    nPlayerLives[player]++;
                                    if (player == nLocalPlayer) {
                                        BuildStatusAnim(2 * (nPlayerLives[player] - 1) + 146, 0);
                                    }
                                    tintGreen = 32;
                                    tintRed = 32;
                                    pickFlag = 3;
                                    break;
                                case 20:
                                case 22:
                                case 46:
                                case 23:
                                case 47:
                                case 24:
                                case 48:
                                case 26:
                                case 50:
                                case 27:
                                case 51:
                                    int v251 = 1;
                                    int v136 = 0;
                                    switch (statnum - 906) {
                                        case 20:
                                            v251 = 0;
                                            break;
                                        case 22:
                                        case 46:
                                            v136 = 6;
                                            break;
                                        case 23:
                                        case 47:
                                            v136 = 24;
                                            v251 = 2;
                                            break;
                                        case 24:
                                        case 48:
                                            v136 = 100;
                                            v251 = 3;
                                            break;
                                        case 26:
                                        case 50:

                                            v136 = 20;
                                            v251 = 5;
                                            break;
                                        case 27:
                                        case 51:
                                            v251 = 6;
                                            v136 = 2;
                                            break;
                                    }

                                    int v261 = 1 << v251;
                                    if ((nPlayerWeapons[player] & (1 << v251)) != 0) {
                                        if (levelnum > 20) {
                                            AddAmmo(player, weaponInfo[1].field_1A, v136);
                                        }
                                    } else {
                                        SetNewWeaponIfBetter(player, v251);
                                        nPlayerWeapons[player] |= v261;
                                        AddAmmo(player, weaponInfo[v251].field_1A, v136);
                                        nSnd = StaticSound[72];
                                    }

                                    if (v251 == 2) {
                                        CheckClip(player);
                                    }

                                    if (statBase <= 50) {
                                        pickFlag = 3;
                                        break;
                                    }

                                    s231.setCstat( 32768);
                                    DestroyItemAnim(v231);
                                    pickFlag = 2;
                                    break;
                                case 31:
                                    if (!AddAmmo(player, 5, 1)) {
                                        break;
                                    }
                                    nSnd = StaticSound[69];
                                    pickFlag = 3;
                                    break;
                                case 32:
                                    if (!AddAmmo(player, 6, s231.getHitag())) {
                                        break;
                                    }
                                    nSnd = StaticSound[69];
                                    pickFlag = 3;
                                    break;
                                case 39:
                                case 40:
                                case 41:
                                case 42:
                                    int v140 = 0;
                                    switch (statnum - 906) {
                                        case 39:
                                            break;
                                        case 40:
                                            v140 = 1;
                                            break;
                                        case 41:
                                            v140 = 2;
                                            break;
                                        case 42:
                                            v140 = 3;
                                            break;
                                    }
                                    nSnd = -1;
                                    int v115;
                                    if (v140 != 0) {
                                        v115 = 4096 << v140;
                                    } else {
                                        v115 = 4096;
                                    }

                                    if ((PlayerList[player].KeysBitMask & v115) != 0) {
                                        break;
                                    }
                                    if (player == nLocalPlayer) {
                                        BuildStatusAnim(2 * v140 + 36, nLocalPlayer ^ player);
                                    }
                                    PlayerList[player].KeysBitMask |= v115;
                                    if (numplayers <= 1) {
                                        pickFlag = 3;
                                        break;
                                    }

                                    pickFlag = 2;
                                    break;
                                case 43:
                                case 44:
                                    if (PlayerList[player].MagicAmount >= 1000) {
                                        break;
                                    }

                                    nSnd = StaticSound[67];
                                    PlayerList[player].MagicAmount =  BClipHigh(PlayerList[player].MagicAmount + 100,
                                            1000);
                                    if (player == nLocalPlayer) {
                                        SetMagicFrame();
                                    }
                                    pickFlag = 3;
                                    magicPickuped = true;
                                    break;
                                case 53:
                                    if (player == nLocalPlayer) {
                                        int v144 = s231.getOwner();
                                        AnimList[v144].nAction++;
                                        AnimFlags[v144] &= (byte) 0xEF;
                                        AnimList[v144].nSeq = 0;
                                        engine.changespritestat( v231,  899);
                                    }

                                    SetSavePoint(player, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pSprite.getAng());
                                    break;
                                case 54:
                                    if (!DemoScreen.isDemoPlaying()) {
                                        FinishLevel();
                                    }
                                    DestroyItemAnim(v231);
                                    engine.mydeletesprite( v231);
                                    break;
                            }

                            if ((pickFlag & 1) != 0) {
                                if (levelnum <= 20 || statBase == 25 || statBase == 50) {
                                    DestroyItemAnim(v231);
                                    engine.mydeletesprite( v231);
                                } else {
                                    StartRegenerate(v231);
                                }
                            }

                            if ((pickFlag & 2) != 0) {
                                if (player == nLocalPlayer) {
                                    if (nItemText[statBase] > -1/* && numplayers == 1*/) {
                                        final String message = getPickupString(statBase, magicPickuped, healthPickuped);

                                        StatusMessage(400, message, player);
                                    }
                                    TintPalette(tintRed, tintGreen, 0);
                                    if (nSnd > -1) {
                                        PlayLocalSound(nSnd, 0);
                                    }
                                }
                            }
                        }
                    }


                    if (bTouchFloor[0]) {
                        Sector sec = boardService.getSector(pSprite.getSectnum());
                        if (sec != null && sec.getLotag() > 0) {
                            SignalRun(sec.getLotag() - 1, nEvent5 | player);
                        }
                    }

                    if (osectnum != pSprite.getSectnum()) {
                        Sector osec = boardService.getSector(osectnum);
                        if (osec != null && osec.getLotag() > 0) {
                            SignalRun(osec.getLotag() - 1, nEvent7 | player);
                        }

                        Sector sec = boardService.getSector(pSprite.getSectnum());
                        if (sec != null && sec.getLotag() > 0) {
                            SignalRun(sec.getLotag() - 1, nEvent6 | player);
                        }
                    }

                    if (PlayerList[player].mummified != 0) {
                        if ((sPlayerInput[player].bits & nFire) != 0) {
                            FireWeapon(player);
                        }
                        if (anim != 15) {
                            if (totalvel[player] <= 1) {
                                next_anim = 13;
                            } else {
                                next_anim = 14;
                            }
                        }
                    } else {
                        if ((sPlayerInput[player].bits & nOpen) != 0) {
                            Wall nearWall = boardService.getWall(neartag.tagwall);
                            if (nearWall != null) {
                                if (nearWall.getLotag() > 0) {
                                    SignalRun(nearWall.getLotag() - 1, nEvent4 | player);
                                }
                            }
                            Sector nearSector = boardService.getSector(neartag.tagsector);
                            if (nearSector != null ) {
                                if (nearSector.getLotag() > 0) {
                                    SignalRun(nearSector.getLotag() - 1, nEvent4 | player);
                                }
                            }
                        }

                        if ((sPlayerInput[player].bits & nFire) != 0) {
                            FireWeapon(player);
                        } else {
                            StopFiringWeapon(player);
                        }

                        Sector sec = boardService.getSector(pSprite.getSectnum());
                        if (sec != null && nStandHeight > sec.getFloorz() - sec.getCeilingz()) {
                            v233 = 1;
                        }

                        if ((sPlayerInput[player].bits & nJump) != 0) {
                            PlayerList[player].crouch_toggle = false;
                            if (bUnderwater) {
                                pSprite.setZvel(-2048);
                                next_anim = 10;
                            } else if (bTouchFloor[0] && (anim < 6 || anim > 8)) {
                                pSprite.setZvel(-3584);
                                next_anim = 3;
                            }
                        } else if ((sPlayerInput[player].bits & nCrouch) != 0) {
                            if (bUnderwater) {
                                pSprite.setZvel(2048);
                                next_anim = 10;
                            } else {
                                int elev = PlayerList[player].eyelevel;
                                if (elev < -8320) {
                                    PlayerList[player].eyelevel =  (((-8320 - elev) >> 1) + elev);
                                }
                                if (totalvel[player] >= 1) {
                                    next_anim = 7;
                                } else {
                                    next_anim = 6;
                                }
                            }
                        } else {
                            if (PlayerList[player].HealthAmount > 0) {
                                PlayerList[player].eyelevel += (nActionEyeLevel[anim] - PlayerList[player].eyelevel) >> 1;
                                if (bUnderwater) {
                                    if (totalvel[player] <= 1) {
                                        next_anim = 9;
                                    } else {
                                        next_anim = 10;
                                    }
                                } else {
                                    if (v233 != 0) {
                                        if (totalvel[player] >= 1) {
                                            next_anim = 7;
                                        } else {
                                            next_anim = 6;
                                        }
                                    } else {
                                        if (totalvel[player] <= 1) {
                                            next_anim = 0;
                                        } else if (totalvel[player] <= 30) {
                                            next_anim = 2;
                                        } else {
                                            next_anim = 1;
                                        }
                                    }
                                }
                            }

                            if (v233 == 0 && (sPlayerInput[player].bits & nFire) != 0) {
                                if (bUnderwater) {
                                    next_anim = 11;
                                } else if (next_anim != 2 && next_anim != 1) {
                                    next_anim = 5;
                                }
                            }
                        }

                        int newWeapon = ((sPlayerInput[player].bits >> 13) & 0xF) - 1;
                        if (newWeapon != -1) {

                            switch (newWeapon) {
                                case 9: // last weapon
                                    if (((1 << PlayerList[player].lastUsedWeapon) & nPlayerWeapons[player]) != 0) {
                                        SetNewWeapon(player, PlayerList[player].lastUsedWeapon);
                                    }
                                    break;
                                case 7: // prev
                                case 8: // next
                                    int weap = WeaponChange(player, newWeapon == 8);
                                    SetNewWeapon(player, weap);
                                    break;
                                default:
                                    if (((1 << newWeapon) & nPlayerWeapons[player]) != 0) {
                                        SetNewWeapon(player, newWeapon);
                                    }
                                    break;
                            }
                        }
                    }

                    if (next_anim != anim && anim != 4) {
                        anim = next_anim;
                        PlayerList[player].anim_ =  next_anim;
                        PlayerList[player].animCount = 0;
                    }

                    if (player == nLocalPlayer) {
                        if ((sPlayerInput[player].bits & nLookUp) != 0) {
                            bLockPan = true;
                            if (!(PlayerList[player].horiz >= (isOriginal() ? MAXOHORIZ : MAXHORIZ))) {
                                PlayerList[player].horiz += 4;
                            }
                            nDestVertPan[player] = PlayerList[player].horiz;
                        } else if ((sPlayerInput[player].bits & nLookDown) != 0) {
                            bLockPan = true;
                            bPlayerPan = true;
                            if (PlayerList[player].horiz > ((isOriginal() ? MINOHORIZ : MINHORIZ) + 4)) {
                                PlayerList[player].horiz -= 4;
                            }
                            nDestVertPan[player] = PlayerList[player].horiz;
                        } else if ((sPlayerInput[player].bits & nAimDown) != 0) {
                            bLockPan = false;
                            bPlayerPan = false;
                            if (PlayerList[player].horiz > ((isOriginal() ? MINOHORIZ : MINHORIZ) + 4)) {
                                PlayerList[player].horiz -= 4;
                            }
                            nDestVertPan[player] = PlayerList[player].horiz;
                        } else if ((sPlayerInput[player].bits & nAimUp) != 0) {
                            bLockPan = false;
                            bPlayerPan = false;
                            if (!(PlayerList[player].horiz >= (isOriginal() ? MAXOHORIZ : MAXHORIZ))) {
                                PlayerList[player].horiz += 4;
                            }
                            nDestVertPan[player] = PlayerList[player].horiz;
                        } else if ((sPlayerInput[player].bits & nAimCenter) != 0) {
                            bLockPan = false;
                            bPlayerPan = false;
                            nDestVertPan[player] = 92;
                        }

                        if (sPlayerInput[player].horiz != 0) {
                            bLockPan = true;
                            bPlayerPan = true;
                            if (isOriginal()) {
                                PlayerList[player].horiz = BClipRange(PlayerList[player].horiz + sPlayerInput[player].horiz, MINOHORIZ, MAXOHORIZ);
                            } else {
                                PlayerList[player].horiz = BClipRange(PlayerList[player].horiz + sPlayerInput[player].horiz, MINHORIZ, MAXHORIZ);
                            }
                            nDestVertPan[player] = PlayerList[player].horiz;
                        }
                        sPlayerInput[player].nWeaponAim = (short) PlayerList[player].horiz;

                        if (totalvel[player] > 20) {
                            bPlayerPan = false;
                        }

                        int dVertPan = (int) (nDestVertPan[player] - PlayerList[player].horiz);
                        if (dVertPan != 0) {
                            int val = dVertPan / 4;
                            if (klabs(val) >= 4) {
                                if (val >= 4) {
                                    PlayerList[player].horiz += 4;
                                } else if (val <= -4) {
                                    PlayerList[player].horiz -= 4;
                                }
                            } else {
                                PlayerList[player].horiz += dVertPan / 2.0f;
                            }
                        }
                    }
                }

                if (player == nLocalPlayer) {
                    nLocalEyeSect = nPlayerViewSect[nLocalPlayer];
                    CheckAmbience(nLocalEyeSect);
                }
                int seqBase = (ActionSeq[anim].seq + SeqOffsets[PlayerList[player].seq]);
                MoveSequence(spr, seqBase, PlayerList[player].animCount);
                PlayerList[player].animCount++;
                if (PlayerList[player].animCount >= SeqSize[seqBase]) {
                    PlayerList[player].animCount = 0;
                    switch (PlayerList[player].anim_) {
                        case 3:
                            PlayerList[player].animCount =  (SeqSize[seqBase] - 1);
                            break;
                        case 4:
                            PlayerList[player].anim_ = 0;
                            break;
                        case 16:
                            PlayerList[player].animCount =  (SeqSize[seqBase] - 1);
                            Sector sec = boardService.getSector(pSprite.getSectnum());
                            if (sec != null && pSprite.getZ() < sec.getFloorz()) {
                                pSprite.setZ(pSprite.getZ() + 256);
                            }
                            if (RandomSize(5) == 0) {
                                int[] out = WheresMyMouth(player);
                                BuildAnim(-1, 71, 0, out[0], out[1], pSprite.getZ() + 3840, out[3], 75, -128);
                            }
                            break;
                        case 17:
                            PlayerList[player].anim_ = 18;
                            break;
                        case 19:
                            pSprite.setCstat(pSprite.getCstat() | 0x8000);
                            PlayerList[player].anim_ = 20;
                            break;
                    }
                }

                if (player == nLocalPlayer) {
                    initx = pSprite.getX();
                    inity = pSprite.getY();
                    initz = pSprite.getZ();
                    initsect = pSprite.getSectnum();
                    inita = pSprite.getAng();
                }

                if (PlayerList[player].HealthAmount == 0) {
                    nYDamage[player] = 0;
                    nXDamage[player] = 0;
                    if (PlayerList[player].eyelevel >= -2816) {
                        PlayerList[player].eyelevel = -2816;
                        dVertPan[player] = 0;
                    } else {
                        if (PlayerList[player].horiz >= 92) {
                            PlayerList[player].horiz += dVertPan[player];
                            if (PlayerList[player].horiz < 200) {
                                if (PlayerList[player].horiz <= 92) {
                                    if ((SectFlag[pSprite.getSectnum()] & 0x2000) == 0) {
                                        SetNewWeapon(player, nDeathType[player] + 8);
                                    }
                                }
                            } else {
                                PlayerList[player].horiz = 199;
                            }
                            --dVertPan[player];
                        } else {
                            PlayerList[player].horiz = 91;
                            PlayerList[player].eyelevel -= dVertPan[player] << 8;
                        }
                    }
                }
                pDSprite.setX(pSprite.getX());
                pDSprite.setY(pSprite.getY());
                pDSprite.setZ(pSprite.getZ());
                if (SectAbove[pSprite.getSectnum()] <= -1) {
                    pDSprite.setCstat( 32768);
                } else {
                    pDSprite.setAng(pSprite.getAng());
                    engine.mychangespritesect( dspr, SectAbove[pSprite.getSectnum()]);
                    pDSprite.setCstat(257);
                }
                MoveWeapons(player);
                return;
            case nEventView:
                Renderer renderer = game.getRenderer();
                TSprite tsp = renderer.getRenderedSprites().get((short) (nStack & 0xFFFF));
                PlotSequence(tsp, ActionSeq[anim].seq + SeqOffsets[PlayerList[player].seq],
                        PlayerList[player].animCount, ActionSeq[anim].a2 ? 1 : 0);
                return;

            case nEventDamage:
                if (a2 == 0) {
                    return;
                }
                v229 = (short) (nStack & 0xFFFF);
                break;
            case nEventRadialDamage:
                if (PlayerList[player].HealthAmount > 0) {
                    if ((a2 = CheckRadialDamage(spr)) == 0) {
                        return;
                    }
                    v229 = (short) nRadialOwner;
                }
                break;
            default:
                return;
        }

        if (PlayerList[player].HealthAmount != 0) {
            if (PlayerList[player].invisibility == 0) {
                PlayerList[player].HealthAmount -= a2;
                if (player == nLocalPlayer) {
                    TintPalette(a2 >> 2, 0, 0);
                    SetHealthFrame(-1);
                }
            }

            if (PlayerList[player].HealthAmount > 0) {
                if (a2 > 40 || (totalmoves & 0xF) < 2) {
                    if (PlayerList[player].invisibility == 0) {
                        if ((SectFlag[pSprite.getSectnum()] & 0x2000) != 0) {
                            if (anim != 12) {
                                PlayerList[player].animCount = 0;
                                PlayerList[player].anim_ = 12;
                            }
                        } else if (anim != 4) {
                            PlayerList[player].animCount = 0;
                            PlayerList[player].anim_ = 4;
                            if (v229 > -1) {
                                nPlayerSwear[player]--;
                                if (nPlayerSwear[player] <= 0) {
                                    D3PlayFX(StaticSound[52], dspr);
                                    nPlayerSwear[player] =  (RandomSize(3) + 4);
                                }
                            }
                        }
                    }
                }
            } else {
                if (v229 < 0) {
                    nPlayerScore[player]--;
                }
//				else  XXX Crash v229 == 20811
//					if (boardService.getSprite(v229).statnum == 100) {
//					if (GetPlayerFromSprite(v229) == player)
//						--nPlayerScore[player];
//					else
//						++nPlayerScore[player];
//				}

                int deathType;
                if ((nStack & EVENT_MASK) == nEventRadialDamage) {
                    for (int i = 122; i <= 131; i++) {
                        BuildCreatureChunk(spr, GetSeqPicnum(25, i, 0));
                    }
                    deathType = 1;
                } else {
                    deathType = 0;
                }
                StartDeathSeq(player, deathType);
            }
        }
    }

    private static String getPickupString(int statBase, boolean magicPickuped, int healthPickuped) {
        String message = gString[nItemText[statBase] + nItemTextIndex];
        if (magicPickuped) {
            message += " +10%";
        } else if (healthPickuped != -65536) {
            if (healthPickuped > 0) {
                message += " +";
            } else {
                message += " ";
            }
            message += (((healthPickuped * 100) / 800)) + "%";
        }
        return message;
    }

//    public static void RestorePlayer(final short player) {
//        Gdx.app.postRunnable(new Runnable() {
//            @Override
//            public void run() {
//                if (nNetPlayerCount == 0 && lastlevel == levelnum) {
//                    RestoreSavePoint(player);
//                    RestartPlayer(player);
//                    InitPlayerKeys(player);
//                }
//            }
//        });
//    }

    public static void SetCounter(int value) {
        if (value <= 999) {
            if (value < 0) {
                value = 0;
            }
        } else {
            value = 999;
        }

        nCounterDest = value;
    }

    public static void SetPlayerMummified(int nPlayer, int mummified) {
        PlayerList[nPlayer].mummified =  mummified;
        int nSprite = PlayerList[nPlayer].spriteId;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            pSprite.setXvel(0);
            pSprite.setYvel(0);
        }

        if (mummified != 0) {
            PlayerList[nPlayer].anim_ = 13;
            PlayerList[nPlayer].seq = 10;
        } else {
            PlayerList[nPlayer].anim_ = 0;
            PlayerList[nPlayer].seq = 25;
        }

        PlayerList[nPlayer].animCount = 0;
    }

    public static void SetCounterImmediate(int a1) {
        SetCounter(a1);
        nCounter = nCounterDest;
        SetCounterDigits();
    }

    public static void SetCounterDigits() {
        nDigit[2] = 3 * (nCounter / 100 % 10);
        nDigit[1] = 3 * (nCounter / 10 % 10);
        nDigit[0] = 3 * (nCounter % 10);
    }

    public static void UseItem(int nPlayer, int nItem) {
        switch (nItem) {
            case 4:
                UseEye(nPlayer);
                break;
            case 5:
                UseMask(nPlayer);
                break;
            case 0:
                UseHeart(nPlayer);
                break;
            case 2:
                UseTorch(nPlayer);
                break;
            case 1:
                UseScarab(nPlayer);
                break;
            case 3:
                UseHand(nPlayer);
                break;
        }

        int amount = --PlayerList[nPlayer].ItemsAmount[nItem];
        if (nPlayer == nLocalPlayer) {
            BuildStatusAnim(2 * amount + 156, nLocalPlayer ^ nPlayer);
        }
        int i = nItem;
        if (amount == 0) {
            i = 0;
            while (i < 6 && PlayerList[nPlayer].ItemsAmount[i] <= 0) {
                i++;
            }
            if (i == 6) {
                i = -1;
            }
        }
        PlayerList[nPlayer].MagicAmount -= nItemMagic[nItem];
        SetPlayerItem(nPlayer, i);
        if (nPlayer == nLocalPlayer) {
            SetMagicFrame();
        }
    }

    public static void UseEye(int nPlayer) {
        int nSprite = PlayerList[nPlayer].spriteId;
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if (nPlayerInvisible[nPlayer] >= 0) {
            nPlayerInvisible[nPlayer] = 900;
        }
        pSprite.setCstat(pSprite.getCstat() | 0x8000);
        int nFloorSpr = nPlayerFloorSprite[nPlayer];
        Sprite pFloorSpr = boardService.getSprite(nFloorSpr);

        if (pFloorSpr != null) {
            pFloorSpr.setCstat(pFloorSpr.getCstat() | 0x8000);
        }

        if (nPlayer == nLocalPlayer) {
            ItemFlash();
            D3PlayFX(StaticSound[31], nSprite);
        }
    }

    public static void UseMask(int nPlayer) {
        PlayerList[nPlayer].AirMaskAmount = 1350;
        PlayerList[nPlayer].AirAmount = 100;
        if (nPlayer == nLocalPlayer) {
            SetAirFrame();
            D3PlayFX(StaticSound[31], PlayerList[nPlayer].spriteId);
        }
    }

    public static void UseHeart(int nPlayer) {
        if (PlayerList[nPlayer].HealthAmount < 800) {
            PlayerList[nPlayer].HealthAmount = 800;
        }
        if (nPlayer == nLocalPlayer) {
            ItemFlash();
            SetHealthFrame(1);
            D3PlayFX(StaticSound[31], PlayerList[nPlayer].spriteId);
        }
    }

    public static void UseScarab(int nPlayer) {
        if (PlayerList[nPlayer].invisibility < 900) {
            PlayerList[nPlayer].invisibility = 900;
        }
        if (nPlayer == nLocalPlayer) {
            ItemFlash();
            D3PlayFX(StaticSound[31], PlayerList[nPlayer].spriteId);
        }
    }

    public static void UseHand(int nPlayer) {
        nPlayerDouble[nPlayer] = 1350;
        if (nPlayer == nLocalPlayer) {
            ItemFlash();
            D3PlayFX(StaticSound[31], PlayerList[nPlayer].spriteId);
        }
    }

    public static void UseCurItem(int nPlayer, int nItem) {
        if (nItem >= 0 && PlayerList[nPlayer].ItemsAmount[nItem] > 0
                && nItemMagic[nItem] <= PlayerList[nPlayer].MagicAmount) {
            sPlayerInput[nPlayer].field_F = (byte) nItem;

            System.err.println("Use item " + nItem);
        }
    }

    public static void SetNextItem(int a1) {
        int nItem = nPlayerItem[a1];
        int nNextItem = 6;
        for (; nNextItem > 0; nNextItem--) {
            if (++nItem == 6) {
                nItem = 0;
            }
            if (PlayerList[a1].ItemsAmount[nItem] != 0) {
                break;
            }
        }
        if (nNextItem > 0) {
            SetPlayerItem(a1, nItem);
        }
    }

    public static void SetPrevItem(int a1) {
        int nItem = nPlayerItem[a1];
        if (nItem == -1) {
            return;
        }

        int nNextItem = 6;
        for (; nNextItem > 0; nNextItem--) {
            if (--nItem < 0) {
                nItem = 5;
            }
            if (PlayerList[a1].ItemsAmount[nItem] != 0) {
                break;
            }
        }
        if (nNextItem > 0) {
            SetPlayerItem(a1, nItem);
        }
    }

    public static void ItemFlash() {
        TintPalette(4, 4, 4);
    }

    public static void FillItems(int nPlayer) {
        for (int i = 0; i < 6; i++) {
            PlayerList[nPlayer].ItemsAmount[i] = 5;
        }
        PlayerList[nPlayer].MagicAmount = 1000;
        if (nPlayer == nLocalPlayer) {
            ItemFlash();
            SetMagicFrame();
        }

        if (nPlayerItem[nPlayer] == -1) {
            SetPlayerItem(nPlayer, 0);
        }

        StatusMessage(750, "All items loaded for player " + nPlayer, nLocalPlayer);
    }

    public static void PlayAlert(String message, int nPlayer) {
        StatusMessage(300, message, nPlayer);
        PlayLocalSound(StaticSound[63], 0);
    }

    public static class Action {
        public final int seq;
        final boolean a2;

        public Action(int seq, boolean a2) {
            this.seq =  seq;
            this.a2 = a2;
        }
    }
}
