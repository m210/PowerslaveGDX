// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.SafeLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Random {

    private static int randA;
    private static int randB;
    private static int randC;

    public static void InitRandom() {
        randA = 0;
        randB = 286331153;
        randC = 16843009;
    }

    public static void saveRandom(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, randA);
        StreamUtils.writeInt(os, randB);
        StreamUtils.writeInt(os, randC);
    }

    public static void loadRandom(SafeLoader loader, InputStream is) throws IOException {
        loader.randA = StreamUtils.readInt(is);
        loader.randB = StreamUtils.readInt(is);
        loader.randC = StreamUtils.readInt(is);
    }

    public static void loadRandom(SafeLoader loader) {
        randA = loader.randA;
        randB = loader.randB;
        randC = loader.randC;
    }

    public static int RandomBit() {
        randA = (randA >> 1) | ((((randA & 0xFF) ^ (((randA >> 1) ^ (randA >> 2) ^ (randA >> 31) ^ (randA >> 6) ^ (randA >> 4)) & 0xFF)) & 1) << 31);
        randB = (randB >> 1) | (((((randB >> 2) & 0xFF) ^ (randB >> 30)) & 1) << 30);
        randC = (randC >> 1) | (((((randC >> 1) & 0xFF) ^ (randC >> 28)) & 1) << 28);
        return (((randA == 0) ? 1 : 0) & (randC & 0xFF) | (randB & randA) & 0xFF) & 1;
    }

    public static int RandomByte() {
        return (RandomBit() << 7) | (RandomBit() << 6) | (RandomBit() << 5) | (RandomBit() << 4) | (RandomBit() << 3) | (RandomBit() << 2) | (RandomBit() << 1) | RandomBit();
    }

    public static int RandomWord() {
        return (RandomByte() << 8) | RandomByte();
    }

    public static int RandomLong() {
        return (RandomWord() << 16) | RandomWord();
    }

    public static int RandomSize(int len) {
        int result = 0;
        while (len-- > 0) {
            result = 2 * result | RandomBit();
        }
        return result;
    }
}
