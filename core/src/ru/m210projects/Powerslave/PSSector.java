// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.BlockInfo;
import ru.m210projects.Powerslave.Type.MoveSectStruct;
import ru.m210projects.Powerslave.Type.SafeLoader;
import ru.m210projects.Powerslave.Type.TrailPointStruct;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Bullet.*;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Map.SnapBobs;
import static ru.m210projects.Powerslave.Map.nPushBlocks;
import static ru.m210projects.Powerslave.Object.*;
import static ru.m210projects.Powerslave.Palette.TintPalette;
import static ru.m210projects.Powerslave.Random.RandomSize;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.Sound.PlayFX2;
import static ru.m210projects.Powerslave.Sound.StaticSound;
import static ru.m210projects.Powerslave.Sprites.CheckRadialDamage;
import static ru.m210projects.Powerslave.Sprites.DamageEnemy;
import static ru.m210projects.Powerslave.Switch.*;
import static ru.m210projects.Powerslave.View.StatusMessage;

public class PSSector {

    public static int overridesect;
    public static final int[] NearSector = new int[MAXSECTORS];
    public static int NearCount;

    public static int LinkCount;
    public static final byte[][] LinkMap = new byte[1024][8];
    private static final Vector2 tmpVec = new Vector2();

    public static Vector2 MoveSector(int nObject, int angle, int xvec, int yvec) {
        Sector pObject = boardService.getSector(nObject);
        if (pObject != null) {
            Wall wall = boardService.getWall(pObject.getWallptr());
            if (wall == null) {
                return tmpVec.setZero();
            }

            int v4, v5;
            if (angle < 0) {
                v5 = yvec;
                v4 = xvec;
                angle = engine.GetMyAngle(xvec, yvec);
            } else {
                v4 = EngineUtils.sin((angle + 512) & 0x7FF) << 6;
                v5 = EngineUtils.sin(angle & 0x7FF) << 6;
            }

            int v53 = pObject.getFloorz();
            int flags = SectFlag[nObject] & 0x2000;

            BlockInfo bInfo = sBlockInfo[pObject.getExtra()];

            int block_x = bInfo.cx;
            int oldx = block_x;
            int block_y = bInfo.cy;
            int oldy = block_y;

            int nSector = nObject;
            final int nNextSector = wall.getNextsector();
            Sector nextSec = boardService.getSector(nNextSector);

            int z1 = 0, z2 = 0;
            if (nextSec != null) {
                if (flags != 0) {
                    z2 = pObject.getCeilingz();
                    z1 = nextSec.getCeilingz() + 256;
                    pObject.setCeilingz(nextSec.getCeilingz());
                } else {
                    z2 = pObject.getFloorz();
                    z1 = nextSec.getFloorz() - 256;
                    pObject.setFloorz(nextSec.getFloorz());
                }
            }

            engine.clipmove(block_x, block_y, z1, nSector, v4, v5, bInfo.field_8, 0, 0, CLIPMASK1);
            if (clipmove_sectnum > -1) {
                block_x = clipmove_x;
                block_y = clipmove_y;
                nSector = clipmove_sectnum;
            }

            int dx = block_x - oldx;
            int dy = block_y - oldy;
            if (nSector == nNextSector || nSector == nObject) {
                if (flags == 0) {
                    block_x = oldx;
                    block_y = oldy;
                    engine.clipmove(block_x, block_y, z2, nSector, v4, v5, bInfo.field_8, 0, 0, CLIPMASK1);
                    if (clipmove_sectnum > -1) {
                        block_x = clipmove_x;
                        block_y = clipmove_y;
                    }

                    if (klabs(dx) > klabs(block_x - oldx)) {
                        dx = block_x - oldx;
                    }
                    if (klabs(dy) > klabs(block_y - oldy)) {
                        dy = block_y - oldy;
                    }
                }
            } else {
                dx = 0;
                dy = 0;
            }

            if ((dx | dy) != 0) {
                for (ListNode<Sprite> node = boardService.getSectNode(nObject), next; node != null; node = next) {
                    next = node.getNext();
                    int i = node.getIndex();
                    Sprite pSprite = node.get();
                    game.pInt.setsprinterpolate(i, pSprite);
                    if (pSprite.getStatnum() >= 99) {
                        if (flags != 0 || pSprite.getZ() != z2 || (pSprite.getCstat() & 0x8000) != 0) {
                            block_x = pSprite.getX();
                            block_y = pSprite.getY();
                            nSector = nObject;

                            engine.clipmove(block_x, block_y, pSprite.getZ(), nSector, -dx, -dy, 4 * pSprite.getClipdist(), 0, 0, CLIPMASK0);
                            if (clipmove_sectnum > -1) {
                                pSprite.setZ(clipmove_z);
                                nSector = clipmove_sectnum;
                            }
                            if (nSector >= 0 && nSector < 1024 && nSector != nObject) {
                                engine.mychangespritesect(i, nSector);
                            }
                        }
                    } else {
                        pSprite.setX(pSprite.getX() + dx);
                        pSprite.setY(pSprite.getY() + dy);
                    }
                }

                for (ListNode<Sprite> node = boardService.getSectNode(nNextSector), next; node != null; node = next) {
                    next = node.getNext();
                    int i = node.getIndex();
                    Sprite pSprite = node.get();
                    if (pSprite.getStatnum() >= 99) {
                        block_x = pSprite.getX();
                        block_y = pSprite.getY();
                        nSector = nNextSector;

                        int dist = 4 * pSprite.getClipdist();
                        engine.clipmove(block_x, block_y, pSprite.getZ(), nSector, -dx - (long) EngineUtils.sin((angle + 512) & 0x7FF) * dist, -dy - (long) EngineUtils.sin(angle) * dist, dist, 0, 0, CLIPMASK0);
                        if (clipmove_sectnum > -1) {
                            pSprite.setZ(clipmove_z);
                            nSector = clipmove_sectnum;
                        }

                        if (nSector != nNextSector && (nSector == nObject || nNextSector == nObject)) {
                            if (nSector != nObject || v53 >= pSprite.getZ()) {
                                if (nSector >= 0 && nSector < 1024) {
                                    engine.mychangespritesect( i, nSector);
                                }
                            } else {
                                engine.movesprite( i, (dx << 14) + EngineUtils.sin((angle + 512) & 0x7FF) * pSprite.getClipdist(), (dy << 14) + pSprite.getClipdist() * EngineUtils.sin(angle), 0, 0, 0, 0);
                            }
                        }
                    }
                }

                for (short i = pObject.getWallptr(); i < pObject.getWallptr() + pObject.getWallnum(); i++) {
                    Wall w = boardService.getWall(i);
                    if (w != null) {
                        engine.dragpoint(i, dx + w.getX(), dy + w.getY());
                    }
                }
                bInfo.cx += dx;
                bInfo.cy += dy;
            }

            dx <<= 14;
            dy <<= 14;
            if (flags == 0) {
                for (ListNode<Sprite> node = boardService.getSectNode(nObject); node != null; node = node.getNext()) {
                    int i = node.getIndex();
                    Sprite pSprite = node.get();
                    if (pSprite.getStatnum() >= 99 && z2 == pSprite.getZ() && (pSprite.getCstat() & 0x8000) == 0) {
                        nSector =  nObject;
                        game.pInt.setsprinterpolate(i, pSprite);

                        engine.clipmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector, dx, dy, 4 * pSprite.getClipdist(), 5120, -5120, CLIPMASK0);
                        if (clipmove_sectnum > -1) {
                            pSprite.setX(clipmove_x);
                            pSprite.setY(clipmove_y);
                            pSprite.setZ(clipmove_z);
                        }
                    }
                }
            }

            if (flags != 0) {
                pObject.setCeilingz(z2);
            } else {
                pObject.setFloorz(z2);
            }
            tmpVec.set(dx, dy);
        } else {
            tmpVec.set(xvec, yvec);
        }
        return tmpVec;
    }

    public static int feebtag(int x, int y, int z, int sectnum, int a6, int dist) {
        Sector sector = boardService.getSector(sectnum);
        if (sector == null) {
            return -1;
        }

        int out = -1;
        int sec = sectnum;
        for (ListNode<Wall> wn = sector.getWallNode(); wn != null; wn = wn.getNext()) {
            if (sec != -1) {
                for (ListNode<Sprite> node = boardService.getSectNode(sec); node != null; node = node.getNext()) {
                    Sprite pSprite = node.get();
                    if (pSprite.getStatnum() >= 900 && (pSprite.getCstat() & 0x8000) == 0) {
                        int v11 = pSprite.getX() - x;
                        int v14 = pSprite.getY() - y;
                        int v13 = pSprite.getZ() - z;

                        if (v13 < 5120 && v13 > -25600) {
                            int v15 = EngineUtils.sqrt(v14 * v14 + v11 * v11);
                            if (v15 < dist && (pSprite.getStatnum() != 950 && pSprite.getStatnum() != 949 || (a6 & 1) == 0) && (pSprite.getStatnum() != 912 && pSprite.getStatnum() != 913 || (a6 & 2) == 0)) {
                                dist = v15;
                                out = node.getIndex();
                            }
                        }
                    }

                }
            }
            sec = wn.get().getNextsector();
        }

        return out;
    }

    public static void DoMovingSects() {
        for (int i = 0; i < nMoveSects; i++) {
            MoveSectStruct v0 = sMoveSect[i];
            if (v0.field_0 != -1) {
                int v2 = v0.field_E;
                if (v2 == -1 || channel[v2].field_4 != 0) {
                    Sector sec = boardService.getSector(v0.field_0);
                    if (sec == null) {
                        continue;
                    }

                    BlockInfo v3 = sBlockInfo[sec.getExtra()];
                    if (v0.field_4 == -1) {
                        int v4 = v0.field_6;
                        if ((v4 & 0x20) != 0) {
                            ChangeChannel(v0.field_E, 0);
                        }
                        int v5 = v4 & 0x10;
                        if (v5 != 0) {
                            sMoveDir[i] = (byte) -sMoveDir[i];
                            if (sMoveDir[i] <= 0) {
                                v0.field_4 = sTrail[v0.field_2].field_4;
                            } else {
                                v0.field_4 = sTrail[v0.field_2].field_0;
                            }
                        } else {
                            v0.field_4 = sTrail[v0.field_2].field_0;
                        }
                    }

                    TrailPointStruct v9 = sTrailPoint[v0.field_4];
                    int dx = v9.x - v3.cx;
                    int dy = v9.y - v3.cy;
                    int ang = engine.GetMyAngle(dx, dy) & 0x7FF;
                    int v29 = 16 * EngineUtils.sin(ang) * v0.field_A;
                    int v30 = 16 * EngineUtils.sin((ang + 512) & 0x7FF) * v0.field_A;
                    int v16 = dx << 14;
                    int v20 = dy << 14;

                    if (klabs(v30) > klabs(v16) || klabs(v29) > klabs(v20)) {
                        v29 = v20;
                        v30 = v16;
                        if (sMoveDir[i] <= 0) {
                            v0.field_4 = nTrailPointPrev[v0.field_4];
                        } else {
                            v0.field_4 = nTrailPointNext[v0.field_4];
                        }
                    }

                    if (v0.field_8 != -1) {
                        Vector2 vec = MoveSector(v0.field_8, -1, v30, v29);
                        v30 = (int) vec.x;
                        v29 = (int) vec.y;
                    }
                    int v28 = v30;
                    int v27 = v29;
                    Vector2 vec = MoveSector(v0.field_0, -1, v30, v29);
                    v30 = (int) vec.x;
                    v29 = (int) vec.y;
                    if (v30 != v28 || v29 != v27) {
                        MoveSector(v0.field_8, -1, v28, v27);
                        MoveSector(v0.field_8, -1, v30, v29);
                    }
                }
            }
        }
    }

    public static Vector2 CheckSectorFloor(int nSector, int z, int dx, int dy) {
        tmpVec.set(dx, dy);
        Sector sec = boardService.getSector(nSector);
        if (sec != null && SectSpeed[nSector] != 0) {
            int v10 = SectFlag[nSector] & 0x7FF;
            if (z >= sec.getFloorz()) {
                tmpVec.x += SectSpeed[nSector] * 8 * EngineUtils.sin((v10 + 512) & 0x7FF);
                tmpVec.y += SectSpeed[nSector] * 8 * EngineUtils.sin(v10 & 0x7FF);

            } else {
                if ((SectFlag[nSector] & 0x800) != 0) {
                    tmpVec.x += SectSpeed[nSector] * 16 * EngineUtils.sin((v10 + 512) & 0x7FF);
                    tmpVec.y += SectSpeed[nSector] * 16 * EngineUtils.sin(v10 & 0x7FF);
                }
            }
        }
        return tmpVec;
    }

    public static void saveSecExtra(OutputStream os) throws IOException {
        for (int i = 0; i < 1024; i++) {
            StreamUtils.writeShort(os, SectSoundSect[i]);
            StreamUtils.writeShort(os, SectSound[i]);
            StreamUtils.writeShort(os, SectAbove[i]);
            StreamUtils.writeShort(os, SectBelow[i]);
            StreamUtils.writeShort(os, SectDepth[i]);
            StreamUtils.writeShort(os, SectFlag[i]);
            StreamUtils.writeShort(os, SectSpeed[i]);
            StreamUtils.writeShort(os, SectDamage[i]);
        }
    }

    public static void loadSecExtra(SafeLoader loader, InputStream is) throws IOException {
        for (int i = 0; i < 1024; i++) {
            loader.SectSoundSect[i] =  StreamUtils.readShort(is);
            loader.SectSound[i] =  StreamUtils.readShort(is);
            loader.SectAbove[i] =  StreamUtils.readShort(is);
            loader.SectBelow[i] =  StreamUtils.readShort(is);
            loader.SectDepth[i] =  StreamUtils.readShort(is);
            loader.SectFlag[i] =  StreamUtils.readShort(is);
            loader.SectSpeed[i] =  StreamUtils.readShort(is);
            loader.SectDamage[i] =  StreamUtils.readShort(is);
        }
    }

    public static void loadSecExtra(SafeLoader loader) {
        System.arraycopy(loader.SectSoundSect, 0, SectSoundSect, 0, 1024);
        System.arraycopy(loader.SectSound, 0, SectSound, 0, 1024);
        System.arraycopy(loader.SectAbove, 0, SectAbove, 0, 1024);
        System.arraycopy(loader.SectBelow, 0, SectBelow, 0, 1024);
        System.arraycopy(loader.SectDepth, 0, SectDepth, 0, 1024);
        System.arraycopy(loader.SectFlag, 0, SectFlag, 0, 1024);
        System.arraycopy(loader.SectSpeed, 0, SectSpeed, 0, 1024);
        System.arraycopy(loader.SectDamage, 0, SectDamage, 0, 1024);
    }

    public static void SnapSectors(int a1, int a2, int a3) {
        Sector v3 = boardService.getSector(a1);
        Sector v4 = boardService.getSector(a2);
        if (v3 == null || v4 == null) {
            return;
        }

        int v29 = v3.getWallnum();
        int v30 = v4.getWallnum();
        int i = 0;
        while (i < v29) {
            int v5 = v4.getWallptr();
            int v6 = 0x7FFFFFF;
            int v7 = v3.getWallptr();
            int j = 0;
            int v9 = 0x7FFFFFF;
            int v31 = 0;
            Wall w7 = boardService.getWall(v7);
            if (w7 != null) {
                int v26 = w7.getX();
                int v24 = w7.getY();
                while (j < v30) {
                    Wall w5 = boardService.getWall(v7);
                    if (w5 != null) {
                        int v11 = v26 - w5.getX();
                        int v12 = v24 - w5.getY();
                        int v13 = v26 - w5.getX();
                        if (v13 < 0) {
                            v13 = -v13;
                        }
                        int v22 = v13;
                        int v14 = v12;
                        if (v12 < 0) {
                            v14 = -v12;
                        }
                        int v23 = v14 + v22;
                        int v15 = v6;
                        if (v6 < 0) {
                            v15 = -v6;
                        }
                        int v21 = v15;
                        int v16 = v9;
                        if (v9 < 0) {
                            v16 = -v9;
                        }
                        if (v23 < v16 + v21) {
                            v31 = v5;
                            v6 = v11;
                            v9 = v12;
                        }
                    }
                    ++j;
                    ++v5;
                }
            }
            ++i;
            ++v7;

            Wall w31 = boardService.getWall(v31);
            if (w31 != null) {
                engine.dragpoint(v31, v6 + w31.getX(), v9 + w31.getY());
            }
        }
        if (a3 != 0) {
            Sector s2 = boardService.getSector(a2);
            Sector s1 = boardService.getSector(a1);
            if (s1 != null && s2 != null) {
                s2.setCeilingz(s1.getFloorz());
            }
        }
        if ((SectFlag[a1] & 0x1000) != 0) {
            SnapBobs(a1, a2);
        }
    }

    public static int FindWallSprites(final int a1) {
        Sector sec = boardService.getSector(a1);
        if (sec == null) {
            return -1;
        }

        int miny = 0x7FFFFFFF;
        int maxy = -0x7FFFFFFE;
        int minx = 0x7FFFFFFF;
        int maxx = -0x7FFFFFFE;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall w = wn.get();
            if (minx > w.getX()) {
                minx = w.getX();
            }
            if (maxx < w.getX()) {
                maxx = w.getX();
            }
            if (miny > w.getY()) {
                miny = w.getY();
            }
            if (maxy < w.getY()) {
                maxy = w.getY();
            }
        }
        int x1 = minx - 5;
        int y1 = miny - 5;
        int x2 = maxx + 5;
        int y2 = maxy + 5;
        int spr = -1;

        for (int i = 0; i < MAXSPRITESV7; i++) {
            Sprite s = boardService.getSprite(i);
            if (s != null && s.getLotag() == 0 && (s.getCstat() & 0x50) == 80) {
                if (s.getX() >= x1 && x2 >= s.getX() && s.getY() >= y1 && s.getY() <= y2) {
                    s.setOwner(spr);
                    spr =  i;
                }
            }
        }

        if (spr == -1) {
            spr = engine.insertsprite(a1,  401);
            Sprite s = boardService.getSprite(spr);
            if (s != null) {
                s.setX((x2 + x1) / 2);
                s.setY((y2 + y1) / 2);
                s.setZ(sec.getFloorz());
                s.setCstat(32768);
                s.setOwner(-1);
                s.setLotag(0);
                s.setHitag(0);
            }
        }

        return spr;
    }

    public static int BuildLink(int a1, int... a2) {
        int v2 = -1;
        if (LinkCount > 0) {
            int v3 = 0;
            v2 = --LinkCount;
            while (v3 < 8) {
                int v5 = -1;
                if (v3 < a1) {
                    v5 = a2[v3];
                }
                LinkMap[v2][v3] = (byte) v5;
                v3++;
            }
        }
        return v2;
    }

    public static void saveLinks(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  LinkCount);
        for (int i = 0, j; i < 1024; i++) {
            for (j = 0; j < 8; j++) {
                StreamUtils.writeByte(os, LinkMap[i][j]);
            }
        }
    }

    public static void loadLinks(SafeLoader loader, InputStream is) throws IOException {
        loader.LinkCount = StreamUtils.readShort(is);
        for (int i = 0, j; i < 1024; i++) {
            for (j = 0; j < 8; j++) {
                loader.LinkMap[i][j] = StreamUtils.readByte(is);
            }
        }
    }

    public static void loadLinks(SafeLoader loader) {
        LinkCount = loader.LinkCount;
        for (int i = 0; i < 1024; i++) {
            System.arraycopy(loader.LinkMap[i], 0, LinkMap[i], 0, 8);
        }
    }

    public static void ProcessSectorTag(final int nSector, int nLotag, int nHitag) {
        Sector nNext;
        final Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return;
        }

        int nChannel = AllocChannel(nHitag % 1000);
        int[] v263 = new int[8];

        int var_24 = nHitag / 1000 << 12;
        int v269 = 4 * BClipLow(nLotag / 1000, 1);
        int v270 = 100 * v269;

        switch (nLotag % 1000 - 1) {
            case 0:
            case 69:
            case 70:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                AddRunRec(channel[nChannel].head, BuildSwPressSector(nChannel, BuildLink(1, 1), nSector, var_24));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 60));
                break;
            case 1:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPressSector(nChannel, BuildLink(1, 1), nSector, var_24));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 60));
                break;
            case 4:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz() + 1, -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                break;
            case 5:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 400, 400, 2, nNext.getFloorz(), sec.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(2, 1, 1), nSector));
                sec.setFloorz(nNext.getFloorz());
                return;
            case 6:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                AddRunRec(channel[nChannel].head, BuildSwNotOnPause(nChannel, BuildLink(2, -1, 0), nSector, 8));
                break;
            case 7:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz() + 1, 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                break;
            case 8:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 150));
                break;
            case 9:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));

                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                AddRunRec(channel[nChannel].head, BuildSwNotOnPause(nChannel, BuildLink(2, -1, 0), nSector, 8));
                break;
            case 10:
            case 13:
            case 37:
            case 14:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                break;
            case 11:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 150));
                break;
            case 12:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwReady(nChannel, BuildLink(2, 1, 0)));
                break;

            case 15:
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), sec.getFloorz() - 8));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                AddRunRec(channel[nChannel].head, BuildSwReady(nChannel, BuildLink(2, -1, 0)));
                break;
            case 16:
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), sec.getFloorz() - 8));
                AddRunRec(channel[nChannel].head, BuildSwReady(nChannel, BuildLink(2, -1, 0)));
                break;
            case 17:
                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getFloorz(), sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2));
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), (sec.getFloorz() - sec.getCeilingz()) / 2 + sec.getCeilingz() - 8));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                break;
            case 20:
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(2, 1, 1), nSector));
                break;
            case 23:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 60));
                break;
            case 24:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 300));
                break;
            case 25:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 450));
                break;
            case 26:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 600));
                break;
            case 27:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 900));
                break;
            case 30:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 0x7FFF, 0x7FFF, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                break;
            case 31:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 0x7FFF, 0x7FFF, 2, sec.getFloorz(), nNext.getFloorz()));
                break;
            case 32:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(20, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, nNext.getCeilingz(), sec.getFloorz()));
                break;
            case 33:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(28, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, nNext.getCeilingz(), sec.getFloorz()));
                break;
            case 34:
            case 35: {
                nEnergyTowers++;
                int i = BuildEnergyBlock(nSector);
                if (nLotag == 36) {
                    nFinaleSpr = i;
                }
                return;
            }
            case 36:
            case 67:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                break;
            case 38:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 0x7FFF, 0x7FFF, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                AddRunRec(channel[nChannel].head, BuildSwNotOnPause(nChannel, BuildLink(2, -1, 0), nSector, 8));
                break;
            case 39:
                AddMovingSector(nSector, nLotag, nHitag % 1000, 2);
                return;
            case 40:
                AddMovingSector(nSector, nLotag, nHitag % 1000, 18);
                return;
            case 41:
                AddMovingSector(nSector, nLotag, nHitag % 1000, 58);
                return;
            case 42:
                AddMovingSector(nSector, nLotag, nHitag % 1000, 122);
                return;
            case 43:
                AddMovingSector(nSector, nLotag, nHitag % 1000, 90);
                return;
            case 44:
                CreatePushBlock(nSector);
                return;
            case 47:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), nNext.getCeilingz()));
                break;
            case 48:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), nNext.getCeilingz()));
                break;
            case 50:
                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getFloorz(), sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2));
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), 200, v270, 2, sec.getCeilingz(), sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2 - 8));
                AddRunRec(channel[nChannel].head, BuildSwReady(nChannel, BuildLink(2, 1, 0)));
                break;
            case 51:
                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2, sec.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2, sec.getCeilingz()));
                AddRunRec(channel[nChannel].head, BuildSwPressSector(nChannel, BuildLink(1, 1), nSector, var_24));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 60));
                break;
            case 52:
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2, sec.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz() + (sec.getFloorz() - sec.getCeilingz()) / 2, sec.getCeilingz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 150));
                break;
            case 53:
            case 54:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                AddRunRec(channel[nChannel].head, BuildSwPressSector(nChannel, BuildLink(1, 1), nSector, var_24));
                break;

            case 55:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                break;
            case 56:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz(), nNext.getFloorz()));
                break;
            case 57:
                AddRunRec(channel[nChannel].head, BuildSwPressSector(nChannel, BuildLink(1, 1), nSector, var_24));
            case 62:
                if ((nLotag % 1000) == 63) {
                    nEnergyChan = nChannel;
                }

                if ((nNext = getNextSectorNeighbor(nSector, sec.getCeilingz(), -1, -1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getCeilingz()));
                break;
            case 58:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(1, 1), nSector));
                AddRunRec(channel[nChannel].head, BuildSwNotOnPause(nChannel, BuildLink(1, 1), nSector, 60));
                break;
            case 60: {
                v263[0] = sec.getFloorz();
                int i;
                for (i = 1; i < 8; i++) {
                    Sector v212 = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, -1);
                    if (v212 == null) {
                        break;
                    }
                    v263[i] = v212.getFloorz();
                }
                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, i, v263));
                break;
            }
            case 61: {
                v263[0] = sec.getFloorz();
                int i;
                for (i = 1; i < 8; i++) {
                    Sector v220 = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1);
                    if (v220 == null) {
                        break;
                    }
                    v263[i] = v220.getFloorz();
                }
                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), v270, v270, i, v263));
                break;
            }
            case 63:
                AddRunRec(channel[nChannel].head, BuildSwStepOn(nChannel, BuildLink(2, 0, 0), nSector));
                break;
            case 74:
                AddRunRec(channel[nChannel].head, BuildElevC(0, nChannel, nSector, FindWallSprites(nSector), v270, v270, 2, sec.getCeilingz(), sec.getFloorz()));
                break;

            case 22:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 0x7FFF, 200, 2, sec.getFloorz(), nNext.getFloorz()));
                AddRunRec(channel[nChannel].head, BuildSwPause(nChannel, BuildLink(2, -1, 0), 60 * v269));
                break;
            case 49:
                if ((nNext = getNextSectorNeighbor(nSector, sec.getFloorz(), 1, 1)) == null) {
                    break;
                }

                AddRunRec(channel[nChannel].head, BuildElevF(nChannel, nSector, FindWallSprites(nSector), 0x7FFF, 200, 2, nNext.getFloorz(), sec.getFloorz()));
                break;

            case 79:
                SectFlag[nSector] |= 0x8000;
                break;
        }
    }

    private static Sector getNextSectorNeighbor(int sectnum, int thez, int topbottom, int direction) {
        int index = engine.nextsectorneighborz(sectnum, thez, topbottom, direction);
        return boardService.getSector(index);
    }

    private static int GrabPushBlock() {
        if (nPushBlocks < 100) {
            if (sBlockInfo[nPushBlocks] == null) {
                sBlockInfo[nPushBlocks] = new BlockInfo();
            } else {
                sBlockInfo[nPushBlocks].clear();
            }
            return nPushBlocks++;
        }
        return -1;
    }

    public static void savePushBlocks(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nPushBlocks);
        for (int i = 0; i < nPushBlocks; i++) {
            sBlockInfo[i].save(os);
        }
    }

    public static void loadPushBlocks(SafeLoader loader) {
        nPushBlocks = loader.nPushBlocks;
        for (int i = 0; i < loader.nPushBlocks; i++) {
            if (sBlockInfo[i] == null) {
                sBlockInfo[i] = new BlockInfo();
            }
            sBlockInfo[i].copy(loader.sBlockInfo[i]);
        }
    }

    public static void loadPushBlocks(SafeLoader loader, InputStream is) throws IOException {
        loader.nPushBlocks = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nPushBlocks; i++) {
            if (loader.sBlockInfo[i] == null) {
                loader.sBlockInfo[i] = new BlockInfo();
            }
            loader.sBlockInfo[i].load(is);
        }
    }

    private static void CreatePushBlock(int a1) {
        int block = GrabPushBlock();
        BlockInfo binfo = sBlockInfo[block];

        Sector sec = boardService.getSector(a1);
        if (sec == null) {
            return;
        }

        int wx = 0, wy = 0;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            wx += wn.get().getX();
            wy += wn.get().getY();
        }

        binfo.cx = wx / sec.getWallnum();
        binfo.cy = wy / sec.getWallnum();

        int i = engine.insertsprite( a1,  0);
        Sprite spr = boardService.getSprite(i);
        if (spr != null) {
            binfo.sprite = i;
            spr.setX(binfo.cx);
            spr.setY(binfo.cy);
            spr.setZ(sec.getFloorz() - 256);
            spr.setCstat(32768);

            int clipdist = 0;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall w = wn.get();
                int dist = EngineUtils.sqrt((binfo.cx - w.getX()) * (binfo.cx - w.getX()) + (binfo.cy - w.getY()) * (binfo.cy - w.getY()));
                if (dist > clipdist) {
                    clipdist = dist;
                }
            }

            binfo.field_8 = clipdist;
            spr.setClipdist((4 * clipdist) & 0xFF);
            sec.setExtra(block);
        }
    }

    public static void AddMovingSector(int a1, int a2, int a3, int a4) {
        if (nMoveSects >= 50) {
            throw new AssertException("Too many moving sectors");
        }

        CreatePushBlock(a1);
        sMoveDir[nMoveSects] = 1;
        if (sMoveSect[nMoveSects] == null) {
            sMoveSect[nMoveSects] = new MoveSectStruct();
        }

        MoveSectStruct v10 = sMoveSect[nMoveSects++];
        v10.field_2 =  FindTrail(a3);
        v10.field_4 = -1;
        v10.field_8 = -1;
        v10.field_6 =  a4;
        v10.field_A =  (a2 / 1000 + 1);
        v10.field_0 =  a1;
        if ((a4 & 8) != 0) {
            v10.field_E =  AllocChannel(a3 % 1000);
        } else {
            v10.field_E = -1;
        }
        Sector sec = boardService.getSector(a1);
        if (sec != null) {
            sec.setFloorstat(sec.getFloorstat() | 0x40);
        }
    }

    public static void saveMoves(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nMoveSects);
        for (int i = 0; i < 50; i++) {
            StreamUtils.writeByte(os, sMoveDir[i]);
        }
        for (int i = 0; i < nMoveSects; i++) {
            sMoveSect[i].save(os);
        }
    }

    public static void loadMoves(SafeLoader loader, InputStream is) throws IOException {
        loader.nMoveSects = StreamUtils.readShort(is);
        for (int i = 0; i < 50; i++) {
            loader.sMoveDir[i] = StreamUtils.readByte(is);
        }
        for (int i = 0; i < loader.nMoveSects; i++) {
            if (loader.sMoveSect[i] == null) {
                loader.sMoveSect[i] = new MoveSectStruct();
            }
            loader.sMoveSect[i].load(is);
        }
    }

    public static void loadMoves(SafeLoader loader) {
        nMoveSects = loader.nMoveSects;
        System.arraycopy(loader.sMoveDir, 0, sMoveDir, 0, nMoveSects);
        for (int i = 0; i < loader.nMoveSects; i++) {
            if (sMoveSect[i] == null) {
                sMoveSect[i] = new MoveSectStruct();
            }
            sMoveSect[i].copy(loader.sMoveSect[i]);
        }
    }

    private static int BuildEnergyBlock(int nSector) {
        Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return -1;
        }

        int wx = 0, wy = 0;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall w = wn.get();
            wx += w.getX();
            wy += w.getY();

            w.setPal(0);
            w.setShade(50);
            w.setPicnum(3621);
        }

        int cx = wx / sec.getWallnum();
        int cy = wy / sec.getWallnum();
        int nextsec = sec.getWallNode().get().getNextsector();
        int i = engine.insertsprite( nSector,  406);
        Sprite spr = boardService.getSprite(i);
        Sector s = boardService.getSector(nextsec);
        if (spr != null && s != null) {
            spr.setX(cx);
            spr.setY(cy);
            spr.setZ(s.getFloorz());
            spr.setXrepeat(BClipHigh((s.getFloorz() - sec.getFloorz()) >> 8, 255));
            spr.setCstat(32768);
            spr.setXvel(0);
            spr.setYvel(0);
            spr.setZvel(0);
            spr.setLotag((HeadRun() + 1));
            spr.setHitag(0);
            spr.setOwner(AddRunRec(spr.getLotag() - 1, nEvent37 | i));
            spr.setExtra(-1);
            sec.setExtra(i);
            nEnergyBlocks++;
            return i;
        }
        return -1;
    }

    public static void FuncEnergyBlock(int nStack, int nDamage, int RunPtr) {
        int nEnergy = RunData[RunPtr].getObject();
        Sprite pEnergy = boardService.getSprite(nEnergy);
        if (pEnergy == null) {
            return;
        }

        int nSector = pEnergy.getSectnum();
        Sector sec = boardService.getSector(nSector);
        if (sec == null) {
            return;
        }

        if (sec.getExtra() != -1) {
            switch (nStack & EVENT_MASK) {
                case nEventRadialDamage:
                    sec.setFloorz(pEnergy.getZ());
                    pEnergy.setZ(pEnergy.getZ() - 256);
                    nDamage = CheckRadialDamage(nEnergy);
                    pEnergy.setZ(pEnergy.getZ() + 256);
                    sec.setFloorz(sec.getFloorz());
                case nEventDamage:
                    if (nDamage > 0) {
                        nDamage >>= 2;
                        if (nDamage < pEnergy.getXrepeat()) {
                            pEnergy.setXrepeat(pEnergy.getXrepeat() - nDamage);
                            int spr = engine.insertsprite( lasthitsect,  0);
                            Sprite s = boardService.getSprite(spr);
                            if (s != null) {
                                s.setAng(nStack);
                                s.setX(lasthitx);
                                s.setY(lasthity);
                                s.setZ(lasthitz);
                                BuildSpark(spr, 0);
                                engine.mydeletesprite(spr);
                            }
                        } else {
                            pEnergy.setXrepeat(0);
                            ExplodeEnergyBlock(nEnergy);
                        }
                    }
            }
        }
    }

    private static void ExplodeEnergyBlock(int nEnergy) {
        Sprite pEnergy = boardService.getSprite(nEnergy);
        if (pEnergy == null) {
            return;
        }

        Sector pSector = boardService.getSector(pEnergy.getSectnum());
        if (pSector == null) {
            return;
        }

        for (ListNode<Wall> wn = pSector.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall pNext = boardService.getWall(wn.get().getNextwall());
            if (pNext != null) {
                if (pNext.getPal() >= 4) {
                    pNext.setPal(7);
                } else {
                    pNext.setPal(0);
                }
                pNext.setShade(50);
            }
        }

        if (pSector.getFloorpal() >= 4) {
            pSector.setFloorpal(7);
        } else {
            pSector.setFloorpal(0);
        }
        pSector.setFloorshade(50);
        pSector.setExtra(-1);
        pSector.setFloorz(pEnergy.getZ());
        pEnergy.setZ((pEnergy.getZ() + pSector.getFloorz()) / 2);
        BuildSpark(nEnergy, 3);
        pEnergy.setCstat(0);
        pEnergy.setXrepeat(100);
        PlayFX2(StaticSound[78], nEnergy);
        pEnergy.setXrepeat(0);
        nEnergyTowers--;
        for (int i = 0; i < 20; i++) {
            pEnergy.setAng( RandomSize(11));
            BuildSpark(nEnergy, 1);
        }
        TintPalette(16, 16, 16);
        if (nEnergyTowers == 1) {
            ChangeChannel(nEnergyChan, 1);
            StatusMessage(1000, "TAKE OUT THE CONTROL CENTER!", -1);
        } else if (nEnergyTowers != 0) {
            StatusMessage(500, nEnergyTowers + " TOWERS REMAINING", -1);
        } else {
            nFinaleSpr = nEnergy;
            lFinaleStart = BClipLow(engine.getTotalClock(), 1);

            for (int s = 0; s < boardService.getSectorCount(); s++) {
                Sector pSec = boardService.getSector(s);
                if (pSec != null) {
                    if (pSec.getCeilingpal() == 1) {
                        pSec.setCeilingpal(0);
                    }
                    if (pSec.getFloorpal() == 1) {
                        pSec.setFloorpal(0);
                    }

                    for (ListNode<Wall> wn = pSec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wall = wn.get();
                        if (wall.getPal() == 1) {
                            wall.setPal(0);
                        }
                    }
                }
            }
            KillCreatures();
        }
        engine.changespritestat(nEnergy,  0);
    }

    private static void KillCreatures() {
        for (int statnum = 99; statnum < 108; statnum++) {
            if (statnum != 100) {
                for (ListNode<Sprite> node = boardService.getStatNode(statnum); node != null; node = node.getNext()) {
                    DamageEnemy(node.getIndex(), -1, 1600);
                }
            }
        }
    }
}
