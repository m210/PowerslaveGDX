// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Powerslave.Type.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.boardService;
import static ru.m210projects.Powerslave.Main.engine;
import static ru.m210projects.Powerslave.Random.RandomSize;

public class Light {

    public static int bDoFlicks;
    public static int bDoGlows;
    public static final int[] flickermask = new int[25];
    public static int nFlashDepth = 2;
    public static int bTorch;
    private static int nFlickerCount;
    private static int nGlowCount;
    private static int nFlowCount;
    private static int nFlashes;
    private static int nFirstFlash = -1;
    private static int nLastFlash = -1;
    private static final int[] nFreeFlash = new int[2000];
    private static final int[] nNextFlash = new int[2000];
    private static final FlashStruct[] sFlash = new FlashStruct[2000];
    private static final GrowStruct[] sGlow = new GrowStruct[50];
    private static final FlickerStruct[] sFlicker = new FlickerStruct[100];
    private static final FlowStruct[] sFlowInfo = new FlowStruct[375];

    public static FlashStruct GrabFlash() {
        if (nFlashes < 2000) {
            int nFlash = nFreeFlash[nFlashes++];
            nNextFlash[nFlash] = -1;
            if (nLastFlash <= -1) {
                nFirstFlash = nFlash;
            } else {
                nNextFlash[nLastFlash] = nFlash;
            }
            nLastFlash = nFlash;
            if (sFlash[nFlash] == null) {
                sFlash[nFlash] = new FlashStruct();
            }

            return sFlash[nFlash];
        }

        return null;
    }

    public static void saveLights(OutputStream os) throws IOException {
        saveFlashes(os);
        saveGlows(os);
        saveFlows(os);
        saveFlickers(os);
    }

    public static void loadLights(SafeLoader loader) {
        loadFlashes(loader);
        loadGlows(loader);
        loadFlows(loader);
        loadFlickers(loader);

        for (int i = 0; i < 25; i++) {
            flickermask[i] = 2 * RandomSize(31);
        }
        bDoFlicks = 0;
        bDoGlows = 0;
    }

    public static void loadLights(SafeLoader loader, InputStream is) throws IOException {
        loadFlashes(loader, is);
        loadGlows(loader, is);
        loadFlows(loader, is);
        loadFlickers(loader, is);
    }

    private static void saveFlashes(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nFlashes);
        StreamUtils.writeShort(os,  nFirstFlash);
        StreamUtils.writeShort(os,  nLastFlash);
        for (int i = 0; i < 2000; i++) {
            StreamUtils.writeShort(os, nFreeFlash[i]);
            StreamUtils.writeShort(os, nNextFlash[i]);
        }

        if (nFlashes != 0) {
            for (int i = nFirstFlash; i >= 0; i = nNextFlash[i]) {
                sFlash[i].save(os);
            }
        }
    }

    private static void loadFlashes(SafeLoader loader) {
        nFlashes = loader.nFlashes;
        nFirstFlash = loader.nFirstFlash;
        nLastFlash = loader.nLastFlash;

        System.arraycopy(loader.nFreeFlash, 0, nFreeFlash, 0, 2000);
        System.arraycopy(loader.nNextFlash, 0, nNextFlash, 0, 2000);

        if (nFlashes != 0) {
            for (int i = nFirstFlash; i >= 0; i = nNextFlash[i]) {
                if (sFlash[i] == null) {
                    sFlash[i] = new FlashStruct();
                }
                sFlash[i].copy(loader.sFlash[i]);
            }
        }
    }

    private static void loadFlashes(SafeLoader loader, InputStream is) throws IOException {
        loader.nFlashes = StreamUtils.readShort(is);
        loader.nFirstFlash = StreamUtils.readShort(is);
        loader.nLastFlash = StreamUtils.readShort(is);
        for (int i = 0; i < 2000; i++) {
            loader.nFreeFlash[i] =  StreamUtils.readShort(is);
            loader.nNextFlash[i] =  StreamUtils.readShort(is);
        }

        if (loader.nFlashes != 0) {
            for (int i = loader.nFirstFlash; i >= 0; i = loader.nNextFlash[i]) {
                if (loader.sFlash[i] == null) {
                    loader.sFlash[i] = new FlashStruct();
                }
                loader.sFlash[i].load(is);
            }
        }
    }

    private static void saveGlows(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nGlowCount);
        for (int i = 0; i < nGlowCount; i++) {
            sGlow[i].save(os);
        }
    }

    private static void loadGlows(SafeLoader loader) {
        nGlowCount = loader.nGlowCount;
        for (int i = 0; i < loader.nGlowCount; i++) {
            if (sGlow[i] == null) {
                sGlow[i] = new GrowStruct();
            }
            sGlow[i].copy(loader.sGlow[i]);
        }
    }

    private static void loadGlows(SafeLoader loader, InputStream is) throws IOException {
        loader.nGlowCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nGlowCount; i++) {
            if (loader.sGlow[i] == null) {
                loader.sGlow[i] = new GrowStruct();
            }
            loader.sGlow[i].load(is);
        }
    }

    private static void saveFlows(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nFlowCount);
        for (int i = 0; i < nFlowCount; i++) {
            sFlowInfo[i].save(os);
        }
    }

    private static void loadFlows(SafeLoader loader, InputStream is) throws IOException {
        loader.nFlowCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nFlowCount; i++) {
            if (loader.sFlowInfo[i] == null) {
                loader.sFlowInfo[i] = new FlowStruct();
            }
            loader.sFlowInfo[i].load(is);
        }
    }

    private static void loadFlows(SafeLoader loader) {
        nFlowCount = loader.nFlowCount;
        for (int i = 0; i < loader.nFlowCount; i++) {
            if (sFlowInfo[i] == null) {
                sFlowInfo[i] = new FlowStruct();
            }
            sFlowInfo[i].copy(loader.sFlowInfo[i]);
        }
    }

    private static void saveFlickers(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nFlickerCount);
        for (int i = 0; i < nFlickerCount; i++) {
            sFlicker[i].save(os);
        }
    }

    private static void loadFlickers(SafeLoader loader, InputStream is) throws IOException {
        loader.nFlickerCount = StreamUtils.readShort(is);
        for (int i = 0; i < loader.nFlickerCount; i++) {
            if (loader.sFlicker[i] == null) {
                loader.sFlicker[i] = new FlickerStruct();
            }
            loader.sFlicker[i].load(is);
        }
    }

    private static void loadFlickers(SafeLoader loader) {
        nFlickerCount = loader.nFlickerCount;
        for (int i = 0; i < loader.nFlickerCount; i++) {
            if (sFlicker[i] == null) {
                sFlicker[i] = new FlickerStruct();
            }
            sFlicker[i].copy(loader.sFlicker[i]);
        }
    }

    public static void InitLights() {
        nFlickerCount = 0;
        for (int i = 0; i < 25; i++) {
            flickermask[i] = 2 * RandomSize(31);
        }

        nGlowCount = 0;
        nFlowCount = 0;
        nFlashes = 0;
        bDoFlicks = 0;
        bDoGlows = 0;

        for (int i = 0; i < 2000; i++) {
            nFreeFlash[i] =  i;
            nNextFlash[i] = -1;
        }

        nFirstFlash = -1;
        nLastFlash = -1;
    }

    public static void AddFlash(int sectnum, int x, int y, int ignored, int a5) {
        int nDepth = a5 >> 8;
        int nShade = 0;
        if (nDepth < nFlashDepth) {
            int v37 = a5 & 0x80;
            int v42 = ((nDepth + 1) << 8) | (a5 & 0xFF);

            Sector pSector = boardService.getSector(sectnum);
            if (pSector == null) {
                return;
            }

            int v40 = 0;
            for (ListNode<Wall> wn = pSector.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall pWall = wn.get();
                int cx = (pWall.getWall2().getX() + pWall.getX()) / 2;
                int cy = (pWall.getWall2().getY() + pWall.getY()) / 2;
                int nNextSector = pWall.getNextsector();
                Sector pNextSector;
                if (nNextSector <= -1) {
                    pNextSector = null;
                } else {
                    pNextSector = boardService.getSector(nNextSector);
                }

                int v14 = -255;
                if ((a5 & 0x40) == 0) {
                    v14 = ((klabs(x - cx) + klabs(y - cy)) >> 4) - 255;
                }

                if (v14 < 0) {
                    v40++;
                    nShade += v14;
                    if (pWall.getPal() < 5 && (pNextSector == null || pNextSector.getFloorz() < pSector.getFloorz())) {
                        FlashStruct pFlash = GrabFlash();
                        if (pFlash == null) {
                            return;
                        }

                        pFlash.field_0 = (byte) (v37 | 2);
                        pFlash.nShade = pWall.getShade();
                        pFlash.nObject = wn.getIndex();
                        pWall.setShade((byte) BClipLow(v14 + pWall.getShade(), -127));
                        pWall.setPal(pWall.getPal() + 7);
                        if (nDepth == 0 && pWall.getOverpicnum() == 0 && pNextSector != null) {
                            AddFlash(pWall.getNextsector(), x, y, ignored, v42);
                        }
                    }
                }
            }

            if (v40 == 0 || pSector.getFloorpal() >= 4) {
                return;
            }

            FlashStruct v21 = GrabFlash();
            if (v21 == null) {
                return;
            }

            v21.field_0 = (byte) (v37 | 1);
            v21.nShade = pSector.getFloorshade();
            v21.nObject =  sectnum;
            pSector.setFloorshade((byte) BClipLow(nShade + pSector.getFloorshade(), -127));
            pSector.setFloorpal(pSector.getFloorpal() + 7);

            if ((pSector.getCeilingstat() & 1) == 0) {
                if (pSector.getCeilingpal() < 4) {
                    FlashStruct v23 = GrabFlash();
                    if (v23 != null) {
                        v23.field_0 = (byte) (v37 | 3);
                        v23.nShade = pSector.getCeilingshade();
                        v23.nObject =  sectnum;
                        pSector.setCeilingshade((byte) BClipLow(nShade + pSector.getCeilingshade(), -127));
                        pSector.setCeilingpal(pSector.getCeilingpal() + 7);
                    }
                }
            }

            for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
                int j = node.getIndex();
                Sprite pSprite = node.get();
                if (pSprite.getPal() < 4) {
                    FlashStruct pFlash = GrabFlash();
                    if (pFlash != null) {
                        pFlash.field_0 = (byte) (v37 | 4);
                        pFlash.nShade = pSprite.getShade();
                        pFlash.nObject = j;
                        pSprite.setPal(pSprite.getPal() + 7);

                        int v14 = -255;
                        if ((a5 & 0x40) == 0) {
                            v14 = ((klabs(x - pSprite.getX()) + klabs(y - pSprite.getY())) >> 4) - 255;
                        }

                        if (v14 < 0) {
                            pSprite.setShade((byte) BClipLow(pSprite.getShade() + v14, -127));
                        }
                    }
                }
            }
        }
    }

    public static void UndoFlashes() {
        int nNext = -1;
        if (nFlashes != 0) {
            for (int i = nFirstFlash; i >= 0; i = nNextFlash[i]) {
                FlashStruct v3 = sFlash[i];
                final int nObject = v3.nObject;

                switch ((v3.field_0 & 0x3F) - 1) {
                    case 0: {
                        Sector sec = boardService.getSector(nObject);
                        if (sec != null) {
                            if ((v3.field_0 & 0x80) != 0 && (sec.getFloorshade() + 6) < v3.nShade) {
                                nNext = i;
                                sec.setFloorshade(sec.getFloorshade() + 6);
                                continue;
                            } else {
                                sec.setFloorpal(sec.getFloorpal() - 7);
                                sec.setFloorshade(v3.nShade);
                            }
                        }
                    } break;
                    case 1: {
                        Wall wall = boardService.getWall(nObject);
                        if (wall != null) {
                            if ((v3.field_0 & 0x80) != 0 && (wall.getShade() + 6) < v3.nShade) {
                                nNext = i;
                                wall.setShade(wall.getShade() + 6);
                                continue;
                            } else {
                                wall.setPal(wall.getPal() - 7);
                                wall.setShade(v3.nShade);
                            }
                        }
                    } break;
                    case 2: {
                        Sector sec = boardService.getSector(nObject);
                        if (sec != null) {
                            if ((v3.field_0 & 0x80) != 0 && (sec.getCeilingshade() + 6) < v3.nShade) {
                                nNext = i;
                                sec.setCeilingshade(sec.getCeilingshade() + 6);
                                continue;
                            } else {
                                sec.setCeilingpal(sec.getCeilingpal() - 7);
                                sec.setCeilingshade(v3.nShade);
                            }
                        }
                    } break;
                    case 3:
                        Sprite spr = boardService.getSprite(nObject);
                        if (spr != null) {
                            if (spr.getPal() < 7) {
                                break;
                            }

                            if ((v3.field_0 & 0x80) != 0 && (spr.getShade() + 6) < v3.nShade) {
                                nNext = i;
                                spr.setShade(spr.getShade() + 6);
                                continue;
                            } else {
                                spr.setPal(spr.getPal() - 7);
                                spr.setShade(v3.nShade);
                            }
                        }
                        break;
                }

                nFreeFlash[--nFlashes] =  i;
                if (nNext != -1) {
                    nNextFlash[nNext] = nNextFlash[i];
                }
                if (i == nFirstFlash) {
                    nFirstFlash = nNextFlash[nFirstFlash];
                }
                if (i == nLastFlash) {
                    nLastFlash = nNext;
                }
            }
        }
    }

    public static void AddGlow(int a1, int a2) {
        if (nGlowCount < 50) {
            if (sGlow[nGlowCount] == null) {
                sGlow[nGlowCount] = new GrowStruct();
            }
            sGlow[nGlowCount].field_6 =  a2;
            sGlow[nGlowCount].field_4 =  a1;
            sGlow[nGlowCount].field_0 = -1;
            sGlow[nGlowCount].field_2 = 0;
            nGlowCount++;
        }
    }

    public static void AddFlicker(int a1, int a2) {
        if (nFlickerCount < 100) {
            if (sFlicker[nFlickerCount] == null) {
                sFlicker[nFlickerCount] = new FlickerStruct();
            }
            sFlicker[nFlickerCount].field_0 =  a2;
            sFlicker[nFlickerCount].field_2 =  a1;
            if (a2 >= 25) {
                a2 = 24;
            }
            sFlicker[nFlickerCount++].field_4 = flickermask[a2];
        }
    }

    public static void DoGlows() {
        if (++bDoGlows < 3) {
            return;
        }

        int v0 = 0;
        int v1 = 0;
        bDoGlows = 0;
        while (v0 < nGlowCount) {
            int v2 = sGlow[v1].field_2 + 1;
            int v3 = sGlow[v1].field_4;
            sGlow[v1].field_2 =  v2;
            int v4 = sGlow[v1].field_0;
            if (v2 >= sGlow[v1].field_6) {
                sGlow[v1].field_2 = 0;
                sGlow[v1].field_0 =  -sGlow[v1].field_0;
            }
            Sector sec = boardService.getSector(v3);
            if (sec != null) {
                sec.setCeilingshade(sec.getCeilingshade() + v4);
                sec.setFloorshade(sec.getFloorshade() + v4);
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall w = wn.get();
                    w.setShade(w.getShade() + v4);
                }
            }

            ++v1;
            ++v0;
        }
    }

    public static void DoFlickers() {
        bDoFlicks = bDoFlicks ^ 1;
        if (bDoFlicks == 0) {
            return;
        }
        int v0 = 0;
        int v1 = 0;
        while (v0 < nFlickerCount) {
            int v2 = sFlicker[v1].field_4 & 1;
            int v3 = sFlicker[v1].field_2;
            int v4 = (sFlicker[v1].field_4 >>> 1) & 1;
            sFlicker[v1].field_4 = (sFlicker[v1].field_4 << 31) | (sFlicker[v1].field_4 >>> 1);

            if ((v2 ^ v4) != 0) {
                int v5 = sFlicker[v1].field_0;
                if (v2 == 0) {
                    v5 = -sFlicker[v1].field_0;
                }

                Sector sec = boardService.getSector(v3);
                if (sec != null) {
                    sec.setCeilingshade(sec.getCeilingshade() + v5);
                    sec.setFloorshade(sec.getFloorshade() + v5);
                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall w = wn.get();
                        w.setShade(w.getShade() + v5);
                    }
                }
            }
            ++v1;
            ++v0;
        }
    }

    public static void AddFlow(int nObject, int nVelocity, int nState) {
        if (nFlowCount < 375) {
            if (sFlowInfo[nFlowCount] == null) {
                sFlowInfo[nFlowCount] = new FlowStruct();
            }

            FlowStruct pFlow = sFlowInfo[nFlowCount];
            nFlowCount++;

            switch (nState) {
                case 0:
                case 1:
                    Sprite pSprite = boardService.getSprite(nObject);
                    nObject = pSprite != null ? pSprite.getSectnum() : -1;
                    Sector pSector = boardService.getSector(nObject);
                    if (pSector != null && pSprite != null) {
                        pFlow.xmax = (engine.getTile(pSector.getFloorpicnum()).getWidth() << 14) - 1;
                        pFlow.ymax = (engine.getTile(pSector.getFloorpicnum()).getHeight() << 14) - 1;
                        pFlow.xvel = nVelocity * -EngineUtils.sin((pSprite.getAng() + 512) & 0x7FF);
                        pFlow.yvel = nVelocity * EngineUtils.sin(pSprite.getAng() & 0x7FF);
                    }
                    break;
                case 2:
                case 3:
                    int nDirection = 1536;
                    if (nState == 2) {
                        nDirection = 512;
                    }

                    Wall pWall = boardService.getWall(nObject);
                    if (pWall != null) {
                        pFlow.xmax = pWall.getXrepeat() * engine.getTile(pWall.getPicnum()).getWidth() << 8;
                        pFlow.ymax = pWall.getYrepeat() * engine.getTile(pWall.getPicnum()).getHeight() << 8;
                        pFlow.xvel = nVelocity * -EngineUtils.sin((nDirection + 512) & 0x7FF);
                        pFlow.yvel = nVelocity * EngineUtils.sin(nDirection & 0x7FF);
                    }
                    break;
            }

            pFlow.xpanning = pFlow.ypanning = 0;
            pFlow.nObject = nObject;
            pFlow.nState =  nState;
        }
    }

    public static void DoFlows() {
        for (int i = 0; i < nFlowCount; i++) {
            FlowStruct pFlow = sFlowInfo[i];
            pFlow.xpanning += pFlow.xvel;
            pFlow.ypanning += pFlow.yvel;

            switch (pFlow.nState) {
                case 0: {
                    pFlow.xpanning &= pFlow.xmax;
                    pFlow.ypanning &= pFlow.ymax;
                    Sector sec = boardService.getSector(pFlow.nObject);
                    if (sec != null) {
                        sec.setFloorxpanning((pFlow.xpanning >> 14));
                        sec.setFloorypanning((pFlow.ypanning >> 14));
                    }
                } break;
                case 1: {
                    Sector sec = boardService.getSector(pFlow.nObject);
                    if (sec != null) {
                        sec.setCeilingxpanning((pFlow.xpanning >> 14));
                        sec.setCeilingypanning((pFlow.ypanning >> 14));
                    }
                    pFlow.xpanning &= pFlow.xmax;
                    pFlow.ypanning &= pFlow.ymax;
                } break;
                case 2: {
                    Wall wall = boardService.getWall(pFlow.nObject);
                    if (wall != null) {
                        wall.setXpanning((pFlow.xpanning >> 14));
                        wall.setYpanning((pFlow.ypanning >> 14));
                    }

                    if (pFlow.xpanning < 0) {
                        pFlow.xpanning += pFlow.xmax;
                    }
                    if (pFlow.ypanning < 0) {
                        pFlow.ypanning += pFlow.ymax;
                    }
                } break;
                case 3: {
                    Wall wall = boardService.getWall(pFlow.nObject);
                    if (wall != null) {
                        wall.setXpanning((pFlow.xpanning >> 14));
                        wall.setYpanning((pFlow.ypanning >> 14));
                    }

                    if (pFlow.xpanning >= pFlow.xmax) {
                        pFlow.xpanning -= pFlow.xmax;
                    }
                    if (pFlow.ypanning >= pFlow.ymax) {
                        pFlow.ypanning -= pFlow.ymax;
                    }
                } break;
            }
        }
    }

    public static void DoLights() {
        DoFlickers();
        DoGlows();
        DoFlows();
    }

    public static void BuildFlash(int player, int ignored, int value) {
        if (player == nLocalPlayer) {
            flash = -value;
        }
    }

    public static void UseTorch(int nPlayer) {
        if (nPlayerTorch[nPlayer] == 0) {
            engine.getPaletteManager().SetTorch(nPlayer, 1);
        }
        nPlayerTorch[nPlayer] = 900;
    }
}
