// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

public class BulletInfo {
    public final int force;
    public final int inf_2;
    public final int inf_4;
    public final int inf_8;
    public final int inf_A;
    public final int inf_C;
    public final int flags;
    public final int inf_10;
    public final int inf_12;

    public BulletInfo(int inf_0, int inf_2, int inf_4, int inf_8, int inf_A, int inf_C, int flags, int inf_10, int inf_12) {
        this.force =  inf_0;
        this.inf_2 =  inf_2;
        this.inf_4 = inf_4;
        this.inf_8 =  inf_8;
        this.inf_A =  inf_A;
        this.inf_C =  inf_C;
        this.flags =  flags;
        this.inf_10 =  inf_10;
        this.inf_12 =  inf_12;
    }
}
