// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class BubbleStruct {

    public int field_0;
    public int nSprite = -1;
    public int nSeq;
    public int field_6;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, nSprite);
        StreamUtils.writeShort(os, nSeq);
        StreamUtils.writeShort(os, field_6);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        nSprite =  StreamUtils.readShort(is);
        nSeq =  StreamUtils.readShort(is);
        field_6 =  StreamUtils.readShort(is);
    }

    public void copy(BubbleStruct src) {
        field_0 = src.field_0;
        nSprite = src.nSprite;
        nSeq = src.nSeq;
        field_6 = src.field_6;
    }
}
