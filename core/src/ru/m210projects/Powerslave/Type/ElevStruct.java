// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class ElevStruct {

    public static final int size = 56;

    public int field_0;
    public int channel;
    public int sectnum;
    public int field_6;
    public int field_A;
    public int field_E;
    public int field_10;

    public final int[] field_12 = new int[8];
    public int field_32;
    public int field_34;
    public int field_36;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, channel);
        StreamUtils.writeShort(os, sectnum);
        StreamUtils.writeInt(os, field_6);
        StreamUtils.writeInt(os, field_A);
        StreamUtils.writeShort(os, field_E);
        StreamUtils.writeShort(os, field_10);

        for (int i = 0; i < 8; i++) {
            StreamUtils.writeInt(os, field_12[i]);
        }
        StreamUtils.writeShort(os, field_32);
        StreamUtils.writeShort(os, field_34);
        StreamUtils.writeShort(os, field_36);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        channel =  StreamUtils.readShort(is);
        sectnum =  StreamUtils.readShort(is);
        field_6 = StreamUtils.readInt(is);
        field_A = StreamUtils.readInt(is);
        field_E =  StreamUtils.readShort(is);
        field_10 =  StreamUtils.readShort(is);

        for (int i = 0; i < 8; i++) {
            field_12[i] = StreamUtils.readInt(is);
        }
        field_32 =  StreamUtils.readShort(is);
        field_34 =  StreamUtils.readShort(is);
        field_36 =  StreamUtils.readShort(is);
    }

    public String toString() {
        StringBuilder text = new StringBuilder("field_0 " + field_0 + "\r\n");
        text.append("channel ").append(channel).append("\r\n");
        text.append("sectnum ").append(sectnum).append("\r\n");
        text.append("field_6 ").append(field_6).append("\r\n");
        text.append("field_A ").append(field_A).append("\r\n");
        text.append("field_E ").append(field_E).append("\r\n");
        text.append("field_10 ").append(field_10).append("\r\n");
        for (int i = 0; i < 8; i++) {
            text.append("field_12[").append(i).append("] ").append(field_12[i]).append("\r\n");
        }
        text.append("field_32 ").append(field_32).append("\r\n");
        text.append("field_34 ").append(field_34).append("\r\n");
        text.append("field_36 ").append(field_36).append("\r\n");

        return text.toString();
    }

    public void copy(ElevStruct bb) {
        field_0 = bb.field_0;
        channel = bb.channel;
        sectnum = bb.sectnum;
        field_6 = bb.field_6;
        field_A = bb.field_A;
        field_E = bb.field_E;
        field_10 = bb.field_10;
        System.arraycopy(bb.field_12, 0, field_12, 0, 8);
        field_32 = bb.field_32;
        field_34 = bb.field_34;
        field_36 = bb.field_36;
    }
}
