// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class FlowStruct {

    public static final int size = 28;

    public int nObject;
    public int nState;
    public int xpanning;
    public int ypanning;
    public int xvel;
    public int yvel;
    public int xmax;
    public int ymax;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, nObject);
        StreamUtils.writeShort(os, nState);
        StreamUtils.writeInt(os, xpanning);
        StreamUtils.writeInt(os, ypanning);
        StreamUtils.writeInt(os, xvel);
        StreamUtils.writeInt(os, yvel);
        StreamUtils.writeInt(os, xmax);
        StreamUtils.writeInt(os, ymax);
    }

    public void load(InputStream is) throws IOException {
        nObject =  StreamUtils.readShort(is);
        nState =  StreamUtils.readShort(is);
        xpanning = StreamUtils.readInt(is);
        ypanning = StreamUtils.readInt(is);
        xvel = StreamUtils.readInt(is);
        yvel = StreamUtils.readInt(is);
        xmax = StreamUtils.readInt(is);
        ymax = StreamUtils.readInt(is);
    }

    public void copy(FlowStruct src) {
        nObject = src.nObject;
        nState = src.nState;
        xpanning = src.xpanning;
        ypanning = src.ypanning;
        xvel = src.xvel;
        yvel = src.yvel;
        xmax = src.xmax;
        ymax = src.ymax;
    }
}
