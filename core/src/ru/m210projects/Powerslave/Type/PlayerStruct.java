// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;

import java.io.OutputStream;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;
import java.util.Arrays;

import static ru.m210projects.Powerslave.Globals.nDoppleSprite;
import static ru.m210projects.Powerslave.Main.boardService;
import static ru.m210projects.Powerslave.Main.game;

public class PlayerStruct {

    public final PLocation prevView = new PLocation();

    public int nPlayer;
    public int HealthAmount;
    public int animCount;
    public int anim_;
    public int spriteId;
    public int mummified;
    public int network;
    public int invisibility;
    public int AirAmount;
    public int seq;
    public int AirMaskAmount;
    public int KeysBitMask;
    public int MagicAmount;
    public final byte[] ItemsAmount = new byte[8];
    public final int[] AmmosAmount = new int[9];
    public int currentWeapon;
    public int seqOffset;
    public int weaponFire;
    public int newWeapon;
    public int weaponState;
    public int lastWeapon;
    public int RunFunc;

    //GDX
    public int turnAround;
    public int lastUsedWeapon;
    public float ang;
    public float horiz;
    public int eyelevel;
    public boolean crouch_toggle;

    public static int size(boolean isGDX) {
        if (isGDX) {
            return 64 + 15;
        }
        return 64;
    }

    public Sprite getSprite() {
        return boardService.getSprite(spriteId);
    }

    public void UpdatePlayerLoc() {
        Sprite psp = boardService.getSprite(spriteId);
        if (psp == null) {
            return;
        }

        ILoc oldLoc = game.pInt.getsprinterpolate(spriteId);
        if (oldLoc != null) {
            oldLoc.x = psp.getX();
            oldLoc.y = psp.getY();
            oldLoc.z = psp.getZ();
        } else {
            game.pInt.setsprinterpolate(spriteId, psp);
        }
        game.pInt.setsprinterpolate(nDoppleSprite[nPlayer], boardService.getSprite(nDoppleSprite[nPlayer]));

        prevView.x = psp.getX();
        prevView.y = psp.getY();
        prevView.z = psp.getZ() + eyelevel;
        prevView.ang = ang;
        prevView.horiz = horiz;
    }

    public void writeObject(OutputStream os, boolean isGDX) throws IOException {
        StreamUtils.writeShort(os, HealthAmount);
        StreamUtils.writeShort(os, animCount);
        StreamUtils.writeShort(os, anim_);
        StreamUtils.writeShort(os,  spriteId);
        StreamUtils.writeShort(os, mummified);
        StreamUtils.writeShort(os, network);
        StreamUtils.writeShort(os, invisibility);
        StreamUtils.writeShort(os, AirAmount);
        StreamUtils.writeShort(os, seq);
        StreamUtils.writeShort(os, AirMaskAmount);
        StreamUtils.writeShort(os, KeysBitMask);
        StreamUtils.writeShort(os, MagicAmount);
        StreamUtils.writeBytes(os, ItemsAmount);
        for (int i = 0; i < 9; i++) {
            StreamUtils.writeShort(os, AmmosAmount[i]);
        }
        StreamUtils.writeShort(os, currentWeapon);
        StreamUtils.writeShort(os, seqOffset);
        StreamUtils.writeShort(os, weaponFire);
        StreamUtils.writeShort(os, newWeapon);
        StreamUtils.writeShort(os, weaponState);
        StreamUtils.writeShort(os, lastWeapon);
        StreamUtils.writeShort(os, RunFunc);

        if (isGDX) {
            StreamUtils.writeShort(os, turnAround);
            StreamUtils.writeShort(os, lastUsedWeapon);
            StreamUtils.writeFloat(os, ang);
            StreamUtils.writeFloat(os, horiz);
            StreamUtils.writeShort(os, eyelevel);
            StreamUtils.writeByte(os, crouch_toggle ? (byte) 1 : 0);
        }
    }

    public void readObject(InputStream is, boolean isGDX) throws IOException {
        HealthAmount =  StreamUtils.readShort(is);
        animCount =  StreamUtils.readShort(is);
        anim_ =  StreamUtils.readShort(is);
        spriteId = StreamUtils.readShort(is);
        mummified =  StreamUtils.readShort(is);
        network =  StreamUtils.readShort(is);
        invisibility =  StreamUtils.readShort(is);
        AirAmount =  StreamUtils.readShort(is);
        seq =  StreamUtils.readShort(is);
        AirMaskAmount =  StreamUtils.readShort(is);
        KeysBitMask =  StreamUtils.readShort(is);
        MagicAmount =  StreamUtils.readShort(is);
        StreamUtils.readBytes(is, ItemsAmount);
        for (int i = 0; i < 9; i++) {
            AmmosAmount[i] =  StreamUtils.readShort(is);
        }
        currentWeapon =  StreamUtils.readShort(is);
        seqOffset =  StreamUtils.readShort(is);
        weaponFire =  StreamUtils.readShort(is);
        newWeapon =  StreamUtils.readShort(is);
        weaponState =  StreamUtils.readShort(is);
        lastWeapon =  StreamUtils.readShort(is);
        RunFunc =  StreamUtils.readShort(is);

        if (isGDX) {
            turnAround =  StreamUtils.readShort(is);
            lastUsedWeapon =  StreamUtils.readShort(is);
            ang = StreamUtils.readFloat(is);
            horiz = StreamUtils.readFloat(is);
            eyelevel =  StreamUtils.readShort(is);
            crouch_toggle = StreamUtils.readBoolean(is);
        }
    }

    public void reset() {
        HealthAmount = 0;
        animCount = 0;
        anim_ = 0;
        spriteId = 0;
        mummified = 0;
        network = 0;
        invisibility = 0;
        AirAmount = 0;
        seq = 0;
        AirMaskAmount = 0;
        KeysBitMask = 0;
        MagicAmount = 0;
        Arrays.fill(ItemsAmount, (byte) 0);
        Arrays.fill(AmmosAmount,  0);
        currentWeapon = 0;
        seqOffset = 0;
        weaponFire = 0;
        newWeapon = 0;
        weaponState = 0;
        lastWeapon = 0;
        RunFunc = 0;

        turnAround = 0;
        lastUsedWeapon = 0;
        ang = 0;
        horiz = 92;
        eyelevel = -14080;
        crouch_toggle = false;
    }

    public void copy(PlayerStruct src) {
        HealthAmount = src.HealthAmount;
        animCount = src.animCount;
        anim_ = src.anim_;
        spriteId = src.spriteId;
        mummified = src.mummified;
        network = src.network;
        invisibility = src.invisibility;
        AirAmount = src.AirAmount;
        seq = src.seq;
        AirMaskAmount = src.AirMaskAmount;
        KeysBitMask = src.KeysBitMask;
        MagicAmount = src.MagicAmount;
        System.arraycopy(src.ItemsAmount, 0, ItemsAmount, 0, ItemsAmount.length);
        System.arraycopy(src.AmmosAmount, 0, AmmosAmount, 0, AmmosAmount.length);
        currentWeapon = src.currentWeapon;
        seqOffset = src.seqOffset;
        weaponFire = src.weaponFire;
        newWeapon = src.newWeapon;
        weaponState = src.weaponState;
        lastWeapon = src.lastWeapon;
        RunFunc = src.RunFunc;

        turnAround = src.turnAround;
        lastUsedWeapon = src.lastUsedWeapon;
        ang = src.ang;
        horiz = src.horiz;
        eyelevel = src.eyelevel;
        crouch_toggle = src.crouch_toggle;
    }
}
