// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class BlockInfo {

    public int cx, cy;
    public int field_8;
    public int sprite;
    public int field_E;

    public void clear() {
        this.cx = 0;
        this.cy = 0;
        this.field_8 = 0;
        this.sprite = -1;
        this.field_E = 0;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, cx);
        StreamUtils.writeInt(os, cy);
        StreamUtils.writeInt(os, field_8);
        StreamUtils.writeShort(os,  sprite);
        StreamUtils.writeShort(os, field_E);
    }

    public void load(InputStream is) throws IOException {
        cx = StreamUtils.readInt(is);
        cy = StreamUtils.readInt(is);
        field_8 = StreamUtils.readInt(is);
        sprite = StreamUtils.readShort(is);
        field_E =  StreamUtils.readShort(is);
    }

    public void copy(BlockInfo src) {
        cx = src.cx;
        cy = src.cy;
        field_8 = src.field_8;
        sprite = src.sprite;
        field_E = src.field_E;
    }
}
