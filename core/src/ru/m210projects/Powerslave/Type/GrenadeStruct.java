// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class GrenadeStruct {
    public static final int size = 32;

    public int field_0;
    public int field_2;
    public int nSprite;
    public int field_6;
    public int field_8;
    public int ActionSeq;
    public int field_C;
    public int field_E;
    public int field_10;
    public int xvel;
    public int yvel;
    public int field_1C;
    public int field_1E;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, field_2);
        StreamUtils.writeShort(os, nSprite);
        StreamUtils.writeShort(os, field_6);
        StreamUtils.writeShort(os, field_8);
        StreamUtils.writeShort(os, ActionSeq);
        StreamUtils.writeShort(os, field_C);
        StreamUtils.writeShort(os, field_E);
        StreamUtils.writeInt(os, field_10);
        StreamUtils.writeInt(os, xvel);
        StreamUtils.writeInt(os, yvel);
        StreamUtils.writeShort(os, field_1C);
        StreamUtils.writeShort(os, field_1E);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        field_2 =  StreamUtils.readShort(is);
        nSprite =  StreamUtils.readShort(is);
        field_6 =  StreamUtils.readShort(is);
        field_8 =  StreamUtils.readShort(is);
        ActionSeq =  StreamUtils.readShort(is);
        field_C =  StreamUtils.readShort(is);
        field_E =  StreamUtils.readShort(is);
        field_10 = StreamUtils.readInt(is);
        xvel = StreamUtils.readInt(is);
        yvel = StreamUtils.readInt(is);
        field_1C =  StreamUtils.readShort(is);
        field_1E =  StreamUtils.readShort(is);
    }

    public void copy(GrenadeStruct src) {
        field_0 = src.field_0;
        field_2 = src.field_2;
        nSprite = src.nSprite;
        field_6 = src.field_6;
        field_8 = src.field_8;
        ActionSeq = src.ActionSeq;
        field_C = src.field_C;
        field_E = src.field_E;
        field_10 = src.field_10;
        xvel = src.xvel;
        yvel = src.yvel;
        field_1C = src.field_1C;
        field_1E = src.field_1E;
    }
}
