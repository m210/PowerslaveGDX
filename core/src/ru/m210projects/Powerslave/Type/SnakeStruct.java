// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class SnakeStruct {

    public static final int size = 32;

    public int nTarget;
    public final int[] nSprite = new int[8];
    public int field_10;
    public int nFunc = -1;
    public final byte[] field_14 = new byte[8];
    public int zvec;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os,  nTarget);
        for (int i = 0; i < 8; i++) {
            StreamUtils.writeShort(os, nSprite[i]);
        }
        StreamUtils.writeShort(os, field_10);
        StreamUtils.writeShort(os, nFunc);
        for (int i = 0; i < 8; i++) {
            StreamUtils.writeByte(os, field_14[i]);
        }
        StreamUtils.writeShort(os, zvec);
    }

    public void load(InputStream is) throws IOException {
        nTarget = StreamUtils.readShort(is);
        for (int i = 0; i < 8; i++) {
            nSprite[i] =  StreamUtils.readShort(is);
        }
        field_10 =  StreamUtils.readShort(is);
        nFunc =  StreamUtils.readShort(is);
        StreamUtils.readBytes(is, field_14);
        zvec =  StreamUtils.readShort(is);
    }

    public void copy(SnakeStruct src) {
        nTarget = src.nTarget;
        System.arraycopy(src.nSprite, 0, nSprite, 0, 8);
        field_10 = src.field_10;
        nFunc = src.nFunc;
        System.arraycopy(src.field_14, 0, field_14, 0, 8);
        zvec = src.zvec;
    }

}
