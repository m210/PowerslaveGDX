// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class TrapStruct {

    public int field_0;
    public int field_2;
    public int field_4;
    public int nWall;
    public int nWall2;
    public int field_A;
    public int field_C;
    public int field_E;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, field_2);
        StreamUtils.writeShort(os, field_4);
        StreamUtils.writeShort(os, nWall);
        StreamUtils.writeShort(os, nWall2);
        StreamUtils.writeShort(os, field_A);
        StreamUtils.writeShort(os, field_C);
        StreamUtils.writeShort(os, field_E);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        field_2 =  StreamUtils.readShort(is);
        field_4 =  StreamUtils.readShort(is);
        nWall =  StreamUtils.readShort(is);
        nWall2 =  StreamUtils.readShort(is);
        field_A =  StreamUtils.readShort(is);
        field_C =  StreamUtils.readShort(is);
        field_E =  StreamUtils.readShort(is);
    }

    public void copy(TrapStruct src) {
        field_0 = src.field_0;
        field_2 = src.field_2;
        field_4 = src.field_4;
        nWall = src.nWall;
        nWall2 = src.nWall2;
        field_A = src.field_A;
        field_C = src.field_C;
        field_E = src.field_E;
    }
}
