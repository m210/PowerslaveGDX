package ru.m210projects.Powerslave.Type;

import ru.m210projects.Build.Architecture.common.audio.SoundData;

import java.nio.ByteBuffer;

public class SoundResource {

    public final int num;
    public final boolean loop;
    public final SoundData data; // pointer to voc data

    public SoundResource(SoundData soundData, int num, boolean loop) {
        this.data = soundData;
        this.num = num;
        this.loop = loop;
    }

    public int getRate() {
        return data.getRate();
    }

    public ByteBuffer getBuffer() {
        return data.getData();
    }

    public int getBits() {
        return data.getBits();
    }
}
