// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Powerslave.Globals.moveframes;

public class DemoFile {

    public int level;
    public short weapons;
    public short clip, items;
    public short lives;
    public final PlayerStruct player = new PlayerStruct();
    public final List<Input> inputs = new ArrayList<>();
    public final List<Integer> frames = new ArrayList<>();
    public int cnt = 0;

    public DemoFile(InputStream is) throws IOException {
        level = StreamUtils.readUnsignedByte(is);
        weapons =  StreamUtils.readShort(is);
        StreamUtils.readShort(is); //currentWeapon
        clip =  StreamUtils.readShort(is);
        items =  StreamUtils.readShort(is);
        player.readObject(is, false);
        lives =  StreamUtils.readShort(is);

        while(is.available() >= 36) {
            frames.add(StreamUtils.readInt(is));
            inputs.add(new Input().readObject(is));
        }
    }

    public Input ReadPlaybackInput() {
        if (cnt < inputs.size()) {
            moveframes = frames.get(cnt);
            return inputs.get(cnt++);
        }
        return null;
    }
}
