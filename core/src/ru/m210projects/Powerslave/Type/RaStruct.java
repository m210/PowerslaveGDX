// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class RaStruct {
    public static final int size = 16;

    public int nState;
    public int nSeq;
    public int nFunc;
    public int nSprite;
    public int nTarget;
    public int field_A;
    public int field_C;
    public int nPlayer;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, nState);
        StreamUtils.writeShort(os, nSeq);
        StreamUtils.writeShort(os, nFunc);
        StreamUtils.writeShort(os,  nSprite);
        StreamUtils.writeShort(os, nTarget);
        StreamUtils.writeShort(os, field_A);
        StreamUtils.writeShort(os, field_C);
        StreamUtils.writeShort(os, nPlayer);
    }

    public void load(InputStream is) throws IOException {
        nState =  StreamUtils.readShort(is);
        nSeq =  StreamUtils.readShort(is);
        nFunc =  StreamUtils.readShort(is);
        nSprite = StreamUtils.readShort(is);
        nTarget =  StreamUtils.readShort(is);
        field_A =  StreamUtils.readShort(is);
        field_C =  StreamUtils.readShort(is);
        nPlayer =  StreamUtils.readShort(is);
    }

    public void copy(RaStruct src) {
        nState = src.nState;
        nSeq = src.nSeq;
        nFunc = src.nFunc;
        nSprite = src.nSprite;
        nTarget = src.nTarget;
        field_A = src.field_A;
        field_C = src.field_C;
        nPlayer = src.nPlayer;
    }
}
