// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;

import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class RunData {

    public static final int size = 8;

    private RunEvent RunEvent = new RunEvent();
    public int RunPtr;
    public int RunNum;

    public void save(OutputStream os) throws IOException {
        RunEvent.writeObject(os);
        StreamUtils.writeShort(os, RunPtr);
        StreamUtils.writeShort(os, RunNum);
    }

    public void load(InputStream is) throws IOException {
        RunEvent.readObject(is);
        RunPtr =  StreamUtils.readShort(is);
        RunNum =  StreamUtils.readShort(is);
    }

    @Deprecated
    public int getEvent() {
        return RunEvent.nObject | (RunEvent.nFunc << 16);
    }

    @Deprecated
    public void setEvent(int RunEvent) {
        this.RunEvent.nObject = (short) (RunEvent & 0xFFFF);
        this.RunEvent.nFunc = (RunEvent >> 16);
    }

    public int getObject() {
        return RunEvent.nObject;
    }

    public int getFunc() {
        return RunEvent.nFunc;
    }

    public void clear() {
        this.RunNum = -1;
        this.RunPtr = -1;
        this.RunEvent.nObject = -1;
        this.RunEvent.nFunc = -1;
    }

    @Override
    public String toString() {
        String text = "RunEvent " + RunEvent + "\r\n";
        text += "RunPtr " + RunPtr + "\r\n";
        text += "RunNum " + RunNum + "\r\n";
        return text;
    }

    public void copy(RunData src) {
        RunEvent = src.RunEvent;
        RunPtr = src.RunPtr;
        RunNum = src.RunNum;
    }

    private static class RunEvent {

        int nObject = -1;
        int nFunc = -1;

        public void readObject(InputStream is) throws IOException {
            nObject = StreamUtils.readInt(is);
            nFunc = StreamUtils.readInt(is);
        }

        public void writeObject(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, nObject);
            StreamUtils.writeInt(os, nFunc);
        }

        @Override
        public String toString() {
            return "RunEvent{" +
                    "nObject=" + nObject +
                    ", nFunc=" + nFunc +
                    '}';
        }
    }
}
