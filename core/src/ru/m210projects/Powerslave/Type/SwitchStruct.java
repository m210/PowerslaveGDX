// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class SwitchStruct {

    public int field_0;
    public int nPause;
    public int nChannel;
    public int nLink;
    public int field_8;
    public int nSector;
    public int field_C;
    public int nWall;
    public int field_10;
    public int field_12;
//	public int field_1A;
//	public int field_1C;
//	public int field_1E;

    public void clear() {
        this.field_0 = 0;
        this.nPause = 0;
        this.nChannel = 0;
        this.nLink = 0;
        this.field_8 = 0;
        this.nSector = 0;
        this.field_C = 0;
        this.nWall = 0;
        this.field_10 = 0;
        this.field_12 = 0;
//		this.field_1A = 0;
//		this.field_1C = 0;
//		this.field_1E = 0;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, nPause);
        StreamUtils.writeShort(os, nChannel);
        StreamUtils.writeShort(os, nLink);
        StreamUtils.writeShort(os, field_8);
        StreamUtils.writeShort(os, nSector);
        StreamUtils.writeShort(os, field_C);
        StreamUtils.writeShort(os, nWall);

        StreamUtils.writeShort(os, field_10);
        StreamUtils.writeShort(os, field_12);
//		StreamUtils.writeShort(os, field_1A);
//		StreamUtils.writeShort(os, field_1C);
//		StreamUtils.writeShort(os, field_1E);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        nPause =  StreamUtils.readShort(is);
        nChannel =  StreamUtils.readShort(is);
        nLink =  StreamUtils.readShort(is);
        field_8 =  StreamUtils.readShort(is);
        nSector =  StreamUtils.readShort(is);
        field_C =  StreamUtils.readShort(is);
        nWall =  StreamUtils.readShort(is);

        field_10 =  StreamUtils.readShort(is);
        field_12 =  StreamUtils.readUnsignedShort(is);
//		field_1A = StreamUtils.readShort(is);
//		field_1C = StreamUtils.readShort(is);
//		field_1E = StreamUtils.readShort(is);
    }

    public void copy(SwitchStruct src) {
        field_0 = src.field_0;
        nPause = src.nPause;
        nChannel = src.nChannel;
        nLink = src.nLink;
        field_8 = src.field_8;
        nSector = src.nSector;
        field_C = src.field_C;
        nWall = src.nWall;

        field_10 = src.field_10;
        field_12 = src.field_12;
    }
}
