// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.


package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class FishChunk {
    public static final int size = 6;

    public int nSprite;
    public int nSeq;
    public int ActionSeq;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, nSprite);
        StreamUtils.writeShort(os, nSeq);
        StreamUtils.writeShort(os, ActionSeq);
    }

    public void load(InputStream is) throws IOException {
        nSprite =  StreamUtils.readShort(is);
        nSeq =  StreamUtils.readShort(is);
        ActionSeq =  StreamUtils.readShort(is);
    }

    public void copy(FishChunk src) {
        nSprite = src.nSprite;
        nSeq = src.nSeq;
        ActionSeq = src.ActionSeq;
    }
}
