// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Enemies.Enemy.EnemyStruct;
import ru.m210projects.Powerslave.Enemies.Lion.LionStruct;
import ru.m210projects.Powerslave.Enemies.Queen.QueenStruct;
import ru.m210projects.Powerslave.Enemies.Scorp.ScorpStruct;
import ru.m210projects.Powerslave.Enemies.Set.SetStruct;
import ru.m210projects.Powerslave.Enemies.Wasp.WaspStruct;
import ru.m210projects.Powerslave.Slide.PointStruct;
import ru.m210projects.Powerslave.Slide.SlideStruct;
import ru.m210projects.Powerslave.Slide.SlideStruct2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Powerslave.Anim.MAX_LIL_ANIM;
import static ru.m210projects.Powerslave.Anim.loadAnm;
import static ru.m210projects.Powerslave.Bullet.loadBullets;
import static ru.m210projects.Powerslave.Enemies.Anubis.MAXANUBIS;
import static ru.m210projects.Powerslave.Enemies.Anubis.loadAnubis;
import static ru.m210projects.Powerslave.Enemies.Fish.MAX_FISHS;
import static ru.m210projects.Powerslave.Enemies.Fish.loadFish;
import static ru.m210projects.Powerslave.Enemies.LavaDude.MAX_LAVAS;
import static ru.m210projects.Powerslave.Enemies.LavaDude.loadLava;
import static ru.m210projects.Powerslave.Enemies.Lion.MAXLION;
import static ru.m210projects.Powerslave.Enemies.Lion.loadLion;
import static ru.m210projects.Powerslave.Enemies.Mummy.MAX_MUMMIES;
import static ru.m210projects.Powerslave.Enemies.Mummy.loadMummy;
import static ru.m210projects.Powerslave.Enemies.Queen.MAX_EGGS;
import static ru.m210projects.Powerslave.Enemies.Queen.loadQueen;
import static ru.m210projects.Powerslave.Enemies.Ra.loadRa;
import static ru.m210projects.Powerslave.Enemies.Rat.MAX_RAT;
import static ru.m210projects.Powerslave.Enemies.Rat.loadRat;
import static ru.m210projects.Powerslave.Enemies.Rex.MAX_REX;
import static ru.m210projects.Powerslave.Enemies.Rex.loadRex;
import static ru.m210projects.Powerslave.Enemies.Roach.MAXROACH;
import static ru.m210projects.Powerslave.Enemies.Roach.loadRoach;
import static ru.m210projects.Powerslave.Enemies.Scorp.MAXSCORP;
import static ru.m210projects.Powerslave.Enemies.Scorp.loadScorp;
import static ru.m210projects.Powerslave.Enemies.Set.MAX_SET;
import static ru.m210projects.Powerslave.Enemies.Set.loadSet;
import static ru.m210projects.Powerslave.Enemies.Spider.MAX_SPIDERS;
import static ru.m210projects.Powerslave.Enemies.Spider.loadSpider;
import static ru.m210projects.Powerslave.Enemies.Wasp.MAX_WASPS;
import static ru.m210projects.Powerslave.Enemies.Wasp.loadWasp;
import static ru.m210projects.Powerslave.Globals.nLocalPlayer;
import static ru.m210projects.Powerslave.Grenade.MAX_GRENADES;
import static ru.m210projects.Powerslave.Grenade.loadGrenades;
import static ru.m210projects.Powerslave.Light.loadLights;
import static ru.m210projects.Powerslave.LoadSave.SAVESCREENSHOTSIZE;
import static ru.m210projects.Powerslave.Main.game;
import static ru.m210projects.Powerslave.Menus.PSMenuUserContent.episodeManager;
import static ru.m210projects.Powerslave.Object.*;
import static ru.m210projects.Powerslave.Random.loadRandom;
import static ru.m210projects.Powerslave.RunList.MAXRUN;
import static ru.m210projects.Powerslave.RunList.loadRunList;
import static ru.m210projects.Powerslave.PSSector.*;
import static ru.m210projects.Powerslave.Slide.loadSlide;
import static ru.m210projects.Powerslave.Snake.MAX_SNAKES;
import static ru.m210projects.Powerslave.Snake.loadSnakes;
import static ru.m210projects.Powerslave.Sprites.loadBubbles;
import static ru.m210projects.Powerslave.Switch.MAXSWITCH;
import static ru.m210projects.Powerslave.Switch.loadSwitches;

public class SafeLoader {

    public boolean gClassicMode;

    //Classic mode
    public String savename;
    public int level;
    public int best;
    public int lastlevel;
    public int warp_on;
    public Entry boardfilename;
    public final int[] nPlayerWeapons = new int[8];
    public final int[] nPlayerClip = new int[8];
    public final int[] nPistolClip = new int[8];
    public final int[] nPlayerLives = new int[8];
    public final int[] nPlayerItem = new int[8];
    public final PlayerStruct[] PlayerList = new PlayerStruct[8];

    //MapInfo
    public Sector[] sector;
    public Wall[] wall;
    public List<Sprite> sprite;
    public final int[] nNetStartSprite = new int[8];

    //Data
    public final int[] nBodySprite = new int[50];
    public int nCurBodyNum, nBodyTotal;
    public final int[] nChunkSprite = new int[75];
    public final int[] nBodyGunSprite = new int[50];
    public int nCurChunkNum, nCurBodyGunNum, nChunkTotal;
    public int nAnimsFree;
    public final int[] AnimsFree = new int[MAX_LIL_ANIM];
    public final int[] AnimRunRec = new int[MAX_LIL_ANIM];
    public final byte[] AnimFlags = new byte[MAX_LIL_ANIM];
    public final AnimStruct[] AnimList = new AnimStruct[MAX_LIL_ANIM];
    public short initsect;
    public int initx, inity, initz, inita;
    public int nFreeze;
    public short connecthead;
    public final short[] connectpoint2 = new short[MAXPLAYERS];
    public int nXDamage, nYDamage;
    public short nDoppleSprite, nPlayerTorch;
    public short nPlayerInvisible, nPlayerDouble, nPlayerViewSect, nPlayerFloorSprite, nPlayerScore, nPlayerGrenade;
    public short nPlayerSnake, nDestVertPan, dVertPan, nDeathType, nQuake;
    public boolean bTouchFloor;
    public int ElevCount;
    public final ElevStruct[] Elevator = new ElevStruct[1024];
    public int WallFaceCount;
    public final WallFaceStruct[] WallFace = new WallFaceStruct[4096];
    public int ObjectCount;
    public final ObjectStruct[] ObjectList = new ObjectStruct[128];
    public int nDrips;
    public final DripStruct[] sDrip = new DripStruct[50];
    public int nTrails;
    public int nTrailPoints;
    public final TrailStruct[] sTrail = new TrailStruct[40];
    public final TrailPointStruct[] sTrailPoint = new TrailPointStruct[100];
    public final int[] nTrailPointPrev = new int[100];
    public final int[] nTrailPointNext = new int[100];
    public final int[] nTrailPointVal = new int[100];
    public int nTraps;
    public final TrapStruct[] sTrap = new TrapStruct[40];
    public final int[] nTrapInterval = new int[40];
    public int nBobs;
    public final BobStruct[] sBob = new BobStruct[200];
    public final int[] sBobID = new int[200];
    public int randA;
    public int randB;
    public int randC;
    public int lasthitx, lasthity, lasthitz;
    public int lasthitsect, lasthitwall, lasthitsprite;
    public int nBulletCount;
    public int nBulletsFree;
    public final int[] BulletFree = new int[500];
    public final int[] nBulletEnemy = new int[500];
    public final BulletStruct[] BulletList = new BulletStruct[500];
    public int nMachineCount;
    public final BubbleMachineStruct[] Machine = new BubbleMachineStruct[125];
    public int nFreeCount;
    public final byte[] nBubblesFree = new byte[200];
    public final BubbleStruct[] BubbleList = new BubbleStruct[200];
    public int nGrenadeCount;
    public int nGrenadesFree;
    public final int[] GrenadeFree = new int[MAX_GRENADES];
    public final int[] nGrenadePlayer = new int[MAX_GRENADES];
    public final GrenadeStruct[] GrenadeList = new GrenadeStruct[MAX_GRENADES];
    public int nSnakesFree;
    public final int[] SnakeFree = new int[MAX_SNAKES];
    public final int[] nSnakePlayer = new int[MAX_SNAKES];
    public final SnakeStruct[] SnakeList = new SnakeStruct[MAX_SNAKES];
    public int totalmoves;
    public int moveframes;
    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public final BitMap show2dwall = new BitMap(MAXWALLSV7);
    public final BitMap show2dsprite = new BitMap(MAXSPRITESV7);
    public short nCreaturesLeft;
    public short nCreaturesMax;
    public short nSpiritSprite;
    public short nMagicCount;
    public short nRegenerates;
    public short nFirstRegenerate;
    public short nNetStartSprites;
    public short nCurStartSprite;
    public short nNetPlayerCount;
    public short nRadialSpr;
    public short nRadialDamage;
    public short nDamageRadius;
    public short nRadialOwner;
    public short nRadialBullet;
    public short nDronePitch;
    public short nFinaleSpr;
    public short lFinaleStart;
    public short nFinaleStage;
    public short nSmokeSparks;
    public int AnubisCount;
    public int nAnubisDrum;
    public final EnemyStruct[] AnubisList = new EnemyStruct[MAXANUBIS];
    public int LionCount;
    public final LionStruct[] LionList = new LionStruct[MAXLION];
    public int FishCount;
    public int nChunksFree;
    public final int[] nFreeChunk = new int[MAX_FISHS];
    public final FishChunk[] FishChunk = new FishChunk[MAX_FISHS];
    public final EnemyStruct[] FishList = new EnemyStruct[MAX_FISHS];
    public int LavaCount;
    public final EnemyStruct[] LavaList = new EnemyStruct[MAX_LAVAS];
    public int MummyCount;
    public final EnemyStruct[] MummyList = new EnemyStruct[MAX_MUMMIES];
    public int QueenCount;
    public final int[] nEggFree = new int[MAX_EGGS];
    public int nEggsFree;
    public final EnemyStruct[] QueenEgg = new EnemyStruct[MAX_EGGS];
    public int nVelShift;
    public final EnemyStruct QueenHead = new EnemyStruct();
    public int nHeadVel;
    public final QueenStruct QueenList = new QueenStruct();
    public int QueenChan;
    public final int[] tailspr = new int[7];
    public int nQHead;
    public final int[] MoveQX = new int[25];
    public final int[] MoveQY = new int[25];
    public final int[] MoveQZ = new int[25];
    public final int[] MoveQA = new int[25];
    public final int[] MoveQS = new int[25];
    public final RaStruct[] Ra = new RaStruct[8];
    public int nRatCount;
    public int nMinChunk;
    public int nMaxChunk, nPlayerPic;
    public final EnemyStruct[] RatList = new EnemyStruct[MAX_RAT];
    public int RexCount;
    public final EnemyStruct[] RexList = new EnemyStruct[MAX_REX];
    public final int[] RexChan = new int[MAX_REX];
    public int RoachCount;
    public final EnemyStruct[] RoachList = new EnemyStruct[MAXROACH];
    public int ScorpCount;
    public final ScorpStruct[] ScorpList = new ScorpStruct[MAXSCORP];
    public final int[] ScorpChan = new int[MAXSCORP];
    public int SetCount;
    public final SetStruct[] SetList = new SetStruct[MAX_SET];
    public final int[] SetChan = new int[MAX_SET];
    public int SpiderCount;
    public final EnemyStruct[] SpiderList = new EnemyStruct[MAX_SPIDERS];
    public int nWaspCount;
    public int nVelShift_X_1;
    public final WaspStruct[] WaspList = new WaspStruct[MAX_WASPS];
    public short nEnergyChan;
    public short nEnergyBlocks;
    public short nEnergyTowers;
    public short nSwitchSound;
    public short nStoneSound;
    public short nElevSound;
    public short nStopSound;
    public int lCountDown;
    public short nRedTicks;
    public short nAlarmTicks;
    public short nClockVal;
    public short nButtonColor;
    public int nFlickerCount;
    public int nGlowCount;
    public int nFlowCount;
    public int nFlashes;
    public int nFirstFlash = -1;
    public int nLastFlash = -1;
    public final int[] nFreeFlash = new int[2000];
    public final int[] nNextFlash = new int[2000];
    public final FlashStruct[] sFlash = new FlashStruct[2000];
    public final GrowStruct[] sGlow = new GrowStruct[50];
    public final FlickerStruct[] sFlicker = new FlickerStruct[100];
    public final FlowStruct[] sFlowInfo = new FlowStruct[375];
    public final byte[] sMoveDir = new byte[50];
    public final MoveSectStruct[] sMoveSect = new MoveSectStruct[50];
    public int nMoveSects;
    public int nPushBlocks;
    public final BlockInfo[] sBlockInfo = new BlockInfo[100];
    public int PointCount;
    public final int[] PointFree = new int[1024];
    public int SlideCount;
    public final int[] SlideFree = new int[128];
    public final PointStruct[] PointList = new PointStruct[1024];
    public final SlideStruct[] SlideData = new SlideStruct[128];
    public final SlideStruct2[] SlideData2 = new SlideStruct2[128];
    public int SwitchCount;
    public final SwitchStruct[] SwitchData = new SwitchStruct[MAXSWITCH];
    public int LinkCount;
    public final byte[][] LinkMap = new byte[1024][8];
    public final int[] SectFlag = new int[MAXSECTORS];
    public final int[] SectDepth = new int[MAXSECTORS];
    public final int[] SectSpeed = new int[MAXSECTORS];
    public final int[] SectDamage = new int[MAXSECTORS];
    public final int[] SectAbove = new int[MAXSECTORS];
    public final int[] SectBelow = new int[MAXSECTORS];
    public final int[] SectSound = new int[MAXSECTORS];
    public final int[] SectSoundSect = new int[MAXSECTORS];
    public int ChannelList;
    public int ChannelLast;
    public final Channel[] channel = new Channel[4096];
    public int NewRun;
    public int RunCount = -1;
    public int RunChain;
    public final int[] RunFree = new int[MAXRUN];
    public final RunData[] RunData = new RunData[MAXRUN];
    public int bTorch;
    public EpisodeInfo addon;
    public boolean packedAddon;
    public String addonFileName;
    private String message;

    public SafeLoader() {
        for (int i = 0; i < 8; i++) {
            PlayerList[i] = new PlayerStruct();
        }
    }

    public boolean load(InputStream is) {
        try {
            addon = null;
            addonFileName = null;
            message = null;
            LoadGDXHeader(is);

            if (gClassicMode) {
                nPlayerWeapons[nLocalPlayer] = StreamUtils.readShort(is);
                PlayerList[nLocalPlayer].currentWeapon = StreamUtils.readShort(is);
                nPlayerClip[nLocalPlayer] = StreamUtils.readShort(is);
                nPlayerItem[nLocalPlayer] = StreamUtils.readShort(is);
                PlayerList[nLocalPlayer].readObject(is, false);
                nPlayerLives[nLocalPlayer] = StreamUtils.readShort(is);
            } else {
                lastlevel = StreamUtils.readInt(is);

                MapLoad(is);
                loadAnm(this, is);
                loadSprites(is);
                loadSectors(is);

                loadWallFaces(this, is);
                loadRunList(this, is);
                loadPlayer(is);
                loadRandom(this, is);

                totalmoves = StreamUtils.readInt(is);
                moveframes = StreamUtils.readInt(is);

                if (is.available() != 0) {
                    return false;
                }
            }

            if (warp_on == 2) { // try to find addon
                addon = findAddon(addonFileName);
                if (addon == null) {
                    message = "Can't find user episode file: " + boardfilename.getName();
                    warp_on = 1;

                    lastlevel = 0;
                    level = 0;
                    best = 0;
                }
            }

            return true;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    private EpisodeInfo findAddon(String addonFileName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                return episodeManager.getEpisode(addonEntry);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }

    public void LoadHeader(InputStream is) throws IOException {
        // Load version
        StreamUtils.skip(is, 8); // time
        savename = StreamUtils.readString(is, 32).trim();
        gClassicMode = StreamUtils.readBoolean(is);
        level = StreamUtils.readShort(is);
        best = StreamUtils.readShort(is);
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        boolean hasCapt = StreamUtils.readBoolean(is);
        if (hasCapt) {
            StreamUtils.skip(is, SAVESCREENSHOTSIZE);
        }

        warp_on = StreamUtils.readByte(is);
        if (warp_on == 2) {
            addonFileName = StreamUtils.readDataString(is).toLowerCase();
        }

        boardfilename = game.getCache().getEntry(StreamUtils.readDataString(is), true);
    }

    public EpisodeInfo LoadGDXHeader(InputStream is) throws IOException {
        gClassicMode = false;
        level = -1;
        best = -1;
        warp_on = 0;
        packedAddon = false;
        addon = null;
        addonFileName = null;
        boardfilename = DUMMY_ENTRY;

        LoadHeader(is);
        LoadGDXBlock(is);

        if (warp_on == 2) { // try to find addon
            addon = findAddon(addonFileName);
        }

        return addon;
    }

    public void MapLoad(InputStream is) throws IOException {
        sector = new Sector[StreamUtils.readInt(is)];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);
        for (int i = 0; i < numSprites; i++) {
            sprite.add(new Sprite().readObject(is));
        }
    }


    private void loadPlayer(InputStream is) throws IOException {
        PlayerList[nLocalPlayer].readObject(is, true);

        connecthead = StreamUtils.readShort(is);
        for (int i = 0; i < connectpoint2.length; i++) {
            connectpoint2[i] = StreamUtils.readShort(is);
        }

        bTorch = StreamUtils.readByte(is);
        nFreeze = StreamUtils.readInt(is);
        nXDamage = StreamUtils.readInt(is);
        nYDamage = StreamUtils.readInt(is);
        nDoppleSprite = StreamUtils.readShort(is);
        nPlayerClip[nLocalPlayer] = StreamUtils.readShort(is);
        nPistolClip[nLocalPlayer] = StreamUtils.readShort(is);
        nPlayerTorch = StreamUtils.readShort(is);
        nPlayerWeapons[nLocalPlayer] = StreamUtils.readShort(is);
        nPlayerLives[nLocalPlayer] = StreamUtils.readShort(is);
        nPlayerItem[nLocalPlayer] = StreamUtils.readShort(is);
        nPlayerInvisible = StreamUtils.readShort(is);
        nPlayerDouble = StreamUtils.readShort(is);
        nPlayerViewSect = StreamUtils.readShort(is);
        nPlayerFloorSprite = StreamUtils.readShort(is);
        nPlayerScore = StreamUtils.readShort(is);
        nPlayerGrenade = StreamUtils.readShort(is);
        nPlayerSnake = StreamUtils.readShort(is);
        nDestVertPan = StreamUtils.readShort(is);
        dVertPan = StreamUtils.readShort(is);
        nDeathType = StreamUtils.readShort(is);
        nQuake = StreamUtils.readShort(is);
        bTouchFloor = StreamUtils.readBoolean(is);

        initx = StreamUtils.readInt(is);
        inity = StreamUtils.readInt(is);
        initz = StreamUtils.readInt(is);
        inita = StreamUtils.readShort(is);
        initsect = StreamUtils.readShort(is);

        show2dsector.readObject(is);
        show2dwall.readObject(is);
        show2dsprite.readObject(is);
    }

    private void loadSprites(InputStream is) throws IOException {
        loadEnemies(is);

        loadBullets(this, is);
        loadGrenades(this, is);
        loadBubbles(this, is);
        loadSnakes(this, is);
        loadObjects(this, is);
        loadTraps(this, is);
        loadDrips(this, is);

        nCreaturesLeft = StreamUtils.readShort(is);
        nCreaturesMax = StreamUtils.readShort(is);
        nSpiritSprite = StreamUtils.readShort(is);
        nMagicCount = StreamUtils.readShort(is);
        nRegenerates = StreamUtils.readShort(is);
        nFirstRegenerate = StreamUtils.readShort(is);
        nNetStartSprites = StreamUtils.readShort(is);
        nCurStartSprite = StreamUtils.readShort(is);
        nNetPlayerCount = StreamUtils.readShort(is);

        for (int i = 0; i < 8; i++) {
            nNetStartSprite[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < nChunkSprite.length; i++) {
            nChunkSprite[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < nBodyGunSprite.length; i++) {
            nBodyGunSprite[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < nBodySprite.length; i++) {
            nBodySprite[i] = StreamUtils.readShort(is);
        }

        nCurChunkNum = StreamUtils.readShort(is);
        nCurBodyNum = StreamUtils.readShort(is);
        nCurBodyGunNum = StreamUtils.readShort(is);
        nBodyTotal = StreamUtils.readShort(is);
        nChunkTotal = StreamUtils.readShort(is);

        nRadialSpr = StreamUtils.readShort(is);
        nRadialDamage = StreamUtils.readShort(is);
        nDamageRadius = StreamUtils.readShort(is);
        nRadialOwner = StreamUtils.readShort(is);
        nRadialBullet = StreamUtils.readShort(is);

        nDronePitch = StreamUtils.readShort(is);
        nFinaleSpr = StreamUtils.readShort(is);
        nFinaleStage = StreamUtils.readShort(is);
        lFinaleStart = StreamUtils.readShort(is);
        nSmokeSparks = StreamUtils.readShort(is);
    }

    private void loadSectors(InputStream is) throws IOException {
        loadLights(this, is);
        loadElevs(this, is);
        loadBobs(this, is);
        loadMoves(this, is);
        loadTrails(this, is);
        loadPushBlocks(this, is);
        loadSlide(this, is);
        loadSwitches(this, is);
        loadLinks(this, is);
        loadSecExtra(this, is);

        nEnergyChan = StreamUtils.readShort(is);
        nEnergyBlocks = StreamUtils.readShort(is);
        nEnergyTowers = StreamUtils.readShort(is);

        nSwitchSound = StreamUtils.readShort(is);
        nStoneSound = StreamUtils.readShort(is);
        nElevSound = StreamUtils.readShort(is);
        nStopSound = StreamUtils.readShort(is);

        lCountDown = StreamUtils.readInt(is);
        nAlarmTicks = StreamUtils.readShort(is);
        nRedTicks = StreamUtils.readShort(is);
        nClockVal = StreamUtils.readShort(is);
        nButtonColor = StreamUtils.readShort(is);
    }

    private void loadEnemies(InputStream is) throws IOException {
        loadAnubis(this, is);
        loadFish(this, is);
        loadLava(this, is);
        loadLion(this, is);
        loadMummy(this, is);
        loadQueen(this, is);
        loadRa(this, is);
        loadRat(this, is);
        loadRex(this, is);
        loadRoach(this, is);
        loadScorp(this, is);
        loadSet(this, is);
        loadSpider(this, is);
        loadWasp(this, is);
    }

    public String getMessage() {
        return message;
    }
}
