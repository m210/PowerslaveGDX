// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

import static ru.m210projects.Powerslave.Main.boardService;

public class BulletStruct {

    public int baseSeq;
    public int frmOffset;
    public int nSprite;
    public int bull_6;
    public int bull_8;
    public int type;
    public int zang;
    public int bull_E;
    public int bull_10;
    public byte bull_12;
    public byte bull_13;
    public int xvec;
    public int yvec;
    public int zvec;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, baseSeq);
        StreamUtils.writeShort(os, frmOffset);
        StreamUtils.writeShort(os,  nSprite);
        StreamUtils.writeShort(os, bull_6);
        StreamUtils.writeShort(os, bull_8);
        StreamUtils.writeShort(os, type);
        StreamUtils.writeShort(os, zang);
        StreamUtils.writeShort(os, bull_E);
        StreamUtils.writeShort(os, bull_10);
        StreamUtils.writeByte(os, bull_12);
        StreamUtils.writeByte(os, bull_13);
        StreamUtils.writeInt(os, xvec);
        StreamUtils.writeInt(os, yvec);
        StreamUtils.writeInt(os, zvec);
    }

    public void load(InputStream is) throws IOException {
        baseSeq =  StreamUtils.readShort(is);
        frmOffset =  StreamUtils.readShort(is);
        nSprite = StreamUtils.readShort(is);
        bull_6 =  StreamUtils.readShort(is);
        bull_8 =  StreamUtils.readShort(is);
        type =  StreamUtils.readShort(is);
        zang =  StreamUtils.readShort(is);
        bull_E =  StreamUtils.readShort(is);
        bull_10 =  StreamUtils.readShort(is);
        bull_12 = StreamUtils.readByte(is);
        bull_13 = StreamUtils.readByte(is);
        xvec = StreamUtils.readInt(is);
        yvec = StreamUtils.readInt(is);
        zvec = StreamUtils.readInt(is);
    }

    public void copy(BulletStruct src) {
        baseSeq = src.baseSeq;
        frmOffset = src.frmOffset;
        nSprite = src.nSprite;
        bull_6 = src.bull_6;
        bull_8 = src.bull_8;
        type = src.type;
        zang = src.zang;
        bull_E = src.bull_E;
        bull_10 = src.bull_10;
        bull_12 = src.bull_12;
        bull_13 = src.bull_13;
        xvec = src.xvec;
        yvec = src.yvec;
        zvec = src.zvec;
    }

    public static class BulletResult {
        private int nSprite = -1;
        private int nBullet = -1;

        public BulletResult setResult(int nSprite, int nBullet) {
            this.nBullet = nBullet;
            this.nSprite = nSprite;
            return this;
        }

        public boolean hasResult() {
            return nSprite != -1 || nBullet != -1;
        }

        public int getSpriteIndex() {
            return nSprite;
        }

        public Sprite getSprite() {
            return boardService.getSprite(nSprite);
        }

        public int getBullet() {
            return nBullet;
        }
    }
}
