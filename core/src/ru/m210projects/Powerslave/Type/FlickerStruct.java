// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Type;

import java.io.InputStream;

import java.io.OutputStream;
import ru.m210projects.Build.filehandle.StreamUtils;
import java.io.IOException;

public class FlickerStruct {

    public int field_0;
    public int field_2;
    public int field_4;

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, field_0);
        StreamUtils.writeShort(os, field_2);
        StreamUtils.writeInt(os, field_4);
    }

    public void load(InputStream is) throws IOException {
        field_0 =  StreamUtils.readShort(is);
        field_2 =  StreamUtils.readShort(is);
        field_4 = StreamUtils.readInt(is);
    }

    public void copy(FlickerStruct src) {
        field_0 = src.field_0;
        field_2 = src.field_2;
        field_4 = src.field_4;
    }
}
