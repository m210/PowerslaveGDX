// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Powerslave.Main;
import ru.m210projects.Powerslave.Type.EpisodeEntry;
import ru.m210projects.Powerslave.Type.EpisodeInfo;
import ru.m210projects.Powerslave.Type.EpisodeManager;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.HIGHEST;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_DIRECTORY;
import static ru.m210projects.Powerslave.Factory.PSMenuHandler.ADDON;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Main.*;

public class PSMenuUserContent extends BuildMenu {

    public static final EpisodeManager episodeManager = new EpisodeManager();
    private static GrpFile usergroup;
    public static boolean usecustomarts;
    private final Main app;

    public PSMenuUserContent(final Main app) {
        super(app.pMenu);
        this.app = app;
        MenuTitle title = new PSTitle("User content", 160, 15, 0);

        int width = 240;
        MenuFileBrowser list = new MenuFileBrowser(app, app.getFont(3), app.getFont(0), app.getFont(3), 40, 55, width, 1, 17, BACKGROUND) {
            @Override
            public void init() {
                registerExtension("map", 0, 0);
                registerExtension("zip", 5, 1);
                registerExtension("pk3", 5, 1);
                registerClass(EpisodeEntry.Addon.class, 5, 2);
            }

            @Override
            public void handleDirectory(Directory dir) {
                EpisodeInfo ep = episodeManager.getEpisode(dir.getDirectoryEntry());
                if (ep != null) {
                    // don't show main
                    if (ep.equals(gOriginalEpisode)) {
                        return;
                    }
                    addFile(ep.getEpisodeEntry());
                }
            }

            @Override
            public void handleFile(FileEntry file) {
                if (file.isExtension("map")) {
                    Directory dir = file.getParent();
                    EpisodeInfo addon = episodeManager.getEpisode(dir.getDirectoryEntry());
                    if (addon != null) {
                        for (int j = 0; j < addon.maps(); j++) {
                            if (file.getName().equalsIgnoreCase(addon.gMapInfo.get(j).path)) {
                                return; // This file has added to addon
                            }
                        }
                    }
                    addFile(file);
                } else if (file.isExtension("zip") || file.isExtension("pk3")) {
                    EpisodeInfo ep = episodeManager.getEpisode(file);
                    if (ep != null) {
                        addFile(ep.getEpisodeEntry());
                    }
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                switch (fil.getExtension()) {
                    case "MAP":
                        launchMap(fil);
                        app.pMenu.mClose();
                        break;
                    case "GRP":
                    case "ZIP":
                    case "PK3":
                        launchEpisode(episodeManager.getEpisode(fil));
                        break;
                    default:
                        if (fil instanceof EpisodeEntry) {
                            launchEpisode(episodeManager.getEpisode(fil));
                        }
                        break;
                }
            }

            @Override
            protected void drawHeader(Renderer renderer, int x1, int x2, int y) {
                /*directories*/
                app.getFont(1).drawTextScaled(renderer, x1, y, dirs, 0.5f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
                /*files*/
                app.getFont(1).drawTextScaled(renderer, x2 + 13, y, ffs, 0.5f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
            }
        };

        list.topPal = 20;
        addItem(title, false);
        addItem(list, true);
    }

    public static void resetEpisodeResources(EpisodeInfo def) {
        Console.out.println("Resetting custom resources", OsdColor.GREEN);
        if (usergroup != null) {
            game.getCache().removeGroup(usergroup);
        }
        usergroup = null;
        gCurrentEpisode = def;

        if (!usecustomarts) {
            game.setDefs(game.baseDef);
            return;
        }

        System.err.println("Reset to default resources");
        if (engine.loadpics() == 0) {
            throw new AssertException("ART files not found " + game.getCache().getGameDirectory().getPath().resolve(engine.getTileManager().getTilesPath()));
        }

        if(!game.setDefs(game.baseDef)) {
            game.baseDef.apply();
        }

        usecustomarts = false;
    }

    public static void checkEpisodeResources(EpisodeInfo addon) {
        if (addon.equals(gCurrentEpisode)) {
            return;
        }

        resetEpisodeResources(gOriginalEpisode);

        usergroup = new GrpFile("RemovableGroup");
        EpisodeEntry.Addon addonEntry = addon.getEpisodeEntry();
        DefScript addonScript;
        Group parent = addonEntry.getGroup();

        addonScript = new DefScript(game.baseDef, addonEntry.getFileEntry());
        if (!(parent instanceof Directory)) { // is group entry
            try {
                Entry res = parent.getEntry(appdef); // load def scripts
                if (res.exists()) {
                    addonScript.loadScript(parent.getName() + " script", res);
                }
                searchEpisodeResources(parent, usergroup);
            } catch (Exception e) {
                throw new WarningException("Error found in " + addonEntry.getName() + "\r\n" + e.getMessage());
            }
        } else {
            if (!game.getCache().isGameDirectory(parent)) {
                searchEpisodeResources(parent, usergroup);
                Entry def = parent.getEntry(appdef);
                if (def.exists()) {
                    addonScript.loadScript(def);
                }
            }
        }

        // Loading user package files
        game.getCache().addGroup(usergroup, HIGHEST);
        InitGroupResources(addonScript, usergroup.getEntries());
        gCurrentEpisode = addon;
        game.setDefs(addonScript);
    }

    private static void searchEpisodeResources(Group container, GrpFile resourceHolder) {
        for (Entry file : container.getEntries()) {
            Group subContainer = DUMMY_DIRECTORY;
            if (file.isDirectory() && file instanceof FileEntry) {
                subContainer = ((FileEntry) file).getDirectory();
            } else if (file.isExtension("pk3") || file.isExtension("zip") || file.isExtension("grp") || file.isExtension("rff")) {
                subContainer = game.getCache().newGroup(file);
            }

            if (!subContainer.equals(DUMMY_DIRECTORY)) {
                searchEpisodeResources(subContainer, resourceHolder);
            } else {
                resourceHolder.addEntry(file);
            }
        }
    }

    public static void InitGroupResources(DefScript addonScript, java.util.List<Entry> list) {
        for (Entry res : list) {
            switch (res.getExtension()) {
                case "ART":
                    engine.loadpic(res);
                    usecustomarts = true;
                    break;
                case "DEF":
                    if (!res.getName().equalsIgnoreCase(appdef)) {
                        addonScript.loadScript(res.getName() + " script", res);
                        Console.out.println("Found def-script. Loading " + res.getName());
                    }
                    break;
            }
        }
    }

    private void launchEpisode(EpisodeInfo ep) {
        if (ep == null) {
            return;
        }

        ((NewAddon) app.menu.mMenus[ADDON]).setAddon(ep);
        app.pMenu.mOpen(app.menu.mMenus[ADDON], -1);
    }

    private void launchMap(final FileEntry fil) {
        if (fil == null) {
            return;
        }

        gGameScreen.newgame(fil, 0, false);
    }

}
