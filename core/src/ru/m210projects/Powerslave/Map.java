// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Powerslave.Screens.DemoScreen;
import ru.m210projects.Powerslave.Type.*;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Powerslave.Anim.*;
import static ru.m210projects.Powerslave.Bullet.InitBullets;
import static ru.m210projects.Powerslave.Cinema.DoAfterCinemaScene;
import static ru.m210projects.Powerslave.Cinema.GoToTheCinema;
import static ru.m210projects.Powerslave.Enemies.Anubis.BuildAnubis;
import static ru.m210projects.Powerslave.Enemies.Anubis.InitAnubis;
import static ru.m210projects.Powerslave.Enemies.Fish.BuildFish;
import static ru.m210projects.Powerslave.Enemies.Fish.InitFishes;
import static ru.m210projects.Powerslave.Enemies.LavaDude.BuildLava;
import static ru.m210projects.Powerslave.Enemies.LavaDude.InitLava;
import static ru.m210projects.Powerslave.Enemies.Lion.BuildLion;
import static ru.m210projects.Powerslave.Enemies.Lion.InitLion;
import static ru.m210projects.Powerslave.Enemies.Mummy.BuildMummy;
import static ru.m210projects.Powerslave.Enemies.Mummy.InitMummy;
import static ru.m210projects.Powerslave.Enemies.Queen.BuildQueen;
import static ru.m210projects.Powerslave.Enemies.Queen.InitQueens;
import static ru.m210projects.Powerslave.Enemies.Ra.InitRa;
import static ru.m210projects.Powerslave.Enemies.Rat.BuildRat;
import static ru.m210projects.Powerslave.Enemies.Rat.InitRats;
import static ru.m210projects.Powerslave.Enemies.Rex.BuildRex;
import static ru.m210projects.Powerslave.Enemies.Rex.InitRexs;
import static ru.m210projects.Powerslave.Enemies.Roach.BuildRoach;
import static ru.m210projects.Powerslave.Enemies.Roach.InitRoach;
import static ru.m210projects.Powerslave.Enemies.Scorp.BuildScorp;
import static ru.m210projects.Powerslave.Enemies.Scorp.InitScorp;
import static ru.m210projects.Powerslave.Enemies.Set.BuildSet;
import static ru.m210projects.Powerslave.Enemies.Set.InitSets;
import static ru.m210projects.Powerslave.Enemies.Spider.BuildSpider;
import static ru.m210projects.Powerslave.Enemies.Spider.InitSpider;
import static ru.m210projects.Powerslave.Enemies.Wasp.BuildWasp;
import static ru.m210projects.Powerslave.Enemies.Wasp.InitWasps;
import static ru.m210projects.Powerslave.Energy.InitEnergyTile;
import static ru.m210projects.Powerslave.Globals.*;
import static ru.m210projects.Powerslave.Grenade.InitGrenades;
import static ru.m210projects.Powerslave.Light.*;
import static ru.m210projects.Powerslave.LoadSave.gAutosaveRequest;
import static ru.m210projects.Powerslave.LoadSave.gClassicMode;
import static ru.m210projects.Powerslave.Main.*;
import static ru.m210projects.Powerslave.Object.*;
import static ru.m210projects.Powerslave.RunList.*;
import static ru.m210projects.Powerslave.PSSector.*;
import static ru.m210projects.Powerslave.Slide.*;
import static ru.m210projects.Powerslave.Snake.InitSnakes;
import static ru.m210projects.Powerslave.Sound.*;
import static ru.m210projects.Powerslave.SpiritHead.nSpiritSprite;
import static ru.m210projects.Powerslave.Sprites.InitBubbles;
import static ru.m210projects.Powerslave.Sprites.InitChunks;
import static ru.m210projects.Powerslave.Switch.BuildSwPressWall;
import static ru.m210projects.Powerslave.Switch.InitSwitch;
import static ru.m210projects.Powerslave.Weapons.InitWeapons;

public class Map {

    public static int nPushBlocks;
    public static int nRegenerates;
    public static int nFirstRegenerate;

    public static int nSwitchSound;
    public static int nStoneSound;
    public static int nElevSound;
    public static int nStopSound;
    public static int ldMapZoom;
    public static int lMapZoom;

    public static int nTraps;
    public static final TrapStruct[] sTrap = new TrapStruct[40];
    public static final int[] nTrapInterval = new int[40];

    private static final Runnable showCredits = () -> {
        GoToTheCinema(9, game.rMenu);
        nPlayerLives[0] = 0;
    };

    private static final Runnable toNextMap = () -> {
        levelnew = levelnum + 1;
        checkNextMap();
    };

    private static final int[] word_14032 = {938, 937, 57, 56, 55, 54, 52, 51, 50, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 29, 28, 27, 19, 18, 17, 16, 15, 14, 10, 9, 2, 1, 0};

    public static void checkNextMap() {
        if (gCurrentEpisode == gOriginalEpisode) {
            if (nNetPlayerCount == 0 && !DemoScreen.isDemoPlaying() && levelnew > 0 && levelnew <= 20) {
                if (levelnum > nBestLevel) {
                    nBestLevel = levelnum;
                }
                gMap.showMap(levelnum, levelnew, nBestLevel);
            }
        } else if (mUserFlag != UserFlag.UserMap && levelnew > 0) {
            if (levelnew <= gCurrentEpisode.maps()) {
                if (levelnum > nBestLevel) {
                    nBestLevel = levelnum;
                }
                if (levelnew > 1 || gClassicMode) {
                    gAutosaveRequest = true;
                }
                gGameScreen.changemap(levelnew, null);
            } else {
                Gdx.app.postRunnable(showCredits);
            }
        }
    }

    public static void FinishLevel() {
        bCamera = false;
        bMapMode = false;
        StopAllSounds();
        if (gCurrentEpisode != gOriginalEpisode || levelnum != 20) {
            SetLocalChan(1);
            PlayLocalSound(StaticSound[59], 0);
            SetLocalChan(0);
        }

        if (mUserFlag == UserFlag.UserMap || gCurrentEpisode == gTrainingEpisode) {
            Gdx.app.postRunnable(() -> {
                nPlayerLives[0] = 0;
                game.EndGame();
            });
        } else {
            if (gCurrentEpisode == gOriginalEpisode) {
                if (!DoAfterCinemaScene(levelnum, levelnum == 20 ? showCredits : toNextMap)) {
                    levelnew = levelnum + 1;
                }
            } else {
                levelnew = levelnum + 1;
            }
        }
    }

    public static void UpdateMap() {
        Sector sec = boardService.getSector(initsect);
        if (sec != null) {
            if (sec.getCeilingpal() != 3 || nPlayerTorch[nLocalPlayer] != 0) {
                MarkSectorSeen(initsect);
            }
        }
    }

    private static void MarkSectorSeen(int sect) {
        if (!show2dsector.getBit(sect)) {
            show2dsector.setBit(sect);

            Sector sec = boardService.getSector(sect);
            if (sec != null) {
                int startwall = sec.getWallptr();
                int endwall = startwall + sec.getWallnum();
                for (int j = startwall; j < endwall; j++) {
                    show2dwall.setBit(j);
                }
            }
        }
    }

    public static void GrabMap() {
        for (int i = 0; i < boardService.getSectorCount(); i++) {
            MarkSectorSeen(i);
        }
    }

    public static boolean LoadLevel(Entry entry, int num) {
        try {
            Board board = engine.loadboard(entry);
            if (num == 20) {
                lCountDown = 81000;
                nAlarmTicks = 30;
                nRedTicks = 0;
                nClockVal = 0;
                nEnergyTowers = 0;
            }

            nCreaturesLeft = 0;
            nCreaturesMax = 0;
            nFreeze = 0;
            nSpiritSprite = -1;
            InitLion();
            InitRexs();
            InitSets();
            InitQueens();
            InitRoach();
            InitWasps();
            InitRats();
            InitBullets();
            InitWeapons();
            InitGrenades();
            InitAnims();
            InitSnakes();
            InitFishes();
            InitLights();
            InitMap();
            InitBubbles();
            InitObjects();
            InitLava();
            nPushBlocks = 0;
            InitAnubis();
            InitSpider();
            InitMummy();
            InitScorp();
            InitPlayer();
            nRegenerates = 0;
            nFirstRegenerate = -1;
            nMagicCount = 0;
//		InitInput();
            if (num == 20) {
                InitEnergyTile();
            }

            if (num <= 15) {
                nSwitchSound = 33;
                nStoneSound = 23;
                nElevSound = 23;
                nStopSound = 66;
            } else {
                nSwitchSound = 35;
                nStoneSound = 23;
                nElevSound = 51;
                nStopSound = 35;
            }

            BuildPos out = board.getPos();
            initx = out.x;
            inity = out.y;
            initz = out.z;
            inita = out.ang;
            initsect = out.sectnum;

            pskyoff[0] = 0;
            pskyoff[1] = 0;
            pskyoff[2] = 0;
            pskyoff[3] = 0;
            parallaxtype = 0;
            visibility = 1024; //2048;
            Renderer renderer = game.getRenderer();
            renderer.setParallaxOffset(256);
            flash = 0;
            pskybits = 2;
            // precache()
            LoadObjects();
            levelnum = num;
//		LoadSong("lev" + num + ".xmi");

            return true;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\n" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + entry);
            }
        }
        return false;
    }

    public static void LoadObjects() {
        InitRun();
        InitChan();
        LinkCount = 1024; // InitLink();
        InitPoint();
        InitSlide();
        InitSwitch();
        InitElev();
        InitWallFace();
        InitSectFlag();

        Sector[] sectors = boardService.getBoard().getSectors();
        for (short i = 0; i < sectors.length; ++i) {
            Sector sec = sectors[i];
            short hitag = sec.getHitag();
            short lotag = sec.getLotag();
            sec.setHitag(0);
            sec.setLotag(0);
            sec.setExtra(-1);
            if (hitag != 0 || lotag != 0) {
                sec.setLotag( (HeadRun() + 1));
                sec.setHitag(lotag);
                ProcessSectorTag(i, lotag, hitag);
            }
        }

        Wall[] walls = boardService.getBoard().getWalls();
        for (short j = 0; j < walls.length; ++j) {
            Wall wal = walls[j];
            wal.setExtra(-1);
            short lotag = wal.getLotag();
            wal.setLotag(0);
            if (wal.getHitag() != 0 || lotag != 0) {
                wal.setLotag( (HeadRun() + 1));
                ProcessWallTag(j, lotag, wal.getHitag());
            }
        }
        ExamineSprites();
        PostProcess();
        InitRa();
        InitChunks();

        for (short i = 0; i < 4096; ++i) {
            ChangeChannel(i, 0);
            ReadyChannel(i);
        }

        nCamerax = initx;
        nCameray = inity;
        nCameraz = initz;
    }

    public static void PostProcess() {
        int v0 = 0;
        int v1 = 0;
        int v2 = 0;
        while (true) {
            if (v1 >= nMoveSects) {
                int v9 = 0;
                int v42 = 0;
                int v36 = 0;

                while (v9 < nBobs) {
                    if (sBob[v42].field_3 == 0) {
                        int v10 = 0;
                        int v11 = 0;
                        int v45 = sBobID[v36];
                        while (v10 < nBobs) {
                            if (v10 != v9 && sBob[v42].field_3 != 0 && v45 == sBobID[v11]) {
                                SnapSectors(v9, v10, 0);
                            }
                            ++v11;
                            ++v10;
                        }
                    }
                    ++v9;
                    ++v42;
                    ++v36;
                }
                if (levelnew == 20) {
                    int v12 = 0;
                    int v13 = 0;
                    int v43 = 0;
                    while (v12 < boardService.getSectorCount()) {
                        Sector sec = boardService.getSector(v13);
                        if (sec != null) {
                            short v14 = sec.getWallptr();
                            short v15 = sec.getWallnum();
                            SectSoundSect[v43] = v12;
                            short v16 = v14;
                            int v17 = v14 + v15;

                            SectSound[v43] = StaticSound[62];
                            while (v16 < v17) {
                                Wall wall = boardService.getWall(v16);
                                if (wall != null && wall.getPicnum() == 3603) {
                                    wall.setPal(1);
                                    int i = engine.insertsprite(v12, 407);
                                    Sprite spr = boardService.getSprite(i);
                                    if (spr != null) {
                                        spr.setCstat(32768);
                                    }
                                }
                                ++v16;
                                ++v14;
                            }
                            ++v13;
                            ++v12;
                            ++v43;
                        }
                    }
                    int v19 = 0;
                    int v20 = 0;
                    do {
                        Sprite spr = boardService.getSprite(v20);
                        if (spr != null && spr.getStatnum() < MAXSTATUS && spr.getPicnum() == 3603) {
                            engine.changespritestat( v19,  407);
                            spr.setPal(1);
                        }
                        ++v19;
                        ++v20;
                    } while (v19 < MAXSPRITESV7);

                } else {
                    int v21 = 0;
                    int v40 = 0;
                    int v37 = 0;
                    int v35 = 0;
                    while (v21 < boardService.getSectorCount()) {
                        int v44 = 30000;
                        if (SectSpeed[v37] != 0 && SectDepth[v40] != 0 && (SectFlag[v37] & PS_HIT_FLAG) == 0) {
                            SectSoundSect[v37] =  v21;
                            SectSound[v37] =  StaticSound[43];
                        } else {
                            int v22 = 0;
                            int v23 = 0;
                            int v39 = 0;
                            while (v22 < boardService.getSectorCount()) {
                                if (v21 != v22 && SectSpeed[v23] != 0 && (SectFlag[v37] & PS_HIT_FLAG) == 0) {
                                    Sector s1 = boardService.getSector(v35);
                                    Sector s2 = boardService.getSector(v39);
                                    if (s1 != null &&s2 != null) {
                                        Wall w1 = boardService.getWall(s1.getWallptr());
                                        Wall w2 = boardService.getWall(s2.getWallptr());
                                        if (w1 != null && w2 != null) {
                                            int v41 = w1.getX() - w2.getX();
                                            int v26 = v41;
                                            if (v41 < 0) {
                                                v26 = -v41;
                                            }
                                            int v38 = v26;
                                            int v27 = w1.getY() - w2.getY();
                                            if (v27 < 0) {
                                                v27 = -v27;
                                            }
                                            if (v38 < 15000 && v27 < 15000) {
                                                int v28 = v38 + v27;
                                                if (v28 < v44) {
                                                    int v29 = StaticSound[43];
                                                    v44 = v28;
                                                    SectSoundSect[v37] = v22;
                                                    SectSound[v37] = v29;
                                                }
                                            }
                                        }
                                    }
                                }
                                ++v23;
                                ++v22;
                                ++v39;
                            }
                        }
                        ++v21;
                        ++v40;
                        ++v37;
                        ++v35;
                    }
                }

                short v30 = 0;
                int v31 = 0;
                while (true) {
                    if (v30 >= ObjectCount) {
                        return;
                    }

                    Sprite spr = boardService.getSprite(ObjectList[v31].field_6);
                    if (spr != null && spr.getStatnum() == 152) {
                        if (ObjectList[v31].obj_A != 0) {
                            int v32 = ObjectList[v31].obj_A;
                            short v33 = 0;
                            int v34 = 0;
                            ObjectList[v31].obj_A = -1;
                            while (true) {
                                if (v33 >= ObjectCount) {
                                    break;
                                }
                                if (v33 != v30 && spr.getStatnum() == 152 && v32 == ObjectList[v34].obj_A) {
                                    ObjectList[v31].obj_A = v33;
                                    ObjectList[v34].obj_A = v30;
                                    break;
                                }
                                ++v34;
                                ++v33;
                            }

                        } else {
                            ObjectList[v31].obj_A = -1;
                        }
                    }
                    ++v31;
                    ++v30;
                }
            }
            sMoveSect[v0].field_4 = sTrail[sMoveSect[v0].field_2].field_0;
            if ((sMoveSect[v0].field_6 & 0x40) != 0) {
                ChangeChannel(sMoveSect[v0].field_E, 1);
            }
            int v3 = sMoveSect[v0].field_0;
            if ((SectFlag[v3] & 0x2000) != 0) {
                Sector v4 = boardService.getSector(v3);
                if (v4 != null) {
                    v4.setCeilingstat(v4.getCeilingstat() | 0x40);
                    v4.setFloorstat(v4.getFloorstat() & ~0x40);
                }

                int v5 = 0;
                int v6 = 0;
                while (true) {
                    if (v5 >= nMoveSects) {
                        break;
                    }
                    if (v1 != v5 && sMoveSect[v0].field_2 == sMoveSect[v6].field_2) {
                        int v7 = sMoveSect[v0].field_0;
                        sMoveSect[v6].field_8 = sMoveSect[v0].field_0;
                        SnapSectors(sMoveSect[v6].field_0, v7, 0);
                        sMoveSect[v2].field_0 =  v5;
                        break;
                    }
                    ++v6;
                    ++v5;
                }

            }
            ++v2;
            ++v1;
            ++v0;
        }
    }

    public static void InitSectFlag() {
        for (int i = 0; i < 1024; i++) {
            SectSoundSect[i] = -1;
            SectSound[i] = -1;
            SectAbove[i] = -1;
            SectBelow[i] = -1;
            SectDepth[i] = 0;
            SectFlag[i] = 0;
            SectSpeed[i] = 0;
            SectDamage[i] = 0;
        }
    }

    public static void InitObjects() {
        ObjectCount = 0;
        nTraps = 0;
        nDrips = 0;
        nBobs = 0;
        nTrails = 0;
        nTrailPoints = 0;
        nMoveSects = 0;

        nEnergyBlocks = 0;
        nDronePitch = 0;
        nFinaleStage = 0;
        lFinaleStart = 0;
        nSmokeSparks = 0;

        for (int i = 0; i < 40; i++) {
            if (sTrail[i] == null) {
                sTrail[i] = new TrailStruct();
            }
            sTrail[i].clear();
        }
    }

    public static void InitMap() {
        show2dsector.clear();
        show2dwall.clear();
        show2dsprite.clear();
        ldMapZoom = 64;
        lMapZoom = 1000;
    }

    public static void InitPlayer() {
        for (int i = 0; i < 8; i++) {
            PlayerList[i].spriteId = -1;
            PlayerList[i].crouch_toggle = false;
        }
    }

    public static void SnapBobs(int a1, int a2) {
        int v3 = -1;
        int v6 = -1;
        int v8;
        for (int v4 = 0; v4 < nBobs; v4++) {
            int v7 = sBob[v4].field_0;
            if (v7 == a1) {
                v8 = v3;
                v6 = v4;
            } else {
                if (a2 != v7) {
                    continue;
                }
                v8 = v6;
                v3 = v4;
            }
            if (v8 != -1) {
                break;
            }
        }
        if (v3 == -1 && v6 == -1) {
            return;
        }

        sBob[v3].field_2 = sBob[v6].field_2;
    }

    public static void BuildBubbleMachine(int a1) {
        if (nMachineCount >= 125) {
            throw new AssertException("too many bubble machines in level " + levelnew);
        }
        if (Machine[nMachineCount] == null) {
            Machine[nMachineCount] = new BubbleMachineStruct();
        }

        BubbleMachineStruct v2 = Machine[nMachineCount];
        v2.field_4 = 75;
        v2.field_2 =  a1;
        v2.field_0 = v2.field_4;
        ++nMachineCount;

        Sprite pSprite = boardService.getSprite(a1);
        if (pSprite != null) {
            pSprite.setCstat(32768);
        }
    }

    public static void AddSectorBob(int a1, int a2, int a3) {
        if (nBobs >= 200) {
            throw new AssertException("Too many bobs!");
        }

        if (sBob[nBobs] == null) {
            sBob[nBobs] = new BobStruct();
        }

        Sector sec = boardService.getSector(a1);
        sBob[nBobs].field_3 = (byte) a3;
        int nSector;
        if (sec != null) {
            if (a3 != 0) {
                nSector = sec.getCeilingz();
            } else {
                nSector = sec.getFloorz();
            }
            sBob[nBobs].field_4 = nSector;
        }
        sBob[nBobs].field_2 = (byte) (16 * a2);
        sBobID[nBobs] = a2;
        sBob[nBobs].field_0 = a1;
        SectFlag[a1] |= 0x1000;
        nBobs++;
    }

    public static void ProcessWallTag(short wallnum, int nLotag, int nHitag) {
        int real_chan = AllocChannel(nHitag % 1000);
        if (real_chan < 0 || real_chan >= 4096) {
            throw new AssertException("real_chan>=0 && real_chan <MAXCHAN");
        }

        int nVelocity = nLotag / 1000;
        if (nVelocity == 0) {
            nVelocity = 1;
        }

        Wall[] walls = boardService.getBoard().getWalls();
        Wall wall = walls[wallnum];

        switch (nLotag % 1000) {
            case 1:
                AddRunRec(channel[real_chan].head, BuildWallFace(real_chan, wallnum, 2, wall.getPicnum(), wall.getPicnum() + 1));
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(2, 1, 0), wallnum));
                return;
            case 6:
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(2, 1, 0), wallnum));
                return;
            case 7:
                AddRunRec(channel[real_chan].head, BuildWallFace(real_chan, wallnum, 2, wall.getPicnum(), wall.getPicnum() + 1));
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(1, 1), wallnum));
                return;
            case 8:
                AddRunRec(channel[real_chan].head, BuildWallFace(real_chan, wallnum, 2, wall.getPicnum(), wall.getPicnum() + 1));
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(2, -1, 0), wallnum));
                return;
            case 9:
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(2, 1, 1), wallnum));
                return;
            case 10:
                AddRunRec(channel[real_chan].head, BuildSwPressWall(real_chan, BuildLink(2, -1, 0), wallnum));
                return;
            case 12:
            case 14:
            case 16:
            case 19:
            case 20:
                int w1 = 0, w2 = 0;
                for (int i = wall.getPoint2(); i != wallnum; i = walls[i].getPoint2()) {
                    w2 = w1;
                    w1 = i;
                }
                AddRunRec(channel[real_chan].head, BuildSlide(real_chan, wallnum, w1, w2, wall.getPoint2(), walls[wall.getPoint2()].getPoint2(), walls[walls[wall.getPoint2()].getPoint2()].getPoint2()));
                return;
            case 24:
                AddFlow(wallnum, 4 * nVelocity, 3);
                return;
            case 25:
                AddFlow(wallnum, 4 * nVelocity, 2);
        }
    }

    public static void ProcessTrailSprite(int spritenum, int a2, int a3) {
        if (nTrailPoints >= 100) {
            throw new AssertException("Too many trail point sprites (900-949)... increase MAX_TRAILPOINT");
        }

        int p = nTrailPoints;
        if (sTrailPoint[p] == null) {
            sTrailPoint[p] = new TrailPointStruct();
        }

        Sprite pSprite = boardService.getSprite(spritenum);
        if (pSprite == null) {
            return;
        }

        sTrailPoint[p].x = pSprite.getX();
        sTrailPoint[p].y = pSprite.getY();
        nTrailPoints++;

        int v8 = FindTrail(a3);
        int v9 = sTrail[v8].field_0;
        nTrailPointVal[p] =  ((a2 + 124) & 0xFF);
        if (v9 == -1) {
            sTrail[v8].field_0 =  p;
            sTrail[v8].field_4 =  p;
            nTrailPointNext[p] = -1;
            nTrailPointPrev[p] = -1;
        } else {
            int v10 = -1;
            int v11;
            while (true) {
                if (v9 == -1) {
                    break;
                }

                v11 = v9;
                if (nTrailPointVal[v9] > a2 - 900) {
                    nTrailPointPrev[p] = nTrailPointPrev[v11];
                    nTrailPointPrev[v11] =  p;
                    nTrailPointNext[p] =  v9;
                    if (v9 == sTrail[v8].field_0) {
                        sTrail[v8].field_0 =  p;
                    }
                    break;
                }

                v10 = v9;
                v9 = nTrailPointNext[v9];
            }

            if (v9 == -1) {
                nTrailPointNext[v10] =  p;
                nTrailPointPrev[p] =  v10;
                nTrailPointNext[p] = -1;
                sTrail[v8].field_4 =  p;
            }
        }

        engine.deletesprite(spritenum);
    }

    public static void BuildItemAnim(final int a1) {
        Sprite sprite = boardService.getSprite(a1);
        if (sprite == null) {
            return;
        }

        int v4 = (sprite.getStatnum() - 906);
        if (nItemAnimInfo[v4].field_0 < 0) {
            int v9 = nItemAnimInfo[v4].field_2;
            sprite.setOwner(-1);
            sprite.setYrepeat(v9);
            sprite.setXrepeat(v9);
        } else {
            int v10 = BuildAnim(a1, 41, nItemAnimInfo[v4].field_0, sprite.getX(), sprite.getY(), sprite.getZ(), sprite.getSectnum(), nItemAnimInfo[v4].field_2, 20);
            int v6 =  GetAnimSprite(v10);
            Sprite spr2 = boardService.getSprite(v6);
            if (spr2 == null) {
                return;
            }

            if (v4 == 44) {
                spr2.setCstat(spr2.getCstat() | 2);
            }
            engine.changespritestat(v6, sprite.getStatnum());
            spr2.setOwner( v10);
            spr2.setHitag(sprite.getHitag());
        }
    }

    public static void SetBelow(int a1, int a2) {
        SectBelow[a1] = a2;
    }

    public static void SetAbove(int a1, int a2) {
        SectAbove[a1] = a2;
    }

    public static void ProcessSpriteTag(final int nSprite, int nStatnum, int a3) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return;
        }

        if ((pSprite.getCstat() & 48) == 48) {
            pSprite.setCstat(pSprite.getCstat() & ~48);
        }

        int ch = AllocChannel(a3 % 1000);
        if (nStatnum < 6 || nStatnum > 60) {
            if (nStatnum >= 900 && nStatnum <= 949) {
                ProcessTrailSprite(nSprite, nStatnum, a3);
                return;
            }
            int nVelocity = nStatnum / 1000;
            if (nVelocity == 0) {
                nVelocity = 1;
            }

            int v7 = nStatnum % 1000;
            if (!bNoCreatures || v7 < 100 || v7 > 118) {
                int v8 = 39;
                for (int v9 = 0; v8 >= 0 && v9 < word_14032.length; v8--) {
                    if (word_14032[v9++] == v7 - 61) {
                        break;
                    }
                }

                switch (v8) {
                    case 0:
                        engine.mydeletesprite(nSprite);
                        return;
                    case 1:
                        nSpiritSprite = nSprite;
                        pSprite.setCstat(pSprite.getCstat() | 0x8000);
                        return;
                    case 2:
                        nNetStartSprite[nNetStartSprites] = nSprite;
                        pSprite.setCstat( 32768);
                        ++nNetStartSprites;
                        return;
                    case 3:
                        engine.changespritestat(nSprite,  405);
                        pSprite.setCstat( 32768);
                        return;
                    case 4:
                        BuildDrip(nSprite);
                        return;
                    case 5:
                        AddRunRec(channel[ch].head, BuildFireBall(nSprite, a3, nVelocity));
                        return;
                    case 6:
                        BuildObject(nSprite, 1, a3);
                        return;
                    case 7:
                        BuildObject(nSprite, 0, a3);
                        return;
                    case 8:
                        AddRunRec(channel[ch].head, BuildArrow(nSprite, nVelocity));
                        return;
                    case 9:
                        AddFlow(nSprite, nVelocity, 1);
                        SectFlag[pSprite.getSectnum()] |= 800;
                        engine.mydeletesprite(nSprite);
                        return;
                    case 10:
                    case 13:
                        SectSpeed[pSprite.getSectnum()] =  nVelocity;
                        SectFlag[pSprite.getSectnum()] =  (pSprite.getAng() | SectFlag[pSprite.getSectnum()]);
                        engine.mydeletesprite(nSprite);
                        return;
                    case 11:
                        SectFlag[pSprite.getSectnum()] |= 0x2000;
                        engine.mydeletesprite(nSprite);
                        return;
                    case 12:
                        AddFlow(nSprite, nVelocity, 0);
                        break;
                    case 14:
                        BuildObject(nSprite, 3, a3);
                        return;
                    case 15:
                        BuildBubbleMachine(nSprite);
                        return;
                    case 16:
                        SectDepth[pSprite.getSectnum()] =  (a3 << 8);
                        break;
                    case 17:
                        AddSectorBob(pSprite.getSectnum(), a3, 0);
                        break;
                    case 18:
                        int v20 =  (a3 >> 2);
                        if (v20 == 0) {
                            v20 = 1;
                        }

                        SectFlag[pSprite.getSectnum()] |= PS_HIT_FLAG;
                        SectDamage[pSprite.getSectnum()] = v20;
                        engine.mydeletesprite(nSprite);
                        return;
                    case 19:
                        AddSectorBob(pSprite.getSectnum(), a3, 1);
                        break;
                    case 20:
                        SetBelow(pSprite.getSectnum(), a3);
                        SnapSectors(pSprite.getSectnum(), a3, 1);
                        break;
                    case 21:
                        SetAbove(pSprite.getSectnum(), a3);
                        SectFlag[pSprite.getSectnum()] |= 0x2000;
                        break;
                    case 22:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildAnubis(nSprite, 0, 0, 0, 0, 0, 0);
                        return;
                    case 23:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildMummy(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 24:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildLion(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 25:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildRoach(0, nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 26:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildRoach(1, nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 27:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildSpider(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 28:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildFish(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 29:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildRex(nSprite, 0, 0, 0, 0, 0, ch);
                        return;
                    case 30:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildLava(nSprite, 0, 0,0, 0, ch);
                        return;
                    case 31:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildSet(nSprite, 0, 0, 0, 0, 0, ch);
                        return;
                    case 32:
                        BuildScorp(nSprite, 0, 0, 0, 0, 0, ch);
                        return;
                    case 33:
                        BuildQueen(nSprite, 0, 0, 0, 0, 0, ch);
                        return;
                    case 34:
                        BuildRat(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 35:
                        BuildRat(nSprite, 0, 0, 0, 0, -1);
                        return;
                    case 36:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildWasp(nSprite, 0, 0, 0, 0, 0);
                        return;
                    case 37:
                        if (bNoCreatures) {
                            break;
                        }
                        BuildAnubis(nSprite, 0, 0, 0, 0, 0, 1);
                        return;
                    case 38:
                        AddGlow(pSprite.getSectnum(), nVelocity);
                        break;
                    case 39:
                        AddFlicker(pSprite.getSectnum(), nVelocity);
                        break;
                }
            }
        } else {
            if (nStatnum < 16) {
                if (nStatnum < 12) {
                    if (nStatnum == 8) {
                        a3 =  (3 * (a3 / 3));
                    }
                } else if (nStatnum == 12) {
                    a3 = 40;
                } else if (nStatnum == 13) {
                    a3 = 160;
                } else if (nStatnum == 14) {
                    a3 = -200;
                }

                pSprite.setHitag(a3);
                engine.changespritestat(nSprite,  (nStatnum + 900));
                pSprite.setCstat(pSprite.getCstat() & ~0x101);
                BuildItemAnim(nSprite);
                return;
            }

            if (nStatnum > 16) {
                if (!gClassicMode) {
                    switch (nStatnum) {
                        case 25: //extra life
                            int index;
                            do {
                                index = (int) (7 + (44 * Math.random())); //(7 - 50)
                            } while (!changeitem(nSprite, index));

                            engine.changespritestat(nSprite,  (index + 900));
                            pSprite.setCstat(pSprite.getCstat() & ~0x101);
                            BuildItemAnim(nSprite);
                            return;
                        case 59: //checkpoint
                            engine.mydeletesprite(nSprite);
                            return;
                    }
                }

                if (nStatnum == 27) {
                    pSprite.setHitag(1);
                    engine.changespritestat(nSprite,  909);
                    pSprite.setCstat(pSprite.getCstat() & ~0x101);
                    BuildItemAnim(nSprite);
                    return;
                }

                if ((nStatnum == 25 || nStatnum == 59) && nNetPlayerCount != 0) {
                    engine.mydeletesprite(nSprite);
                    return;
                }

                pSprite.setHitag(a3);
                engine.changespritestat(nSprite,  (nStatnum + 900));
                pSprite.setCstat(pSprite.getCstat() & ~0x101);
                BuildItemAnim(nSprite);
                return;
            }
        }

        engine.mydeletesprite(nSprite);
    }

    public static boolean changeitem(int spr, int index) {
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            return false;
        }

        switch (index) {
            case 11: //the map
            case 18: //heart
            case 19: //scarabey
            case 21: //invisibility
            case 22: //torch
            case 37: //cobra
            case 38: //raw energy
            case 49: //magic
            case 13: //health
            case 14: //venom
                pSprite.setHitag(1);
                return true;
            case 50: //soul
                pSprite.setHitag(10);
                return true;
            case 6: //speed loader
                pSprite.setHitag(6);
                pSprite.setPicnum(877);
                return true;
            case 7: //fuel
                pSprite.setHitag(25);
                pSprite.setPicnum(879);
                return true;
            case 8: //m60 ammo
                pSprite.setHitag(50);
                pSprite.setPicnum(882);
                return true;
            case 27: //grenade
                pSprite.setHitag(1);
                pSprite.setPicnum(878);
                return true;
            case 20: //increased weapon power
                pSprite.setHitag(1);
                pSprite.setPicnum(752);
                return true;
            case 23: //sobek mask
                pSprite.setHitag(1);
                pSprite.setPicnum(754);
                return true;
            case 28: //magnum
                pSprite.setHitag(6);
                pSprite.setPicnum(488);
                return true;
            case 29: //m60
                pSprite.setHitag(1);
                pSprite.setPicnum(490);
                return true;
            case 30: //flame thower
                pSprite.setHitag(1);
                pSprite.setPicnum(491);
                return true;
            case 32: //cobra staff
                pSprite.setPicnum(899);
                pSprite.setHitag(1);
                return true;
            case 33: //eye of ra
                pSprite.setHitag(1);
                pSprite.setPicnum(3455);
                return true;
        }

        return false;
    }

    public static void ExamineSprites() {
        nNetStartSprites = 0;
        nCurStartSprite = 0;

        for (short i = 0; i < MAXSPRITESV7; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr != null && spr.getStatnum() == 0) {
                if (spr.getLotag() != 0) {
                    int lotag = spr.getLotag();
                    short hitag = spr.getHitag();
                    spr.setLotag(0);
                    spr.setHitag(0);
                    ProcessSpriteTag(i, lotag, hitag);
                } else {
                    engine.changespritestat(i,  0);
                }
            }
        }

        if (nNetPlayerCount == 0) {
            return;
        }

        int i = engine.insertsprite(initsect,  0);
        Sprite spr = boardService.getSprite(i);
        if (spr != null) {
            spr.setX(initx);
            spr.setY(inity);
            spr.setZ(initz);
            spr.setCstat(32768);
        }

        nNetStartSprite[nNetStartSprites] =  i;
        nNetStartSprites++;
    }
}
