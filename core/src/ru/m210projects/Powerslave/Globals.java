// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave;

import com.badlogic.gdx.Screen;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Powerslave.Main.UserFlag;
import ru.m210projects.Powerslave.Screens.GameScreen;
import ru.m210projects.Powerslave.Type.*;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Powerslave.Main.game;

public class Globals {

    public static final int BACKBUTTON = 9216;
    public static final int MOUSECURSOR = 9217;
    public static final int THEFONT = 9220;
    public static final int HUDLEFT = 9218;
    public static final int HUDRIGHT = 9219;

    public static final int PS_HIT_TYPE_MASK = 0xE000;
    public static final int PS_HIT_INDEX_MASK = 0x1FFF;
    public static final int PS_HIT_SECTOR = 0x4000;
    public static final int PS_HIT_FLAG = 0x4000;
    public static final int PS_HIT_WALL = 0x8000;
    public static final int PS_HIT_SPRITE = 0xC000;

    public static final int EVENT_MASK = 0x7F0000; // (16 - 22 bits)
    public static final int nEvent1 = 1 << 16; // 16 bit FIXME movehit
    public static final int nEventProcess = 2 << 16; // 0x20000 FIXME movehit
    public static final int nEvent3 = 3 << 16; // 0x30000 FIXME movehit
    public static final int nEvent4 = 4 << 16; // 0x40000
    public static final int nEvent5 = 5 << 16;  // 0x50000
    public static final int nEvent6 = 6 << 16;  // 0x60000
    public static final int nEvent7 = 7 << 16; // 0x70000
    public static final int nEventDamage = 8 << 16; // 0x80000
    public static final int nEventView = 9 << 16; // 0x90000
    public static final int nEventRadialDamage = 10 << 16; // 0xA0000
    public static final int nEvent11 = 11 << 16; // bullet
    public static final int nEvent12 = 12 << 16; // spider
    public static final int nEvent13 = 13 << 16; // sprites
    public static final int nEvent14 = 14 << 16; // mummy
    public static final int nEvent15 = 15 << 16; // grenade
    public static final int nEvent16 = 16 << 16; // anim
    public static final int nEvent17 = 17 << 16; // shake
    public static final int nEvent18 = 18 << 16; // fish
    public static final int nEvent19 = 19 << 16; // lion
    public static final int nEvent20 = 20 << 16; // bubble
    public static final int nEvent21 = 21 << 16; // lavadude
    public static final int nEvent22 = 22 << 16; // lavadude
    public static final int nEvent23 = 23 << 16; // object
    public static final int nEvent24 = 24 << 16; // rex
    public static final int nEvent25 = 25 << 16; // set
    public static final int nEvent26 = 26 << 16; // queen
    public static final int nEvent27 = 27 << 16; // queen
    public static final int nEvent28 = 28 << 16; // roach
    public static final int nEvent29 = 29 << 16; // QueenEgg
    public static final int nEvent30 = 30 << 16; // wasp
    public static final int nEvent31 = 31 << 16; // trap
    public static final int nEvent32 = 32 << 16; // fish
    public static final int nEvent33 = 33 << 16; // ra
    public static final int nEvent34 = 34 << 16; // scorp
    public static final int nEvent35 = 35 << 16; // set
    public static final int nEvent36 = 36 << 16; // rat
    public static final int nEvent37 = 37 << 16; // energy block
    public static final int nEvent38 = 38 << 16; // spark

    public static final int[] nItemText = {-1, -1, -1, -1, -1, -1, 18, 20, 19, 13, -1, 10, 1, 0, 2, -1, 3, -1, 4, 5,
            9, 6, 7, 8, -1, 11, -1, 13, 12, 14, 15, -1, 16, 17, -1, -1, -1, 21, 22, -1, -1, -1, -1, -1, -1, 23, 24, 25,
            26, 27, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    public static final String[] gString = {"CINEMAS", "The ancient Egyptian city", "of Karnak has been seized",
            "by unknown powers, and great", "turmoil is spreading into", "neighboring lands, posing",
            "a grave threat to planet", "Earth. Militant forces from", "all parts of the globe have",
            "entered the Karnak Valley,", "but none have returned. The", "only known information",
            "regarding this crisis came", "from a dying Karnak villager", "who managed to wander out of",
            "the valley to safety.", "'They've stolen the great", "king's Mummy...', murmured",
            "the dying villager, but the", "villager died before he", "could say more. With no", "other options, world",
            "leaders have chosen to drop", "you into the valley via", "helicopter in an attempt",
            "to find and destroy the", "threatening forces and", "resolve the mystery that", "has engulfed this once",
            "peaceful land. Flying at", "high altitude to avoid", "being shot down like others",
            "before you, your copter", "mysteriously explodes in the", "air as you barely escape,",
            "with no possible contact", "with the outside world.", "Scared as hell, you descend",
            "into the heart of Karnak...", "home to the celebrated", "burial crypt of the great", "King Ramses.", "END",
            "An evil force known as the", "Kilmaat has besieged the", "sanctity of my palace and",
            "is pulling at the very", "tendrils of my existence.", "These forces intend to",
            "enslave me by reanimating", "my preserved corpse. I have", "protected my corpse with a",
            "genetic key.  If you are", "unsuccessful I cannot", "protect civilization, and",
            "chaos will prevail. I am", "being torn between worlds", "and this insidious",
            "experiment must be stopped.", "END", "I have hidden a mystical", "gauntlet at El Kab that will",
            "channel my energy through", "your hands. Find the", "gauntlet and cross the Aswan",
            "High Dam to defeat the evil", "beast Set.", "END", "Set was a formidable foe.",
            "No mortal has even conquered", "their own fear, much less", "entered mortal battle and", "taken his life.",
            "END", "You've made it halfway toward", "fullfilling your destiny.", "The Kilmaat are growing",
            "restless with your progress.", "Seek out a temple in this", "citadel where I will provide",
            "more information", "END", "The Kilmaat race has", "continued their monsterous",
            "animal-human experiments in", "an effort to solve the key of", "animating my corpse. The",
            "victory defeating Set didn't", "slow you down as much as", "they had planned. They are",
            "actively robbing a slave", "girl of her life to create", "another monsterous",
            "abomination, combining human", "and insect intent on slaying", "you.  Prepare yourself for",
            "battle as she will be waiting", "for you at the Luxor Temple. ", "END", "You've done well to defeat",
            "Selkis. You have distracted", "the Kilmaat with your", "presence and they have", "temporarily abandoned",
            "animation of my corpse.", "The alien Queen Kilmaatikhan", "has a personal vendetta",
            "against you. Arrogance is", "her weakness, and if you can", "defeat Kilmaatikhan, the",
            "battle will be won.", "END", "The Kilmaat have been", "destroyed. Unfortunately,", "your recklessness has",
            "destroyed the earth and all", "of its inhabitants.  All that", "remains is a smoldering hunk", "of rock.",
            "END", "The Kilmaat have been", "defeated and you single", "handedly saved the Earth", "from destruction.",
            "", "", "", "Your bravery and heroism", "are legendary.", "END", "ITEMS", "Life Blood", "Life", "Venom",
            "You're losing your grip", "Full Life", "Invincibility", "Invisibility", "Torch", "Sobek Mask",
            "Increased weapon power!", "The Map!", "An extra life!", ".357 Magnum!", "Grenade", "M-60",
            "Flame Thrower!", "Cobra Staff!", "The Eye of Rah Gauntlet!", "Speed Loader", "Ammo", "Fuel", "Cobra!",
            "Raw Energy", "Power Key", "Time Key", "War Key", "Earth Key", "Magic", "Location Preserved", "COPYRIGHT",
            "Lobotomy Software, Inc.", "3D engine by 3D Realms", "", "LASTLEVEL", "INCOMING MESSAGE", "",
            "OUR LATEST SCANS SHOW", "THAT THE ALIEN CRAFT IS", "POWERING UP, APPARENTLY", "IN AN EFFORT TO LEAVE.",
            "THE BAD NEWS IS THAT THEY", "SEEM TO HAVE LEFT A DEVICE", "BEHIND, AND ALL EVIDENCE",
            "SAYS ITS GOING TO BLOW A", "BIG HOLE IN OUR FINE PLANET.", "A SQUAD IS TRYING TO DISMANTLE",
            "IT RIGHT NOW, BUT NO LUCK SO", "FAR, AND TIME IS RUNNING OUT.", "", "GET ABOARD THAT CRAFT NOW",
            "BEFORE IT LEAVES, THEN FIND", "AND SHOOT ALL THE ENERGY", "TOWERS TO GAIN ACCESS TO THE",
            "CONTROL ROOM. THERE YOU NEED TO", "TAKE OUT THE CONTROL PANELS AND", "THE CENTRAL POWER SOURCE.  THIS",
            "IS THE BIG ONE BUDDY, BEST OF", "LUCK... FOR ALL OF US.", "", "", "CREDITS", "Exhumed", "",
            "Executive Producers", " ", "Brian McNeely", "Paul Lange", "", "Game Concept", " ", "Paul Lange", "",
            "Game Design", " ", "Brian McNeely", "", "Additional Design", " ", "Paul Knutzen", "Paul Lange",
            "John Van Deusen", "Kurt Pfeifer", "Dominick Meissner", "Dane Emerson", "", "Game Programming", " ",
            "Kurt Pfeifer", "John Yuill", "", "Additional Programming", " ", "Paul Haugerud", "",
            "Additional Technical Support", " ", "John Yuill", "Paul Haugerud", "Jeff Blazier", "", "Level Design",
            " ", "Paul Knutzen", "", "Additional Levels", " ", "Brian McNeely", "", "Monsters and Weapons ", " ",
            "John Van Deusen", "", "Artists", " ", "Brian McNeely", "Paul Knutzen", "John Van Deusen", "Troy Jacobson",
            "Kevin Chung", "Eric Klokstad", "Richard Nichols", "Joe Kresoja", "Jason Wiggin", "",
            "Music and Sound Effects", " ", "Scott Branston", "", "Product Testing", " ", "Dominick Meissner",
            "Tom Kristensen", "Jason Wiggin", "Mark Coates", "", "Instruction Manual", " ", "Tom Kristensen", "",
            "Special Thanks", " ", "Jacqui Lyons", "Marjacq Micro, Ltd.", "Mike Brown", "Ian Mathias", "Cheryl Luschei",
            "3D Realms", "Kenneth Silverman", "Greg Malone", "Miles Design", "REDMOND AM/PM MINI MART",
            "7-11 Double Gulp", "", "Thanks for playing", "", "The end", "", "Guess youre stuck here",
            "until the song ends", "", "Maybe this is a good", "time to think about all", "the things you can do",
            "after the music is over.", "", "Or you could just stare", "at this screen", "",
            "and watch these messages", "go by...", "", "...and wonder just how long", "we will drag this out...", "",
            "and believe me, we can drag", "it out for quite a while.", "", "should be over soon...", "",
            "any moment now...", "", "", "", "See ya", "", "END", "PASSWORDS", "HOLLY", "KIMBERLY", "LOBOCOP",
            "LOBODEITY", "LOBOLITE", "LOBOPICK", "LOBOSLIP", "LOBOSNAKE", "LOBOSPHERE", "LOBOSWAG", "LOBOXY", "",
            "PASSINFO", "", "HI SWEETIE, I LOVE YOU", "", "", "SNAKE CAM ENABLED", "FLASHES TOGGLED", "FULL MAP",
            "", "", "", "", "", "EOF",};
    public static final int nCreepyTime = 180; //6sec * 30tics
    public static final int LOGO = 3442; //3592 - Exhumed
    public static final int BACKGROUND = 3352;  //3581 - Exhumed
    public static EpisodeInfo gTrainingEpisode;
    public static EpisodeInfo gOriginalEpisode;
    public static EpisodeInfo gCurrentEpisode;
    public static boolean followmode = false;
    public static int followx, followy;
    public static int followvel, followsvel, followa;
    public static float followang;
    public static UserFlag mUserFlag = UserFlag.None;
    public static Entry boardfilename;
    public static final byte[][] origpalookup = new byte[12][];
    public static final PlayerStruct[] PlayerList = new PlayerStruct[8];
    public static int nLocalPlayer;
    public static int PlayerCount;
    public static final int[] nPlayerDX = new int[8];
    public static final int[] nPlayerDY = new int[8];
    public static final int[] nXDamage = new int[8];
    public static final int[] nYDamage = new int[8];
    public static final int[] nDoppleSprite = new int[8];
    public static final int[] nPlayerOldWeapon = new int[8];
    public static final int[] nPlayerClip = new int[8]; //M60 Clip
    public static final int[] nPistolClip = new int[8]; //Pistol reload
    public static final int[] nPlayerPushSound = new int[8];
    public static final int[] nTauntTimer = new int[8];
    public static final int[] nPlayerTorch = new int[8];
    public static final int[] nPlayerWeapons = new int[8];
    public static final int[] nPlayerLives = new int[8];
    public static final int[] nPlayerItem = new int[8];
    public static final int[] nPlayerInvisible = new int[8];
    public static final int[] nPlayerDouble = new int[8];
    public static final int[] nPlayerViewSect = new int[8];
    public static final int[] nPlayerFloorSprite = new int[8];
    public static final int[] nPlayerScore = new int[8];
    public static final int[] nPlayerPushSect = new int[8];
    public static final int[] totalvel = new int[8];
    public static final int[] nNetStartSprite = new int[8];
    public static int nCurStartSprite, nNetStartSprites;
    public static int nNetPlayerCount;
    public static final int[] nPlayerSwear = new int[8];
    public static final int[] nPlayerGrenade = new int[8];
    public static final int[] nPlayerSnake = new int[8];
    public static final int[] nTemperature = new int[8];
    public static final int[] nBreathTimer = new int[8];
    public static final float[] nDestVertPan = new float[8];
    public static final int[] dVertPan = new int[8];
    public static final int[] nDeathType = new int[8];
    public static final boolean[] bTouchFloor = new boolean[8];
    public static final int[] nQuake = new int[8];
    public static final RaStruct[] Ra = new RaStruct[8];
    public static final Input[] sPlayerInput = new Input[8];
    public static final BlockInfo[] sBlockInfo = new BlockInfo[100];
    public static int nRadialSpr;
    public static int nRadialDamage;
    public static int nDamageRadius;
    public static int nRadialOwner;
    public static int nRadialBullet;
    public static int nCreaturesLeft;
    public static int nCreaturesMax;
    public static int nMagicCount;
    public static final int[] SectFlag = new int[MAXSECTORS];
    public static final int[] SectDepth = new int[MAXSECTORS];
    public static final int[] SectSpeed = new int[MAXSECTORS];
    public static final int[] SectDamage = new int[MAXSECTORS];
    public static final int[] SectAbove = new int[MAXSECTORS];
    public static final int[] SectBelow = new int[MAXSECTORS];
    public static final int[] SectSound = new int[MAXSECTORS];
    public static final int[] SectSoundSect = new int[MAXSECTORS];
    public static int nMachineCount;
    public static final BubbleMachineStruct[] Machine = new BubbleMachineStruct[125];
    public static int nCamerax, nCameray, nCameraz;
    public static float nCameraa, nCamerapan;
    public static int besttarget;
    public static boolean bPlayerPan;
    public static boolean bLockPan;
    public static boolean bSlipMode;
    public static int nStandHeight;
    public static int nPlayerPic;
    public static int nFreeze;
    public static final StatusAnim[] StatusAnim = new StatusAnim[50];
    public static int nItemTextIndex;
    public static int nItemAltSeq, nItemFrame, nItemFrames;
    public static final int[] nItemSeqOffset = {91, 72, 76, 79, 68, 87, 83};
    public static int nItemSeq;
    public static final int[] nItemMagic = {500, 1000, 100, 500, 400, 200, 700, 0};

    //	public static int screensize;
    public static int airframe;
    public static int initsect;
    public static int inita;
    public static int initx, inity, initz;
    public static int lastlevel;
    public static int levelnew;
    public static int levelnum;
    public static int nBestLevel;
    public static int totalmoves;
    public static int moveframes;
    public static int flash;
    public static int nMagicSeq;
    public static int nPreMagicSeq;
    public static int nSavePointSeq;
    public static boolean bSnakeCam;
    public static int nSnakeCam = -1;
    public static boolean bMapMode;
    public static int nShadowPic;
    public static int nShadowWidth;
    public static int nFlameHeight;
    public static int nBackgroundPic;
    public static int nPilotLightBase;
    public static int nPilotLightCount;
    public static int nPilotLightFrame;
    public static int nFontFirstChar;
    public static int nHealthFrames;
    public static int nMagicFrames;
    public static int nStatusSeqOffset;
    public static int nHealthFrame;
    public static int nMagicFrame;
    public static int nHealthLevel;
    public static int nMagicLevel;
    public static int nMeterRange;
    public static int magicperline;
    public static int healthperline;
    public static int nAirFrames;
    public static int airperline;
    public static int nCounter;
    public static int nCounterDest;
    public static int nCreepyTimer;
    public static int nCounterBullet;

    public static boolean bNoCreatures;
    public static int nFreeCount;
    public static final byte[] nBubblesFree = new byte[200];
    public static final BubbleStruct[] BubbleList = new BubbleStruct[200];

    public static int nDrips;
    public static final DripStruct[] sDrip = new DripStruct[50];

    public static int nTrails;
    public static int nTrailPoints;
    public static final TrailStruct[] sTrail = new TrailStruct[40];
    public static final TrailPointStruct[] sTrailPoint = new TrailPointStruct[100];
    public static final int[] nTrailPointPrev = new int[100];
    public static final int[] nTrailPointNext = new int[100];
    public static final int[] nTrailPointVal = new int[100];

    public static int nEnergyChan;
    public static int nEnergyBlocks;
    public static int nEnergyTowers;
    public static int nFinaleSpr;

    public static int lCountDown;
    public static int nAlarmTicks;
    public static int nRedTicks;
    public static int nClockVal;
    public static int nButtonColor;

    public static int nDronePitch;
    public static int nFinaleStage;
    public static int lFinaleStart;
    public static int nSmokeSparks;

    public static boolean bCamera;

    public static int nLastAnim;
    public static int nFirstAnim;
    public static int nAnimsFree_X_1;

    public static int message_timer;
    public static String message_text;

    public static final int nNetTime = -1;

    public static int nBobs;
    public static final BobStruct[] sBob = new BobStruct[200];
    public static final int[] sBobID = new int[200];

    public static int ObjectCount;
    public static final ObjectStruct[] ObjectList = new ObjectStruct[128];
    public static final int[] ObjectSeq = {46, -1, 72, -1};
    public static final int[] ObjectStatnum = {141, 152, 98, 97};

    public static final ItemAnimStruct[] nItemAnimInfo = {new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(6, 64), new ItemAnimStruct(-1, 48), new ItemAnimStruct(0, 64), new ItemAnimStruct(1, 64),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(4, 64), new ItemAnimStruct(5, 64),
            new ItemAnimStruct(16, 64), new ItemAnimStruct(10, 64), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(8, 64), new ItemAnimStruct(9, 64), new ItemAnimStruct(-1, 40),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(7, 64), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(14, 64), new ItemAnimStruct(15, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(17, 48), new ItemAnimStruct(18, 48), new ItemAnimStruct(19, 48),
            new ItemAnimStruct(20, 48), new ItemAnimStruct(24, 64), new ItemAnimStruct(21, 64),
            new ItemAnimStruct(23, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(11, 30),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32),
            new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32), new ItemAnimStruct(-1, 32)};

    public static final byte[] sMoveDir = new byte[50];
    public static final MoveSectStruct[] sMoveSect = new MoveSectStruct[50];
    public static int nMoveSects;

    public static boolean isOriginal() {
        Screen scr = game.getScreen();
        boolean isOriginal = true;
        if (scr instanceof GameScreen) {
            isOriginal = ((GameScreen) scr).isOriginal();
        }

        return isOriginal;
    }

    /**
     * It's necessary converting to old format of hit variable, because Powerslave using
     * highest bits for game processes in the same variable (starts from 0x10000 (16 bit))
     * So, max supported index in Powerslave is 16383
     *
     * @param hit variable returned by getzrange or clipmove
     *       Sprites 0xC000_0000
     *       Walls 0x8000_0000
     *       Sectors 0x4000_0000
     * @return converted hit variable to old format
     *      Sprites 0xC000 (14 and 15 bit)
     *      Walls 0x8000 (15 bit)
     *      Sectors 0x4000 (14 bit)
     */
    public static int CONVERT_HIT(int hit) {
        int type = hit & HIT_TYPE_MASK;
        int index = hit & HIT_INDEX_MASK;

        switch (type) {
            case HIT_SPRITE:
                return index | PS_HIT_SPRITE;
            case HIT_WALL:
                return index | PS_HIT_WALL;
            case HIT_SECTOR:
                return index | PS_HIT_SECTOR;
        }

        return 0;
    }
}
