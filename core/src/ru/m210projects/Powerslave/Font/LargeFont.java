package ru.m210projects.Powerslave.Font;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.AtlasCharInfo;
import ru.m210projects.Build.Types.font.BitmapFont;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Powerslave.Main.engine;

public class LargeFont extends BitmapFont {

    public LargeFont() {
        super(EngineUtils.getLargeFont().getData(), 128, 128, 16, 16);
    }

    @Override
    protected AtlasCharInfo setChar(char ch, int width, int height, int cols, int rows) {
        return new AtlasCharInfo(this, ch, 0, 0.7f, width, height, cols, rows);
    }

    @Override
    public int drawText(Renderer renderer, int x, int y, char[] text, float scale, int shade, int palnum, TextAlign align, Transparent transparent, boolean shadow) {
        PaletteManager manager = engine.getPaletteManager();
        shade = manager.getPalookup(0, shade);
        if (!manager.isValidPalette(palnum)) {
            palnum = 0;
        }

        if (shadow) {
            renderer.printext(this, (int) (x + scale), (int) (y + scale), text, scale, 0, 96, align, transparent, false);
        }

        byte[] palookup = manager.makePalookup(palnum, null, 0, 0, 0, 0);
        int dacol = palookup[(shade << 8)] & 0xFF;
        return super.drawText(renderer, x, y, text, scale, shade, dacol, align, transparent, false);
    }

}
