// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Font;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.AtlasCharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

public class FontA extends Font {

    public FontA(final Engine draw) {
        this.size = 8;

        final int cols = 16;
        final int rows = 8;
        ArtEntry pic = draw.getTile(159);

        for (char ch = 0; ch < cols * rows; ch++) {
            this.addCharInfo(ch, new AtlasCharInfo(this, ch, 159, 0.7f, pic.getWidth(), pic.getHeight(), cols, rows));
        }

        setVerticalScaled(false);
    }
}
