// This file is part of PowerslaveGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// PowerslaveGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PowerslaveGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PowerslaveGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Powerslave.Font;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

public class GameFont extends Font {

    public GameFont(Engine draw) {
        this.size = draw.getTile(3522).getHeight();

        this.addCharInfo(' ', new CharInfo(this, -1, 0.7f, 12));

        int[][] symbInfo = {{3522, 26, 'a'}, {3522, 26, 'A'}, {3555, 10, '0'}};
        for (int j = 0, nTile; j < symbInfo.length; j++) {
            nTile = symbInfo[j][0];
            for (int i = 0; i < symbInfo[j][1]; i++) {
                ArtEntry pic = draw.getTile(nTile + i);

                if (pic.getWidth() != 0) {
                    char symbol = (char) (i + symbInfo[j][2]);
                    this.addChar(symbol, nTile + i, pic.getWidth() + 1);
                }
            }
        }
        this.addChar('.', 3548, 3);
        this.addChar('!', 3549, 3);
        this.addChar('?', 3550, 7);
        this.addChar(',', 3551, 3);
        this.addChar('`', 3552, 3);
        this.addChar('"', 3553, 4);
        this.addChar('-', 3554, 6);
        this.addChar('_', 3554, 6);
    }

    protected void addChar(char ch, int nTile, int width) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.7f, width, width, 0, 0));
    }
}
