package ru.m210projects.Powerslave.Font;

import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Powerslave.Globals.THEFONT;
import static ru.m210projects.Powerslave.Main.engine;

public class MenuFont extends Font {

    public MenuFont() {
        this.size = (int) (engine.getTile(THEFONT).getHeight() * 0.7f);
        this.addCharInfo(' ', new PSCharInfo(this, -1, 0.7f, 8, 8, 0, 0));

        int[][] symbInfo = {{THEFONT, 26, 'a'}, {THEFONT, 26, 'A'}, {THEFONT + 26, 10, '0'}};
        for (int j = 0, nTile; j < symbInfo.length; j++) {
            nTile = symbInfo[j][0];
            for (int i = 0; i < symbInfo[j][1]; i++) {
                ArtEntry pic = engine.getTile(nTile + i);

                if (pic.getWidth() != 0) {
                    char symbol = (char) (i + symbInfo[j][2]);
                    this.addCharInfo(symbol, new PSCharInfo(this, nTile + i, 0.7f, pic.getWidth() + 1));
                }
            }
        }

        this.addCharInfo('!', new PSCharInfo(this, THEFONT + 36, 0.7f, 6));
        this.addCharInfo('$', new PSCharInfo(this, THEFONT + 37, 0.7f, 9));
        this.addCharInfo('%', new PSCharInfo(this, THEFONT + 38, 0.7f, 9));
        this.addCharInfo('(', new PSCharInfo(this, THEFONT + 39, 0.7f, 9));
        this.addCharInfo(')', new PSCharInfo(this, THEFONT + 40, 0.7f, 9));
        this.addCharInfo('<', new PSCharInfo(this, THEFONT + 41, 0.7f, 9, 9, 0, 2));
        this.addCharInfo('>', new PSCharInfo(this, THEFONT + 42, 0.7f, 9, 9, 0, 2));
        this.addCharInfo('?', new PSCharInfo(this, THEFONT + 43, 0.7f, 9));
        this.addCharInfo('/', new PSCharInfo(this, THEFONT + 44, 0.7f, 9));
        this.addCharInfo('-', new PSCharInfo(this, THEFONT + 45, 0.7f, 9));
        this.addCharInfo('_', new PSCharInfo(this, THEFONT + 45, 0.7f, 9, 9, 0, 5));
        this.addCharInfo('.', new PSCharInfo(this, THEFONT + 46, 0.7f, 6, 6, 0, 6));
        this.addCharInfo(',', new PSCharInfo(this, THEFONT + 47, 0.7f, 6, 6, 0, 6));
        this.addCharInfo(';', new PSCharInfo(this, THEFONT + 48, 0.7f, 6));
        this.addCharInfo(':', new PSCharInfo(this, THEFONT + 49, 0.7f, 6, 6, 0, 2));
        this.addCharInfo('[', new PSCharInfo(this, THEFONT + 50, 0.7f, 6));
        this.addCharInfo(']', new PSCharInfo(this, THEFONT + 51, 0.7f, 6));
    }

    public static class PSCharInfo extends CharInfo {

        public PSCharInfo(Font parent, int tile, float tileScale, int cellSize, int width, int xOffset, int yOffset) {
            super(parent, tile, tileScale, cellSize, width, xOffset, yOffset);
        }

        public PSCharInfo(Font parent, int tile, float tileScale, int cellSize) {
            super(parent, tile, tileScale, cellSize);
        }

        @Override
        public int getWidth() {
            return (int) ((engine.getTile(tile).getWidth() * 0.7f) + 0.7f);
        }

        @Override
        public int getHeight() {
            return (int) ((engine.getTile(tile).getHeight() * 0.7f) + 1);
        }
    }
}
